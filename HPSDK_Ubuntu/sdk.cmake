# SDK configuration helper
# include this in your CMake project to setup the SDK paths

#####################################
# configure SDK paths
if (EXISTS $ENV{SDKDIR})
    # set this env variable when using sdk.cmake in your custom project.
    # This way the SDK works from any location.
    set(SDKDIR $ENV{SDKDIR})
elseif (EXISTS ${CMAKE_CURRENT_LIST_DIR}/src/uabase/base.h OR
        EXISTS ${CMAKE_CURRENT_LIST_DIR}/include/uabase/base.h)
    # Using sdk.cmake from SDK directory (SDK build)
    set(SDKDIR ${CMAKE_CURRENT_LIST_DIR})
else ()
    message(FATAL_ERROR "Could not find SDK, please configure the SDKDIR environment variable.")
endif ()
# add our cmake folder to the CMake module search path
set(CMAKE_MODULE_PATH ${SDKDIR}/cmake ${CMAKE_MODULE_PATH})

include(MessageUtils)

# configure RPATH
set(CMAKE_SKIP_BUILD_RPATH FALSE)
set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)
set(CMAKE_INSTALL_RPATH "\$ORIGIN/../lib")
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

#####################################
# Find optional system packages
# check if libuuid is available
find_package(UUID)
# check if libunwind is available
find_package(Libunwind)
# detect OpenSSL paths
find_package(OpenSSL)
find_package(mbedTLS)
find_package(LwIP)
#####################################

#####################################
# platform detection
if (NOT CMAKE_CROSSCOMPILING)
    if (${CMAKE_SYSTEM_NAME} MATCHES "Linux")
        set(SDK_PLATFORM_NAME "linux")
        set(SDK_SYSTEM_LIBS pthread rt)
    elseif (${CMAKE_SYSTEM_NAME} MATCHES "Windows")
        set(SDK_PLATFORM_NAME "win32")
        if (MSVC)
            include_directories(${SDKDIR}/include/platform/win32/compat)
        endif ()
        set(SDK_SYSTEM_LIBS ws2_32 rpcrt4)
    else ()
        message(FATAL_ERROR "Your system is currently not supported: ${CMAKE_SYSTEM_NAME}")
    endif ()
elseif (NOT SDK_PLATFORM_NAME)
    # when crosscompiling the SDK_PLATFORM_NAME must be set in the toolchain file
    message(FATAL_ERROR "SDK_PLATFORM_NAME is not set, possibly missing in toolchain cmake file")
endif ()

# add platform define like "UA_LINUX"
string(TOUPPER ${SDK_PLATFORM_NAME} SDK_PLATFORM_NAME)
add_definitions(-DUA_${SDK_PLATFORM_NAME})
# platform layer is lowercase like "linux"
string(TOLOWER ${SDK_PLATFORM_NAME} SDK_PLATFORM_NAME)

# check if assembler source are set and enable ASM if needed
if (DEFINED SDK_ASM_SRCS)
    enable_language(ASM)
endif ()

# helper function for parsing settings from the provided config.h
# this is used for binary SDKs without sources
macro (ParseSetting name)
    string(REGEX MATCH "\#define ${name}" ${name} ${CURRENT_CONFIG})
    # string to bool
    if (${name})
        set(${name} TRUE)
    else ()
        set(${name} FALSE)
    endif ()
endmacro ()

# create library names
if (SDK_BUILD)
    # setup include paths
    if (EXISTS ${CMAKE_CURRENT_LIST_DIR}/src/uabase/base.h)
        set(_BASE_DIR ${CMAKE_CURRENT_LIST_DIR}/src)
    elseif (EXISTS ${CMAKE_CURRENT_LIST_DIR}/include/uabase/base.h)
        set(_BASE_DIR ${CMAKE_CURRENT_LIST_DIR}/include)
    endif ()
    include_directories(${_BASE_DIR})
    include_directories(${_BASE_DIR}/platform/${SDK_PLATFORM_NAME})
    if (${SDK_PLATFORM_NAME} MATCHES "win32" AND MSVC)
        include_directories(${_BASE_DIR}/platform/${SDK_PLATFORM_NAME}/compat)
    endif ()
    include_directories(${src_BINARY_DIR})

    # in this case we can use the CMake projects names instead of library names
    set(PLATFORM_LIBRARY           "platform")
    set(CRASHHANDLER_LIBRARY       "crashhandler")
    set(UTIL_LIBRARY               "util")
    set(MEMORY_LIBRARY             "memory")
    set(IPC_LIBRARY                "ipc")
    set(TRACE_LIBRARY              "trace")
    set(NETWORK_LIBRARY            "network")
    set(TIMER_LIBRARY              "timer")
    set(UABASE_LIBRARY             "uabase")
    set(UACONFIG_LIBRARY           "uaconfig")
    set(UAENCODER_LIBRARY          "uaencoder")
    set(UATCP_LIBRARY              "uatcp")
    set(UASECONV_LIBRARY           "uaseconv")
    set(UACRYPTO_LIBRARY           "uacrypto")
    set(UAPKI_LIBRARY              "uapki")
    set(UAPKI_STORE_LIBRARY        "uapki_store")
    set(UAPKI_STORE_PROXY_LIBRARY  "pki_store_proxy")
    set(UAPKI_STORE_STUB_LIBRARY   "pki_store_stub")
    set(UAPROVIDER_LIBRARY         "uaprovider")
    set(UAINTERNALPROVIDER_LIBRARY "uainternalprovider")
    set(UASERVERPROVIDER_LIBRARY   "uaserverprovider")
    set(UASERVER_LIBRARY           "uaserver")
    set(ENCODER_PROXY_LIBRARY      "encoder_proxy")
    set(ENCODER_STUB_LIBRARY       "encoder_stub")
    set(UASESSION_PROXY_LIBRARY    "uasession_proxy")
    set(UASESSION_STUB_LIBRARY     "uasession_stub")
    set(UATCPMSG_PROXY_LIBRARY     "uatcpmsg_proxy")
    set(UATCPMSG_STUB_LIBRARY      "uatcpmsg_stub")
    set(UASECONV_PROXY_LIBRARY     "uaseconv_proxy")
    set(UASECONV_STUB_LIBRARY      "uaseconv_stub")
    set(XMLPARSER_LIBRARY          "xmlparser")

    # Backend selection: the user may select the backend he wishes.
    # By default we select the free OpenSSL library with fallback to mbedTLS.
    # If both are found the user can choose between those libraries.
    unset(LIBUAPKI_BACKENDS)
    if (OPENSSL_FOUND)
        set(SECURITY_BACKEND "openssl" CACHE STRING "")
        list(APPEND LIBUAPKI_BACKENDS "openssl")
    endif ()
    if (MBEDTLS_FOUND)
        set(SECURITY_BACKEND "mbedtls" CACHE STRING "")
        list(APPEND LIBUAPKI_BACKENDS "mbedtls")
    endif ()
    set(SECURITY_BACKEND "null" CACHE STRING "")
    list(APPEND LIBUAPKI_BACKENDS "null")
    # create list of security backends with first found as default
    # names must match the folder names in the source tree
    set_property(CACHE SECURITY_BACKEND PROPERTY STRINGS ${LIBUAPKI_BACKENDS})
else ()
    # setup include and linker paths
    if (EXISTS ${CMAKE_INSTALL_PREFIX}/include)
        include_directories(${CMAKE_INSTALL_PREFIX}/include)
        include_directories(${CMAKE_INSTALL_PREFIX}/include/platform/${SDK_PLATFORM_NAME})
    else ()
        include_directories(${SDKDIR}/include)
        include_directories(${SDKDIR}/include/platform/${SDK_PLATFORM_NAME})
    endif ()
    include_directories(${CMAKE_CURRENT_BINARY_DIR})
    if (EXISTS ${CMAKE_INSTALL_PREFIX}/lib)
        link_directories(${CMAKE_INSTALL_PREFIX}/lib)
    else ()
        link_directories(${SDKDIR}/lib)
    endif ()

    set(PLATFORM_LIBRARY           debug   "platformd"          optimized   "platform")
    set(CRASHHANDLER_LIBRARY       debug   "crashhandlerd"      optimized   "crashhandler")
    set(UTIL_LIBRARY               debug   "utild"              optimized   "util")
    set(MEMORY_LIBRARY             debug   "memoryd"            optimized   "memory")
    set(IPC_LIBRARY                debug   "ipcd"               optimized   "ipc")
    set(TRACE_LIBRARY              debug   "traced"             optimized   "trace")
    set(NETWORK_LIBRARY            debug   "networkd"           optimized   "network")
    set(TIMER_LIBRARY              debug   "timerd"             optimized   "timer")
    set(UABASE_LIBRARY             debug   "uabased"            optimized   "uabase")
    set(UACONFIG_LIBRARY           debug   "uaconfigd"          optimized   "uaconfig")
    set(UAENCODER_LIBRARY          debug   "uaencoderd"         optimized   "uaencoder")
    set(UATCP_LIBRARY              debug   "uatcpd"             optimized   "uatcp")
    set(UASECONV_LIBRARY           debug   "uaseconvd"          optimized   "uaseconv")
    set(UACRYPTO_LIBRARY           debug   "uacryptod"          optimized   "uacrypto")
    set(UAPKI_LIBRARY              debug   "uapkid"             optimized   "uapki")
    set(UAPKI_STORE_LIBRARY        debug   "uapki_stored"       optimized   "uapki_store")
    set(UAPKI_STORE_PROXY_LIBRARY  debug   "pki_store_proxyd"   optimized   "pki_store_proxy")
    set(UAPKI_STORE_STUB_LIBRARY   debug   "pki_store_stubd"    optimized   "pki_store_stub")
    set(UAPROVIDER_LIBRARY         debug   "uaproviderd"        optimized   "uaprovider")
    set(UAINTERNALPROVIDER_LIBRARY debug   "uainternalproviderd" optimized  "uainternalprovider")
    set(UASERVERPROVIDER_LIBRARY   debug   "uaserverproviderd"  optimized   "uaserverprovider")
    set(UASERVER_LIBRARY           debug   "uaserverd"          optimized   "uaserver")
    set(ENCODER_PROXY_LIBRARY      debug   "encoder_proxyd"     optimized   "encoder_proxy")
    set(ENCODER_STUB_LIBRARY       debug   "encoder_stubd"      optimized   "encoder_stub")
    set(UASESSION_PROXY_LIBRARY    debug   "uasession_proxyd"   optimized   "uasession_proxy")
    set(UASESSION_STUB_LIBRARY     debug   "uasession_stubd"    optimized   "uasession_stub")
    set(UATCPMSG_PROXY_LIBRARY     debug   "uatcpmsg_proxyd"    optimized   "uatcpmsg_proxy")
    set(UATCPMSG_STUB_LIBRARY      debug   "uatcpmsg_stubd"     optimized   "uatcpmsg_stub")
    set(UASECONV_PROXY_LIBRARY     debug   "uaseconv_proxyd"    optimized   "uaseconv_proxy")
    set(UASECONV_STUB_LIBRARY      debug   "uaseconv_stubd"     optimized   "uaseconv_stub")
    set(XMLPARSER_LIBRARY          debug   "xmlparserd"         optimized   "xmlparser")

    # parse SDK configuration from generated config.h files
    file(READ ${SDKDIR}/include/platform/platform_config.h CURRENT_CONFIG)
    ParseSetting(BUILD_LIBUACRYPTO)
    ParseSetting(BUILD_LIBUAPKI)
    string(REGEX MATCH "\#define SECURITY_BACKEND \"([a-zA-Z_]+)\"" SECURITY_BACKEND ${CURRENT_CONFIG})
    set(SECURITY_BACKEND ${CMAKE_MATCH_1})
    ParseSetting(SUPPORT_FILE_IO)
    file(READ ${SDKDIR}/include/trace/trace_config.h CURRENT_CONFIG)
    ParseSetting(TRACE_ENABLED)

    # output settings
    display_variable(BUILD_LIBUACRYPTO)
    display_variable(BUILD_LIBUAPKI)
    display_variable(SECURITY_BACKEND)
    display_variable(SUPPORT_FILE_IO)
    display_variable(TRACE_ENABLED)
endif ()

# setup the selected crypto includes
if (SECURITY_BACKEND STREQUAL openssl)
    include_directories(${OPENSSL_INCLUDE_DIR})
elseif (SECURITY_BACKEND STREQUAL mbedtls)
    include_directories(${MBEDTLS_INCLUDE_DIR})
endif ()

if (NOT NO_INCLUDE_SDK_LIBRARIES)
    include(${CMAKE_CURRENT_LIST_DIR}/sdk_libraries.cmake)
endif ()
