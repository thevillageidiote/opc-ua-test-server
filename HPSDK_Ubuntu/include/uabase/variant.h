/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_VARIANT_H_
#define _UABASE_VARIANT_H_

#include <stdint.h>
#include <stdbool.h>
#include <uabase/datetime.h>
#include <uabase/statuscode.h>
#include <uabase/string.h>
#include <uabase/indexrange.h>
#include <uabase/bytestring.h>
#include <platform/platform.h>

/**
 * @defgroup ua_base_variant ua_variant
 *  @brief A union of all built-in data types including an
 * OpcUa_ExtensionObject.
 *
 * Variants can also contain arrays of any of these built-in
 * types. Variants are used to store any value or parameter with a
 * data type of BaseDataType or one of its subtypes.
 *
 * Variants can be empty. An empty Variant is described as having a
 * null value and should be treated like a null column in a SQL
 * database. A null value in a Variant may not be the same as a null
 * value for data types that support nulls such as Strings. Some
 * development platforms may not be able to preserve the distinction
 * between a null for a DataType and a null for a Variant. Therefore
 * applications shall not rely on this distinction.
 *
 * Variants can contain arrays of Variants but they cannot directly
 * contain another Variant.
 *
 * DataValue and DiagnosticInfo types only have meaning when returned
 * in a response message with an associated StatusCode. As a result,
 * Variants cannot contain instances of DataValue or
 * DiagnosticInfo. This requirement means that if an attribute
 * supports the writing of a null value, it shall also support writing
 * of an empty Variant and vice versa.
 *
 * Variables with a DataType of BaseDataType are mapped to a Variant,
 * however, the ValueRank and ArrayDimensions attributes place
 * restrictions on what is allowed in the Variant. For example, if the
 * ValueRank is Scalar, the Variant may only contain scalar values.
 *
 * ExtensionObjects and Variants allow unlimited nesting which could
 * result in stack overflow errors even if the message size is less
 * than the maximum allowed. Decoders shall support at least 100
 * nesting levels. Decoders shall report an error if the number of
 * nesting levels exceeds what it supports.
 *
 * @ingroup ua_base
 * @{
 */

struct ua_guid;
struct ua_nodeid;

/* TODO: do we really need this??? */
//#define UA_SUPPORT_DATAVALUE_IN_VARIANT

/**
 * @brief Type identifier for a variant.
 */
enum ua_variant_type {
    UA_VT_NULL = 0,
    UA_VT_BOOLEAN = 1,
    UA_VT_SBYTE = 2,
    UA_VT_BYTE = 3,
    UA_VT_INT16 = 4,
    UA_VT_UINT16 = 5,
    UA_VT_INT32 = 6,
    UA_VT_UINT32 = 7,
    UA_VT_INT64 = 8,
    UA_VT_UINT64 = 9,
    UA_VT_FLOAT = 10,
    UA_VT_DOUBLE = 11,
    UA_VT_STRING = 12,
    UA_VT_DATETIME = 13,
    UA_VT_GUID = 14,
    UA_VT_BYTESTRING = 15,
    UA_VT_XMLELEMENT = 16,
    UA_VT_NODEID = 17,
    UA_VT_EXPANDEDNODEID = 18,
    UA_VT_STATUSCODE = 19,
    UA_VT_QUALIFIEDNAME = 20,
    UA_VT_LOCALIZEDTEXT = 21,
    UA_VT_EXTENSIONOBJECT = 22,
#ifdef UA_SUPPORT_DATAVALUE_IN_VARIANT
    UA_VT_DATAVALUE = 23,
#endif
    UA_VT_VARIANT = 24,
    UV_VT_ENCODED_BINARY = 25,
    UA_VT_IS_MATRIX = 0xc0,
    UA_VT_IS_ARRAY = 0x80,
    UA_VT_ARRAYMASK = 0xc0,
    UA_VT_TYPEMASK = 0x3f
};

#ifdef UA_SUPPORT_ARRAY_IN_VARIANT
struct ua_variant_array {
    int len;
    union {
        bool *b;
        uint8_t *ui8;
        int8_t *i8;
        uint16_t *ui16;
        int16_t *i16;
        uint32_t *ui32;
        int32_t *i32;
        uint64_t *ui64;
        int64_t *i64;
        float *f;
        double *d;
        struct ua_string *s;
        ua_datetime *dt;
        struct ua_guid *g;
        struct ua_bytestring *bs;
        struct ua_xmlelement *xml;
        struct ua_nodeid *nodeid;
        struct ua_expandednodeid *enodeid;
        ua_statuscode *status;
        struct ua_qualifiedname *qn;
        struct ua_localizedtext *lt;
        struct ua_extensionobject *eo;
#ifdef UA_SUPPORT_DATAVALUE_IN_VARIANT
        struct ua_datavalue *dv;
#endif
        struct ua_variant *var;
    } value;
};

# ifdef UA_SUPPORT_MATRIX_IN_VARIANT
struct ua_variant_matrix {
    /* the array must be allocated before usage! */
    struct ua_variant_array *array;
    /* the first element of this array is the number of following elements */
    int32_t *dim;
};
# endif /* UA_SUPPORT_MATRIX_IN_VARIANT */
#endif /* UA_SUPPORT_ARRAY_IN_VARIANT */

/**
 * @brief Structure for an UA Variant, see also @ref ua_base_variant.
 * @see ua_base_variant
 */
struct ua_variant {
    uint8_t type; /**< Type of the variant @ref ua_variant_type. */
    /* here we have some padding bytes, that we cannot avoid */
    union {
        bool b;
        uint8_t ui8;
        int8_t i8;
        uint16_t ui16;
        int16_t i16;
        uint32_t ui32;
        int32_t i32;
        uint64_t ui64;
        int64_t i64;
        float f;
        double d;
        struct ua_string s;
        ua_datetime dt;
        struct ua_guid *g;
        struct ua_bytestring bs;
        struct ua_xmlelement *xml;
        struct ua_nodeid *nodeid;
        struct ua_expandednodeid *enodeid;
        ua_statuscode status;
        struct ua_qualifiedname *qn;
        struct ua_localizedtext *lt;
        struct ua_extensionobject *eo;
#ifdef UA_SUPPORT_DATAVALUE_IN_VARIANT
        struct ua_datavalue *dv;
#endif
#ifdef UA_SUPPORT_ARRAY_IN_VARIANT
        struct ua_variant_array array;
# ifdef UA_SUPPORT_MATRIX_IN_VARIANT
        struct ua_variant_matrix matrix;
# endif /* UA_SUPPORT_MATRIX_IN_VARIANT */
#endif /* UA_SUPPORT_ARRAY_IN_VARIANT */
    } value;
};

void ua_variant_init(struct ua_variant *v);
void ua_variant_clear(struct ua_variant *v);

void ua_variant_set_bool(struct ua_variant *v, bool val);
void ua_variant_set_byte(struct ua_variant *v, uint8_t val);
void ua_variant_set_sbyte(struct ua_variant *v, int8_t val);
void ua_variant_set_uint16(struct ua_variant *v, uint16_t val);
void ua_variant_set_int16(struct ua_variant *v, int16_t val);
void ua_variant_set_uint32(struct ua_variant *v, uint32_t val);
void ua_variant_set_int32(struct ua_variant *v, int32_t val);
void ua_variant_set_uint64(struct ua_variant *v, uint64_t val);
void ua_variant_set_int64(struct ua_variant *v, int64_t val);
void ua_variant_set_float(struct ua_variant *v, float val);
void ua_variant_set_double(struct ua_variant *v, double val);
int ua_variant_set_string(struct ua_variant *v, const char *val);
int ua_variant_set_stringn(struct ua_variant *v, const char *val, size_t len);
void ua_variant_attach_string(struct ua_variant *v, char *val);
void ua_variant_attach_stringn(struct ua_variant *v, char *val, size_t len);
int ua_variant_smart_attach_const_string(struct ua_variant *v, const char *val);
int ua_variant_smart_attach_const_stringn(struct ua_variant *v, const char *val, size_t len);
int ua_variant_set_bytestring(struct ua_variant *v, const char *data, int len);
int ua_variant_set_xmlelement(struct ua_variant *v, const char *data, int len);
int ua_variant_set_nodeid(struct ua_variant *v, const struct ua_nodeid *id);
int ua_variant_set_qualifiedname(struct ua_variant *v, int nsindex, const char *name);
int ua_variant_set_localizedtext(struct ua_variant *v, const char *locale, const char *text);
#ifdef UA_SUPPORT_DATAVALUE_IN_VARIANT
void ua_variant_set_datavalue(struct ua_variant *v, const struct ua_datavalue *val);
#endif
int ua_variant_set_extensionobject(struct ua_variant *v, const void *obj, const struct ua_nodeid *type_id);
int ua_variant_attach_extensionobject(struct ua_variant *v, void *obj, const struct ua_nodeid *type_id);
int ua_variant_detach_extensionobject(struct ua_variant *v, void **obj, struct ua_nodeid *type_id);

int ua_variant_compare(const struct ua_variant *a, const struct ua_variant *b) UA_PURE_FUNCTION;
int ua_variant_copy(struct ua_variant *dst, const struct ua_variant *src);
ua_statuscode ua_variant_read_indexrange(struct ua_variant *dst, const struct ua_variant *src, const struct ua_indexrange *ranges, unsigned int num_ranges);

#ifdef ENABLE_TO_STRING
int ua_variant_to_string(const struct ua_variant *src, struct ua_string *dst);
#endif /* ENABLE_TO_STRING */
#ifdef ENABLE_FROM_STRING
int ua_variant_from_string(struct ua_variant *dst, uint8_t type, const struct ua_string *src);
#endif /* ENABLE_FROM_STRING */

/** Test if the ua_variant \c v is a null variant */
static inline bool ua_variant_is_null(const struct ua_variant *v)
{
    return (v->type == UA_VT_NULL);
}

int ua_variant_set_bool_array(struct ua_variant *v, bool *val, size_t num);
int ua_variant_set_byte_array(struct ua_variant *v, uint8_t *val, size_t num);
int ua_variant_set_sbyte_array(struct ua_variant *v, int8_t *val, size_t num);
int ua_variant_set_uint16_array(struct ua_variant *v, uint16_t *val, size_t num);
int ua_variant_set_int16_array(struct ua_variant *v, int16_t *val, size_t num);
int ua_variant_set_uint32_array(struct ua_variant *v, uint32_t *val, size_t num);
int ua_variant_set_int32_array(struct ua_variant *v, int32_t *val, size_t num);
int ua_variant_set_uint64_array(struct ua_variant *v, uint64_t *val, size_t num);
int ua_variant_set_int64_array(struct ua_variant *v, int64_t *val, size_t num);
int ua_variant_set_float_array(struct ua_variant *v, float *val, size_t num);
int ua_variant_set_double_array(struct ua_variant *v, double *val, size_t num);
int ua_variant_set_extensionobject_array(struct ua_variant *v, const void *obj, size_t num, const struct ua_nodeid *type_id);

/** @} */

#endif /* _UABASE_VARIANT_H_ */

