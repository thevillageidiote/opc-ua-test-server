/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_EXPANDEDNODEID_H_
#define _UABASE_EXPANDEDNODEID_H_

#include <uabase/nodeid.h>
#include <uabase/string.h>
#include <platform/platform.h>

/**
 * @ingroup ua_base
 * @struct ua_expandednodeid
 * Extends the NodeId structure by allowing the NamespaceUri to be
 * explicitly specified instead of using the NamespaceIndex.
 *
 * The NamespaceUri is optional. If it is specified, the
 * NamespaceIndex inside the NodeId shall be ignored.
 *
 * An instance of an ExpandedNodeId may still use the NamespaceIndex
 * instead of the NamespaceUri. In this case, the NamespaceUri is ignored.
 *
 * @sa ua_nodeid
 *
 * @var ua_expandednodeid::id
 * The NodeId for a node in the adress space (see ua_nodeid).
 *
 * @var ua_expandednodeid::ns_uri
 * This index is the index of that Server in the local Server’s Server
 * table. The index of the local Server in the Server table is always
 * 0. All remote Servers have indexes greater than 0. The Server table
 * is contained in the Server Object in the address space (see Part 3
 * and Part 5 of the OPC UA Specification). The Client may read the
 * Server table Variable to access the description of the target
 * Server.
 *
 * @var ua_expandednodeid::server_index
 * The URI of the namespace.
 *
 * If this parameter is specified, the namespace index is ignored.
 */
struct ua_expandednodeid {
    struct ua_nodeid id;
    struct ua_string ns_uri;
    uint32_t server_index;
};

void ua_expandednodeid_init(struct ua_expandednodeid *t);
void ua_expandednodeid_clear(struct ua_expandednodeid *t);

int ua_expandednodeid_compare(const struct ua_expandednodeid *a, const struct ua_expandednodeid *b) UA_PURE_FUNCTION;
int ua_expandednodeid_copy(struct ua_expandednodeid *dst, const struct ua_expandednodeid *src);

#ifdef ENABLE_TO_STRING
int ua_expandednodeid_to_string(const struct ua_expandednodeid *id, struct ua_string *dst);
int ua_expandednodeid_snprintf(char *dst, size_t size, const struct ua_expandednodeid *id);
#endif /* ENABLE_TO_STRING */
#ifdef ENABLE_FROM_STRING
int ua_expandednodeid_from_string(struct ua_expandednodeid *id, const struct ua_string *src);
#endif /* ENABLE_FROM_STRING */

#endif /* _UABASE_EXPANDEDNODEID_H_ */
