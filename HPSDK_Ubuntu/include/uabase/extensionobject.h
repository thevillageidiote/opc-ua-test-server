/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_EXTENSIONOBJECT_H_
#define _UABASE_EXTENSIONOBJECT_H_

/**
 * @defgroup ua_base_extensionobject ua_extensionobject
 * @ingroup ua_base
 *
 * A container for any structured DataTypes which cannot be represented
 * as one of the other built-in data types.
 *
 * The ExtensionObject contains a complex value serialized as a
 * sequence of bytes, as an XML element or not encoded as an
 * Encodeable. It also contains an identifier which indicates what
 * data it contains and how it is encoded.
 *
 * Structured DataTypes are represented in a Server address space as
 * subtypes of the Structure DataType. The DataEncodings available for
 * any given structured DataTypes are represented as a
 * DataTypeEncoding object in the Server address space. The NodeId for
 * the DataTypeEncoding Object is the identifier stored in the
 * ExtensionObject. Part 3 of the OPC UA Specification describes how
 * DataTypeEncoding Nodes are related to other Nodes of the address
 * space. Server implementers should use namespace qualified numeric
 * NodeIds for any DataTypeEncoding objects they define. This will
 * minimize the overhead introduced by packing structured DataType
 * values into an extensionobject.
 *
 * ExtensionObjects and Variants allow unlimited nesting which could
 * result in stack overflow errors even if the message size is less
 * than the maximum allowed. Decoders shall support at least 100
 * nesting levels. Decoders shall report an error if the number of
 * nesting levels exceeds what it supports.
 *
 * An ExtensionObject is encoded as sequence of bytes prefixed by the
 * NodeId of its DataTypeEncoding and the number of bytes encoded.
 *
 * An ExtensionObject may be encoded by the Application which means it
 * is passed as a ByteString or an XmlElement to the encoder. In this
 * case, the encoder will be able to write the number of bytes in the
 * object before it encodes the bytes. However, an ExtensionObject may
 * know how to encode/decode itself which means the encoder shall
 * calculate the number of bytes before it encodes the object or it
 * shall be able to seek backwards in the stream and update the length
 * after encoding the body.

 * When a decoder encounters an ExtensionObject it shall check if it
 * recognizes the DataTypeEncoding identifier. If it does then it can
 * call the appropriate function to decode the object body. If the
 * decoder does not recognize the type it shall use the Encoding to
 * determine if the body is a ByteString or an XmlElement and then
 * decode the object body or treat it as opaque data and skip over it.
 *
 * @{
 */

#include <uabase/base.h>
#include <uabase/nodeid.h>
#include <uabase/bytestring.h>
#include <platform/platform.h>

/** Indicates how the body of an OpcUa_ExtensionObject is encoded. */
enum ua_extensionobject_encoding {
    UA_EXTENSIONOBJECT_NONE = 0,             /**< No body is encoded. */
    UA_EXTENSIONOBJECT_ENCODEABLEOBJECT = 1, /**< The object is contained unencoded as an encodeable object. */
    UA_EXTENSIONOBJECT_BINARY = 2,           /**< The body is encoded according to the binary encoding rules. */
    UA_EXTENSIONOBJECT_XML = 3               /**< The body is encoded according to the XML encoding rules. */
};

struct ua_extensionobject_properties {
    struct ua_nodeid type_id;
    struct ua_nodeid encoding_id;
    size_t size;
    compare_t compare;
    copy_t copy;
    clear_t clear;
    decode_t decode;
    encode_t encode;
};

/**
 * @brief Structure for an UA Extensionobject, see also @ref ua_base_extensionobject.
 * @see ua_base_extensionobject
 */
struct ua_extensionobject {
    struct ua_nodeid type_id;      /**< The identifier for the DataTypeEncoding node in the Server’s AddressSpace. */
    struct ua_nodeid encoding_id;
    union {
        void *obj;
        struct ua_bytestring binary;
    } body; /**< The body of the ua_extensionobject as in \ref ua_extensionobject_encoding. */
    enum ua_extensionobject_encoding encoding;/**< The encoding used for the body. */
};

/**
 * @internal
 * Interface for the lookup function, that must be provided when calling ua_extensionobject_add_nsindex().
 * The function gets the \c type_id to lookup and must write the found properties into \c prop.
 * The return value is zero on success or a negative number on failure.
 */
typedef int (*ua_extensionobject_lookup_type_t) (const struct ua_nodeid *type_id, struct ua_extensionobject_properties *prop, void *cb_data);

/**
 * @internal
 * Interface for the lookup function, that must be provided when calling ua_extensionobject_add_nsindex().
 * The function gets the \c encoding_id to lookup and must write the found properties into \c prop.
 * The return value is zero on success or a negative number on failure.
 */
typedef int (*ua_extensionobject_lookup_encoding_t) (const struct ua_nodeid *encoding_id, struct ua_extensionobject_properties *prop, void *cb_data);

void ua_extensionobject_init(struct ua_extensionobject *t);
void ua_extensionobject_clear(struct ua_extensionobject *t);

int ua_extensionobject_compare(const struct ua_extensionobject *a, const struct ua_extensionobject *b) UA_PURE_FUNCTION;
int ua_extensionobject_copy(struct ua_extensionobject *dst, const struct ua_extensionobject *src);

int ua_extensionobject_create_object(struct ua_extensionobject *t, const struct ua_nodeid *type_id, void **object);
int ua_extensionobject_create_object_numeric(struct ua_extensionobject *t, uint16_t nsindex, uint32_t numeric, void **object);

int ua_extensionobject_register_nsindex(uint16_t nsindex,
                                        ua_extensionobject_lookup_type_t type_lookup, void *type_cb_data,
                                        ua_extensionobject_lookup_encoding_t encoding_lookup, void *encoding_cb_data);
int ua_extensionobject_unregister_nsindex(uint16_t nsindex);

/** @} */

#endif /* _UABASE_EXTENSIONOBJECT_H_ */
