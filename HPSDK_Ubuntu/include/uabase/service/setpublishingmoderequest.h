/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_SETPUBLISHINGMODEREQUEST_H_
#define _UABASE_SETPUBLISHINGMODEREQUEST_H_

#include <stdbool.h>
#include <stdint.h>
#include <uabase/base_config.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_setpublishingmoderequest
 *  Asynchronous call to enable sending of Notifications on one or more
 *  Subscriptions.
 *
 *  \var ua_setpublishingmoderequest::publishing_enabled
 *  A Boolean parameter indicating whether publishing is enabled.
 *
 *  It has the following values: <dl> <dt>TRUE</dt> <dd>publishing of
 *  NotificationMessages is enabled for the Subscription.</dd> <dt>FALSE</dt>
 *  <dd>publishing of NotificationMessages is disabled for the Subscription.</dd>
 *  </dl>
 *
 *  The value of this parameter does not affect the value of the monitoring mode
 *  Attribute of MonitoredItems. Setting this value to FALSE does not discontinue
 *  the sending of keep-alive Messages.
 *
 *  \var ua_setpublishingmoderequest::num_subscription_ids
 *  Number of elements in \ref ua_setpublishingmoderequest::subscription_ids.
 *
 *  \var ua_setpublishingmoderequest::subscription_ids
 *  List of Server-assigned identifiers for the Subscriptions to enable or disable.
*/
struct ua_setpublishingmoderequest {
    bool publishing_enabled;
    int32_t num_subscription_ids;
    uint32_t *subscription_ids;
};

void ua_setpublishingmoderequest_init(struct ua_setpublishingmoderequest *t);
void ua_setpublishingmoderequest_clear(struct ua_setpublishingmoderequest *t);

#endif /* _UABASE_SETPUBLISHINGMODEREQUEST_H_ */

