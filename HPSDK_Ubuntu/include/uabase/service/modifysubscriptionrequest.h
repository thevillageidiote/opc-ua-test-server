/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_MODIFYSUBSCRIPTIONREQUEST_H_
#define _UABASE_MODIFYSUBSCRIPTIONREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_modifysubscriptionrequest
 *  Asynchronous call to modify a Subscription
 *
 *  Illegal request values for parameters that can be revised do not generate
 *  errors. Instead the server will choose default values and indicate them in the
 *  corresponding revised parameter.
 *
 *  Changes to the Subscription settings shall be applied immediately by the
 *  Server. They take effect as soon as practical but not later than twice the new
 *  \ref ua_ModifySubscriptionResponse::revised_publishing_interval.
 *
 *  \var ua_modifysubscriptionrequest::subscription_id
 *  The Server-assigned identifier for the Subscription.
 *
 *  \var ua_modifysubscriptionrequest::requested_publishing_interval
 *  This interval defines the cyclic rate that the Subscription is being requested
 *  to return Notifications to the Client.
 *
 *  This interval is expressed in milliseconds.
 *
 *  The negotiated value for this parameter returned in the response is used as the
 *  default sampling interval for MonitoredItems assigned to this Subscription.
 *
 *  If the requested value is 0 or negative, the server shall revise with the
 *  fastest supported publishing interval.
 *
 *  \var ua_modifysubscriptionrequest::requested_lifetime_count
 *  The requested lifetime count. The lifetime count shall be a mimimum of three
 *  times the keep keep-alive count.
 *
 *  When the publishing timer has expired this number of times without a Publish
 *  request being available to send a NotificationMessage, then the Subscription
 *  shall be deleted by the Server.
 *
 *  \var ua_modifysubscriptionrequest::requested_max_keep_alive_count
 *  Requested maximum keep-alive count.
 *
 *  When the publishing timer has expired this number of times without requiring
 *  any NotificationMessage to be sent, the Subscription sends a keep-alive Message
 *  to the Client.
 *
 *  The negotiated value for this parameter is returned in the response.
 *
 *  If the requested value is 0, the server shall revise with the smallest
 *  supported keep-alive count.
 *
 *  \var ua_modifysubscriptionrequest::max_notifications_per_publish
 *  The maximum number of notifications that the Client wishes to receive in a
 *  single Publish response.
 *
 *  A value of zero indicates that there is no limit.
 *
 *  \var ua_modifysubscriptionrequest::priority
 *  Indicates the relative priority of the Subscription.
 *
 *  When more than one Subscription needs to send Notifications, the Server should
 *  dequeue a Publish request to the Subscription with the highest priority number.
 *  For Subscriptions with equal priority the Server should dequeue Publish
 *  requests in a round-robin fashion. Any Subscription that needs to send a
 *  keep-alive Message shall take precedence regardless of its priority, in order
 *  to prevent the Subscription from expiring.
 *
 *  A Client that does not require special priority settings should set this value
 *  to zero.
*/
struct ua_modifysubscriptionrequest {
    uint32_t subscription_id;
    double requested_publishing_interval;
    uint32_t requested_lifetime_count;
    uint32_t requested_max_keep_alive_count;
    uint32_t max_notifications_per_publish;
    uint8_t priority;
};

void ua_modifysubscriptionrequest_init(struct ua_modifysubscriptionrequest *t);
void ua_modifysubscriptionrequest_clear(struct ua_modifysubscriptionrequest *t);

#endif /* _UABASE_MODIFYSUBSCRIPTIONREQUEST_H_ */

