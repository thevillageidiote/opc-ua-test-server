/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_CREATESESSIONRESPONSE_H_
#define _UABASE_CREATESESSIONRESPONSE_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/bytestring.h>
#include <uabase/nodeid.h>
#include <uabase/structure/endpointdescription.h>
#include <uabase/structure/signaturedata.h>
#include <uabase/structure/signedsoftwarecertificate.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_createsessionresponse
 * Creates a new session with the server.
*/
struct ua_createsessionresponse {
    struct ua_nodeid session_id;
    struct ua_nodeid authentication_token;
    double revised_session_timeout;
    struct ua_bytestring server_nonce;
    struct ua_bytestring server_certificate;
    int32_t num_server_endpoints;
    struct ua_endpointdescription *server_endpoints;
    int32_t num_server_software_certificates;
    struct ua_signedsoftwarecertificate *server_software_certificates;
    struct ua_signaturedata server_signature;
    uint32_t max_request_message_size;
};

void ua_createsessionresponse_init(struct ua_createsessionresponse *t);
void ua_createsessionresponse_clear(struct ua_createsessionresponse *t);

#endif /* _UABASE_CREATESESSIONRESPONSE_H_ */

