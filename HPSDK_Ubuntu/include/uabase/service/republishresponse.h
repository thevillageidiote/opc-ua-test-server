/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_REPUBLISHRESPONSE_H_
#define _UABASE_REPUBLISHRESPONSE_H_

#include <uabase/base_config.h>
#include <uabase/structure/notificationmessage.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_republishresponse
 *
 *  \var ua_republishresponse::notification_message
 *  The requested NotificationMessage.
*/
struct ua_republishresponse {
    struct ua_notificationmessage notification_message;
};

void ua_republishresponse_init(struct ua_republishresponse *t);
void ua_republishresponse_clear(struct ua_republishresponse *t);

#endif /* _UABASE_REPUBLISHRESPONSE_H_ */

