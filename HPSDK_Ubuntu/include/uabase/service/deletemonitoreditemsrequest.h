/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_DELETEMONITOREDITEMSREQUEST_H_
#define _UABASE_DELETEMONITOREDITEMSREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_deletemonitoreditemsrequest
 *  Asynchronous call to delete monitored items.
 *
 *  This Service is used to remove one or more MonitoredItems of a Subscription.
 *  When a MonitoredItem is deleted, its triggered item links are also deleted.
 *
 *  Successful removal of a MonitoredItem, however, might not remove notifications
 *  for the MonitoredItem that are in the process of being sent by the
 *  Subscription. Therefore, Clients may receive notifications for the
 *  MonitoredItem after they have received a positive response that the
 *  MonitoredItem has been deleted.
 *
 *  \var ua_deletemonitoreditemsrequest::subscription_id
 *  The Server-assigned identifier for the Subscription that contains the
 *  MonitoredItems to be deleted
 *
 *  \var ua_deletemonitoreditemsrequest::num_monitored_item_ids
 *  Number of elements in \ref ua_deletemonitoreditemsrequest::monitored_item_ids.
 *
 *  \var ua_deletemonitoreditemsrequest::monitored_item_ids
 *  List of Server-assigned ids for the MonitoredItems to be deleted.
*/
struct ua_deletemonitoreditemsrequest {
    uint32_t subscription_id;
    int32_t num_monitored_item_ids;
    uint32_t *monitored_item_ids;
};

void ua_deletemonitoreditemsrequest_init(struct ua_deletemonitoreditemsrequest *t);
void ua_deletemonitoreditemsrequest_clear(struct ua_deletemonitoreditemsrequest *t);

#endif /* _UABASE_DELETEMONITOREDITEMSREQUEST_H_ */

