/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_REPUBLISHREQUEST_H_
#define _UABASE_REPUBLISHREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_republishrequest
 *  Asynchronous republish call to get lost notifications.
 *
 *  This Service requests the Subscription to republish a NotificationMessage from
 *  its retransmission queue. If the Server does not have the requested message in
 *  its retransmission queue, it returns an error response.
 *
 *  \var ua_republishrequest::subscription_id
 *  The Server assigned identifier for the Subscription to be republished.
 *
 *  \var ua_republishrequest::retransmit_sequence_number
 *  The sequence number of a specific NotificationMessage to be republished.
*/
struct ua_republishrequest {
    uint32_t subscription_id;
    uint32_t retransmit_sequence_number;
};

void ua_republishrequest_init(struct ua_republishrequest *t);
void ua_republishrequest_clear(struct ua_republishrequest *t);

#endif /* _UABASE_REPUBLISHREQUEST_H_ */

