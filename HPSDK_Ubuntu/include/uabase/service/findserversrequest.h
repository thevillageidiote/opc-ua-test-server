/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_FINDSERVERSREQUEST_H_
#define _UABASE_FINDSERVERSREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/string.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_findserversrequest
 *  Asynchronous call to return the Servers known to a Server or Discovery Server.
 *
 *  The Client may reduce the number of results returned by specifying filter
 *  criteria. A Discovery Server returns an empty list if no Servers match the
 *  criteria specified by the client.
 *
 *  Every Server shall provide a Discovery Endpoint that supports this Service. The
 *  Server shall always return a record that describes itself, however in some
 *  cases more than one record may be returned. Gateway Servers shall return a
 *  record for each Server that they provide access to plus (optionally) a record
 *  that allows the Gateway Server to be accessed as an ordinary OPC UA Server.
 *  Non-transparent redundant Servers shall provide a record for each Server in the
 *  redundant set.
 *
 *  Every Server shall have a globally unique identifier called the ServerUri. This
 *  identifier should be a fully qualified domain name; however, it may be a GUID
 *  or similar construct that ensures global uniqueness. The ServerUri returned by
 *  this Service shall be the same value that appears in index 0 of the ServerArray
 *  property (see Part 5 of the OPC UA Specification). The ServerUri is returned as
 *  the applicationUri field in the \ref ua_applicationdescription.
 *
 *  Every Server shall also have a human readable identifier, called the
 *  ServerName, which is not necessarily globally unique. This identifier may be
 *  available in multiple locales.
 *
 *  A Server may have multiple HostNames. For this reason, the Client shall pass
 *  the URL it used to connect to the Endpoint to this Service. The implementation
 *  of this Service shall use this information to return responses that are
 *  accessible to the Client via the provided URL.
 *
 *  This Service shall not require any message security but it may require
 *  transport layer security.
 *
 *  Some Servers may be accessed via a Gateway Server and shall have a value
 *  specified for gatewayServerUri in their \ref ua_applicationdescription. The
 *  discoveryUrls provided in ApplicationDescription shall belong to the Gateway
 *  Server. Some Discovery Servers may return multiple records for the same Server
 *  if that Server can be accessed via multiple paths.
 *
 *  This Service can be used without security and it is therefore vulnerable to
 *  Denial Of Service (DOS) attacks. A Server should minimize the amount of
 *  processing required to send the response for this Service. This can be achieved
 *  by preparing the result in advance. The Server should also add a short delay
 *  before starting processing of a request during high traffic conditions.
 *
 *  The DiscoveryUrl returned by this Service is ambiguous if there are multiple
 *  TransportProfiles (e.g. UA XML or UA Binary encoding) associated with the URL
 *  scheme. Clients that support multiple TransportProfiles should attempt to use
 *  alternate TransportProfiles if the first choice does not succeed.
 *
 *  \var ua_findserversrequest::endpoint_url
 *  The network address that the Client used to access the Discovery Endpoint.
 *
 *  The Server uses this information for diagnostics and to determine what URLs to
 *  return in the response.
 *
 *  The Server should return a suitable default URL if it does not recognize the
 *  HostName in the URL.
 *
 *  \var ua_findserversrequest::num_locale_ids
 *  Number of elements in \ref ua_findserversrequest::locale_ids.
 *
 *  \var ua_findserversrequest::locale_ids
 *  List of locales to use.
 *
 *  The server should return the applicationName in the \ref
 *  ua_applicationdescription using one of locales specified. If the server
 *  supports more than one of the requested locales, the server shall use the
 *  locale that appears first in this list. If the server does not support any of
 *  the requested locales, it chooses an appropriate default locale.
 *
 *  The server chooses an appropriate default locale if this list is empty.
 *
 *  \var ua_findserversrequest::num_server_uris
 *  Number of elements in \ref ua_findserversrequest::server_uris.
 *
 *  \var ua_findserversrequest::server_uris
 *  List of servers to return.
 *
 *  All known servers are returned if the list is empty.
 *
 *  A serverUri matches the applicationUri from the \ref ua_applicationdescription.
*/
struct ua_findserversrequest {
    struct ua_string endpoint_url;
    int32_t num_locale_ids;
    struct ua_string *locale_ids;
    int32_t num_server_uris;
    struct ua_string *server_uris;
};

void ua_findserversrequest_init(struct ua_findserversrequest *t);
void ua_findserversrequest_clear(struct ua_findserversrequest *t);

#endif /* _UABASE_FINDSERVERSREQUEST_H_ */

