/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_DELETESUBSCRIPTIONSREQUEST_H_
#define _UABASE_DELETESUBSCRIPTIONSREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_deletesubscriptionsrequest
 *  Asynchronous call to delete one or more Subscriptions that belong to the
 *  Client’s Session.
 *
 *  Successful completion of this Service causes all MonitoredItems that use the
 *  Subscription to be deleted. If this is the last Subscription for the Session,
 *  then all Publish requests still queued for that Session are dequeued and shall
 *  be returned with Bad_NoSubscription.
 *
 *  Subscriptions that were transferred to another Session must be deleted by the
 *  Client that owns the Session.
 *
 *  \var ua_deletesubscriptionsrequest::num_subscription_ids
 *  Number of elements in \ref ua_deletesubscriptionsrequest::subscription_ids.
 *
 *  \var ua_deletesubscriptionsrequest::subscription_ids
 *  The Server-assigned identifier for the Subscription.
*/
struct ua_deletesubscriptionsrequest {
    int32_t num_subscription_ids;
    uint32_t *subscription_ids;
};

void ua_deletesubscriptionsrequest_init(struct ua_deletesubscriptionsrequest *t);
void ua_deletesubscriptionsrequest_clear(struct ua_deletesubscriptionsrequest *t);

#endif /* _UABASE_DELETESUBSCRIPTIONSREQUEST_H_ */

