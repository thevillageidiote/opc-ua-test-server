/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_REGISTERNODESRESPONSE_H_
#define _UABASE_REGISTERNODESRESPONSE_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/nodeid.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_registernodesresponse
 *
 *  \var ua_registernodesresponse::num_registered_node_ids
 *  Number of elements in \ref ua_registernodesresponse::registered_node_ids.
 *
 *  \var ua_registernodesresponse::registered_node_ids
 *  A list of NodeIds which the Client shall use for subsequent access operations.
 *
 *  The size and order of this list matches the size and order of the \ref
 *  ua_RegisterNodesRequest::nodes_to_register request parameter.
 *
 *  The Server may return the NodeId from the request or a new (an alias) NodeId.
 *  It is recommended that the Server returns a numeric NodeId for aliasing.
 *
 *  In case no optimization is supported for a node, the Server shall return the
 *  NodeId from the request.
*/
struct ua_registernodesresponse {
    int32_t num_registered_node_ids;
    struct ua_nodeid *registered_node_ids;
};

void ua_registernodesresponse_init(struct ua_registernodesresponse *t);
void ua_registernodesresponse_clear(struct ua_registernodesresponse *t);

#endif /* _UABASE_REGISTERNODESRESPONSE_H_ */

