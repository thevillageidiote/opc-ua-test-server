/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_MODIFYMONITOREDITEMSREQUEST_H_
#define _UABASE_MODIFYMONITOREDITEMSREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/structure/monitoreditemmodifyrequest.h>
#include <uabase/structure/timestampstoreturn.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_modifymonitoreditemsrequest
 *  Asynchronous call to modify monitored items.
 *
 *  This service is used to modify MonitoredItems of a Subscription. Changes to the
 *  MonitoredItem settings shall be applied immediately by the Server. They take
 *  effect as soon as practical, but not later than twice the new
 *  revisedSamplingInterval.
 *
 *  The return diagnostic info setting in the request header of the
 *  CreateMonitoredItems or the last ModifyMonitoredItems Service is applied to the
 *  Monitored Items and is used as the diagnostic information settings when sending
 *  notifications in the Publish response.
 *
 *  Illegal request values for parameters that can be revised do not generate
 *  errors. Instead the server will choose default values and indicate them in the
 *  corresponding revised parameter.
 *
 *  \var ua_modifymonitoreditemsrequest::subscription_id
 *  The Server-assigned identifier for the Subscription used to qualify the
 *  monitoredItemId
 *
 *  \var ua_modifymonitoreditemsrequest::ts
 *  An enumeration that specifies the timestamp Attributes to be transmitted for
 *  each MonitoredItem to be modified.
 *
 *  When monitoring Events, this applies only to Event fields that are of type
 *  DataValue.
 *
 *  \var ua_modifymonitoreditemsrequest::num_items_to_modify
 *  Number of elements in \ref ua_modifymonitoreditemsrequest::items_to_modify.
 *
 *  \var ua_modifymonitoreditemsrequest::items_to_modify
 *  The list of MonitoredItems to modify.
*/
struct ua_modifymonitoreditemsrequest {
    uint32_t subscription_id;
    enum ua_timestampstoreturn ts;
    int32_t num_items_to_modify;
    struct ua_monitoreditemmodifyrequest *items_to_modify;
};

void ua_modifymonitoreditemsrequest_init(struct ua_modifymonitoreditemsrequest *t);
void ua_modifymonitoreditemsrequest_clear(struct ua_modifymonitoreditemsrequest *t);

#endif /* _UABASE_MODIFYMONITOREDITEMSREQUEST_H_ */

