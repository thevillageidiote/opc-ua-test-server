/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_BROWSEREQUEST_H_
#define _UABASE_BROWSEREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/structure/browsedescription.h>
#include <uabase/structure/viewdescription.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_browserequest
 *  The Browse service is used by a client to navigate through the address space by
 *  passing a starting node and browse filters. The server returns the list of
 *  referenced nodes matching the filter criteria.
 *
 *  The Browse service takes a list of starting nodes and returns a list of
 *  connected nodes for each starting node. Nevertheless, most clients will only
 *  pass one starting node for the main purpose of building a tree hierarchy. Since
 *  the OPC UA address space can be a full-meshed network and is not limited to a
 *  pure hierarchy, the capability to pass in a list of starting nodes is mainly
 *  used to browse metadata like the properties of a list of variables.
 *
 *  \var ua_browserequest::view
 *  Description of the View to browse.
 *
 *  An empty ViewDescription value indicates the entire address space. If
 *  ViewDescription is empty, all references of the node to browse are returned. If
 *  any other view is specified, only the references of the node to browse that are
 *  defined for that view are returned.
 *
 *  \var ua_browserequest::requested_max_references_per_node
 *  Indicates the maximum number of references to return for each starting Node
 *  specified in the request.
 *
 *  The value 0 indicates that the Client is imposing no limitation.
 *
 *  \var ua_browserequest::num_nodes
 *  Number of elements in \ref ua_browserequest::nodes.
 *
 *  \var ua_browserequest::nodes
 *  A list of nodes to Browse
*/
struct ua_browserequest {
    struct ua_viewdescription view;
    uint32_t requested_max_references_per_node;
    int32_t num_nodes;
    struct ua_browsedescription *nodes;
};

void ua_browserequest_init(struct ua_browserequest *t);
void ua_browserequest_clear(struct ua_browserequest *t);

#endif /* _UABASE_BROWSEREQUEST_H_ */

