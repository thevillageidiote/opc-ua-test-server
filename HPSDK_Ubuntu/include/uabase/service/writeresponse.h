/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_WRITERESPONSE_H_
#define _UABASE_WRITERESPONSE_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/diagnosticinfo.h>
#include <uabase/statuscode.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_writeresponse
 *
 *  \var ua_writeresponse::num_results
 *  Number of elements in \ref ua_writeresponse::results.
 *
 *  \var ua_writeresponse::results
 *  List of results for the nodes to write.
 *
 *  The size and order of the list matches the size and order of the \ref
 *  ua_WriteRequest::nodes_to_write request parameter. There is one entry in this
 *  list for each node contained in the \ref ua_WriteRequest::nodes_to_write
 *  parameter.
*/
struct ua_writeresponse {
    int32_t num_results;
    ua_statuscode *results;
#ifdef ENABLE_DIAGNOSTICS
    int32_t num_diag_infos;
    struct ua_diagnosticinfo *diag_infos;
#endif
};

void ua_writeresponse_init(struct ua_writeresponse *t);
void ua_writeresponse_clear(struct ua_writeresponse *t);

#endif /* _UABASE_WRITERESPONSE_H_ */

