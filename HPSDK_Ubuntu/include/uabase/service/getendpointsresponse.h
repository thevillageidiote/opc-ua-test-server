/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_GETENDPOINTSRESPONSE_H_
#define _UABASE_GETENDPOINTSRESPONSE_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/structure/endpointdescription.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_getendpointsresponse
 *
 *  \var ua_getendpointsresponse::num_endpoints
 *  Number of elements in \ref ua_getendpointsresponse::endpoints.
 *
 *  \var ua_getendpointsresponse::endpoints
 *  List of Endpoints that meet criteria specified in the request.
 *
 *  This list is empty if no Endpoints meet the criteria
*/
struct ua_getendpointsresponse {
    int32_t num_endpoints;
    struct ua_endpointdescription *endpoints;
};

void ua_getendpointsresponse_init(struct ua_getendpointsresponse *t);
void ua_getendpointsresponse_clear(struct ua_getendpointsresponse *t);

#endif /* _UABASE_GETENDPOINTSRESPONSE_H_ */

