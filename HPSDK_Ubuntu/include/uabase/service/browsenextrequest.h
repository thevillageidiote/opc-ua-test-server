/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_BROWSENEXTREQUEST_H_
#define _UABASE_BROWSENEXTREQUEST_H_

#include <stdbool.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/bytestring.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_browsenextrequest
 *  The BrowseNext service is only used to continue a Browse if not all results
 *  could be returned by the Browse or a following BrowseNext call. The number of
 *  Nodes to return can be limited by the client in the Browse request or by the
 *  Server during processing the Browse Service call.
 *
 *  The BrowseNext shall be submitted on the same session that was used to submit
 *  the Browse or BrowseNext that is being continued.
 *
 *  \var ua_browsenextrequest::release_continuation_points
 *  A boolean parameter indicating whether continuation points should be released.
 *
 *  It has the following values: <dl> <dt>TRUE</dt> <dd>passed ContinuationPoints
 *  shall be reset to free resources in the Server. The continuation points are
 *  released and the results and diagnosticInfos arrays are empty.</dd>
 *  <dt>FALSE</dt> <dd>passed ContinuationPoints shall be used to get the next set
 *  of browse information.</dd> </dl>
 *
 *  A Client shall always use the continuation point returned by a Browse or
 *  BrowseNext response to free the resources for the continuation point in the
 *  Server. If the Client does not want to get the next set of browse information,
 *  BrowseNext shall be called with this parameter set to TRUE.
 *
 *  \var ua_browsenextrequest::num_continuation_points
 *  Number of elements in \ref ua_browsenextrequest::continuation_points.
 *
 *  \var ua_browsenextrequest::continuation_points
 *  A list of Server-defined opaque values that represent continuation points.
 *
 *  The value for a continuation point was returned to the Client in a previous
 *  Browse or BrowseNext response. These values are used to identify the previously
 *  processed Browse or BrowseNext request that is being continued and the point in
 *  the result set from which the browse response is to continue.
 *
 *  Clients may mix continuation points from different Browse or BrowseNext
 *  responses.
*/
struct ua_browsenextrequest {
    bool release_continuation_points;
    int32_t num_continuation_points;
    struct ua_bytestring *continuation_points;
};

void ua_browsenextrequest_init(struct ua_browsenextrequest *t);
void ua_browsenextrequest_clear(struct ua_browsenextrequest *t);

#endif /* _UABASE_BROWSENEXTREQUEST_H_ */

