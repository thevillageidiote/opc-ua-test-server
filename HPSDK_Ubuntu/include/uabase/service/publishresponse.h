/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_PUBLISHRESPONSE_H_
#define _UABASE_PUBLISHRESPONSE_H_

#include <stdbool.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/diagnosticinfo.h>
#include <uabase/statuscode.h>
#include <uabase/structure/notificationmessage.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_publishresponse
 *
 *  \var ua_publishresponse::subscription_id
 *  The Server-assigned identifier for the Subscription for which notifications are
 *  being returned.
 *
 *  The value 0 is used to indicate that there were no Subscriptions defined for
 *  which a response could be sent.
 *
 *  \var ua_publishresponse::num_available_sequence_numbers
 *  Number of elements in \ref ua_publishresponse::available_sequence_numbers.
 *
 *  \var ua_publishresponse::available_sequence_numbers
 *  A list of sequence number ranges that identify unacknowledged
 *  NotificationMessages that are available for retransmission from the
 *  Subscription’s retransmission queue.
 *
 *  This list is prepared after processing the acknowledgements in the request.
 *
 *  \var ua_publishresponse::more_notifications
 *  A Boolean parameter indicating whether all avalaible notifications could be
 *  included in a single response.
 *
 *  It has the following values:
 *
 *  <dl> <dt>TRUE</dt> <dd>The number of Notifications that were ready to be sent
 *  could not be sent in a single response.</dd> <dt>FALSE</dt> <dd>All
 *  Notifications that were ready are included in the response.</dd> </dl>
 *
 *  \var ua_publishresponse::notification_message
 *  The NotificationMessage that contains the list of Notifications.
 *
 *  \var ua_publishresponse::num_results
 *  Number of elements in \ref ua_publishresponse::results.
 *
 *  \var ua_publishresponse::results
 *  List of results for the acknowledgements.
 *
 *  The size and order of the list matches the size and order of the \ref
 *  ua_PublishRequest::subscription_acknowledgements request parameter.
 *
 *  \var ua_publishresponse::num_diag_infos
 *  Number of elements in \ref ua_publishresponse::diag_infos.
 *
 *  \var ua_publishresponse::diag_infos
 *  List of diagnostic information for the acknowledgements.
 *
 *  The size and order of the list matches the size and order of the \ref
 *  ua_PublishRequest::subscription_acknowledgements request parameter. This list
 *  is empty if diagnostics information was not requested in the request header or
 *  if no diagnostic information was encountered in processing of the request.
*/
struct ua_publishresponse {
    uint32_t subscription_id;
    int32_t num_available_sequence_numbers;
    uint32_t *available_sequence_numbers;
    bool more_notifications;
    struct ua_notificationmessage notification_message;
    int32_t num_results;
    ua_statuscode *results;
#ifdef ENABLE_DIAGNOSTICS
    int32_t num_diag_infos;
    struct ua_diagnosticinfo *diag_infos;
#endif
};

void ua_publishresponse_init(struct ua_publishresponse *t);
void ua_publishresponse_clear(struct ua_publishresponse *t);

#endif /* _UABASE_PUBLISHRESPONSE_H_ */

