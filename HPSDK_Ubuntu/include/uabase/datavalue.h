/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_DATAVALUE_H_
#define _UABASE_DATAVALUE_H_

#include <uabase/variant.h>
#include <platform/platform.h>

struct ua_variant;

/** @ingroup ua_base
 * @struct ua_datavalue
 *  The value and associated information.
 *
 * @var ua_datavalue::value
 * The data value.
 *
 * If the StatusCode indicates an error, the value is to be ignored
 * and the Server shall set it to null.
 *
 * @var ua_datavalue::status
 * The StatusCode that defines with the Server’s ability to
 * access/provide the value.
 *
 * @var ua_datavalue::source_timestamp
 * The sourceTimestamp is used to reflect the timestamp that was
 * applied to a Variable value by the data source.
 *
 * Once a value has been assigned a source timestamp, the source
 * timestamp for that value instance never changes.
 *
 * @var ua_datavalue::server_timestamp
 * The serverTimestamp is used to reflect the time that the Server
 * received a Variable value or knew it to be accurate.
 */
struct ua_datavalue {
    struct ua_variant value;
    ua_statuscode status;
    ua_datetime server_timestamp;
    ua_datetime source_timestamp;
};

void ua_datavalue_init(struct ua_datavalue *v);
void ua_datavalue_clear(struct ua_datavalue *v);

int ua_datavalue_compare(const struct ua_datavalue *a, const struct ua_datavalue *b) UA_PURE_FUNCTION;
int ua_datavalue_copy(struct ua_datavalue *dst, const struct ua_datavalue *src);

#endif /* _UABASE_DATAVALUE_H_ */
