/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_LOCALIZEDTEXT_H_
#define _UABASE_LOCALIZEDTEXT_H_

#include <uabase/string.h>
#include <stdlib.h> /* for size_t */
#include <platform/platform.h>

/**
 * @ingroup ua_base
 * @brief A structure containing a String in a locale-specific translation
 * specified in the identifier for the locale.
 */
struct ua_localizedtext {
    struct ua_string locale; /**< The identifier for the locale, for instance “en-US”. */
    struct ua_string text;   /**< The text in the language given by Locale. */
};

#define UA_LOCALIZEDTEXT_INITIALIZER { UA_STRING_INITIALIZER, UA_STRING_INITIALIZER }

void ua_localizedtext_init(struct ua_localizedtext *t);
void ua_localizedtext_clear(struct ua_localizedtext *t);

int ua_localizedtext_set(struct ua_localizedtext *t, const char *locale, const char *text);

int ua_localizedtext_compare(const struct ua_localizedtext *a, const struct ua_localizedtext *b) UA_PURE_FUNCTION;
int ua_localizedtext_copy(struct ua_localizedtext *dst, const struct ua_localizedtext *src);

#ifdef ENABLE_TO_STRING
int ua_localizedtext_to_string(const struct ua_localizedtext *t, struct ua_string *dst);
int ua_localizedtext_snprintf(char *dst, size_t size, const struct ua_localizedtext *t);
#endif /* ENABLE_TO_STRING */

#ifdef ENABLE_FROM_STRING
int ua_localizedtext_from_string(struct ua_localizedtext *t, const struct ua_string *src);
#endif /* ENABLE_FROM_STRING */

#endif /* _UABASE_LOCALIZEDTEXT_H_ */
