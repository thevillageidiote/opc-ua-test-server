/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef ACCESSLEVELS_H_FNCMY53Z
#define ACCESSLEVELS_H_FNCMY53Z

/*============================================================================
 * Flags that can be set for the AccessLevel attribute.
 *===========================================================================*/

/**
 * @defgroup ua_base_accesslevel ua_accesslevel
 * @ingroup ua_base
 * Possible values for the accesslevel attribute of variable nodes.
 * Multiple accesslevels can be combined using the bitwise OR operator.
 * @{
 */

/** The Variable value cannot be accessed and has no event history. */
#define UA_ACCESSLEVEL_NONE 0x0

/** The current value of the Variable may be read.*/
#define UA_ACCESSLEVEL_CURRENTREAD 0x1

/** The current value of the Variable may be written.*/
#define UA_ACCESSLEVEL_CURRENTWRITE 0x2

/** The current value of the Variable may be read and written.*/
#define UA_ACCESSLEVEL_CURRENTREADWRITE 0x3

/** The history for the Variable may be read.*/
#define UA_ACCESSLEVEL_HISTORYREAD 0x4

/** The history for the Variable may be updated.*/
#define UA_ACCESSLEVEL_HISTORYWRITE 0x8

/** The history value of the Variable may be read and updated. */
#define UA_ACCESSLEVEL_HISTORYREADWRITE 0xC

/** The variable generates SemanticChangeEvents */
#define UA_ACCESSLEVEL_SEMANTICCHANGE 0x10

/** The variable value StatusCode is writable */
#define UA_ACCESSLEVEL_STATUSWRITE 0x20

/** The variable value SourceTimestamp is writable */
#define UA_ACCESSLEVEL_TIMESTAMPWRITE 0x40

/** @} */

#endif /* end of include guard: ACCESSLEVELS_H_FNCMY53Z */

