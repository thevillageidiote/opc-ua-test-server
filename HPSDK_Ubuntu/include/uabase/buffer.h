/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_BUFFER_H_
#define _UABASE_BUFFER_H_

#include <uabase/base_config.h>
#include <platform/platform.h>
#include <platform/memory.h>
#include <stdint.h>

/* minimum buffer size according to Part6 is >8196 (2^13+4); corrected to >=8192 in newer release */
#define UA_BUFFER_MIN_SIZE UINT32_C(8192)

/**
 * @defgroup ua_base_buffer_mgt ua_buffer_mgt
 * @ingroup ua_base
 * @brief Buffer management.
 * This is a central component that may be used from multiple tasks,
 * and so needs to be thread-safe.
 * @{
 */
int ua_buffer_mgt_initialize(void);
void ua_buffer_mgt_cleanup(void);
unsigned int ua_buffer_mgt_register(uintptr_t connid);
int ua_buffer_mgt_unregister(unsigned int conn);
/** @} */

/** The buffer structure is used for protocol encoding/decoding.
 * An OPC UA message can be chunked into multiple parts. Each
 * part is stored in one buffer. These buffers can be chained
 * in forward linked list, so one message consists of one list
 * of buffers.
 * @ingroup ua_base
 */
struct ua_buffer
{
    unsigned int   conn;    /**< connection handle for buffer-mgt */
    unsigned int   pos;     /**< current position */
    unsigned int   len;     /**< length of data */
    struct ua_buffer *next; /**< pointer to next buffer */
    unsigned char data[];   /**< data pointer */
};

void ua_buffer_init(struct ua_buffer *buf);
void ua_buffer_clear(struct ua_buffer *buf);
struct ua_buffer *ua_buffer_alloc(unsigned int conn);
void ua_buffer_free(struct ua_buffer *buf);

/* The following function are tuned for performance and get most likely inlined
 * by the compiler. It is intented that these function have no error checks
 * like pointer checks, length checks, etc. This MUST be done on higher level.
 * Rationale: When reading e.g. 4 bytes for a uint32_t using getbyte it makes
 * no sense to make a length check 4 times. Instead the uint32_decode function
 * checks for 4 remaining bytes in buffer and can then safely call
 * ua_buffer_getbyte 4 times without problems.
 */

/** Returns the remaining bytes that can be read or written of the given buffer \c buf.
 * The number of remaining bytes is computed as \c len - \c pos.
 *
 * @note This function does not perform any security checks. The caller is responsible
 * to provide correct parameters.
 * @memberof ua_buffer
 */
static inline unsigned int ua_buffer_remaining(struct ua_buffer *buf)
{
    return (buf->len - buf->pos);
}

/** Returns the byte of at current position and increments the position counter.
 *
 * @note This function does not perform any security checks. The caller is responsible
 * to provide correct parameters.
 * @memberof ua_buffer
 */
static inline uint8_t ua_buffer_getbyte(struct ua_buffer *buf)
{
    return buf->data[buf->pos++];
}

/** Writes one byte to the current position and increments the position counter.
 * @note This function does not perform any security checks. The caller is responsible
 * to provide correct parameters.
 * @memberof ua_buffer
 */
static inline void ua_buffer_addbyte(struct ua_buffer *buf, uint8_t val)
{
    buf->data[buf->pos++] = val;
}

/** Writes an array of bytes to the buffer.
 * @param buf the destination buffer
 * @param data pointer to source data
 * @param len number of bytes to write
 *
 * @note This function does not perform any security checks. The caller is responsible
 * to provide correct parameters.
 * @memberof ua_buffer
 */
static inline void ua_buffer_write(struct ua_buffer *buf, const void *data, unsigned int len)
{
    ua_memcpy(buf->data + buf->pos, data, len);
    buf->pos += len;
}

/** Reads an array of bytes from the buffer.
 * @param buf the source buffer
 * @param data pointer to destination data
 * @param len number of bytes to read
 *
 * @note This function does not perform any security checks. The caller is responsible
 * to provide correct parameters.
 * @memberof ua_buffer
 */
static inline void ua_buffer_read(struct ua_buffer *buf, void *data, unsigned int len)
{
    ua_memcpy(data, buf->data + buf->pos, len);
    buf->pos += len;
}

#endif /* _UABASE_BUFFER_H_ */
