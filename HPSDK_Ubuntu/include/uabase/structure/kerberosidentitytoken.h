/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_KERBEROSIDENTITYTOKEN_H_
#define _UABASE_KERBEROSIDENTITYTOKEN_H_

#include <platform/platform.h>
#include <uabase/base_config.h>
#include <uabase/bytestring.h>
#include <uabase/string.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_kerberosidentitytoken
*/
struct ua_kerberosidentitytoken {
    struct ua_string policy_id;
    struct ua_bytestring ticket_data;
};

void ua_kerberosidentitytoken_init(struct ua_kerberosidentitytoken *t);
void ua_kerberosidentitytoken_clear(struct ua_kerberosidentitytoken *t);
int ua_kerberosidentitytoken_compare(const struct ua_kerberosidentitytoken *a, const struct ua_kerberosidentitytoken *b) UA_PURE_FUNCTION;
int ua_kerberosidentitytoken_copy(struct ua_kerberosidentitytoken *dst, const struct ua_kerberosidentitytoken *src);

#endif /* _UABASE_KERBEROSIDENTITYTOKEN_H_ */

