/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_BUILDINFO_H_
#define _UABASE_BUILDINFO_H_

#include <platform/platform.h>
#include <uabase/base_config.h>
#include <uabase/datetime.h>
#include <uabase/string.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_buildinfo
 *  Contains elements that describe the build information of the Server.
 *
 *  \var ua_buildinfo::product_uri
 *  URI that identifies the software.
 *
 *  \var ua_buildinfo::manufacturer_name
 *  Name of the software manufacturer.
 *
 *  \var ua_buildinfo::product_name
 *  Name of the software.
 *
 *  \var ua_buildinfo::software_version
 *  Software version.
 *
 *  \var ua_buildinfo::build_number
 *  Build number.
 *
 *  \var ua_buildinfo::build_date
 *  Date and time of the build.
*/
struct ua_buildinfo {
    struct ua_string product_uri;
    struct ua_string manufacturer_name;
    struct ua_string product_name;
    struct ua_string software_version;
    struct ua_string build_number;
    ua_datetime build_date;
};

void ua_buildinfo_init(struct ua_buildinfo *t);
void ua_buildinfo_clear(struct ua_buildinfo *t);
int ua_buildinfo_compare(const struct ua_buildinfo *a, const struct ua_buildinfo *b) UA_PURE_FUNCTION;
int ua_buildinfo_copy(struct ua_buildinfo *dst, const struct ua_buildinfo *src);

#endif /* _UABASE_BUILDINFO_H_ */

