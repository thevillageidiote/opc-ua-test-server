/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_FILTEROPERATOR_H
#define _UABASE_FILTEROPERATOR_H

#include <uabase/base_config.h>

/**
 * @defgroup ua_base_filteroperator ua_filteroperator
 * @ingroup ua_base_enums
 * @brief
 *  Defines the basic operators that can be used in a ContentFilter.
 * @{
 */

/** \enum ua_filteroperator
 *  Defines the basic operators that can be used in a ContentFilter.
 *
 *  \var UA_FILTEROPERATOR_EQUALS
 *  TRUE if operand[0] is equal to operand[1].
 *
 *  If the operands are of different types, the system shall perform any implicit
 *  conversion to a common type. This operator resolves to FALSE if no implicit
 *  conversion is available and the operands are of different types. This operator
 *  returns FALSE if the implicit conversion fails.
 *
 *  \var UA_FILTEROPERATOR_ISNULL
 *  TRUE if operand[0] is a null value.
 *
 *  \var UA_FILTEROPERATOR_GREATERTHAN
 *  TRUE if operand[0] is greater than operand[1].
 *
 *  The following restrictions apply to the operands: <dl> <dt>[0]</dt><dd>Any
 *  operand that resolves to an ordered value.</dd> <dt>[1]</dt><dd>Any operand
 *  that resolves to an ordered value.</dd> </dl>
 *
 *  The same conversion rules as defined for Equals apply.
 *
 *  \var UA_FILTEROPERATOR_LESSTHAN
 *  TRUE if operand[0] is less than operand[1].
 *
 *  The same conversion rules and restrictions as defined for GreaterThan apply.
 *
 *  \var UA_FILTEROPERATOR_GREATERTHANOREQUAL
 *  TRUE if operand[0] is greater than or equal to operand[1].
 *
 *  The same conversion rules and restrictions as defined for GreaterThan apply.
 *
 *  \var UA_FILTEROPERATOR_LESSTHANOREQUAL
 *  TRUE if operand[0] is less than or equal to operand[1].
 *
 *  The same conversion rules and restrictions as defined for GreaterThan apply.
 *
 *  \var UA_FILTEROPERATOR_LIKE
 *  TRUE if operand[0] matches a pattern defined by operand[1].
 *
 *  The following restrictions apply to the operands: <dl> <dt>[0]</dt><dd>Any
 *  operand that resolves to a String.</dd> <dt>[1]</dt><dd>Any operand that
 *  resolves to a String.</dd> </dl>
 *
 *  This operator resolves to FALSE if no operand can be resolved to a string.
 *
 *  \var UA_FILTEROPERATOR_NOT
 *  TRUE if operand[0] is FALSE.
 *
 *  The following restrictions apply to the operands: <dl> <dt>[0]</dt><dd>Any
 *  operand that resolves to a Boolean.</dd> </dl>
 *
 *  If the operand cannot be resolved to a Boolean, the result is a NULL.
 *
 *  \var UA_FILTEROPERATOR_BETWEEN
 *  TRUE if operand[0] is greater or equal to operand[1] and less than or equal to
 *  operand[2].
 *
 *  The following restrictions apply to the operands: <dl> <dt>[0]</dt><dd>Any
 *  operand that resolves to an ordered value.</dd> <dt>[1]</dt><dd>Any operand
 *  that resolves to an ordered value.</dd> <dt>[0]</dt><dd>Any operand that
 *  resolves to an ordered value.</dd> </dl>
 *
 *  If the operands are of different types, the system shall perform any implicit
 *  conversion to match all operands to a common type. If no implicit conversion is
 *  available and the operands are of different types, the particular result is
 *  FALSE.
 *
 *  \var UA_FILTEROPERATOR_INLIST
 *  TRUE if operand[0] is equal to one or more of the remaining operands.
 *
 *  The Equals Operator is evaluated for operand[0] and each remaining operand in
 *  the list. If any Equals evaluation is TRUE, InList returns TRUE.
 *
 *  \var UA_FILTEROPERATOR_AND
 *  TRUE if operand[0] and operand[1] are TRUE.
 *
 *  The following restrictions apply to the operands: <dl> <dt>[0]</dt><dd>Any
 *  operand that resolves to a Boolean.</dd> <dt>[0]</dt><dd>Any operand that
 *  resolves to a Boolean.</dd> </dl>
 *
 *  If any operand cannot be resolved to a Boolean it is considered a NULL.
 *
 *  \var UA_FILTEROPERATOR_OR
 *  TRUE if operand[0] or operand[1] are TRUE.
 *
 *  The following restrictions apply to the operands: <dl> <dt>[0]</dt><dd>Any
 *  operand that resolves to a Boolean.</dd> <dt>[0]</dt><dd>Any operand that
 *  resolves to a Boolean.</dd> </dl>
 *
 *  If any operand cannot be resolved to a Boolean it is considered a NULL.
 *
 *  \var UA_FILTEROPERATOR_CAST
 *  Converts operand[0] to a value with a data type with a NodeId identified by
 *  operand[1].
 *
 *  The following restrictions apply to the operands:
 *
 *  <dl> <dt>[0]</dt><dd>Any operand.</dd> <dt>[1]</dt><dd>Any operand that
 *  resolves to a NodeId or ExpandedNodeId where the Node is of the NodeClass
 *  DataType.</dd> </dl>
 *
 *  If there is any error in conversion or in any of the parameters then the Cast
 *  Operation evaluates to a NULL.
 *
 *  \var UA_FILTEROPERATOR_INVIEW
 *  TRUE if the target Node is contained in the View defined by operand[0].
 *
 *  The following restrictions apply to the operands: <dl> <dt>[0]</dt><dd>Any
 *  operand that resolves to a NodeId that identifies a View Node.</dd> </dl>
 *
 *  If operand[0] does not resolve to a NodeId that identifies a View Node, this
 *  operation shall always be False.
 *
 *  \var UA_FILTEROPERATOR_OFTYPE
 *  TRUE if the target Node is of type operand[0] or of a subtype of operand[0].
 *
 *  The following restrictions apply to the operands: <dl> <dt>[0]</dt><dd>Any
 *  operand that resolves to a NodeId that identifies an ObjectType or VariableType
 *  Node.</dd> </dl>
 *
 *  If operand[0] does not resolve to a NodeId that identifies an ObjectType or
 *  VariableType Node, this operation shall always be False.
 *
 *  \var UA_FILTEROPERATOR_RELATEDTO
 *  TRUE if the target Node is of type Operand[0] and is related to a NodeId of the
 *  type defined in Operand[1] by the Reference type defined in Operand[2].
 *
 *  Operand[0] or Operand[1] can also point to an element Reference where the
 *  referred to element is another RelatedTo operator. This allows chaining of
 *  relationships (e.g. A is related to B is related to C), where the relationship
 *  is defined by the ReferenceType defined in Operand[2]. In this case, the
 *  referred to element returns a list of NodeIds instead of TRUE or FALSE. In this
 *  case if any errors occur or any of the operands cannot be resolved to an
 *  appropriate value, the result of the chained relationship is an empty list of
 *  nodes.
 *
 *  Operand[3] defines the number of hops for which the relationship should be
 *  followed. If Operand[3] is 1, then objects shall be directly related. If a hop
 *  is greater than 1, then a NodeId of the type described in Operand[1] is checked
 *  for at the depth specified by the hop. In this case, the type of the
 *  intermediate Node is undefined, and only the Reference type used to reach the
 *  end Node is defined. If the requested number of hops cannot be followed, then
 *  the result is FALSE, i.e., an empty Node list. If Operand[3] is 0, the
 *  relationship is followed to its logical end in a forward direction and each
 *  Node is checked to be of the type specified in Operand[1]. If any Node
 *  satisfies this criterion, then the result is TRUE, i.e., the NodeId is included
 *  in the sublist.
 *
 *  Operand [4] defines if Operands [0] and [1] should include support for subtypes
 *  of the types defined by these operands. A TRUE indicates support for subtypes
 *  Operand [5] defines if Operand [2] should include support for subtypes of the
 *  reference type. A TRUE indicates support for subtypes.
 *
 *  The following restrictions apply to the operands: <dl> <dt>[0]</dt><dd>Any
 *  operand that resolves to a NodeId or ExpandedNodeId that identifies an
 *  ObjectType or VariableType Node or a reference to another element which is a
 *  RelatedTo operator.</dd> <dt>[1]</dt><dd>Any operand that resolves to a NodeId
 *  or ExpandedNodeId that identifies an ObjectType or VariableType Node or a
 *  reference to another element which is a RelatedTo operator.</dd>
 *  <dt>[2]</dt><dd>Any operand that resolves to a NodeId that identifies a
 *  ReferenceType Node.</dd> <dt>[3]</dt><dd>Any operand that resolves to a value
 *  implicitly convertible to Uint32.</dd> <dt>[4]</dt><dd>Any operand that
 *  resolves to a value implicitly convertible to a boolean; if this operand does
 *  not resolve to a Boolean, then a value of FALSE is used.</dd>
 *  <dt>[5]</dt><dd>>Any operand that resolves to a value implicitly convertible to
 *  a boolean; if this operand does not resolve to a Boolean, then a value of FALSE
 *  is used.</dd> </dl>
 *
 *  If none of the operands [0],[1],[2],[3] resolves to an appropriate value then
 *  the result of this operation shall always be False (or an Empty set in the case
 *  of a nested RelatedTo operand).
 *
 *  \var UA_FILTEROPERATOR_BITWISEAND
 *  The result is an integer which matches the size of the largest operand and
 *  contains a bitwise And operation of the two operands where both have been
 *  converted to the same size (largest of the two operands).
 *
 *  The following restrictions apply to the operands:
 *
 *  <dl> <dt>[0]</dt><dd>Any operand that resolves to an integer.</dd>
 *  <dt>[0]</dt><dd>Any operand that resolves to an integer.</dd> </dl>
 *
 *  If any operand cannot be resolved to an integer, it is considered a NULL.
 *
 *  \var UA_FILTEROPERATOR_BITWISEOR
 *  The result is an integer which matches the size of the largest operand and
 *  contains a bitwise Or operation of the two operands where both have been
 *  converted to the same size (largest of the two operands).
 *
 *  The following restrictions apply to the operands:
 *
 *  <dl> <dt>[0]</dt><dd>Any operand that resolves to an integer.</dd>
 *  <dt>[0]</dt><dd>Any operand that resolves to an integer.</dd> </dl>
 *
 *  If any operand cannot be resolved to an integer, it is considered a NULL.
*/
enum ua_filteroperator
{
    UA_FILTEROPERATOR_EQUALS = 0,
    UA_FILTEROPERATOR_ISNULL = 1,
    UA_FILTEROPERATOR_GREATERTHAN = 2,
    UA_FILTEROPERATOR_LESSTHAN = 3,
    UA_FILTEROPERATOR_GREATERTHANOREQUAL = 4,
    UA_FILTEROPERATOR_LESSTHANOREQUAL = 5,
    UA_FILTEROPERATOR_LIKE = 6,
    UA_FILTEROPERATOR_NOT = 7,
    UA_FILTEROPERATOR_BETWEEN = 8,
    UA_FILTEROPERATOR_INLIST = 9,
    UA_FILTEROPERATOR_AND = 10,
    UA_FILTEROPERATOR_OR = 11,
    UA_FILTEROPERATOR_CAST = 12,
    UA_FILTEROPERATOR_INVIEW = 13,
    UA_FILTEROPERATOR_OFTYPE = 14,
    UA_FILTEROPERATOR_RELATEDTO = 15,
    UA_FILTEROPERATOR_BITWISEAND = 16,
    UA_FILTEROPERATOR_BITWISEOR = 17
};

#ifdef ENABLE_TO_STRING
const char* ua_filteroperator_to_string(enum ua_filteroperator filteroperator);
#endif /* ENABLE_TO_STRING */

/** @}*/

#endif /*_UABASE_FILTEROPERATOR_H*/
