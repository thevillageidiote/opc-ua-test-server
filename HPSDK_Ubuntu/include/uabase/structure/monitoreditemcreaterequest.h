/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_MONITOREDITEMCREATEREQUEST_H_
#define _UABASE_MONITOREDITEMCREATEREQUEST_H_

#include <platform/platform.h>
#include <uabase/base_config.h>
#include <uabase/structure/monitoringmode.h>
#include <uabase/structure/monitoringparameters.h>
#include <uabase/structure/readvalueid.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_monitoreditemcreaterequest
 *  A structure that is defined as the type of the itemsToCreate parameter of the
 *  CreateMonitoredItems service.
 *
 *  \var ua_monitoreditemcreaterequest::item_to_monitor
 *  Identifies an item in the AddressSpace to monitor.
 *
 *  To monitor for Events, the attributeId element of the ReadValueId structure is
 *  the id of the EventNotifier Attribute.
 *
 *  \var ua_monitoreditemcreaterequest::monitoring_mode
 *  The monitoring mode to be set for the MonitoredItem.
 *
 *  \var ua_monitoreditemcreaterequest::requested_parameters
 *  The requested monitoring parameters.
 *
 *  Servers negotiate the values of these parameters based on the Subscription and
 *  the capabilities of the Server.
*/
struct ua_monitoreditemcreaterequest {
    struct ua_readvalueid item_to_monitor;
    enum ua_monitoringmode monitoring_mode;
    struct ua_monitoringparameters requested_parameters;
};

void ua_monitoreditemcreaterequest_init(struct ua_monitoreditemcreaterequest *t);
void ua_monitoreditemcreaterequest_clear(struct ua_monitoreditemcreaterequest *t);
int ua_monitoreditemcreaterequest_compare(const struct ua_monitoreditemcreaterequest *a, const struct ua_monitoreditemcreaterequest *b) UA_PURE_FUNCTION;
int ua_monitoreditemcreaterequest_copy(struct ua_monitoreditemcreaterequest *dst, const struct ua_monitoreditemcreaterequest *src);

#endif /* _UABASE_MONITOREDITEMCREATEREQUEST_H_ */

