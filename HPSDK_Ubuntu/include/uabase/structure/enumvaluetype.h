/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_ENUMVALUETYPE_H_
#define _UABASE_ENUMVALUETYPE_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/localizedtext.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_enumvaluetype
 *  A mapping between a value of an enumerated type and a name and description.
 *
 *  When this type is used in an array representing human readable representations
 *  of an enumeration, each Value shall be unique in that array.
 *
 *  \var ua_enumvaluetype::value
 *  The Integer representation of an Enumeration.
 *
 *  \var ua_enumvaluetype::display_name
 *  A human-readable representation of the Value of the Enumeration.
 *
 *  \var ua_enumvaluetype::description
 *  A localized description of the enumeration value.
 *
 *  This field can contain an empty string if no description is available.
*/
struct ua_enumvaluetype {
    int64_t value;
    struct ua_localizedtext display_name;
    struct ua_localizedtext description;
};

void ua_enumvaluetype_init(struct ua_enumvaluetype *t);
void ua_enumvaluetype_clear(struct ua_enumvaluetype *t);
int ua_enumvaluetype_compare(const struct ua_enumvaluetype *a, const struct ua_enumvaluetype *b) UA_PURE_FUNCTION;
int ua_enumvaluetype_copy(struct ua_enumvaluetype *dst, const struct ua_enumvaluetype *src);

#endif /* _UABASE_ENUMVALUETYPE_H_ */

