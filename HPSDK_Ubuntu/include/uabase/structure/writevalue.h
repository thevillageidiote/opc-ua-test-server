/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_WRITEVALUE_H_
#define _UABASE_WRITEVALUE_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/datavalue.h>
#include <uabase/nodeid.h>
#include <uabase/string.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_writevalue
 *  A structure that is defined as the type of the nodesToWrite parameter of the
 *  Write service.
 *
 *  \var ua_writevalue::node_id
 *  NodeId of the node that contains the attributes.
 *
 *  \var ua_writevalue::attribute_id
 *  Id of the attribute.
 *
 *  This shall be a valid attribute id.
 *
 *  The IntegerIds for the Attributes are defined in NodeAttributesMask.
 *
 *  \var ua_writevalue::index_range
 *  This parameter is used to identify a single element of an array, or a single
 *  range of indexes for arrays.
 *
 *  The first element is identified by index 0 (zero).
 *
 *  This parameter is not used if the specified attribute is not an array. However,
 *  if the specified attribute is an array and this parameter is not used, then all
 *  elements are to be included in the range. The parameter is null if not used.
 *
 *  \var ua_writevalue::value
 *  The node’s attribute value.
 *
 *  If the indexRange parameter is specified, the Value shall be an array even if
 *  only one element is being written.
 *
 *  If the SourceTimestamp or the ServerTimestamp is specified, the Server shall
 *  use these values. The Server returns a Bad_WriteNotSupported error if it does
 *  not support writing of timestamps.
 *
 *  A Server shall return a Bad_TypeMismatch error if the data type of the written
 *  value is not the same type or subtype as the attribute’s DataType. Based on the
 *  DataType hierarchy, subtypes of the attribute DataType shall be accepted by the
 *  Server. For the value attribute the DataType is defined through the DataType
 *  attribute. A ByteString is structurally the same as a one dimensional array of
 *  Byte. A Server shall accept a ByteString if an array of Byte is expected.
 *
 *  The Server returns a Bad_DataEncodingUnsupported error if it does not support
 *  writing of the passed data encoding.
*/
struct ua_writevalue {
    struct ua_nodeid node_id;
    uint32_t attribute_id;
    struct ua_string index_range;
    struct ua_datavalue value;
};

void ua_writevalue_init(struct ua_writevalue *t);
void ua_writevalue_clear(struct ua_writevalue *t);
int ua_writevalue_compare(const struct ua_writevalue *a, const struct ua_writevalue *b) UA_PURE_FUNCTION;
int ua_writevalue_copy(struct ua_writevalue *dst, const struct ua_writevalue *src);

#endif /* _UABASE_WRITEVALUE_H_ */

