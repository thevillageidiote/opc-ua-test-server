/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_AGGREGATECONFIGURATION_H_
#define _UABASE_AGGREGATECONFIGURATION_H_

#include <platform/platform.h>
#include <stdbool.h>
#include <stdint.h>
#include <uabase/base_config.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_aggregateconfiguration
 *  A structure that is defined as the type of the aggregateConfiguration parameter
 *  of the AggregateFilter structure.
 *
 *  \var ua_aggregateconfiguration::use_server_capabilities_defaults
 *  A Boolean parameter indicating whether to use default aggregate configuration
 *  settings.
 *
 *  <dl><dt>TRUE</dt><dd>use aggregate configuration settings as outlined by the
 *  AggregateConfiguration object.</dd><dt>FALSE</dt><dd>use configuration settings
 *  as outlined in the following aggregate configuration parameters.</dd></dl>
 *
 *  \var ua_aggregateconfiguration::treat_uncertain_as_bad
 *  Indicates how the server treats data returned with a StatusCode severity
 *  Uncertain with respect to Aggregate calculations.
 *
 *  It has the following values: <dl> <dt>True</dt><dd>the server considers the
 *  severity equivalent to Bad,</dd> <dt>False</dt><dd>indicates the server
 *  considers the severity equivalent to Good, unless the aggregate definition says
 *  otherwise.</dd> </dl>A value of True indicates the server
 *
 *  The default value is True. Note that the value is still treated as Uncertain
 *  when the StatusCode for the result is calculated.
 *
 *  \var ua_aggregateconfiguration::percent_data_bad
 *  Indicates the minimum percentage of bad data in a given interval required for
 *  the StatusCode for the given interval for processed data request to be set to
 *  Bad.
 *
 *  Uncertain is treated as defined in \ref
 *  ua_AggregateConfiguration::treat_uncertain_as_bad. For details on which
 *  Aggregates use the PercentDataBad Variable, see the definition of each
 *  Aggregate. The default value is 100.
 *
 *  The PercentDataGood and PercentDataBad must follow the following
 *  relationship<br/>PercentDataGood \>= (100 – PercentDataBad).<br/>If they are
 *  equal, the result of the PercentDataGood calculation is used.
 *
 *  \var ua_aggregateconfiguration::percent_data_good
 *  Indicates the minimum percentage of Good data in a given interval required for
 *  the StatusCode for the given interval for the processed data requests to be set
 *  to Good.
 *
 *  For details on which Aggregates use the PercentDataGood Variable, see the
 *  definition of each Aggregate. The default value is 100.
 *
 *  The PercentDataGood and PercentDataBad must follow the following
 *  relationship<br/>PercentDataGood \>= (100 – PercentDataBad).<br/>If they are
 *  equal, the result of the PercentDataGood calculation is used.
 *
 *  \var ua_aggregateconfiguration::use_sloped_extrapolation
 *  Indicates how the server interpolates data when no boundary value exists (i.e.
 *  extrapolating into the future from the last known value).
 *
 *  <dl> <dt>False</dt><dd>the server will use a SteppedExtrapolation format and
 *  hold the last known value constant.</dd> <dt>True</dt><dd>the server will
 *  project the value using UseSlopedExtrapolation mode.</dd> </dl>
 *
 *  The default value is False. For SimpleBounds this value is ignored.
*/
struct ua_aggregateconfiguration {
    bool use_server_capabilities_defaults;
    bool treat_uncertain_as_bad;
    uint8_t percent_data_bad;
    uint8_t percent_data_good;
    bool use_sloped_extrapolation;
};

void ua_aggregateconfiguration_init(struct ua_aggregateconfiguration *t);
void ua_aggregateconfiguration_clear(struct ua_aggregateconfiguration *t);
int ua_aggregateconfiguration_compare(const struct ua_aggregateconfiguration *a, const struct ua_aggregateconfiguration *b) UA_PURE_FUNCTION;
int ua_aggregateconfiguration_copy(struct ua_aggregateconfiguration *dst, const struct ua_aggregateconfiguration *src);

#endif /* _UABASE_AGGREGATECONFIGURATION_H_ */

