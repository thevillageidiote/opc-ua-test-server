/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_BROWSEPATHTARGET_H_
#define _UABASE_BROWSEPATHTARGET_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/expandednodeid.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_browsepathtarget
 *  A structure that is defined as the type of the targets parameter of the results
 *  parameter of the TranslateBrowsePathsToNodeIds service.
 *
 *  A Server may encounter a reference to a node in another Server which it cannot
 *  follow while it is processing the RelativePath. If this happens, the Server
 *  returns the NodeId of the external node and sets the remainingPathIndex
 *  parameter to indicate which RelativePath elements still need to be processed.
 *  To complete the operation the Client shall connect to the other Server and call
 *  this service again using the target as the startingNode and the unprocessed
 *  elements as the relativePath.
 *
 *  \var ua_browsepathtarget::target_id
 *  The identifier for a target of the RelativePath.
 *
 *  \var ua_browsepathtarget::remaining_path_index
 *  The index of the first unprocessed element in the RelativePath.
 *
 *  This value shall be equal to the maximum value of Index data type if all
 *  elements were processed.
*/
struct ua_browsepathtarget {
    struct ua_expandednodeid target_id;
    uint32_t remaining_path_index;
};

void ua_browsepathtarget_init(struct ua_browsepathtarget *t);
void ua_browsepathtarget_clear(struct ua_browsepathtarget *t);
int ua_browsepathtarget_compare(const struct ua_browsepathtarget *a, const struct ua_browsepathtarget *b) UA_PURE_FUNCTION;
int ua_browsepathtarget_copy(struct ua_browsepathtarget *dst, const struct ua_browsepathtarget *src);

#endif /* _UABASE_BROWSEPATHTARGET_H_ */

