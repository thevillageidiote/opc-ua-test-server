/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_CALLMETHODREQUEST_H_
#define _UABASE_CALLMETHODREQUEST_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/nodeid.h>
#include <uabase/variant.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_callmethodrequest
 *  A structure that is defined as the type of the methodsToCall parameter of the
 *  Call service.
 *
 *  \var ua_callmethodrequest::object_id
 *  The NodeId shall be that of the object or ObjectType that is the source of a
 *  HasComponent Reference (or subtype of HasComponent Reference) to the method
 *  specified in methodId.
 *
 *  See Part 3 of the OPC UA Specification for a description of objects and their
 *  methods.
 *
 *  \var ua_callmethodrequest::method_id
 *  NodeId of the method to invoke.
 *
 *  If the objectId is the NodeId of an object, it is allowed to use the NodeId of
 *  a method that is the target of a HasComponent Reference from the ObjectType of
 *  the object.
 *
 *  \var ua_callmethodrequest::num_input_arguments
 *  Number of elements in \ref ua_callmethodrequest::input_arguments.
 *
 *  \var ua_callmethodrequest::input_arguments
 *  List of input argument values.
 *
 *  An empty list indicates that there are no input arguments. The size and order
 *  of this list matches the size and order of the input arguments defined by the
 *  input InputArguments Property of the method.
 *
 *  The name, a description and the data type of each argument are defined by the
 *  Argument structure in each element of the method’s InputArguments Property.
*/
struct ua_callmethodrequest {
    struct ua_nodeid object_id;
    struct ua_nodeid method_id;
    int32_t num_input_arguments;
    struct ua_variant *input_arguments;
};

void ua_callmethodrequest_init(struct ua_callmethodrequest *t);
void ua_callmethodrequest_clear(struct ua_callmethodrequest *t);
int ua_callmethodrequest_compare(const struct ua_callmethodrequest *a, const struct ua_callmethodrequest *b) UA_PURE_FUNCTION;
int ua_callmethodrequest_copy(struct ua_callmethodrequest *dst, const struct ua_callmethodrequest *src);

#endif /* _UABASE_CALLMETHODREQUEST_H_ */

