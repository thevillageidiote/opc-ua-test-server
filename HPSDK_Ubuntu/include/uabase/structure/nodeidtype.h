/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_NODEIDTYPE_H
#define _UABASE_NODEIDTYPE_H

#include <uabase/base_config.h>

/**
 * @defgroup ua_base_nodeidtype ua_nodeidtype
 * @ingroup ua_base_enums
 * @brief
 *  The possible encodings for a NodeId value.
 * @{
 */

/** \enum ua_nodeidtype
 *  The possible encodings for a NodeId value.
*/
enum ua_nodeidtype
{
    UA_NODEIDTYPE_TWOBYTE = 0,
    UA_NODEIDTYPE_FOURBYTE = 1,
    UA_NODEIDTYPE_NUMERIC = 2,
    UA_NODEIDTYPE_STRING = 3,
    UA_NODEIDTYPE_GUID = 4,
    UA_NODEIDTYPE_BYTESTRING = 5
};

#ifdef ENABLE_TO_STRING
const char* ua_nodeidtype_to_string(enum ua_nodeidtype nodeidtype);
#endif /* ENABLE_TO_STRING */

/** @}*/

#endif /*_UABASE_NODEIDTYPE_H*/
