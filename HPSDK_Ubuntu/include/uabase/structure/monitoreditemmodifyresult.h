/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_MONITOREDITEMMODIFYRESULT_H_
#define _UABASE_MONITOREDITEMMODIFYRESULT_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/extensionobject.h>
#include <uabase/statuscode.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_monitoreditemmodifyresult
 *  A structure that is defined as the type of the results parameter of the
 *  ModifyMonitoredItems service.
 *
 *  \var ua_monitoreditemmodifyresult::status_code
 *  StatusCode for the MonitoredItem to be modified.
 *
 *  \var ua_monitoreditemmodifyresult::revised_sampling_interval
 *  The actual sampling interval that the Server will use.
 *
 *  The Server returns the value it will actually use for the sampling interval.
 *  This value is based on a number of factors, including capabilities of the
 *  underlying system.
 *
 *  The Server shall always return a revisedSamplingInterval that is equal or
 *  higher than the requested samplingInterval. If the requestedSamplingInterval is
 *  higher than the maximum sampling interval supported by the Server, the maximum
 *  sampling interval is returned.
 *
 *  \var ua_monitoreditemmodifyresult::revised_queue_size
 *  The actual queue size that the Server will use.
 *
 *  \var ua_monitoreditemmodifyresult::filter_result
 *  Contains any revised parameter values or error results associated with the
 *  MonitoringFilter specified in the request.
 *
 *  This parameter may be omitted if no errors occurred. The MonitoringFilterResult
 *  parameter type is an extensible parameter type.
*/
struct ua_monitoreditemmodifyresult {
    ua_statuscode status_code;
    double revised_sampling_interval;
    uint32_t revised_queue_size;
    struct ua_extensionobject filter_result;
};

void ua_monitoreditemmodifyresult_init(struct ua_monitoreditemmodifyresult *t);
void ua_monitoreditemmodifyresult_clear(struct ua_monitoreditemmodifyresult *t);
int ua_monitoreditemmodifyresult_compare(const struct ua_monitoreditemmodifyresult *a, const struct ua_monitoreditemmodifyresult *b) UA_PURE_FUNCTION;
int ua_monitoreditemmodifyresult_copy(struct ua_monitoreditemmodifyresult *dst, const struct ua_monitoreditemmodifyresult *src);

#endif /* _UABASE_MONITOREDITEMMODIFYRESULT_H_ */

