/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_LITERALOPERAND_H_
#define _UABASE_LITERALOPERAND_H_

#include <platform/platform.h>
#include <uabase/base_config.h>
#include <uabase/variant.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_literaloperand
 *
 *  \var ua_literaloperand::value
 *  A literal value.
*/
struct ua_literaloperand {
    struct ua_variant value;
};

void ua_literaloperand_init(struct ua_literaloperand *t);
void ua_literaloperand_clear(struct ua_literaloperand *t);
int ua_literaloperand_compare(const struct ua_literaloperand *a, const struct ua_literaloperand *b) UA_PURE_FUNCTION;
int ua_literaloperand_copy(struct ua_literaloperand *dst, const struct ua_literaloperand *src);

#endif /* _UABASE_LITERALOPERAND_H_ */

