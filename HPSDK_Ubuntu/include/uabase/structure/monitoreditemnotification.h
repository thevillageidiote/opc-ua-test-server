/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_MONITOREDITEMNOTIFICATION_H_
#define _UABASE_MONITOREDITEMNOTIFICATION_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/datavalue.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_monitoreditemnotification
*/
struct ua_monitoreditemnotification {
    uint32_t client_handle;
    struct ua_datavalue value;
};

void ua_monitoreditemnotification_init(struct ua_monitoreditemnotification *t);
void ua_monitoreditemnotification_clear(struct ua_monitoreditemnotification *t);
int ua_monitoreditemnotification_compare(const struct ua_monitoreditemnotification *a, const struct ua_monitoreditemnotification *b) UA_PURE_FUNCTION;
int ua_monitoreditemnotification_copy(struct ua_monitoreditemnotification *dst, const struct ua_monitoreditemnotification *src);

#endif /* _UABASE_MONITOREDITEMNOTIFICATION_H_ */

