/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_SERVERSTATE_H
#define _UABASE_SERVERSTATE_H

#include <uabase/base_config.h>

/**
 * @defgroup ua_base_serverstate ua_serverstate
 * @ingroup ua_base_enums
 * @brief
 *  An enumeration that defines the execution state of the Server.
 * @{
 */

/** \enum ua_serverstate
 *  An enumeration that defines the execution state of the Server.
 *
 *  \var UA_SERVERSTATE_RUNNING
 *  The server is running normally.
 *
 *  This is the usual state for a server.
 *
 *  \var UA_SERVERSTATE_FAILED
 *  A vendor-specific fatal error has occurred within the server.
 *
 *  The server is no longer functioning. The recovery procedure from this situation
 *  is vendor-specific. Most Service requests should be expected to fail.
 *
 *  \var UA_SERVERSTATE_NOCONFIGURATION
 *  The server is running but has no configuration information loaded and therefore
 *  does not transfer data.
 *
 *  \var UA_SERVERSTATE_SUSPENDED
 *  The server has been temporarily suspended by some vendor-specific method and is
 *  not receiving or sending data.
 *
 *  \var UA_SERVERSTATE_SHUTDOWN
 *  The server has shut down or is in the process of shutting down.
 *
 *  Depending on the implementation, this might or might not be visible to clients.
 *
 *  \var UA_SERVERSTATE_TEST
 *  The server is in Test Mode.
 *
 *  The outputs are disconnected from the real hardware, but the server will
 *  otherwise behave normally. Inputs may be real or may be simulated depending on
 *  the vendor implementation. StatusCode will generally be returned normally.
 *
 *  \var UA_SERVERSTATE_COMMUNICATIONFAULT
 *  The server is running properly, but is having difficulty accessing data from
 *  its data sources.
 *
 *  This may be due to communication problems or some other problem preventing the
 *  underlying device, control system, etc. from returning valid data. It may be a
 *  complete failure, meaning that no data is available, or a partial failure,
 *  meaning that some data is still available. It is expected that items affected
 *  by the fault will individually return with a BAD FAILURE status code indication
 *  for the items.
 *
 *  \var UA_SERVERSTATE_UNKNOWN
 *  This state is used only to indicate that the OPC UA server does not know the
 *  state of underlying servers.
*/
enum ua_serverstate
{
    UA_SERVERSTATE_RUNNING = 0,
    UA_SERVERSTATE_FAILED = 1,
    UA_SERVERSTATE_NOCONFIGURATION = 2,
    UA_SERVERSTATE_SUSPENDED = 3,
    UA_SERVERSTATE_SHUTDOWN = 4,
    UA_SERVERSTATE_TEST = 5,
    UA_SERVERSTATE_COMMUNICATIONFAULT = 6,
    UA_SERVERSTATE_UNKNOWN = 7
};

#ifdef ENABLE_TO_STRING
const char* ua_serverstate_to_string(enum ua_serverstate serverstate);
#endif /* ENABLE_TO_STRING */

/** @}*/

#endif /*_UABASE_SERVERSTATE_H*/
