/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_EVENTFILTER_H_
#define _UABASE_EVENTFILTER_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/structure/contentfilter.h>
#include <uabase/structure/simpleattributeoperand.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_eventfilter
 *  Provides for the filtering and content selection of Event Subscriptions.
 *
 *  \var ua_eventfilter::num_select_clauses
 *  Number of elements in \ref ua_eventfilter::select_clauses.
 *
 *  \var ua_eventfilter::select_clauses
 *  List of the values to return with each event in a notification.
 *
 *  At least one valid clause shall be specified.
 *
 *  \var ua_eventfilter::where_clause
 *  Limit the Notifications to those events that match the criteria defined by this
 *  \ref ua_contentfilter.
 *
 *  The \ref ua_attributeoperand structure may not be used in an EventFilter.
*/
struct ua_eventfilter {
    int32_t num_select_clauses;
    struct ua_simpleattributeoperand *select_clauses;
    struct ua_contentfilter where_clause;
};

void ua_eventfilter_init(struct ua_eventfilter *t);
void ua_eventfilter_clear(struct ua_eventfilter *t);
int ua_eventfilter_compare(const struct ua_eventfilter *a, const struct ua_eventfilter *b) UA_PURE_FUNCTION;
int ua_eventfilter_copy(struct ua_eventfilter *dst, const struct ua_eventfilter *src);

#endif /* _UABASE_EVENTFILTER_H_ */

