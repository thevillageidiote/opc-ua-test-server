/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_ATTRIBUTEWRITEMASK_H
#define _UABASE_ATTRIBUTEWRITEMASK_H

#include <uabase/base_config.h>

/**
 * @defgroup ua_base_attributewritemask ua_attributewritemask
 * @ingroup ua_base_enums
 * @brief
 * Define bits used to indicate which attributes are writable.
 * @{
 */

/** \enum ua_attributewritemask
Define bits used to indicate which attributes are writable.
*/
enum ua_attributewritemask
{
    UA_ATTRIBUTEWRITEMASK_NONE = 0,
    UA_ATTRIBUTEWRITEMASK_ACCESSLEVEL = 1,
    UA_ATTRIBUTEWRITEMASK_ARRAYDIMENSIONS = 2,
    UA_ATTRIBUTEWRITEMASK_BROWSENAME = 4,
    UA_ATTRIBUTEWRITEMASK_CONTAINSNOLOOPS = 8,
    UA_ATTRIBUTEWRITEMASK_DATATYPE = 16,
    UA_ATTRIBUTEWRITEMASK_DESCRIPTION = 32,
    UA_ATTRIBUTEWRITEMASK_DISPLAYNAME = 64,
    UA_ATTRIBUTEWRITEMASK_EVENTNOTIFIER = 128,
    UA_ATTRIBUTEWRITEMASK_EXECUTABLE = 256,
    UA_ATTRIBUTEWRITEMASK_HISTORIZING = 512,
    UA_ATTRIBUTEWRITEMASK_INVERSENAME = 1024,
    UA_ATTRIBUTEWRITEMASK_ISABSTRACT = 2048,
    UA_ATTRIBUTEWRITEMASK_MINIMUMSAMPLINGINTERVAL = 4096,
    UA_ATTRIBUTEWRITEMASK_NODECLASS = 8192,
    UA_ATTRIBUTEWRITEMASK_NODEID = 16384,
    UA_ATTRIBUTEWRITEMASK_SYMMETRIC = 32768,
    UA_ATTRIBUTEWRITEMASK_USERACCESSLEVEL = 65536,
    UA_ATTRIBUTEWRITEMASK_USEREXECUTABLE = 131072,
    UA_ATTRIBUTEWRITEMASK_USERWRITEMASK = 262144,
    UA_ATTRIBUTEWRITEMASK_VALUERANK = 524288,
    UA_ATTRIBUTEWRITEMASK_WRITEMASK = 1048576,
    UA_ATTRIBUTEWRITEMASK_VALUEFORVARIABLETYPE = 2097152
};

#ifdef ENABLE_TO_STRING
const char* ua_attributewritemask_to_string(enum ua_attributewritemask attributewritemask);
#endif /* ENABLE_TO_STRING */

/** @}*/

#endif /*_UABASE_ATTRIBUTEWRITEMASK_H*/
