/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_BROWSERESULT_H_
#define _UABASE_BROWSERESULT_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/bytestring.h>
#include <uabase/statuscode.h>
#include <uabase/structure/referencedescription.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_browseresult
 *  The results of a Browse operation.
 *
 *  \var ua_browseresult::status_code
 *  The status for the BrowseDescription.
 *
 *  This value is set to Good if there are still references to return for the
 *  BrowseDescription.
 *
 *  \var ua_browseresult::continuation_point
 *  A Server defined opaque value that identifies the continuation point.
 *
 *  \var ua_browseresult::num_references
 *  Number of elements in \ref ua_browseresult::references.
 *
 *  \var ua_browseresult::references
 *  The set of references that meet the criteria specified in the
 *  BrowseDescription.
 *
 *  Empty if no references met the criteria.
*/
struct ua_browseresult {
    ua_statuscode status_code;
    struct ua_bytestring continuation_point;
    int32_t num_references;
    struct ua_referencedescription *references;
};

void ua_browseresult_init(struct ua_browseresult *t);
void ua_browseresult_clear(struct ua_browseresult *t);
int ua_browseresult_compare(const struct ua_browseresult *a, const struct ua_browseresult *b) UA_PURE_FUNCTION;
int ua_browseresult_copy(struct ua_browseresult *dst, const struct ua_browseresult *src);

#endif /* _UABASE_BROWSERESULT_H_ */

