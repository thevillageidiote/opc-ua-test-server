/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_APPLICATIONTYPE_H
#define _UABASE_APPLICATIONTYPE_H

#include <uabase/base_config.h>

/**
 * @defgroup ua_base_applicationtype ua_applicationtype
 * @ingroup ua_base_enums
 * @brief
 *  The type of application
 * @{
 */

/** \enum ua_applicationtype
 *  The type of application
 *
 *  \var UA_APPLICATIONTYPE_SERVER
 *  The application is a Server.
 *
 *  \var UA_APPLICATIONTYPE_CLIENT
 *  The application is a Client.
 *
 *  \var UA_APPLICATIONTYPE_CLIENTANDSERVER
 *  The application is a Client and a Server.
 *
 *  \var UA_APPLICATIONTYPE_DISCOVERYSERVER
 *  The application is a DiscoveryServer.
*/
enum ua_applicationtype
{
    UA_APPLICATIONTYPE_SERVER = 0,
    UA_APPLICATIONTYPE_CLIENT = 1,
    UA_APPLICATIONTYPE_CLIENTANDSERVER = 2,
    UA_APPLICATIONTYPE_DISCOVERYSERVER = 3
};

#ifdef ENABLE_TO_STRING
const char* ua_applicationtype_to_string(enum ua_applicationtype applicationtype);
#endif /* ENABLE_TO_STRING */

/** @}*/

#endif /*_UABASE_APPLICATIONTYPE_H*/
