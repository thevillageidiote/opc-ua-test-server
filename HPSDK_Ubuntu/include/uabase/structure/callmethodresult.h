/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_CALLMETHODRESULT_H_
#define _UABASE_CALLMETHODRESULT_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/diagnosticinfo.h>
#include <uabase/statuscode.h>
#include <uabase/variant.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_callmethodresult
 *  A structure that is defined as the type of the results parameter of the Call
 *  service.
 *
 *  \var ua_callmethodresult::status_code
 *  StatusCode of the method executed in the server.
 *
 *  This StatusCode is set to the Bad_InvalidArgument StatusCode if at least one
 *  input argument broke a constraint (e.g. wrong data type, value out of range).
 *
 *  This StatusCode is set to a bad StatusCode if the method execution failed in
 *  the server, e.g. based on an exception or an HRESULT.
 *
 *  \var ua_callmethodresult::num_input_argument_results
 *  Number of elements in \ref ua_callmethodresult::input_argument_results.
 *
 *  \var ua_callmethodresult::input_argument_results
 *  List of StatusCodes for each inputArgument.
 *
 *  \var ua_callmethodresult::num_input_argument_diag_infos
 *  Number of elements in \ref ua_callmethodresult::input_argument_diag_infos.
 *
 *  \var ua_callmethodresult::input_argument_diag_infos
 *  List of diagnostic information for each inputArgument.
 *
 *  This list is empty if diagnostics information was not requested in the request
 *  header or if no diagnostic information was encountered in processing of the
 *  request.
 *
 *  \var ua_callmethodresult::num_output_arguments
 *  Number of elements in \ref ua_callmethodresult::output_arguments.
 *
 *  \var ua_callmethodresult::output_arguments
 *  List of output argument values.
 *
 *  An empty list indicates that there are no output arguments. The size and order
 *  of this list matches the size and order of the output arguments defined by the
 *  OutputArguments Property of the method.
 *
 *  The name, a description and the data type of each argument are defined by the
 *  Argument structure in each element of the methods OutputArguments Property.
*/
struct ua_callmethodresult {
    ua_statuscode status_code;
    int32_t num_input_argument_results;
    ua_statuscode *input_argument_results;
#ifdef ENABLE_DIAGNOSTICS
    int32_t num_input_argument_diag_infos;
    struct ua_diagnosticinfo *input_argument_diag_infos;
#endif
    int32_t num_output_arguments;
    struct ua_variant *output_arguments;
};

void ua_callmethodresult_init(struct ua_callmethodresult *t);
void ua_callmethodresult_clear(struct ua_callmethodresult *t);
int ua_callmethodresult_compare(const struct ua_callmethodresult *a, const struct ua_callmethodresult *b) UA_PURE_FUNCTION;
int ua_callmethodresult_copy(struct ua_callmethodresult *dst, const struct ua_callmethodresult *src);

#endif /* _UABASE_CALLMETHODRESULT_H_ */

