/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_CONTENTFILTERELEMENT_H_
#define _UABASE_CONTENTFILTERELEMENT_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/extensionobject.h>
#include <uabase/structure/filteroperator.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_contentfilterelement
 *  A structure that is defined as the type of the elements parameter of the
 *  ContentFilter structure.
 *
 *  \var ua_contentfilterelement::filter_operator
 *  Filter operator to be evaluated.
 *
 *  \var ua_contentfilterelement::num_filter_operands
 *  Number of elements in \ref ua_contentfilterelement::filter_operands.
 *
 *  \var ua_contentfilterelement::filter_operands
 *  Operands used by the selected operator.
 *
 *  The number and use depend on the operators (see \ref ua_filteroperator). This
 *  array needs at least one entry.
*/
struct ua_contentfilterelement {
    enum ua_filteroperator filter_operator;
    int32_t num_filter_operands;
    struct ua_extensionobject *filter_operands;
};

void ua_contentfilterelement_init(struct ua_contentfilterelement *t);
void ua_contentfilterelement_clear(struct ua_contentfilterelement *t);
int ua_contentfilterelement_compare(const struct ua_contentfilterelement *a, const struct ua_contentfilterelement *b) UA_PURE_FUNCTION;
int ua_contentfilterelement_copy(struct ua_contentfilterelement *dst, const struct ua_contentfilterelement *src);

#endif /* _UABASE_CONTENTFILTERELEMENT_H_ */

