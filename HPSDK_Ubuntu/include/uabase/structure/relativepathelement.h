/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_RELATIVEPATHELEMENT_H_
#define _UABASE_RELATIVEPATHELEMENT_H_

#include <platform/platform.h>
#include <stdbool.h>
#include <uabase/base_config.h>
#include <uabase/nodeid.h>
#include <uabase/qualifiedname.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_relativepathelement
 *  A sequence of References and BrowseNames to follow.
 *
 *  Each element in the sequence is processed by finding the targets and then using
 *  those targets as the starting nodes for the next element. The targets of the
 *  final element are the target of the RelativePath.
 *
 *  \var ua_relativepathelement::reference_type_id
 *  The type of reference to follow from the current node.
 *
 *  The current path cannot be followed any further if the referenceTypeId is not
 *  available on the Node instance.
 *
 *  If not specified, then all References are included and the parameter
 *  includeSubtypes is ignored.
 *
 *  \var ua_relativepathelement::is_inverse
 *  Indicates whether the inverse Reference should be followed. The inverse
 *  reference is followed if this value is TRUE.
 *
 *  \var ua_relativepathelement::include_subtypes
 *  Indicates whether subtypes of the ReferenceType should be followed. Subtypes
 *  are included if this value is TRUE.
 *
 *  \var ua_relativepathelement::target_name
 *  The BrowseName of the target node.
 *
 *  The final element may have an empty targetName. In this situation all targets
 *  of the references identified by the referenceTypeId are the targets of the
 *  RelativePath.
 *
 *  The targetName shall be specified for all other elements.
 *
 *  The current path cannot be followed any further if no targets with the
 *  specified BrowseName exist.
*/
struct ua_relativepathelement {
    struct ua_nodeid reference_type_id;
    bool is_inverse;
    bool include_subtypes;
    struct ua_qualifiedname target_name;
};

void ua_relativepathelement_init(struct ua_relativepathelement *t);
void ua_relativepathelement_clear(struct ua_relativepathelement *t);
int ua_relativepathelement_compare(const struct ua_relativepathelement *a, const struct ua_relativepathelement *b) UA_PURE_FUNCTION;
int ua_relativepathelement_copy(struct ua_relativepathelement *dst, const struct ua_relativepathelement *src);

#endif /* _UABASE_RELATIVEPATHELEMENT_H_ */

