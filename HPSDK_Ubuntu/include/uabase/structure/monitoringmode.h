/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_MONITORINGMODE_H
#define _UABASE_MONITORINGMODE_H

#include <uabase/base_config.h>

/**
 * @defgroup ua_base_monitoringmode ua_monitoringmode
 * @ingroup ua_base_enums
 * @brief
 *  An enumeration that specifies whether sampling and reporting are enabled or
 *  disabled for a MonitoredItem.
 *
 *  The value of the publishing enabled parameter for a Subscription does not
 *  affect the value of the monitoring mode for a MonitoredItem of the
 *  Subscription.
 * @{
 */

/** \enum ua_monitoringmode
 *  An enumeration that specifies whether sampling and reporting are enabled or
 *  disabled for a MonitoredItem.
 *
 *  The value of the publishing enabled parameter for a Subscription does not
 *  affect the value of the monitoring mode for a MonitoredItem of the
 *  Subscription.
 *
 *  \var UA_MONITORINGMODE_DISABLED
 *  The item being monitored is not sampled or evaluated, and Notifications are not
 *  generated or queued. Notification reporting is disabled.
 *
 *  \var UA_MONITORINGMODE_SAMPLING
 *  The item being monitored is sampled and evaluated, and Notifications are
 *  generated and queued. Notification reporting is disabled.
 *
 *  \var UA_MONITORINGMODE_REPORTING
 *  The item being monitored is sampled and evaluated, and Notifications are
 *  generated and queued. Notification reporting is enabled.
*/
enum ua_monitoringmode
{
    UA_MONITORINGMODE_DISABLED = 0,
    UA_MONITORINGMODE_SAMPLING = 1,
    UA_MONITORINGMODE_REPORTING = 2
};

#ifdef ENABLE_TO_STRING
const char* ua_monitoringmode_to_string(enum ua_monitoringmode monitoringmode);
#endif /* ENABLE_TO_STRING */

/** @}*/

#endif /*_UABASE_MONITORINGMODE_H*/
