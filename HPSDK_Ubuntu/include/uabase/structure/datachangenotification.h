/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_DATACHANGENOTIFICATION_H_
#define _UABASE_DATACHANGENOTIFICATION_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/diagnosticinfo.h>
#include <uabase/structure/monitoreditemnotification.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_datachangenotification
*/
struct ua_datachangenotification {
    int32_t num_monitored_items;
    struct ua_monitoreditemnotification *monitored_items;
#ifdef ENABLE_DIAGNOSTICS
    int32_t num_diag_infos;
    struct ua_diagnosticinfo *diag_infos;
#endif
};

void ua_datachangenotification_init(struct ua_datachangenotification *t);
void ua_datachangenotification_clear(struct ua_datachangenotification *t);
int ua_datachangenotification_compare(const struct ua_datachangenotification *a, const struct ua_datachangenotification *b) UA_PURE_FUNCTION;
int ua_datachangenotification_copy(struct ua_datachangenotification *dst, const struct ua_datachangenotification *src);

#endif /* _UABASE_DATACHANGENOTIFICATION_H_ */

