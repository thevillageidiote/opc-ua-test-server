/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_EUINFORMATION_H_
#define _UABASE_EUINFORMATION_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/localizedtext.h>
#include <uabase/string.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_euinformation
 *  Contains information about the EngineeringUnits.
 *
 *  Understanding the units of a measurement value is essential for a uniform
 *  system. In an open system in particular where servers from different cultures
 *  might be used, it is essential to know what the units of measurement are. Based
 *  on such knowledge, values can be converted if necessary before being used.
 *  Therefore, although defined as optional, support of the EngineeringUnits
 *  Property is strongly advised.
 *
 *  To facilitate interoperability, OPC UA specifies how to apply the widely
 *  accepted “Codes for Units of Measurement (Recommendation No. 20)” published by
 *  the “United Nations Centre for Trade Facilitation and Electronic Business” (see
 *  UN/CEFACT). It uses and is based on the International System of Units (SI
 *  Units) but in addition provides a fixed code that can be used for automated
 *  evaluation. This recommendation has been accepted by many industries on a
 *  global basis.
 *
 *  \var ua_euinformation::namespace_uri
 *  Identifies the organization (company, standards organization) that defines the
 *  EUInformation.
 *
 *  \var ua_euinformation::unit_id
 *  Identifier for programmatic evaluation.
 *
 *  −1 is used if a unitId is not available.
 *
 *  \var ua_euinformation::display_name
 *  The displayName of the engineering unit.
 *
 *  This is typically the abbreviation of the engineering unit, for example ”h” for
 *  hour or ”m/s” for meter per second.
 *
 *  \var ua_euinformation::description
 *  Contains the full name of the engineering unit such as ”hour” or ”meter per
 *  second”.
*/
struct ua_euinformation {
    struct ua_string namespace_uri;
    int32_t unit_id;
    struct ua_localizedtext display_name;
    struct ua_localizedtext description;
};

void ua_euinformation_init(struct ua_euinformation *t);
void ua_euinformation_clear(struct ua_euinformation *t);
int ua_euinformation_compare(const struct ua_euinformation *a, const struct ua_euinformation *b) UA_PURE_FUNCTION;
int ua_euinformation_copy(struct ua_euinformation *dst, const struct ua_euinformation *src);

#endif /* _UABASE_EUINFORMATION_H_ */

