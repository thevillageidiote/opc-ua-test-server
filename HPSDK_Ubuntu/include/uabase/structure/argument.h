/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_ARGUMENT_H_
#define _UABASE_ARGUMENT_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/localizedtext.h>
#include <uabase/nodeid.h>
#include <uabase/string.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_argument
 *  Defines a method input or output argument specification.
 *
 *  It is for example used in the input and output argument Properties for methods.
 *
 *  \var ua_argument::name
 *  The name of the argument
 *
 *  \var ua_argument::data_type
 *  The NodeId of the DataType of this argument.
 *
 *  \var ua_argument::value_rank
 *  Indicates whether the DataType is an array and how many dimensions the array
 *  has.
 *
 *  It may have the following values: <dl> <dt>n \> 1</dt> <dd>The DataType is an
 *  array with the specified number of dimensions.</dd> <dt>OneDimension (1)</dt>
 *  <dd>The DataType is an array with one dimension.</dd> <dt>OneOrMoreDimensions
 *  (0)</dt> <dd>The dataType is an array with one or more dimensions.</dd>
 *  <dt>Scalar (−1)</dt> <dd>The DataType is not an array.</dd> <dt>Any (−2)</dt>
 *  <dd>The DataType can be a scalar or an array with any number of
 *  dimensions.</dd> <dt>ScalarOrOneDimension (−3)</dt> <dd>The DataType can be a
 *  scalar or a one dimensional array.</dd> </dl>
 *
 *  \note All DataTypes are considered to be scalar, even if they have array-like
 *  semantics like ByteString and String.
 *
 *  \var ua_argument::num_array_dimensions
 *  Number of elements in \ref ua_argument::array_dimensions.
 *
 *  \var ua_argument::array_dimensions
 *  Specifies the length of each dimension for an array dataType.
 *
 *  It is intended to describe the capability of the DataType, not the current
 *  size.
 *
 *  The number of elements shall be equal to the value of the valueRank. Shall be
 *  null if valueRank ≦ 0.
 *
 *  A value of 0 for an individual dimension indicates that the dimension has a
 *  variable length.
 *
 *  \var ua_argument::description
 *  A localised description of the argument.
*/
struct ua_argument {
    struct ua_string name;
    struct ua_nodeid data_type;
    int32_t value_rank;
    int32_t num_array_dimensions;
    uint32_t *array_dimensions;
    struct ua_localizedtext description;
};

void ua_argument_init(struct ua_argument *t);
void ua_argument_clear(struct ua_argument *t);
int ua_argument_compare(const struct ua_argument *a, const struct ua_argument *b) UA_PURE_FUNCTION;
int ua_argument_copy(struct ua_argument *dst, const struct ua_argument *src);

#endif /* _UABASE_ARGUMENT_H_ */

