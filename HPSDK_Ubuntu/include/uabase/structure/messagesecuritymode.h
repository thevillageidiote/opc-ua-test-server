/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_MESSAGESECURITYMODE_H
#define _UABASE_MESSAGESECURITYMODE_H

#include <uabase/base_config.h>

/**
 * @defgroup ua_base_messagesecuritymode ua_messagesecuritymode
 * @ingroup ua_base_enums
 * @brief
 *  An enumeration that specifies what security should be applied to messages
 *  exchanges during a Session.
 * @{
 */

/** \enum ua_messagesecuritymode
 *  An enumeration that specifies what security should be applied to messages
 *  exchanges during a Session.
 *
 *  \var UA_MESSAGESECURITYMODE_INVALID
 *  The MessageSecurityMode is invalid.
 *
 *  This value is the default value to avoid an accidental choice of no security is
 *  applied. This choice will always be rejected.
 *
 *  \var UA_MESSAGESECURITYMODE_NONE
 *  No security is applied.
 *
 *  \var UA_MESSAGESECURITYMODE_SIGN
 *  All messages are signed but not encrypted.
 *
 *  \var UA_MESSAGESECURITYMODE_SIGNANDENCRYPT
 *  All messages are signed and encrypted.
*/
enum ua_messagesecuritymode
{
    UA_MESSAGESECURITYMODE_INVALID = 0,
    UA_MESSAGESECURITYMODE_NONE = 1,
    UA_MESSAGESECURITYMODE_SIGN = 2,
    UA_MESSAGESECURITYMODE_SIGNANDENCRYPT = 3
};

#ifdef ENABLE_TO_STRING
const char* ua_messagesecuritymode_to_string(enum ua_messagesecuritymode messagesecuritymode);
#endif /* ENABLE_TO_STRING */

/** @}*/

#endif /*_UABASE_MESSAGESECURITYMODE_H*/
