/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef EVENTNOTIFIERS_H_CAIKTV3C
#define EVENTNOTIFIERS_H_CAIKTV3C

/*============================================================================
 * Flags that can be set for the EventNotifier attribute.
 *===========================================================================*/

/* The Object or View produces no event and has no event history. */
#define UA_EVENTNOTIFIER_NONE 0x0

/* The Object or View produces event notifications. */
#define UA_EVENTNOTIFIER_SUBSCRIBETOEVENTS 0x1

/* The Object has an event history which may be read. */
#define UA_EVENTNOTIFIER_HISTORYREAD 0x4

/* The Object has an event history which may be updated. */
#define UA_EVENTNOTIFIER_HISTORYWRITE 0x8

#endif /* end of include guard: EVENTNOTIFIERS_H_CAIKTV3C */

