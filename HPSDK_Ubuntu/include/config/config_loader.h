/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef CONFIG_LOADER_H
#define CONFIG_LOADER_H

#include <stdlib.h>
#include <stdint.h>
#include <platform/platform.h>
#include <platform/file.h>

/* helper MACROS for creating config layout */
#define CONFIG_MEMBER(member, structure) #member, offsetof(struct structure, member)
#define CONFIG_ARRAY(array, len_field, members) (void **) &array, sizeof(*array), &len_field, members, countof(members)

/* limits of the config loader */
#define CONFIG_MAX_LINE_LENGTH UA_STREAM_BUF_SIZE
#define CONFIG_MAX_ARRAY_LENGTH 10


/**
 * This config loader can parse a custom configuration by providing
 * the description of the configuration. The description needs to contain
 * information about the config sections and associated scalar and array
 * values. It is structured like this:
 *
 * section[]
 *  |-name
 *  |-scalar_value[]
 *  |  |-name
 *  |  |-ptr
 *  |  |-type
 *  |
 *  |-array_value[]
 *     |-name
 *     |-ptr
 *     |-element_size
 *     |-ptr_to_length
 *     |-member[]
 *        |-name
 *        |-offset
 *        |-type
 *
 * Example:
 * @code
 * // Given the following configuration:
 * struct my_array {
 *     uint32_t member1;
 *     char *member2;
 * };
 *
 * struct my_config {
 *     bool scalar1;
 *     char *scalar2;
 *     struct my_array *array1;
 *     uint32_t num_array1;
 *
 * struct my_config g_config = {0};
 *
 *
 * // The description would look like the following (with one section, called "section1"):
 * static const struct config_array_member my_array_members[] = {
 *     {CONFIG_MEMBER(member1, my_array), CONFIG_TYPE_UINT32},
 *     {CONFIG_MEMBER(member2, my_array), CONFIG_TYPE_STRING}
 * };
 *
 * static const struct config_array_value section1_arrays[] = {
 *     {"my_array", CONFIG_ARRAY(g_config.array1, g_config.num_array1, my_array_members)}
 * };
 *
 * static const struct config_scalar_value section1_scalars[] = {
 *     {"scalar1", &g_config.scalar1, CONFIG_TYPE_BOOL},
 *     {"scalar2", &g_config.scalar2, CONFIG_TYPE_STRING}
 * };
 *
 * static const struct config_section my_config_sections[] = {
 *     {"section1", section1_scalars, countof(section1_scalars), section1_arrays, countof(section1_arrays)},
 * };
 *
 *
 * // The configuartion can be loaded from file by calling:
 * ret = config_load_from_file("my_settings.conf", my_config_sections, countof(my_config_sections));
 * @endcode
 */

/** Possible types for scalar values and array members. */
#define CONFIG_TYPE_UINT32 0
#define CONFIG_TYPE_STRING 1
#define CONFIG_TYPE_BOOL 2
#define CONFIG_TYPE_INDEX_ARRAY 3
#define CONFIG_TYPE_INT32  4
#define CONFIG_TYPE_UINT16 5
#define CONFIG_TYPE_INT16  6
#define CONFIG_TYPE_UINT8  7
#define CONFIG_TYPE_INT8   8

/** Description of a scalar value. */
struct config_scalar_value {
    const char *value_name;         /**< name of the scalar value */
    void *ptr;                      /**< pointer to the scalar value in the config */
    uint32_t type;                  /**< type of the scalar value */
};

/** Description of one array member. */
struct config_array_member {
    const char *value_name;         /**< name of the array member */
    size_t offset;                  /**< offset of the array member from the start of the array (in bytes) */
    uint32_t type;                  /**< type of the array member */
};

/** Description of an array. */
struct config_array_value {
    const char *value_name;                     /**< name of the array */
    void **ptr;                                 /**< pointer to the arraypointer in the config */
    size_t element_size;                        /**< size of one element of the array (in bytes) */
    uint32_t *length_field;                     /**< pointer to the length field of the array */
    const struct config_array_member *members;  /**< array of member descriptions of the array */
    unsigned int num_members;                   /**< number of elements in the members array */
};

/** Description of sections in the config file. */
struct config_section {
    const char *section_name;                           /**< name of the section */
    const struct config_scalar_value *scalar_values;    /**< array of scalar value descriptions */
    unsigned int num_scalar_values;                     /**< number of elements in the scalar_values array */
    const struct config_array_value *array_values;      /**< array of array value descriptions */
    unsigned int num_array_values;                      /**< number of elements in the array_values array */
};

typedef int (*config_load_value)(char *string, void *value, uint32_t type);
typedef int (*config_clear_value)(void *value, uint32_t type);
typedef int (*config_write_value) (struct ua_filestream *stream, void *value, uint32_t type);

int config_load_from_file(const char *filename, const struct config_section *sections, unsigned int num_sections, config_load_value load_fct, config_clear_value clear_fct);
void config_clear(const struct config_section *sections, unsigned int num_sections, config_clear_value clear_fct);
int config_write_to_file(const char *filename, const struct config_section *sections, unsigned int num_sections, config_write_value write_fct);

#endif /* CONFIG_LOADER_H */
