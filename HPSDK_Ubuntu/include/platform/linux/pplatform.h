/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef UA_LINUX_PLATFORM_H
#define UA_LINUX_PLATFORM_H

#include <platform/platform_config.h>
#include <endian.h>
#include <stddef.h>
#include <assert.h>
#include <features.h> /* for __USE_ISOC11 */
#include <limits.h>   /* for PATH_MAX */
#include <float.h>    /* for MAX and MIN constants */
#include <math.h>     /* for NAN and INFINITY support */
#include <stdio.h>    /* for fprintf (trace) */

#ifdef __cplusplus
# define UA_BEGIN_EXTERN_C extern "C" {
# define UA_END_EXTERN_C }
#else
# define UA_BEGIN_EXTERN_C
# define UA_END_EXTERN_C
#endif

#define UA_UNUSED(x) (void)(x)

#define UA_LITTLE_ENDIAN __LITTLE_ENDIAN
#define UA_BIG_ENDIAN __BIG_ENDIAN
#define UA_PDP_ENDIAN __PDP_ENDIAN
/* configure the general byte order of your host system */
#define UA_BYTE_ORDER __BYTE_ORDER
/* on some systems floating point byte order is different from the
 * integer byte order. using the following defines you can configure this.
 */
#define UA_FLOAT_BYTE_ORDER __BYTE_ORDER
#define UA_DOUBLE_BYTE_ORDER __BYTE_ORDER

/* container_of returns the base address of the structure containing
 * the field 'member'. So it does the same as offsetof() macro which
 * is part of the C standard, just in the opposite direction.
 * Indeed it uses offsetof() to calculate the address.
 *
 * Example:
 * struct foo {
 *     int foo;
 *     int bar;
 *     int x;
 * };
 *
 * struct foo test;
 * struct foo *p;
 * int        *barptr = &test.bar;
 *
 * Assume you have the address of the field bar and want to get the address
 * of test. Do the following.
 *
 * p = offsetof(barptr, struct foo, bar);
 *
 */
#ifndef __STRICT_ANSI__
#define container_of(ptr, type, member) ({                      \
        typeof( ((type *)0)->member ) *__mptr = (ptr);    \
        (type *)(void *)( (char *)__mptr - offsetof(type,member) );})
#else
#define container_of(pointer, type, member) ((type *)( \
   (char *)(pointer) - offsetof(type,member)))
#endif

#define GCC_VERSION (__GNUC__ * 10000 \
                     + __GNUC_MINOR__ * 100 \
                     + __GNUC_PATCHLEVEL__)

/* Force a compilation error if condition is true, but also produce a
   result (of value 0 and type size_t), so the expression can be used
   e.g. in a structure initializer (or where-ever else comma expressions
   aren't permitted). */
#define BUILD_BUG_ON_ZERO(e) (sizeof(struct { int:-!!(e); }))
/* ensures the given argument is an array. Returns zero if arr is an array
 * or produces an error using BUILD_BUG_ON_ZERO
 */
#define __must_be_array(a) BUILD_BUG_ON_ZERO(__builtin_types_compatible_p(typeof(a), typeof(&a[0])))

/* Returns the size of an array (number of elements)
 * We use some GCC magic to make it more typesafe,
 * if this macro is used on a pointer it will break the build
 * instead of producing incorrect results.
 */
#ifndef __STRICT_ANSI__
#define countof(arr) (sizeof(arr) / sizeof((arr)[0]) + __must_be_array(arr))
#else
#define countof(arr) (sizeof(arr) / sizeof((arr)[0]))
#endif

/* GCC byte swap intrinsics */
#if GCC_VERSION >= 40600
# define __HAVE_BUILTIN_BSWAP32__
# define __HAVE_BUILTIN_BSWAP64__
#endif
#if GCC_VERSION >= 40800
# define __HAVE_BUILTIN_BSWAP16__
#endif

#ifdef __HAVE_BUILTIN_BSWAP16__
# define ua_bswap16(x) __builtin_bswap16(x)
#else
# define ua_bswap16(x) ((((uint16_t)(x))>> 8) | (((uint16_t)(x)) << 8))
#endif
#ifdef __HAVE_BUILTIN_BSWAP16__
# define ua_bswap32(x) __builtin_bswap32(x)
#else
# define ua_bswap32(x) (((((uint32_t)(x)) & 0xff000000) >> 24) | \
                        ((((uint32_t)(x)) & 0x00ff0000) >>  8) | \
                        ((((uint32_t)(x)) & 0x0000ff00) <<  8) | \
                        ((((uint32_t)(x)) & 0x000000ff) << 24))
#endif
#ifdef __HAVE_BUILTIN_BSWAP32__
# define ua_bswap64(x) __builtin_bswap64(x)
#else
# define ua_bswap64(x) (((((uint64_t)(x)) & 0xff00000000000000ll) >> 56) | \
                        ((((uint64_t)(x)) & 0x00ff000000000000ll) >> 40) | \
                        ((((uint64_t)(x)) & 0x0000ff0000000000ll) >> 24) | \
                        ((((uint64_t)(x)) & 0x000000ff00000000ll) >>  8) | \
                        ((((uint64_t)(x)) & 0x00000000ff000000ll) <<  8) | \
                        ((((uint64_t)(x)) & 0x0000000000ff0000ll) << 24) | \
                        ((((uint64_t)(x)) & 0x000000000000ff00ll) << 40) | \
                        ((((uint64_t)(x)) & 0x00000000000000ffll) << 56))
#endif

/* UA byte swap macros - convert host byte order to little endian */
#if UA_BYTE_ORDER == UA_LITTLE_ENDIAN
# define ua_htole16(x) (x)
# define ua_htole32(x) (x)
# define ua_htole64(x) (x)
#else
# define ua_htole16(x) ua_bswap16(x)
# define ua_htole32(x) ua_bswap32(x)
# define ua_htole64(x) ua_bswap64(x)
#endif

#define ua_letoh16(x) ua_htole16(x)
#define ua_letoh32(x) ua_htole32(x)
#define ua_letoh64(x) ua_htole64(x)

#ifdef __GNUC__
/* fmt: index of the format string (starting from 1)
   va:  index of the first variable argument (starting from 1) */
# define UA_FORMAT_ARGUMENT(fmt, va) __attribute__ ((format (printf, fmt, va)))
#else /* __GNUC__ */
# define UA_FORMAT_ARGUMENT(fmt, va)
#endif /* __GNUC__ */

/* const function, has no side effects only reads parameters,
 * it does not modifiy anything.
 */
#define UA_CONST_FUNCTION __attribute__ ((const))
/* pure function, has no side effects only ready parameters and global state,
 * it does not modifiy anything.
 */
#define UA_PURE_FUNCTION __attribute__ ((pure))

/* mark non null parameters.
 * Example: my_memcpy (void *dest, const void *src, size_t len) UA_NONNULL(1, 2);
 */
#define UA_NONNULL(...) __attribute__((nonnull(__VA_ARGS__)))

/* Macro for marking functions as deprecated */
#define UA_DEPRECATED __attribute((__deprecated__))

/* Macro to force checking the return values (missing error handling) */
#define UA_WARN_UNUSED_RESULT __attribute((__warn_unused_result__))

/* Function marked with this macro might never return
 * Example: my_exit() UA_NORETURN;
 */
#define UA_NORETURN __attribute((noreturn))

/* This format specifier macro is just another hack for the stupid MS compiler.
 * MSVC does not support C99 and so it does not know %z. Using the macro we can
 * make the code working on both MS compilers and C99 compliant compilers.
 */
#define PRIuSIZE_T "zu" /* size_t (unsigned) */
#define PRIiSIZE_T "zi" /* ssuze_t (signed, but also not supported at all by MS) */

/* portable assertion macro */
#define UA_ASSERT(cond) assert(cond)

/* static assert is standard since C11 */
#if defined __USE_ISOC11 && !defined __cplusplus
# define UA_STATIC_ASSERT(cond, msg) _Static_assert(cond, msg)
#else
# define UA_STATIC_ASSERT(cond, msg)
#endif

int ua_p_platform_init(void);
int ua_p_platform_cleanup(void);
void ua_p_platform_panic(const char *msg, const char *file, int line) UA_NORETURN;

/* GCC provides MIN and MAX macros, but we define our own with UA prefix,
 * because MIN/MAX has portability issues with Euros OS.
 * Macro expansion can have side effects (e.g. MIN(a++, b++)), so we use
 * some GCC magic to avoid this. Nevertheless you should avoid such calls to
 * this macro to keep the code portable.
 */
#ifndef __STRICT_ANSI__
#define UA_MIN(a, b) ({ typeof (a) _a = (a); typeof (b) _b = (b); _a < _b ? _a : _b; })
#define UA_MAX(a, b) ({ typeof (a) _a = (a); typeof (b) _b = (b); _a > _b ? _a : _b; })
#else
#define UA_MIN(x, y) (((x) < (y)) ? (x) : (y))
#define UA_MAX(x, y) (((x) > (y)) ? (x) : (y))
#endif

/* mark dangerous functions as deprected so we get warnings when they are used */
extern char *gets(char *s) UA_DEPRECATED;
extern char *strcpy(char *dest, const char *src) UA_DEPRECATED;
extern char *strcat(char *dest, const char *src) UA_DEPRECATED;
extern int sprintf(char *str, const char *format, ...) UA_DEPRECATED;


/** platform internal trace macro.
 * This is necessary to be able to trace early in PL,
 * before and trace module is loaded.
 * The more sophisticated trace lib is only usable from higher levels.
 */
#define UA_P_TRACE_INFO(msg, ...) fprintf(stdout, msg, ##__VA_ARGS__)
#define UA_P_TRACE_ERROR(msg, ...) fprintf(stderr, msg, ##__VA_ARGS__)

#endif /* end of include guard: UA_LINUX_PLATFORM_H */

