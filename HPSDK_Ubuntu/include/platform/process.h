/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef UA_PLATFORM_FRONTEND_PPROCESS_H
#define UA_PLATFORM_FRONTEND_PPROCESS_H

#include <pprocess_types.h>
#include <stdbool.h>

/**
 * @defgroup platform_process process
 * @ingroup platform
 *
 * Functionality for starting and stopping processes.
 *
 * @code
 * ua_pid_t pid;
 * if (ua_process_support_forking()) {
 *     // install signal handler
 *     ua_process_install_sigchild_handler();
 *     // start a new process
 *     pid = ua_process_start("my_process");
 *     if (pid != UA_PROCESS_PID_INVALID) {
 *         // stop the new process
 *         ua_process_terminate(pid);
 *     }
 * }
 * @endcode
 *
 * Plaforms which don't support multiple process or forking
 * should return false @ref ua_process_support_forking and
 * simply return 0 in all other functions to make these operations a NOP.
 *
 * @{
 */

bool ua_process_support_forking(void);
void ua_process_install_sigchild_handler(void);
ua_pid_t ua_process_start(const char *filename, int argc, const char *argv[]);

int ua_process_terminate(ua_pid_t pid);
int ua_process_kill(ua_pid_t pid);
ua_pid_t ua_process_own_pid(void);

/** @} */

#endif /* end of include guard: UA_PLATFORM_FRONTEND_PPROCESS_H */

