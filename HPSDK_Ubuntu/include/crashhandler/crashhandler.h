/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef CRASHHANDLER_H_Y5MXY4FU
#define CRASHHANDLER_H_Y5MXY4FU

/* The crash handler is an optional component. If you don't have one for your
 * OS, or don't want it you can disable it and use the 'none' dummy implementation.
 * A crash handler is very OS specific, but it is not part of the platform layer,
 * because:
 * 1.) it's optional, and you don't need to port it.
 * 2.) it needs access to other components like the trace component,
 *     which is not possible for the platform layer. The platform layer is
 *     the base for everything and cannot access higher level components,
 *     without creating circular dependencies.
 * The crash handler can catch a segmentation fault and can do smart things
 * like e.g. flushing the trace buffers, creating backtraces, etc.
 */

int crashhandler_install(void);

#endif /* end of include guard: CRASHHANDLER_H_Y5MXY4FU */

