/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef _UA_TCPMSG_TYPES_H_
#define _UA_TCPMSG_TYPES_H_

#include <network/uanetwork.h>
#include <uaprotocol/uatcp/uatcp.h>
#include <uaprotocol/uatcp/config.h>
#include <uabase/buffer.h>
#include <memory/objectpool.h>
#include <util/pointerset.h>
#include <stdbool.h>

struct uatcpmsg_base;

/** Message and security level UA TCP protocol object. */
struct uatcpmsg_connection {
    struct ua_tcp_connection    tcp_conn;           /**< Underlying tcp connection handle. */
    struct seconv_channel      *channel;            /**< Secure channel bound to this connection. */
    struct uatcpmsg_base       *base;               /**< TCP message base object. */
    struct uatcpmsg_connection *listener;           /**< Listener which spawned this connection. */
    ua_tcp_message_callback    *send_cb;            /**< The default send callback. */
    ua_tcp_message_callback    *recv_cb;            /**< The default receive callback. */
    uintptr_t                   cbinfo;             /**< Callback info passed back to session. */
    int                         last_err;           /**< The last connection specific error. */
    uint64_t                    last_access;        /**< Tick count of last connection activity. */
    uint64_t                    msg_start;          /**< Tick count of the first complete chunk of a multipart message. */
    int                         ref_cnt;            /**< Reference counter. */
    unsigned int                buffer_handle;      /**< handle for buffer management. */
    bool                        unusable;           /**< True, if the connection may no longer used. */
};

/** Message context representing a request/response pair. */
struct uatcpmsg_ctxt {
    struct ua_tcp_message       send_msg;           /**< Send message context. */
    struct ua_buffer           *send_buf;           /**< Send buffer. */
    struct ua_tcp_message       recv_msg;           /**< Receive message context. */
    struct ua_buffer           *recv_buf;           /**< Receive buffer. */
    uint32_t                    req_id;             /**< The request id of this request/response pair. */
    uint32_t                    req_handle;         /**< The request handle of the request header. */
    unsigned char               msg_type;           /**< The message type (OPN, MSG). */
    struct uatcpmsg_connection *msg_conn;           /**< The tcp message object. (messaging) */
    int                         last_err;           /**< The last message specific error. */
    void*                       tramp;              /**< Additional data to bypass the call. */
#ifdef UATCP_ENABLE_DEBUG
    int                         cnt;                /**< message context number for debugging purpose */
#endif
};

/** Represents an OPC UA endpoint on the secure channel level. */
struct uatcpmsg_base {
    struct ua_net_base         *netbase;            /**< The related network base object. */
    struct sechan_base         *sechan_base;        /**< The related secure channel base object. */
    uint32_t                    timer;              /**< Interval timer for recurring checks. */
    struct util_pointerset      conn_list;          /**< List of active connections. */
    struct uatcpmsg_connection *listener_conn;      /**< The (only) connection listening for incoming connections. */
    struct uatcpmsg_connection *accept_conn;        /**< The (only) connection reserved for accepting connections. */
};

int uatcpmsg_base_init(struct uatcpmsg_base  *base,
                       struct ua_net_base    *netbase,
                       struct sechan_base    *sechan_base);

void uatcpmsg_base_clear(struct uatcpmsg_base *base);

int uatcpmsg_base_listen(struct uatcpmsg_base        *base,
                         struct ua_net_addr          *addr,
                         uintptr_t                    cbinfo,
                         struct uatcpmsg_connection **listener);

int uatcpmsg_conn_stop_listen(struct uatcpmsg_connection *listener);

#endif /* _UA_TCPMSG_TYPES_H_ */
