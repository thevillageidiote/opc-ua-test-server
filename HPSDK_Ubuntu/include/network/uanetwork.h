/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UANETWORK_H_
#define _UANETWORK_H_

/**
 * @defgroup network network
 * @{
 */

/** calling convention */
#define UA_NET_CCV
/** API definition */
#define UA_NET_API

#include <network/config.h>
#include <platform/platform.h>

UA_BEGIN_EXTERN_C

/*********************************************************************************************************/

/** Storage for an IP address. */
struct ua_ipaddress {
#ifdef UA_NET_SUPPORT_IPV6
    unsigned char bytes[16]; /**< IPv6 uses 16 byte addresses */
#else /* UA_NET_SUPPORT_IPV6 */
    unsigned char bytes[4];  /**< IPv4 uses 4 byte addresses */
#endif /* UA_NET_SUPPORT_IPV6 */
};

/** Identifiers for supported network address families. */
enum ua_net_af {
    ua_net_af_none = 0,
    ua_net_af_ipv4 = 1,
    ua_net_af_ipv6 = 2,
    ua_net_af_any  = 3
};

/** Structure for storing a network/IP address. The size depends on supported protocols. */
struct ua_net_addr {
    enum ua_net_af addr_family; /**< Type of the address. */
    size_t         addr_len;    /**< Number of used bytes in addr field. */
    unsigned short port;        /**< Port number in host byte order. */
    struct ua_ipaddress addr;   /**< Contains address bytes depending on family (network byte order). */
};

/** Socket types for ua_socket_init call. */
enum ua_socket_type {
    ua_socket_type_none = 0,
    ua_socket_type_stream, /**< TCP */
    ua_socket_type_dgram   /**< UDP */
};

/*********************************************************************************************************/

/** Result codes. */
#define UA_NET_E_GOOD                  0 /**< Operation completed successfully. */
#define UA_NET_E_ASYNC                 1 /**< Operation will complete asynchronously. */
#define UA_NET_E_PARTIAL               2 /**< Operation completed partially (completion pending)(support optional). */

#define UA_NET_E_BAD                   -1 /**< Operation or call failed (unspecific or unexpected reason). */
#define UA_NET_E_BADDISCONNECT         -2 /**< Connection is closed. */
#define UA_NET_E_BADSHUTDOWN           -3 /**< Peer shutdown connection in required direction. */
#define UA_NET_E_BADINVALIDARGUMENT    -4 /**< Function called with invalid arguments. */
#define UA_NET_E_BADNOMEM              -5 /**< Operation failed because of not enough memory. */
#define UA_NET_E_BADNOTIMPL            -6 /**< Function not implemented. */
#define UA_NET_E_BADHOSTUNKNOWN        -7 /**< Connect failed because host is not known or unreacheable. */
#define UA_NET_E_BADSYSCALL            -8 /**< Underlying system call failed. */
#define UA_NET_E_BADOPCANCELLED        -9 /**< Operation has been cancelled before completion. */
#define UA_NET_E_BADOPTIMEOUT          -10 /**< Operation timed out. */
#define UA_NET_E_BADIOPENDING          -11 /**< Operation cannot be started because a prior operation is still pending. */
#define UA_NET_E_BADINVALIDSTATE       -12 /**< An object is in an invalid state for the operation. */
#define UA_NET_E_BADWOULDBLOCK         -13 /**< Operation would cause the thread to block. */
#define UA_NET_E_BADNOTSUPPORTED       -14 /**< Operation is not supported. */
#define UA_NET_E_BADTOOSMALL           -15 /**< A given destination was too small. */
#define UA_NET_E_BADHOSTUNREACHABLE    -16 /**< Connect to given address failed. */
#define UA_NET_E_BADSOCKETCLOSED       -17 /**< Operation interrupted because socket has been closed. */
#define UA_NET_E_BADUNEXPECTED         -18 /**< Unexpected error. */

/**
 * @defgroup network_operationflags Operation Control Flags
 * Bitmask values used by ua_socket_op.
 * @{
 */
/** No special behavior requested (strictly asynchronous behavior). */
#define UA_NET_F_NONE                  0x00000000
/** Allow synchronous completion of the operation. */
#define UA_NET_F_ALLOWSYNC             0x00000001
/** Marks operation as complete. */
#define UA_NET_F_COMPLETE              0x00000002
/** Request partial updates for pending operation. (optional) */
#define UA_NET_F_PARTIAL               0x00000004
/* @} defgroup */

/*********************************************************************************************************/

/* forward declaration */
struct ua_socket;

/** Internal socket state. */
enum ua_net_state
{
    UA_NET_STATE_NONE          = 0,
    UA_NET_STATE_LISTEN        = 1,
    UA_NET_STATE_CONNECTING    = 2,
    UA_NET_STATE_ACCEPTING     = 3,
    UA_NET_STATE_SHUTDOWN_SEND = 4,
    UA_NET_STATE_SHUTDOWN_READ = 5,
    UA_NET_STATE_ESTABLISHED   = 6,
    UA_NET_STATE_CLOSING       = 7,
    UA_NET_STATE_ABORTING      = 8,
    UA_NET_STATE_UDP
};

/* forward declaration */
struct ua_socket_op;

/** Placeholder event type. */
#define UA_NET_EVENT_NULL               0x00000000

/** These events should be handled in ua_socket_cb. */

/** The connection was closed locally. */
#define UA_NET_EVENT_CLOSE_COMPLETE     0x00000001

#ifndef UA_NET_NO_TIMEOUT
/** No communication during the configured timespan. */
#define UA_NET_EVENT_TIMEOUT            0x00000002
#endif

/**
 * Signature of a socket callback.
 * @param pSocket The socket object on which the event occured.
 * @param iEventType UA_NET_EVENT_*
 * @param iResult UA_NET_E_* result assigned to the event.
 * @param pvCallbackData The user data specified at socket initialization.
 */
typedef int (ua_socket_cb)(struct ua_socket   *pSocket,
                           unsigned int        iEventType,
                           int                 iResult,
                           void               *pvCallbackData);

/**
 * Signature of an operation callback.
 * @param pSocket The socket object to which the operation was assigned to.
 * @param pOperation The structure defining the completed operation.
 * @param iResult UA_NET_E_* describing wether the operation succeeded or failed.
 */
typedef int (ua_socket_op_cb)(struct ua_socket       *pSocket,
                              struct ua_socket_op    *pOperation,
                              int                     iResult);

/*********************************************************************************************************/

/* definition of platform dependent types */
#include <network/uanetwork_types.h>

/*********************************************************************************************************/

UA_NET_API
int ua_ipaddress_set(struct ua_ipaddress *ip, const unsigned char *addr, unsigned char len);

UA_NET_API
int UA_NET_CCV ua_net_formataddr(struct ua_net_addr *pAddr,
                                 size_t              iBufLen,
                                 char               *pBuf);

UA_NET_API
int UA_NET_CCV ua_net_getaddr(const char         *sIP,
                              struct ua_net_addr *pAddr);

UA_NET_API
int UA_NET_CCV ua_net_makeanyaddr(struct ua_net_addr *pAddr);

/**
 * Initialize network library. Must be called first.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_net_init(void);

/**
 * Cleanup network library. Must be the last call to this API.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_net_clear(void);

/**
 * Create an usable struct ua_socket within the given memory and assigns to base object.
 * Creates new socket handle based on address family and type.
 * @param pNetworkBase The network base object
 * @param pSocket The socket object.
 * @param AddressFamily The address family of the socket to be created.
 * @param SocketType Type of the socket to be created.
 * @param pfCallback Function which will be called if non-operation events occur on the socket.
 * @param pvCallbackData User data passed back to the callback function.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_socket_init(
    struct ua_net_base *pNetworkBase,
    struct ua_socket   *pSocket,
    enum ua_net_af      AddressFamily,
    enum ua_socket_type SocketType,
    ua_socket_cb       *pfCallback,
    void               *pvCallbackData);

/**
 * Create an usable struct ua_socket within the given memory and assigns to base object.
 * Uses given system socket handle instead of creating a new one. Socket is set to non-blocking.
 * @param pNetworkBase The network base object
 * @param pSocket The socket object.
 * @param hSocket A platform dependent socket or file descriptor which will be assigned to the socket.
 * @param pfCallback Function which will be called if non-operation events occur on the socket.
 * @param pvCallbackData User data passed back to the callback function.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_socket_initset(
    struct ua_net_base *pNetworkBase,
    struct ua_socket   *pSocket,
    UA_NET_P_SOCKET     hSocket,
    ua_socket_cb       *pfCallback,
    void               *pvCallbackData);

/**
 * Set SO_REUSEADDR as socket option for the given socket.
 * @param pSocket The socket object.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_socket_set_reuse_address(
    struct ua_socket *pSocket);

/**
 * Get information about the peer address through a connected socket.
 * @param pSocket The socket object.
 * @param pAddress The address information of the peer.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_socket_get_peer_addr(
    struct ua_socket   *pSocket,
    struct ua_net_addr *pAddress);

#ifndef UA_NET_NO_TIMEOUT
/**
 * Set timeout value of a socket (<= 0 to deactivate).
 * @param pSocket
 * @param iTimeOutMS
 */
UA_NET_API void UA_NET_CCV ua_socket_set_timeout(
    struct ua_socket   *pSocket,
    int                 iTimeOutMS);
#endif /* UA_NET_NO_TIMEOUT */

/**
 * Retrieve last socket error.
 * @param pNetworkBase The network base object.
 * @return UA_NET_E_* error code
 */
UA_NET_API unsigned int UA_NET_CCV ua_net_get_last_error(
    struct ua_net_base *pNetworkBase);

/**
 * Bind socket to given address and port.
 * @param pSocket The socket object.
 * @param pAddress The address information for binding the socket.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_socket_bind(
    struct ua_socket   *pSocket,
    struct ua_net_addr *pAddress);

/**
 * Put socket into listen mode.
 * @param pSocket The socket object.
 * @param pAddress The address information for binding the socket.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_socket_listen(
    struct ua_socket   *pSocket);

/**
 * Start asynchronous accept and notify completion through operation object.
 * @param pListenSocket Socket which has been put into listening mode.
 * @param pAcceptSocket Socket which will represent the accepted connection. Handle must be empty (use ua_socket_initset with UA_NET_P_SOCKET_INVALID).
 * @param pAcceptOperation Operation object which will be notified when a client is accepted.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_socket_accept(
    struct ua_socket       *pListenSocket,
    struct ua_socket       *pAcceptSocket,
    struct ua_socket_op    *pAcceptOperation);

/**
 * Start asynchronous connect and notify completion through operation object.
 * @param pSocket The socket object.
 * @param pServerAddress Address information of the host.
 * @param uPort Port number at the host.
 * @param pLocalAddress Bind the client socket to this local address.
 * @param pConnectOperation Initialized async operation structure.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_socket_connect(
    struct ua_socket       *pSocket,
    struct ua_net_addr     *pServerAddress,
    struct ua_net_addr     *pLocalAddress,
    struct ua_socket_op    *pConnectOperation);

/**
 * Start asynchronous write operation and notify completion through operation object.
 * @param pSocket The socket object.
 * @param pSendOperation Initialized operation structure.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_socket_write(
    struct ua_socket       *pSocket,
    struct ua_socket_op    *pSendOperation);

/**
 * Start asynchronous read operation and notify completion through operation object.
 * @param pSocket The socket object.
 * @param pReadOperation Initialized operation structure.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_socket_read(
    struct ua_socket       *pSocket,
    struct ua_socket_op    *pReadOperation);

/** Starts an asynchronous sendto operation and notify completion through operation object.
 * @param pSocket The socket object.
 * @param pOperation Initialized operation structure.
 * @param pDestAddr Destination address for this UDP packet.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_socket_sendto(
    struct ua_socket         *pSocket,
    struct ua_socket_op      *pOperation,
    const struct ua_net_addr *pDestAddr);

/** Starts an asynchronous recvfrom operation and notify completion through operation object.
 * Note that the operation object contains the sender address once the operation is completed.
 * @param pSocket The socket object.
 * @param pOperation Initialized operation structure.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_socket_recvfrom(
    struct ua_socket    *pSocket,
    struct ua_socket_op *pOperation);

/**
 * Shutdown connection in send direction.
 * @param pSocket The socket object.
 */
UA_NET_API void UA_NET_CCV ua_socket_shutdown(
    struct ua_socket   *pSocket);

/**
 * Close socket, remove from base and clear memory.
 * @param pSocket The socket object.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_socket_close(
    struct ua_socket   *pSocket);

/**
 * Calculate the size of the data to be passed to @ref ua_net_base_init.
 * @param num_sockets Number of sockets for the base to initialize.
 * @return Number of bytes to allocate as data.
 */
UA_NET_API unsigned int UA_NET_CCV ua_net_base_size(
    unsigned int num_sockets);

/**
 * Initialize base object.
 * @param pNetworkBase The network base object to be initialized.
 * @param data Preallocated data of size calculated by @ref ua_net_base_size.
 * @param num_sockets Number of sockets for the network base, must be
 * the same as when calculating the data size.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_net_base_init(
    struct ua_net_base *pNetworkBase,
    void *data,
    unsigned int num_sockets);

/**
 * Clear base object.
 * @param pNetworkBase The network base object to be cleared.
 * @return UA_NET_E_* error code
 */
UA_NET_API void * UA_NET_CCV ua_net_base_clear(
    struct ua_net_base *pNetworkBase);

/**
 * Process events of sockets organized by a network base object.
 * @param pNetworkBase The network base object.
 * @param iTimeOutMSec Maximum wait time.
 * @return UA_NET_E_* error code
 */
UA_NET_API int UA_NET_CCV ua_net_do_events(
    struct ua_net_base *pNetworkBase,
    int                 iTimeOutMSec);

UA_END_EXTERN_C

/** @} */

#endif /*_UANETWORK_H_ */
