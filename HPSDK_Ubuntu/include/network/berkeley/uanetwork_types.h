/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


/** @file */

#include <pberkeleysocket_types.h>
#include <network/config.h>

#ifdef UA_USE_SYNCHRONIZATION
/* placeholder for thread synchronization */
#define UA_NET_LOCK void*
#endif /* UA_USE_SYNCHRONIZATION */

#ifdef UA_NET_SUPPORT_IP_FILTER
# include <network/ipfilter.h>
#endif

/**
 * @addtogroup network
 * @{
 */

/**
 * @defgroup network_operationinit Operation Initialization
 * @{
 */

/** Helper function for initializing a socket operation structure. */
#define ua_socket_op_init(xOperation, xBuffer, xCallback, xCallbackData, xFlags) \
    (xOperation)->pfCallback          = xCallback; \
    (xOperation)->pvCallbackData      = (void*)xCallbackData; \
    (xOperation)->buffer              = xBuffer; \
    (xOperation)->iFlags              = xFlags; \
    (xOperation)->x_pSocket           = 0; \
    (xOperation)->x_iOperationType    = 0; \
    (xOperation)->x_pNext             = 0;
/* @} defgroup */

/**
 * Structure representing an asynchronous IO operation. Initialize with 0!
 * The flags allow to control the completion of the operation. If set to
 * UA_NET_F_NONE the operation will finish strictly asynchronously by
 * invoking the callback from within ua_net_do_events(). UA_NET_F_ALLOWSYNC
 * Will allow the operation to finish synchronously. In this case the callback
 * won't be called and the function returns UA_NET_E_GOOD instead of UA_NET_E_ASYNC.
 * If no callback is set the flag UA_NET_F_COMPLETE is set after completion.
 * In this case the owner of the structure must check the field after ua_net_do_events().
 */
struct ua_socket_op
{
    /* API defined data - set before starting operation. */
    struct ua_buffer       *buffer;           /**< Buffer for transmitting data - set before start. */
    unsigned int            iFlags;           /**< Operation control flags. */
    ua_socket_op_cb        *pfCallback;       /**< Event callback pointer. */
    void                   *pvCallbackData;   /**< Data passed to pfCallback. */
    struct ua_net_addr      addr;             /**< Peer address for recvfrom operation */

    /* Implementation specific data - do not modify */
    struct ua_socket       *x_pSocket;        /**< INTERNAL Socket on which the transaction is taking place! */
    int                     x_iOperationType; /**< INTERNAL Operation Type */
    struct ua_socket_op    *x_pNext;          /**< INTERNAL Pointer to next operation in sequence. */
};

/** Internal structure of an async socket. */
struct ua_socket
{
    UA_NET_P_SOCKET         hSocket;           /**< Native socket handle. */
    struct ua_socket_op    *pCurrentSend;      /**< Buffer for sending data. */
    struct ua_socket_op    *pCurrentRead;      /**< Buffer for receiving data. */
    int                     EventBits;         /**< Types of events the socket shoud react to. */
    enum ua_net_state       eState;            /**< Current socket state. */
    unsigned int            uSetTimeOut;       /**< Requested maximum time between two IO operations. */
    unsigned int            uTimeOutCounterMS; /**< Time remaining before the socket times out. */
    ua_socket_cb           *pfCallback;        /**< Event callback pointer. */
    void                   *pvCallbackData;    /**< Data passed to pfCallback. */
    struct ua_net_base     *pNetworkBase;      /**< Parent object */
    int                     iPending;          /**< Pending operation counter. */
    enum ua_socket_type     type;              /**< Socket type: STREAM, DGRAM */
#ifdef UA_NET_SUPPORT_IP_FILTER
    struct ua_ipaddress     peeraddr;          /**< Peer address used for IP filter. */
#endif /* UA_NET_SUPPORT_IP_FILTER */
};

/** Network base object. */
struct ua_net_base
{
    struct ua_socket      **aSockets;            /**< The managed sockets - list used to fill fd_sets */
    int                     num_sockets;         /**< Number of elements in aSockets */
    struct ua_socket       *pAcceptSocket;       /**< Socket with pending accept operation. */
#ifdef UA_NET_SUPPORT_IP_FILTER
    struct ua_ipfilter      ipfilter;            /**< Ipfilter context for this base. */
#endif
};

/** @} */
