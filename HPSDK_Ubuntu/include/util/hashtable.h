/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef HASHTABLE_H_PMHI0URT
#define HASHTABLE_H_PMHI0URT

#include <stdint.h>
#include <util/hashfunctions.h>
#include "hashtable_types.h"

/**
 * @defgroup util_hashtable_group hashtable
 * @ingroup util
 *
 * @brief A table for hashable objects.
 * The objects must have a fixed index, this can be achieved by
 * storing the objects in an array or @ref memory_objectpool.
 *
 * The objects can be of arbitrary type, but must contain the util_hashnode as first member.
 * Think of \<yourtype\> is derived from @ref util_hashnode.
 * @code
 * struct mystruct {
 *     struct util_hashnode header;
 *     int foo;
 *     float bar;
 * }
 * @endcode
 *
 * @{
 */

/** Helper macro to compute the hashtable size.
 * @param num number of objects
 */
#define COMPUTE_HASHTABLE_SIZE(num) (sizeof(struct util_hashtable) + sizeof(unsigned int)*(num))

/** Identifier for invalid util_hashtable_iterator. */
#define UTIL_HASHTABLE_ITERATOR_INVALID -1

/**
 * Foreach loop for iterating through a hashtable.
 * @param it @ref util_hashtable_iterator to access current element.
 * @param ht Hashtable to iterate through.
 */
#define util_hashtable_foreach(it, ht) for (it = util_hashtable_iterator_first(ht); it != -1; it = util_hashtable_iterator_next(ht, it))

/**
 * Structure to include as first field in your custom struct.
 * @see util_hashtable_group
 */
struct util_hashnode {
    int next; /**< Linked list for collision handling. */
};

/** Function which returns the hashnode for a given value. */
typedef struct util_hashnode *(*Tget_hashnode)(unsigned int valueindex, void *userdata);
/** Iterator for @ref util_hashtable_group. */
typedef int64_t util_hashtable_iterator;

/**
 * Structure for the hashtable, see also @ref util_hashtable_group.
 * @see util_hashtable_group
 */
struct util_hashtable {
    unsigned int tablesize; /**< table size in no of entries */
    unsigned int used;      /**< number of used entries */
    Tget_hashnode get_hashnode;  /**< function which returns the hashnode for a given value */
    util_hashtable_get_key get_key;       /**< function which returns the key for a given value */
    util_hashtable_key_compar key_compar; /**< function for comparing two keys */
    util_hashtable_hash hash;             /**< function to compute a key's hash */
    void *userdata;         /**< userdata pointer passed to the callback functions */
    unsigned int table[];   /**< table of indices */
};

size_t util_hashtable_size(unsigned int num_elements);
struct util_hashtable *util_hashtable_init(
        Tget_hashnode get_hashnode,
        util_hashtable_get_key get_key,
        util_hashtable_key_compar key_compar,
        util_hashtable_hash hash,
        void *data, size_t size, void *userdata);
int util_hashtable_add(struct util_hashtable *ht, const void *key, unsigned int valueindex);
int util_hashtable_remove(struct util_hashtable *ht, const void *key);
unsigned int util_hashtable_lookup(struct util_hashtable *ht, const void *key);

util_hashtable_iterator util_hashtable_iterator_first(struct util_hashtable *ht);
util_hashtable_iterator util_hashtable_iterator_next(struct util_hashtable *ht, util_hashtable_iterator it);
unsigned int util_hashtable_iterator_value(struct util_hashtable *ht, util_hashtable_iterator it);
unsigned int util_hashtable_iterator_remove(struct util_hashtable *ht, util_hashtable_iterator it);

/** @} */

#endif /* end of include guard: HASHTABLE_H_PMHI0URT */

