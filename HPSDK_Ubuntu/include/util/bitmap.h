/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef __UTIL_BITMAP_H__
#define __UTIL_BITMAP_H__

#include <stdint.h>
#include <platform/platform.h> /* necessary for inline keyword on Windows */
#include <platform/memory.h>

/**
 * @defgroup util_bitmap bitmap
 * @ingroup util
 * @brief Bitmap datastructure
 *
 * Bitmap to store multipe binary states efficiently
 * in a small amount of memory.
 *
 * Example:
 * @code
 * int num_bits = 100;
 * int idx, size;
 * util_bitmap_t *bitmap;
 *
 * // create bitmap
 * size = util_bitmap_size(num_bits);
 * bitmap = ua_malloc(size);
 * if (bitmap == NULL) goto error;
 * util_bitmap_init(bitmap, size);
 *
 * // mark 5th bit (index 4) as used
 * util_bitmap_mark_as_used(bitmap, 4);
 *
 * // find first free bit
 * idx = util_bitmap_find_free(bitmap, num_bits);
 * @endcode
 * @{
 */

/** @brief The bits are stored in an uintptr_t array */
typedef uintptr_t util_bitmap_t;

/** @brief Number of bits in one uintptr_t */
#define BITMAP_BITSIZE ((int)sizeof(uintptr_t)*8)

int util_bitmap_find_free(util_bitmap_t *bitmap, int num_bits);

/**
 * @brief Computes the size in bytes for \c num_elements bitmap entries.
 *
 * This can be used by the caller to allocate the correct amount of memory
 * for util_bitmap_init.
 * @param num_bits Number of bits to store in the bitmap.
 * @return Size in bytes to be allocated for the bitmap.
 */
static inline int util_bitmap_size(int num_bits)
{
    int size = (num_bits - 1) / BITMAP_BITSIZE + 1; /* compute num of uintptr_t */
    size *= sizeof(util_bitmap_t); /* compute size in bytes */
    return size;
}

/**
 * @brief Initilizes the given bitmap, marks all bits as free/unused.
 *
 * @param bitmap Pointer to bitmap memory.
 * @param size Size in bytes of \c bitmap.
 */
static inline void util_bitmap_init(util_bitmap_t *bitmap, int size)
{
    ua_memset(bitmap, 0, size);
}

/**
 * @brief Clears the given bitmap.
 *
 * Marks all entries as free, does not free the bitmap memory.
 * @param bitmap Pointer to bitmap memory.
 * @param size Size in bytes of \c bitmap.
 */
static inline void util_bitmap_clear(util_bitmap_t *bitmap, int size)
{
    ua_memset(bitmap, 0, size);
}

/**
 * @brief Checks if the given \c bitidx is marked as free.
 *
 * @return 1 if free, 0 if not free.
 */
static inline int util_bitmap_is_free(util_bitmap_t *bitmap, int bitidx)
{
    int i = bitidx / BITMAP_BITSIZE;
    int m = bitidx % BITMAP_BITSIZE;
    util_bitmap_t bit = 1;
    bit <<= m;
    return ((bitmap[i] & bit) == 0);
}

/**
 * @brief Marks a bit in the bitmap as free.
 *
 * @param bitmap Pointer to bitmap memory.
 * @param bitidx Index in bits to mark as free.
 */
static inline void util_bitmap_mark_as_free(util_bitmap_t *bitmap, int bitidx)
{
    int i = bitidx / BITMAP_BITSIZE;
    int m = bitidx % BITMAP_BITSIZE;
    util_bitmap_t bit = 1;
    bit <<= m;
    bitmap[i] &= ~bit;
}

/**
 * @brief Marks a bit in the bitmap as used.
 *
 * @param bitmap Pointer to bitmap memory.
 * @param bitidx Index in bits to mark as used.
 */
static inline void util_bitmap_mark_as_used(util_bitmap_t *bitmap, int bitidx)
{
    int i = bitidx / BITMAP_BITSIZE;
    int m = bitidx % BITMAP_BITSIZE;
    util_bitmap_t bit = 1;
    bit <<= m;
    bitmap[i] |= bit;
}

/** @} */

#endif /* __UTIL_BITMAP_H__ */
