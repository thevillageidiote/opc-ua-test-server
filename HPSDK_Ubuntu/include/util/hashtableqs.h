/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef HASHTABLEQS_H_BUSOYF19
#define HASHTABLEQS_H_BUSOYF19

#include <stdint.h>
#include <platform/platform.h>
#include <util/hashfunctions.h>
#include "hashtable_types.h"

/**
 * @defgroup util_hashtableqs_group hashtableqs
 * @ingroup util
 *
 * A hashtable consisting of a table of indices to objects.
 * This table uses indices instead of pointers to make the hashtable
 * relocatable. A table of objects must be stored outside if this datastructure.
 * This hashtable only stores the indices to this table.
 * It uses quadratic probing for resolving hash collisions.
 * Note, that this means you can only add elements to this table, but cannot
 * remove them anymore. This is mainly useful for static address spaces.
 * Use util_hashtable instead if you need to be able to remove values again.
 *
 * @{
 */

/** Helper macro to compute the hashtable size.
 * @param num number of objects
 */
#define COMPUTE_HASHTABLEQS_SIZE(num) (sizeof(struct util_hashtableqs) + sizeof(unsigned int)*(num))

/** Identifier for invalid util_hashtableqs_iterator. */
#define UTIL_HASHTABLEQS_ITERATOR_INVALID -1

/**
 * Foreach loop for iterating through a hashtableqs.
 * @param it @ref util_hashtableqs_iterator to access current element.
 * @param ht Hashtableqs to iterate through.
 */
#define util_hashtableqs_foreach(it, ht) for (it = util_hashtableqs_iterator_first(ht); it != -1; it = util_hashtableqs_iterator_next(ht, it))

/** Iterator for @ref util_hashtableqs_group. */
typedef int64_t util_hashtableqs_iterator;

/**
 * Structure for hastableqs, see also @ref util_hashtableqs_group.
 * @see util_hashtableqs_group
 */
struct util_hashtableqs {
    unsigned int tablesize; /**< table size in no of entries */
    unsigned int used;      /**< number of used entries */
    util_hashtable_get_key get_key;       /**< function which returns key for a given value */
    util_hashtable_key_compar key_compar; /**< function for comparing two keys */
    util_hashtable_hash hash;             /**< function to compute a key's hash */
    void *userdata;         /**< userdata pointer passed to the callback functions */
    unsigned int table[];   /**< table of indeces */
};

size_t util_hashtableqs_size(unsigned int num_elements);
struct util_hashtableqs *util_hashtableqs_init(
        util_hashtable_get_key get_key,
        util_hashtable_key_compar key_compar,
        util_hashtable_hash hash,
        void *data, size_t size, void *userdata);
int util_hashtableqs_add(struct util_hashtableqs *ht, const void *key, unsigned int value);
unsigned int util_hashtableqs_lookup(struct util_hashtableqs *ht, const void *key);

/**
 * @todo what does this function do?
 */
static inline unsigned int util_hashtableqs_get(struct util_hashtableqs *ht, int idx)
{
    return ht->table[idx];
}

util_hashtableqs_iterator util_hashtableqs_iterator_first(struct util_hashtableqs *ht);
util_hashtableqs_iterator util_hashtableqs_iterator_next(struct util_hashtableqs *ht, util_hashtableqs_iterator it);
unsigned int util_hashtableqs_iterator_value(struct util_hashtableqs *ht, util_hashtableqs_iterator it);

/** @} */

#endif /* end of include guard: HASHTABLEQS_H_BUSOYF19 */

