/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/




#ifndef __UTCTIME_H__
#define __UTCTIME_H__

/**
 * @defgroup util_utctime_group utctime
 * @ingroup util
 * Broken-down time format for UTC time.
 * This type is defined anlogous to the standard ANSI C struct tm.
 * There is no daylight saving information because this structure is only for UTC time.
 * @{
 */

#include <time.h>

/**
 * Structure for utctime, see also @ref util_utctime_group.
 * @see util_utctime_group
 */
struct util_utctime {
    unsigned int utc_year;  /**< The number of years since 1900. */
    unsigned int utc_mon;   /**< The number of months since January, in the range 0 to 11. */
    unsigned int utc_mday;  /**< The day of the month, in the range 1 to 31. */
    unsigned int utc_hour;  /**< The number of hours past midnight, in the range 0 to 23. */
    unsigned int utc_min;   /**< The number of minutes after the hour, in the range 0 to 59. */
    unsigned int utc_sec;   /**< The number of seconds after the minute, normally in the range 0 to 59, but can be up to 60 to allow for leap seconds. */
    unsigned int utc_yday;  /**< The number of days since January 1, in the range 0 to 365. */
    unsigned int utc_wday;  /**< The number of days since Sunday, in the range 0 to 6. */
};

#ifdef __cplusplus
extern "C" {
#endif

time_t util_utctime_to_time_t(const struct util_utctime *utc);
struct util_utctime *util_utctime_from_time_t(struct util_utctime *dst, const time_t *src);

#ifdef __cplusplus
}
#endif

/** @} */

#endif /* __UTCTIME_H__ */

