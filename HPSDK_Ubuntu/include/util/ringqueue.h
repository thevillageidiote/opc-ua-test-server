/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef _UTIL_RINGQUEUE_H_
#define _UTIL_RINGQUEUE_H_

#include <stdbool.h>

/**
 * @defgroup util_ringqueue_group ringqueue
 * @ingroup util
 *
 * A queue that is implemented as a ring buffer.
 * Allows to store arbitrary number of elements,
 * however internal buffer must be provided by user.
 * It is possible to add and remove elements at front
 * and back of the queue.
 *
 * @{
 */

/** Foreach loop for iterating through a ringqueue */
#define util_ringqueue_foreach(i, queue) for (i = util_ringqueue_index_first(queue); i != -1; i = util_ringqueue_index_next(queue, i))

/**
 * Structure for the ringqueue, see also @ref util_ringqueue_group.
 * @see util_ringqueue_group
 */
struct util_ringqueue {
    int start;
    int end;
    int num_elem;
    int count;
    void **data;
};

int util_ringqueue_size(int num_elements);
void util_ringqueue_init(struct util_ringqueue *queue, void *data, int size);
void *util_ringqueue_clear(struct util_ringqueue *queue);
unsigned int util_ringqueue_count(struct util_ringqueue *queue);
bool util_ringqueue_full(struct util_ringqueue *queue);
unsigned int util_ringqueue_max(struct util_ringqueue *queue);

int util_ringqueue_put_front(struct util_ringqueue *queue, void *data);
int util_ringqueue_put_end(struct util_ringqueue *queue, void *data);
void *util_ringqueue_pop_front(struct util_ringqueue *queue);
void *util_ringqueue_pop_end(struct util_ringqueue *queue);

int util_ringqueue_index_first(struct util_ringqueue *queue);
int util_ringqueue_index_next(struct util_ringqueue *queue, int idx);

void *util_ringqueue_get_index(struct util_ringqueue *queue, int idx);
void *util_ringqueue_get_front(struct util_ringqueue *queue);
void *util_ringqueue_get_end(struct util_ringqueue *queue);

/** @} */

#endif /* _UTIL_RINGQUEUE_H_ */
