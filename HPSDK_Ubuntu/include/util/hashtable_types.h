/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef UTIL_HASHTABLE_TYPES_H
#define UTIL_HASHTABLE_TYPES_H

#include <limits.h>
#include <stddef.h>

#define UTIL_HT_KEY_NOT_FOUND UINT_MAX

typedef const void *(*util_hashtable_get_key)(unsigned int valueindex, void *userdata); /**< function which returns the key for a given value */
typedef int (*util_hashtable_key_compar)(const void *key1, const void *key2, void *userdata); /**< function for comparing two keys */
typedef unsigned int (*util_hashtable_hash)(const void *key, void *userdata); /**< function to compute a key's hash */

#endif /* UTIL_HASHTABLE_TYPES_H */
