/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef __MEMPOOL_H__
#define __MEMPOOL_H__

/**
 * @defgroup mempool mempool
 * @ingroup memory
 * Dynamic memory allocator inspired by Doug Lea.
 * The mempool is initialized with an amount of memory,
 * then @ref mempool_alloc can be used to allocate chunks
 * of dynamic size from this memory.
 * @{
 */

#include <platform/platform.h>
#include <stddef.h> /* defines size_t */
#include <stdbool.h>
#include <stdint.h>
#include <memory/memory_config.h>
#ifdef MEMORY_THREADSAFE
# include <platform/mutex.h>
#endif

/* detect 64bit mode */
#if UINTPTR_MAX == 0xffffffffffffffff
# define MP_64BIT
# define BOUND_BEFORE 0xbbbbbbbbbbbbbbbb
# define BOUND_AFTER  0xaaaaaaaaaaaaaaaa
#elif UINTPTR_MAX == 0xffffffff
# define MP_32BIT
# define BOUND_BEFORE 0xbbbbbbbb
# define BOUND_AFTER  0xaaaaaaaa
#else
# error("cannot detect 64bit mode")
#endif

/**************************************/
/* Begin Configuration Section        */
#ifdef MEMORY_ENABLE_DEBUG
# define MP_DEBUG_MEMORY /* enable debug memory bounds */
#endif
/* #define MEMORY_ENABLE_STAT /1* track memory statistics *1/ */
#define MP_USE_SYSTEM_MEMSET /* use system memset from string.h */
#define MP_USE_SYSTEM_MEMCPY /* use system memcpy from string.h */
#if defined(MP_USE_SYSTEM_MEMSET) || defined(MP_USE_SYSTEM_MEMCPY)
# include <string.h>
#endif
/* memset switch */
#ifdef MP_USE_SYSTEM_MEMSET
# define MP_memset(b, c, len) ua_memset(b, c, len)
#else
# define MP_USE_INTERNAL_MEMSET
# define MP_memset(b, c, len) MP_memset_internal(b, c, len)
#endif /* MP_USE_SYSTEM_MEMSET */
/* memcpy switch */
#ifdef MP_USE_SYSTEM_MEMCPY
# define MP_memcpy(dest, src, n) ua_memcpy(dest, src, n)
#else
# define MP_USE_INTERNAL_MEMCPY
# define MP_memcpy(dest, src, n) MP_memcpy_internal(dest, src, n)
#endif /* MP_USE_SYSTEM_MEMCPY */
/* macro to avoid unused variable warnings */
#define MP_UNUSED(x) (void)x
/* End Configuration Section          */
/**************************************/

#ifdef MP_DEBUG_MEMORY
/* this enables some helpfully memsets that are only needed for debugging */
# define ENABLE_DEBUG_MEMSET
#endif

#define SIZE_T_ZERO         ((size_t)0)
#define SIZE_T_ONE          ((size_t)1)
#define SIZE_T_TWO          ((size_t)2)

/* note: MIN_CHUNK_SIZE must be multiple of 8. */
#ifdef MP_64BIT
# ifdef MEMORY_ENABLE_TRACE
#  define MIN_CHUNK_SIZE 40
# else /* MEMORY_ENABLE_TRACE */
#  define MIN_CHUNK_SIZE 32
# endif /* MEMORY_ENABLE_TRACE */
#else /* MP_64BIT */
# ifdef MEMORY_ENABLE_TRACE
#  define MIN_CHUNK_SIZE 24
# else /* MEMORY_ENABLE_TRACE */
#  define MIN_CHUNK_SIZE 16
# endif /* MEMORY_ENABLE_TRACE */
#endif /* MP_64BIT */

/* CHUNK_STEPS must be a multiple of MIN_CHUNK_SIZE */
#define CHUNK_STEPS      MIN_CHUNK_SIZE
#define NUM_BINS         32
#define LAST_BIN         31
#define CHUNK_SIZE_CLEAR_MASK ((size_t)7)
#define CHUNK_SIZE_MASK ((size_t)~7)
#define USED_BIT        SIZE_T_ONE

/* helper macro which cleans out unused size bits */
#define CLEAN_SIZE(x) ((x) & CHUNK_SIZE_MASK)

/* converts a bin index into a bin size */
#define INDEX2SIZE(x) (MIN_CHUNK_SIZE+(x)*CHUNK_STEPS)
/* converts a bin size into a bin index */
#define SIZE2INDEX(x) ((((x)-MIN_CHUNK_SIZE)/CHUNK_STEPS) > LAST_BIN ? LAST_BIN : (((x)-MIN_CHUNK_SIZE)/CHUNK_STEPS))

#ifdef MP_DEBUG_MEMORY
# define STATUS 0 /* HEADER */
# define SIZE   0 /* HEADER */
# define BOUND2 2 /* BOUND for checking overwrite in free() */
# ifdef MEMORY_ENABLE_TRACE
#  define LOG_INDEX 1 /* log index into memory log */
#  define BOUND1    2 /* BOUND for checking overwrite in free() */
#  define PREV      3 /* USED for data when STATUS != FREE */
#  define NEXT      4 /* USED for data when STATUS != FREE */
#  define NODE_HEADER   sizeof(size_t) * 3 /* size+log_index+bound1 in header */
#  define NODE_OVERHEAD sizeof(size_t) * 5 /* NODE_HEADER+BOUND2+size at the end */
# else
#  define BOUND1    1 /* BOUND for checking overwrite in free() */
#  define PREV      2 /* USED for data when STATUS != FREE */
#  define NEXT      3 /* USED for data when STATUS != FREE */
#  define NODE_HEADER   sizeof(size_t) * 2 /* size+bound1 in header */
#  define NODE_OVERHEAD sizeof(size_t) * 4 /* NODE_HEADER+BOUND2+size at the end */
# endif
#else
# define STATUS 0 /* HEADER */
# define SIZE   0 /* HEADER */
# ifdef MEMORY_ENABLE_TRACE
#  define LOG_INDEX 1 /* log index into memory log */
#  define PREV      2 /* USED for data when STATUS != FREE */
#  define NEXT      3 /* USED for data when STATUS != FREE */
#  define NODE_HEADER   sizeof(size_t) * 2 /* size and log_index field in header */
#  define NODE_OVERHEAD sizeof(size_t) * 3 /* NODEHEADER+size at the end */
# else
#  define PREV      1 /* USED for data when STATUS != FREE */
#  define NEXT      2 /* USED for data when STATUS != FREE */
#  define NODE_HEADER   sizeof(size_t) * 1 /* only size field in header */
#  define NODE_OVERHEAD sizeof(size_t) * 2 /* NODEHEADER+size at the end */
# endif
#endif

/** Contains a range of valid memory. This is used to validate pointers.*/
struct mem_range {
    size_t start;
    size_t end;
};

/** Memory chunk layout inspired by Doug Lea Allocator.
 * Node represents one chunk of data of the mem pool.
 * This class has no data and the methods implement an easy access
 * to the raw memory layout. The free nodes are connected by a double
 * linked list. When a block is used (allocated by a user), the
 * pointers are used for user data.
 *
 * Memory layout of a chunk:
 * Offset Length Description
 * 0      4      Status/Size: 0xFFFFFFFF=FREE, otherwise it contains the real size of the node
 * 4      4      Previous: Pointer to previous node / When Status!=FREE this is user data also
 * 8      4      Next: Pointer to next node / When Status!=FREE this is user data also
 * ...    ...    Data: User data
 * Size-4 4      Size: Real size of node. Used to check neighbor node.
 *
 * Memory layout of a chunk on x64:
 * Offset Length Description
 * 0      8      Status/Size: 0xFFFFFFFFFFFFFFFF=FREE, otherwise it contains the real size of the node
 * 4      8      Previous: Pointer to previous node / When Status!=FREE this is user data also
 * 8      8      Next: Pointer to next node / When Status!=FREE this is user data also
 * ...    ...    Data: User data
 * Size-8 8      Size: Real size of node. Used to check neighbor node.
 *
 * The minimum chunk size is 8, so the three least significant bits of size are never used.
 * We can use it for status information. Bit 0 is used to mark the chunk as used or free.
 * Status/Size word: (S=Size Bits, U=UsedBit, .=NotUsed)
 * SSSS SSSS SSSS SSSS SSSS SSSS SSSS S..U (32bit)
 * SSSS SSSS SSSS SSSS SSSS SSSS SSSS SSSS SSSS SSSS SSSS SSSS SSSS SSSS SSSS S..U (64bit)
 */
struct malloc_chunk {
    size_t size;                /* Size and InUse bit */
#ifdef MEMORY_ENABLE_TRACE
    size_t logindex;
#endif
#ifdef MP_DEBUG_MEMORY
    size_t bound1;
#endif
    struct malloc_chunk  *next; /* double linked list previous pointer, only used if free */
    struct malloc_chunk  *prev; /* double linked list previous pointer, only used if free */
};

/** Fast dynamic memory pool.
 * It uses bins of a defined chunk size to sort the chunks.
 * This allows a quick lookup of fitting chunks.
 */
struct mempool {
    unsigned char       *data;
    struct malloc_chunk *bins[NUM_BINS];
    struct mem_range     range;
#ifdef MEMORY_ENABLE_TRACK_USED_MEM
    size_t               used_mem;
#endif /* MEMORY_ENABLE_TRACK_USED_MEM */
#ifdef MEMORY_ENABLE_STAT
    size_t               max_used_mem; /* maximum used physical memory, this includes the header and footer */
#endif /* MEMORY_ENABLE_STAT */
#ifdef MEMORY_THREADSAFE
    ua_mutex_t           mutex; /* mutex for synchronizing mempool access in SHM */
#endif
};

int   mempool_init(struct mempool *pool, unsigned char *data, size_t size);
void  mempool_cleanup(struct mempool *pool);
#ifdef MEMORY_ENABLE_TRACE
void *mempool_alloc(struct mempool *pool, size_t size, const char *file, int line);
void *mempool_realloc(struct mempool *pool, void *ptr, size_t size, const char *file, int line);
void  mempool_free(struct mempool *pool, void *ptr, const char *file, int line);
#else
void *mempool_alloc(struct mempool *pool, size_t size);
void *mempool_realloc(struct mempool *pool, void *ptr, size_t size);
void  mempool_free(struct mempool *pool, void *ptr);
#endif

/** @} */

#endif /* __MEMPOOL_H__ */
