/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef OBJECTPOOL_H_JCQGJEHB
#define OBJECTPOOL_H_JCQGJEHB

/**
 * @defgroup memory_objectpool objectpool
 * @ingroup memory
 *
 * @brief Allocator for fixed size memory chunks.
 * The objectpool is initialized with an amount of memory
 * and a fixed size chunks size. Then @ref mem_objectpool_get
 * can be used to chunks from this memory.
 * @{
 */

#include <platform/platform.h>
#include <memory/memory_config.h>
#include <memory/memory.h>
#include <stdbool.h>
#ifdef MEMORY_THREADSAFE
# include <platform/mutex.h>
#endif
#ifdef MEMORY_OBJECTPOOL_SUPPORT_FILE_IO
# include <platform/file.h>
#endif

#ifdef MEMORY_OBJECTPOOL_USE_MALLOC
/* There is an alternative implementation of the objectpool that uses malloc
 * for each pool element. In combination with valgrind or address sanitizer
 * this allows better memory diagnostics. However this implementation is
 * much slower and has only reduced functionality, so:
 * DO NOT ENABLE FOR PRODUCTIVE USAGE!
 */
#define MEM_OBJECTPOOL_SIZE(num, size) ((num)*sizeof(void *)+sizeof(struct mem_objectpool))
struct mem_objectpool {
    unsigned int id;               /**< pool id */
    size_t num_objects;            /**< number of possible objects in pool */
    size_t object_size;            /**< size of one object in bytes */
    size_t used;                   /**< number of used objects in pool */
#ifdef MEMORY_ENABLE_STAT
    size_t max_used;               /**< maximum used objects in pool */
#endif /* MEMORY_ENABLE_STAT */
    const char *name;              /**< human readable name of buffer */
    void *null_if_static;          /**< this pointer is NULL if the pool is static */
#ifdef MEMORY_THREADSAFE
# error "dynamic objectpool does not work with shared memory"
#endif
    void *list[];                  /**< pointers to allocated objects */
};
#else /* MEMORY_OBJECTPOOL_USE_MALLOC */

/** Helper macro to compute pool sizes.
 * @param num number of objects
 * @param size size of one object in bytes
 */
#define MEM_OBJECTPOOL_SIZE(num, size) ((num)*(size_align((size), sizeof(void *)))+sizeof(struct mem_objectpool))

struct mem_objectpool_element;

/** This is the head structure of a memory pool, see also @ref memory_objectpool.
 * The head and the managed pool data reside in a contiguous array.
 */
struct mem_objectpool {
    unsigned int id;               /**< pool id */
    size_t num_objects;            /**< number of possible objects in pool */
    size_t object_size;            /**< size of one object in bytes, rounded up to pointer size in init */
    size_t used;                   /**< number of used objects in pool */
#ifdef MEMORY_ENABLE_STAT
    size_t max_used;               /**< maximum used objects in pool */
#endif /* MEMORY_ENABLE_STAT */
    const char *name;              /**< human readable name of buffer */
    struct mem_objectpool_element *freelist; /**< pointer to first element in pool */
#ifdef MEMORY_THREADSAFE
    /* note, this is not necessary for static object pools (readonly) */
    ua_mutex_t           mutex;    /**< mutex for synchronizing mempool access in SHM */
#endif
    unsigned char data[];          /**< rest of the pool data */
};
#endif /* MEMORY_OBJECTPOOL_USE_MALLOC */

/** Structure for a static memory pool, see also @ref memory_objectpool.
 * note, the binary layout of mem_objectpool_static must be indentical
 * with objectpool, because the same functions are used on it.
 */
struct mem_objectpool_static {
    unsigned int id;               /**< pool id */
    size_t num_objects;            /**< number of possible objects in pool */
    size_t object_size;            /**< size of one object in bytes */
    size_t used;                   /**< number of used objects in pool */
#ifdef MEMORY_ENABLE_STAT
    size_t max_used;               /**< maximum used objects in pool */
#endif /* MEMORY_ENABLE_STAT */
    const char *name;              /**< human readable name of buffer */
    void *always_null;             /**< pointer that must be NULL for static pools */
#ifdef MEMORY_THREADSAFE
    ua_mutex_t           reserved; /**< this is only here to have the same memory layout as objectpool */
#endif
    unsigned char data[];          /**< rest of the pool data */
};

size_t mem_objectpool_size(size_t num_objects, size_t object_size);
struct mem_objectpool *mem_objectpool_init(size_t num_objects, size_t object_size, void *data, const char *name);
void mem_objectpool_free_all(struct mem_objectpool *pool);
void mem_objectpool_clear(struct mem_objectpool *pool);
void *mem_objectpool_alloc(struct mem_objectpool *pool);
int mem_objectpool_free(struct mem_objectpool *pool, void *el);
int mem_objectpool_free_index(struct mem_objectpool *pool, size_t idx);
void *mem_objectpool_get(struct mem_objectpool *pool, size_t index);
size_t mem_objectpool_indexof(const struct mem_objectpool *pool, const void *el);
bool mem_objectpool_contains(const struct mem_objectpool *pool, const void *el);

#ifdef MEMORY_OBJECTPOOL_SUPPORT_FILE_IO
typedef void (*mem_objectpool_element_printer)(struct ua_filestream *f, void *ptr);
int mem_objectpool_save_file(struct mem_objectpool *pool, ua_file_t f, int (*writeobject)(const void *, ua_file_t f));
int mem_objectpool_get_info(ua_file_t f, size_t *obj_size, size_t *blob_size, size_t *num_objects);
int mem_objectpool_load_file(struct mem_objectpool *pool, ua_file_t f, int (*read_object)(void *, ua_file_t f, size_t));
int mem_objectpool_to_c_source(struct mem_objectpool *pool, struct ua_filestream *f, const char *typename, const char *varname, mem_objectpool_element_printer fct_node_printer);
#endif

/** @} */

#endif /* end of include guard: OBJECTPOOL_H_JCQGJEHB */

