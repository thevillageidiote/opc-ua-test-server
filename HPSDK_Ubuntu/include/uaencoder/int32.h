/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UAENCODER_INT32_H_
#define _UAENCODER_INT32_H_

#include <stdint.h>
#include "uint32.h"

#define ua_encode_int32(ctx, val) ua_encode_uint32(ctx, (const uint32_t *)val)
#define ua_decode_int32_skip(ctx) ua_decode_uint32_skip(ctx)

#define ua_encode_int32_unchecked(ctx, val) ua_encode_uint32_unchecked(ctx, (const uint32_t *)val)
#define ua_decode_int32_unchecked(ctx, val) ua_decode_uint32_unchecked(ctx, (uint32_t *)val)

static inline int ua_decode_int32(struct ua_decoder_context *ctx, int32_t *val)
{
    return ua_decode_uint32(ctx, (uint32_t *) val);
}

#endif /* _UAENCODER_INT32_H_*/
