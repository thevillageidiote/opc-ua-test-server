/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef UA_REF_H
#define UA_REF_H

#include <platform/atomic.h>

struct ua_ref {
    ua_atomic_t refcount;
};

/** Function prototype for object cleanup. */
typedef void (*ref_release)(struct ua_ref *ref);

/** Initialize refcount of object.
 * @param ref Pointer to object's refcount element.
 */
static inline void ua_ref_init(struct ua_ref *ref)
{
    ua_atomic_set(&ref->refcount, 1);
}

/** Increment refcount of object.
 * @param ref Pointer to object's refcount element.
 */
static inline void ua_ref_get(struct ua_ref *ref)
{
    ua_atomic_inc(&ref->refcount);
}

/** Subtract \c count from refcount.
 * @param ref Pointer to object's refcount element.
 * @param count number to subtract
 * @param release pointer to cleanup function this will be called
 *   when the last reference to the obejct is released.
 * @return The resulting refcount. Thus, a refcount of zero means
 *   the object was removed.
 */
static inline int ua_ref_sub(struct ua_ref *ref, unsigned int count, ref_release release)
{
    if (ua_atomic_sub_and_test(count, &ref->refcount)) {
        if (release) release(ref);
        return 0;
    }
    return ref->refcount.counter;
}

/** Decrement refcount of object.
 * @param ref Pointer to object's refcount element.
 */
static inline int ua_ref_put(struct ua_ref *ref, ref_release release)
{
    return ua_ref_sub(ref, 1, release);
}

#endif /* end of include guard: UA_REF_H */

