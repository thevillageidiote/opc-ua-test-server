/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef UA_ADDR_NODE_H
#define UA_ADDR_NODE_H

#include <stdint.h>
#include <stddef.h> /* size_t */

/* forward declarations */
enum ua_nodeclass;
enum ua_variant_type;
struct ua_nodeid;
struct ua_qualifiedname;
struct mem_objectpool;
struct mem_objectpool_static;
struct ua_perm_ctx;
struct ua_staticstore;

/**
 * @defgroup addr_node node
 * @ingroup addressspace
 * Create/delete/find nodes and access base attributes.
 * @{
 */

/** Handle for a node in the addressspace */
typedef int32_t ua_node_t;
/** Value of an invalid node handle */
#define UA_NODE_INVALID (ua_node_t)-1


/* handles for frequently used nodes, valid after serverprovider is initialized */
extern ua_node_t UA_NODE_HASPROPERTY;
extern ua_node_t UA_NODE_HASCOMPONENT;
extern ua_node_t UA_NODE_HASTYPEDEFINITION;
extern ua_node_t UA_NODE_HASSUBTYPE;
extern ua_node_t UA_NODE_HASMODELLINGRULE;
extern ua_node_t UA_NODE_OBJECTSFOLDER;
extern ua_node_t UA_NODE_ORGANIZES;
extern ua_node_t UA_NODE_HIERARCHICALREFERENCES;
extern ua_node_t UA_NODE_NONHIERARCHICALREFERENCES;
extern ua_node_t UA_NODE_FOLDERTYPE;
extern ua_node_t UA_NODE_BASEDATAVARIABLETYPE;
extern ua_node_t UA_NODE_PROPERTYTYPE;
extern ua_node_t UA_NODE_BASEDATATYPE;
extern ua_node_t UA_NODE_BASEOBJECTTYPE;



ua_node_t ua_node_create(uint16_t, enum ua_nodeclass type);
ua_node_t ua_node_create_with_attributes(struct ua_nodeid *nodeid,
                      enum ua_nodeclass nodeclass,
                      uint16_t browsename_nsidx,
                      const char *browsename,
                      const char *displayname,
                      ua_node_t typedef_node,
                      ua_node_t parent,
                      ua_node_t reftype_node);
int ua_node_delete(ua_node_t node);
enum ua_nodeclass ua_node_get_nodeclass(ua_node_t node);
int ua_node_set_nodeid(ua_node_t node, const struct ua_nodeid *id);
int ua_node_get_nodeid(ua_node_t node, struct ua_nodeid *id);
int ua_node_set_displayname(ua_node_t node, const char *name);
int ua_node_set_displayname_n(ua_node_t node, const char *name, size_t len);
const char *ua_node_get_displayname(ua_node_t node);
int ua_node_set_browsename(ua_node_t node, const char *name);
int ua_node_set_browsename_n(ua_node_t node, const char *name, size_t len);
int ua_node_set_browsename0(ua_node_t node, const char *name);
int ua_node_set_browsename0_n(ua_node_t node, const char *name, size_t len);
int ua_node_set_browsename_ex(ua_node_t node, const struct ua_qualifiedname *qn);
int ua_node_get_browsename(ua_node_t node, struct ua_qualifiedname *qn);
int ua_node_set_description(ua_node_t node, const char *text);
int ua_node_set_description_n(ua_node_t node, const char *text, size_t len);
const char *ua_node_get_description(ua_node_t node);
int ua_node_get_dataindex(ua_node_t node);
ua_node_t ua_node_create_with_type(uint16_t, enum ua_nodeclass type, ua_node_t type_id);
ua_node_t ua_node_create_with_type0(uint16_t, enum ua_nodeclass type, uint32_t type_id);
#ifdef UA_AUTHORIZATION_SUPPORT
int ua_node_get_perm(ua_node_t node, struct ua_perm_ctx *perm);
int ua_node_set_perm(ua_node_t node, struct ua_perm_ctx *perm);
#endif


/* Node lookup function based in the node id */
ua_node_t ua_node_find(const struct ua_nodeid *id);
/* Node lookup function for NS0 nodes. */
ua_node_t ua_node_find0(uint32_t numeric_id);
/* Node lookup function for numeric nodes. */
ua_node_t ua_node_find_numeric(uint16_t nsindex, uint32_t numeric_id);
/* Node lookup function for string nodes. */
ua_node_t ua_node_find_string(uint16_t nsindex, const char *string_id);

/*#define _DEBUG_INFO*/
#ifdef _DEBUG_INFO
void ua_node_print(ua_node_t node, const char *prefix);
#endif

/** @} */

#endif /* UA_ADDR_NODE_H */
