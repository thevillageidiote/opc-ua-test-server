/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef VARIABLE_H_J3IPI6UB
#define VARIABLE_H_J3IPI6UB

#include <stdint.h>
#include <stdbool.h>
#include <uaserver/addressspace/addressspace.h>

struct ua_variant;

/**
 * @defgroup variable variable
 * @ingroup addressspace
 * Access for nodeclass variable specific attributes.
 * @{
 */

int ua_variable_set_datatype(ua_node_t node, struct ua_nodeid *datatype);
int ua_variable_set_builtindatatype(ua_node_t node, enum ua_variant_type type);
int ua_variable_get_datatype(ua_node_t node, struct ua_nodeid *datatype);
int ua_variable_set_access_level(ua_node_t node, uint8_t accesslevel);
uint8_t ua_variable_get_access_level(ua_node_t node);
int ua_variable_set_user_access_level(ua_node_t node, uint8_t accesslevel);
uint8_t ua_variable_get_user_access_level(ua_node_t node);
int ua_variable_set_historizing(ua_node_t node, bool historizing);
bool ua_variable_get_historizing(ua_node_t node);
int ua_variable_set_valuerank(ua_node_t node, int32_t valuerank);
int32_t ua_variable_get_valuerank(ua_node_t node);
int ua_variable_set_arraydimensions(ua_node_t node, int32_t num_dimensions, uint32_t *dimensions);
int ua_variable_get_arraydimensions(ua_node_t node, int32_t *num_dimensions, uint32_t *dimensions);
int ua_variable_set_minimum_sampling_interval(ua_node_t node, uint32_t minimum_sampling_interval);
uint32_t ua_variable_get_minimum_sampling_interval(ua_node_t node);
double ua_variable_get_minimum_sampling_interval_dbl(ua_node_t node);
int ua_variable_set_store_index(ua_node_t node, uint8_t storeindex);
int ua_variable_get_store_index(ua_node_t node, uint8_t *storeindex);
int ua_variable_set_value_index(ua_node_t node, unsigned int valueindex);
int ua_variable_get_value_index(ua_node_t node, unsigned int *valueindex);
int ua_variable_set_attributes(ua_node_t node, struct ua_nodeid *datatype_id, int32_t valuerank, uint8_t access_level, bool historizing);
int ua_variable_copy(ua_node_t dst, ua_node_t src);

/** @} */

#endif /* end of include guard: VARIABLE_H_J3IPI6UB */

