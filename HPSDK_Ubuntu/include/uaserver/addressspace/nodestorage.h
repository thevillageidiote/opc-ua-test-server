/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef NODESTORAGE_H
#define NODESTORAGE_H

#include <stdint.h>
#include <uabase/nodeid.h>
#include <uaserver/server_config.h>
#ifdef UA_AUTHORIZATION_SUPPORT
# include <uaserver/session/authorization/authorization.h>
#endif
#ifdef SUPPORT_FILE_IO
# include <platform/file.h>
#endif

#ifdef _MSC_VER
/* MS compiler complains about wrong bitfield types,
 * which is perfectly fine in ANSI C99. However MS only
 * implements C89 (in 2015, belive it or not!).
 * But even with MS compiler the code works, as this functionalty
 * is provided by so called MS extensions to C89.
 */
# pragma warning(disable: 4214)
#endif

/** Internal storage format of a nodeid */
struct ua_nodeid_internal {
    uint32_t type : 2;
    uint32_t identifier : 30;
};

/* default config: sufficient for embedded devices:
 * 32,767 references/NS
 * 65,535 nodes/NS
 * 65,535 NS
 */
typedef int16_t  ns_t;
typedef int16_t  ua_index_t;
typedef uint16_t ua_uindex_t;
typedef int32_t  ua_index_ex_t;
typedef uint32_t uindex_ex_t;
#define INDEX_T_MAX INT16_MAX
#define UINDEX_T_MAX UINT16_MAX
#define INDEX_EX_T_MAX INT32_MAX
#define UINDEX_EX_T_MAX UINT32_MAX

/* detect 64bit mode */
#if UINTPTR_MAX == 0xffffffffffffffff
# define UA_64BIT
#elif UINTPTR_MAX == 0xffffffff
# define UA_32BIT
#else
# error("cannot detect 64bit mode")
#endif

/* note, ua_index_t is a namespace internal index, the NS is implicitly defined.
 * -1 is a nice way to say, all bits are one, independet from the size of the
 * type. for this reason we use a signed datatype.
 * */
#define UA_INDEX_INVALID (ua_index_t)-1
/* ua_index_ex_t included nsidx information. */
#define UA_INDEX_EX_INVALID (ua_index_ex_t)-1

/** Node data for table entries. */
struct ua_node {
    struct ua_nodeid_internal id;
    ua_index_ex_t rev_ref_idx; /* 1st reverse ref can be in another NS */
    ua_index_ex_t fwd_ref_idx; /* fwd references belong to this node in this NS */
    ua_index_t displaynameid;  /* displayname stringtable index */
    ua_index_t browsenameid;   /* browsename stringtable index */
    ua_index_t browsenamens;   /* browsename nsidx */
#ifdef UA_SUPPORT_DESCRIPTION
    ua_index_t descriptionid;  /* description stringtable index */
#endif /* UA_SUPPORT_DESCRIPTION */
    ua_index_t dataindex;
#ifdef UA_AUTHORIZATION_SUPPORT
    struct ua_perm_ctx perm_ctx;
#endif /* UA_AUTHORIZATION_SUPPORT */
    uint8_t nodeclass; /* we don't use the enum here, because enums are sizeof(int) */
};

/** Variable data for table entries. */
struct ua_variable {
    ua_index_ex_t datatype;
    int32_t valuerank;
    uint32_t minimum_sampling_interval; /** us, defined as double in UA, but we don't want that here. */
    int32_t num_dimensions;
    ua_uindex_t dimensionsindex;
    uint8_t accesslevel;       /**< TODO: replace accesslevels with inode info */
#ifdef UA_SUPPORT_HISTORIZING
    bool historizing;          /**< only necessary when compiled with history support */
#endif /* UA_SUPPORT_HISTORIZING */
    uint8_t storeindex;        /**< index to specific store in the SDK managed global store */
    ua_uindex_t valueindex;    /**< index to userdefine value array / userdata */
};

/** VariableType data for table entries. */
struct ua_variabletype {
    ua_index_ex_t datatype;
    int32_t valuerank;
    int32_t num_dimensions;
    ua_uindex_t dimensionsindex;
    bool is_abstract;
};

/** ReferenceType data for table entries. */
struct ua_referencetype {
    ua_index_t inverse_name; /**< inverse_name stringtable index */
    bool is_abstract;
    bool symmetric;
};

#if 0
/** TODO: as long as there is only one attribute,
 * we could store it directly in the dataindex and
 * avoid creating a separate pool
 */
struct ua_object {
    uint32_t event_notifier;
};
#endif

struct ua_method {
    ua_index_t methodindex;
    bool executable;
};

/** Reference data for table entries. */
struct ua_reference {
    ua_index_ex_t src;
    ua_index_ex_t dst;
    ua_index_ex_t reftype; /* or ua_index_t when only ns 0 reftypes are used */
    ua_index_ex_t next_rev;
    ua_index_ex_t next_fwd;
};

#ifdef SUPPORT_FILE_IO
int ua_nodehandle_write(const ua_index_ex_t *handle, ua_file_t f);
int ua_node_write(const void *ptr, ua_file_t f);
int ua_variable_write(const void *ptr, ua_file_t f);
int ua_variabletype_write(const void *ptr, ua_file_t f);
int ua_referencetype_write(const void *ptr, ua_file_t f);
int ua_method_write(const void *ptr, ua_file_t f);
int ua_reference_write(const void *ptr, ua_file_t f);

int ua_nodehandle_read(ua_index_ex_t *handle, ua_file_t f, size_t *blob_size);
int ua_node_read(void *ptr, ua_file_t f, size_t blob_size);
int ua_variable_read(void *ptr, ua_file_t f, size_t blob_size);
int ua_variabletype_read(void *ptr, ua_file_t f, size_t blob_size);
int ua_referencetype_read(void *ptr, ua_file_t f, size_t blob_size);
int ua_method_read(void *ptr, ua_file_t f, size_t blob_size);
int ua_reference_read(void *ptr, ua_file_t f, size_t blob_size);

#ifdef CODE_GENERATOR
void ua_node_print_c_code(struct ua_filestream *f, void *ptr);
void ua_variable_print_c_code(struct ua_filestream *f, void *ptr);
void ua_variabletype_print_c_code(struct ua_filestream *f, void *ptr);
void ua_referencetype_print_c_code(struct ua_filestream *f, void *ptr);
void ua_method_print_c_code(struct ua_filestream *f, void *ptr);
void ua_reference_print_c_code(struct ua_filestream *f, void *ptr);
#endif /* CODE_GENERATOR */
#endif /* SUPPORT_FILE_IO */

#endif /* NODESTORAGE_H */
