/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef UA_ADDR_UTILITY_H
#define UA_ADDR_UTILITY_H

#include "addressspace.h"

/**
 * @defgroup addr_utility utility
 * @ingroup addressspace
 * Addressspace helper and diagnostic functions.
 * @{
 */

ua_ref_t ua_reference_find_with_type(ua_node_t node, ua_node_t reftype);
ua_ref_t ua_reference_find_inv_with_type(ua_node_t node, ua_node_t reftype);
bool ua_node_is_subtypeof(ua_node_t type, ua_node_t basetype);
ua_node_t ua_node_get_typedefinition(ua_node_t node);

/** Enum to determine how to validate value attribute */
enum ua_addressspace_validate_value {
    ua_addressspace_validate_value_none, /**< Do not validate value at all */
    ua_addressspace_validate_value_read, /**< Read value attributes */
    ua_addressspace_validate_value_write /**< Read and write value attributes */
};

int ua_addressspace_validate(uint8_t max_recursion_depth, enum ua_addressspace_validate_value validate_value);

/** @} */

#endif /* UA_ADDR_UTILITY_H */
