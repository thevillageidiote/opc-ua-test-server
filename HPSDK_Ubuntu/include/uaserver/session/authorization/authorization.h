/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef AUTHORIZATION_H
#define AUTHORIZATION_H

#include <stdbool.h>
#include <uabase/statuscode.h>
#include <uabase/structure/messagesecuritymode.h>
#include <uabase/structure/nodeclass.h>
#include <uaserver/addressspace/addressspace.h>
#include <uaserver/server_config.h>

#if UA_AUTHORIZATION_BACKEND == UA_AUTHORIZATION_BACKEND_INODE
#include "inode/inode.h"
#endif

#ifdef SUPPORT_FILE_IO
# include <platform/file.h>
#endif

/* declarations: must be defined in backend */
struct ua_perm_ctx;
struct ua_user_ctx;

/**
 * @addtogroup authorization
 * Interface for implementing an authorization backend.
 *
 * The functions declared here must be implemented by every
 * authorization backend. Further functionallity is backend
 * specific and thus implemented in the backend like
 * @ref inode_authorization.
 * @{
 */

/**
 * Initialize structures needed for managing user and group
 * authorization.
 *
 * @param max_users Maximum number of users, that can be added.
 * @param max_groups Maximum number of groups, that can be added.
 * @return 0 on success or errorcode on failure.
 */
int ua_authorization_init(uint32_t max_users, uint32_t max_groups);

/**
 * Delete all users and groups and clear structures for managing
 * users and groups.
 */
void ua_authorization_clear(void);

/**
 * Get the current default permissions.
 */
int ua_authorization_get_default_perm(struct ua_perm_ctx *perm, enum ua_nodeclass nc);

/**
 * Get the user context for a given user.
 *
 * @param username Name of the user.
 * @param user_ctx The user context.
 * @param mode The messagesecuritymode of the user's connection to the server.
 * @return 0 on success or errorcode on failure.
 */
int ua_authorization_get_user_info(const struct ua_string *username, struct ua_user_ctx *user_ctx, enum ua_messagesecuritymode mode);

/**
 * Check if the attributes of a node are readable by the user.
 * @return 0 if true or statuscode if access is denied.
 */
ua_statuscode ua_authorization_is_attrreadable(ua_node_t node, struct ua_user_ctx *user);

/**
 * Check if the attributes of a node are writable by the user.
 * @return 0 if true or statuscode if access is denied.
 */
ua_statuscode ua_authorization_is_attrwritable(ua_node_t node, struct ua_user_ctx *user);

/**
 * Check if a method is executeable by the user.
 * @return 0 if true or statuscode if access is denied.
 */
ua_statuscode ua_authorization_is_executable(ua_node_t node, struct ua_user_ctx *user);

/**
 * Check if the user may read the events of an object.
 * @return 0 if true or statuscode if access is denied.
 */
ua_statuscode ua_authorization_is_eventreadable(ua_node_t node, struct ua_user_ctx *user);

/**
 * Check if the value of a variable is readable by the user.
 * @return 0 if true or statuscode if access is denied.
 */
ua_statuscode ua_authorization_is_readable(ua_node_t node, struct ua_user_ctx *user);

/**
 * Check if the value of a variable is writable by the user.
 * @return 0 if true or statuscode if access is denied.
 */
ua_statuscode ua_authorization_is_writable(ua_node_t node, struct ua_user_ctx *user);

/**
 * Check if the history of a variable is readable by the user.
 * @return 0 if true or statuscode if access is denied.
 */
ua_statuscode ua_authorization_is_historyreadable(ua_node_t node, struct ua_user_ctx *user);

/**
 * Check if the user can insert values in the history of a variable.
 * @return 0 if true or statuscode if access is denied.
 */
ua_statuscode ua_authorization_is_historyinsertable(ua_node_t node, struct ua_user_ctx *user);

/**
 * Check if the user can modify the history of a variable.
 * @return 0 if true or statuscode if access is denied.
 */
ua_statuscode ua_authorization_is_historymodifiable(ua_node_t node, struct ua_user_ctx *user);

/**
 * Check if the user can delete values from the history of a variable.
 * @return 0 if true or statuscode if access is denied.
 */
ua_statuscode ua_authorization_is_historydeletable(ua_node_t node, struct ua_user_ctx *user);

#ifdef SUPPORT_FILE_IO
/**
 * Load authorization information from files.
 *
 * Backends based on users and groups might use the parameters
 * to specify a user and group file to load the information from.
 * Other backends might give these parameters other semantics.
 *
 * @return 0 on success or errorcode on failure.
 */
int ua_authorization_load_from_file(const char *users_file, const char *groups_file);

/**
 * Print the ua_perm_ctx structure as C code.
 *
 * @param f Filedescriptor to write to.
 * @param perm The ua_perm_ctx structure to print.
 */
void ua_authorization_print_c_code(struct ua_filestream *f, struct ua_perm_ctx *perm);
#endif

/** @}*/

#endif /* end of include guard: AUTHORIZATION_H */

