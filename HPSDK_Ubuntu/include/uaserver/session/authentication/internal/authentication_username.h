/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef AUTHENTICATION_INTERNAL_USERNAME_H
#define AUTHENTICATION_INTERNAL_USERNAME_H

#include <uabase/statuscode.h>
#include <uabase/structure/signaturedata.h>
#include <uabase/structure/usernameidentitytoken.h>
#include <uaserver/session/session.h>

int ua_authentication_username(struct uasession_session *session,
                            struct uasession_channel *channel,
                            struct ua_usernameidentitytoken *token,
                            struct ua_signaturedata *tokensignature,
                            bool channel_changed,
                            ua_statuscode *result,
                            struct ua_string *username,
                            void *cb_data);

#endif /* end of include guard: AUTHENTICATION_INTERNAL_USERNAME_H */
