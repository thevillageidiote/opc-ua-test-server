/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef INTERNAL_AUTHENTICATION_H
#define INTERNAL_AUTHENTICATION_H

#include <memory/memory_config.h>
#include <uabase/statuscode.h>
#include <uabase/string.h>
#include <uabase/bytestring.h>
#ifdef SUPPORT_FILE_IO
# include <platform/file.h>
#endif

#define UA_AUTH_POLICY_ANON 0
#define UA_AUTH_POLICY_PW_CLEARTEXT 1
#define UA_AUTH_POLICY_PW_RSA_15 2
#define UA_AUTH_POLICY_PW_RSA_OAEP 3

/**
 * @defgroup internal_authentication internal
 * @ingroup authentication
 * For more information on the internal authentication backend
 * and usage examples see @ref authentication_internal.
 * @{
 */

/** Identifier for no hash algorithm (cleartext password) */
#define UA_AUTH_HASH_NONE 1
/** Identifier for SHA-256 hash algorithm */
#define UA_AUTH_HASH_SHA256 2

bool ua_authentication_user_exists(const struct ua_string *username);
ua_statuscode ua_authentication_check_password(const struct ua_string *username, const struct ua_bytestring *password);
int ua_authentication_add_user(uint8_t hash_alg, const char *username, const char *salt, const char *hash);
int ua_authentication_remove_user(const char *username);

#ifdef SUPPORT_FILE_IO
int ua_authorization_write_to_filestream(struct ua_filestream *stream);
#endif

#endif /* end of include guard: INTERNAL_AUTHENTICATION_H */

/** @} */
