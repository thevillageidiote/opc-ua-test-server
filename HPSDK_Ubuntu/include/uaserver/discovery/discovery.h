/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UASERVER_DISCOVERY_H
#define UASERVER_DISCOVERY_H

#include <uabase/uatype_config.h>
#include <uaserver/session/session.h>

struct ua_requestheader;
struct ua_readrequest;
struct ua_findserversrequest;
struct ua_getendpointsrequest;
struct ua_endpointdescription;

#ifdef ENABLE_SERVICE_FINDSERVERS
int uasession_findservers(struct ua_requestheader *head, struct ua_findserversrequest *req, struct uasession_msg_ctxt *msg_ctxt);
#endif

#ifdef ENABLE_SERVICE_GETENDPOINTS
int uasession_getendpoints(struct ua_requestheader *head, struct ua_getendpointsrequest *req, struct uasession_msg_ctxt *msg_ctxt);
#endif

int uasession_create_endpointdescriptions(struct uasession_msg_ctxt *msg_ctxt, int32_t *ned, struct ua_endpointdescription **ed);

#endif /* end of include guard: UASERVER_DISCOVERY_H */
