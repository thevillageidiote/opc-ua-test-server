/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef SUPER_NOTIFICATION_H
#define SUPER_NOTIFICATION_H

#include <stdint.h>
#include <uabase/structure/notificationmessage.h>
#include <uabase/extensionobject.h>
#include <uabase/structure/datachangenotification.h>
#include <uabase/structure/eventnotificationlist.h>
#include <uabase/structure/statuschangenotification.h>

/**
 * @internal
 * This structure is comparable to the so called NotificationMessage
 * described in part 4. It consists of an array of extensionobjects
 * with at most two elements, one datachangenotification and one
 * eventfieldlist. These are all contained in the structure to reduce
 * dynamic memory allocation (The changed values/events still need to
 * be allocated dynamically).
 *
 * These structures are stored in the retransmission queue, to their
 * number is limited to max_subscriptionbase * queue_size and it is
 * allocated through an objectpool at program start.
 *
 * Note: statuschangenotifications are allocated completely dynamically
 * as these aren't stored in the retransmission queue.
 */
struct ua_supernotification
{
    uint32_t subscription_id;
    struct ua_notificationmessage msg;
    struct ua_datachangenotification data_change;
#ifdef EVENTS_ENABLED /* TODO fix this define once events are implemented */
    struct ua_eventfieldlist event_change;
    struct ua_extensionobject notification_data[2];
#else
    struct ua_extensionobject notification_data[1];
#endif
};

int ua_supernotification_init(void);
void ua_supernotification_clear(void);

struct ua_supernotification *ua_supernotification_create(void);
void ua_supernotification_delete(struct ua_supernotification *sn);

#endif /* end of include guard: SUPER_NOTIFICATION_H */

/** @} */
