/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef STRINGTABLE_H_G4HRJL9N
#define STRINGTABLE_H_G4HRJL9N

#include <memory/memory_config.h>
#include <common/ref.h>
#include <stdbool.h>
#include <util/hashtable.h>
#include <platform/platform_config.h>
#ifdef SUPPORT_FILE_IO
#include <platform/file.h>
#endif

/* note: this structure inherits util_hashnode to be able to
 * get indexed by the hashtable.
 */
struct ua_hashstring {
    struct util_hashnode header;
    struct ua_ref        refcount;
    size_t               len;  /**< length of string */
    char                *text; /**< pointer to string data */
};

/** Constant string for generating string tables that get compiled into the code. */
#ifdef UA_CONFIG_SMALL_CONST_STRING
struct ua_conststring {
    uint16_t len;   /**< length of string */
};
#else
struct ua_conststring {
    uint32_t len;   /**< length of string */
};
#endif

/** A stringtable maintains a pool of string,
 * which are indexed by a hashtable.
 * This table eliminates double string entries.
 */
struct ua_hashstringtable {
    struct mem_objectpool *stringpool; /**< pointer to pool of hashstrings (dynamic) or conststrings (static) */
    struct util_hashtable *hashtable;  /**< pointer to hashtable (dynamic stringtable) */
    const char **stringtable;          /**< pointer to string table (static stringtable) */
    size_t num_strings;                /**< number of strings in table (virtually before deduplication) */
    bool   is_constant;                /**< is true if the string table is constant, false otherwise */
};

struct mem_objectpool_static;

int ua_hashstringtable_init(struct ua_hashstringtable *tbl, size_t num_strings);
int ua_hashstringtable_init_static(struct ua_hashstringtable *tbl, struct mem_objectpool_static *stringpool, const char *ua_hashstringtable[]);
void ua_hashstringtable_clear(struct ua_hashstringtable *tbl);
size_t ua_hashstringtable_add(struct ua_hashstringtable *tbl, const char *text, size_t len);
size_t ua_hashstringtable_add_const(struct ua_hashstringtable *tbl, const char *text, size_t len);
int ua_hashstringtable_remove(struct ua_hashstringtable *tbl, size_t index);
const char *ua_hashstringtable_get(struct ua_hashstringtable *tbl, size_t index, size_t *len);

void ua_hashstringtable_print_stat(struct ua_hashstringtable *tbl);
#ifdef SUPPORT_FILE_IO
struct ua_addressspace;
int ua_hashstringtable_save_file(struct ua_hashstringtable *tbl, ua_file_t f);
int ua_hashstringtable_load_file(struct ua_hashstringtable *tbl, ua_file_t f);
int ua_hashstringtable_to_c_source(struct ua_addressspace *addr, struct ua_filestream *f);
#endif

#endif /* end of include guard: STRINGTABLE_H_G4HRJL9N */

