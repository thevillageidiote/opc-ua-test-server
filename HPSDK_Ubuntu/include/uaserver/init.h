/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef UASERVER_INIT_H
#define UASERVER_INIT_H

#include <stdlib.h> /* size_t */
#include <uaprovider/provider.h>
#include <uaserver/server_types.h>

/* forward declaration */
struct ua_net_base;

/** \addtogroup uaserver
 * @{
 */

struct uaserver_provider {
    int handle;
    uaprovider_init_func init;
};

int uaserver_init(struct uaserver_provider *provider, int num_provider, struct ua_net_base *net_base);
int uaserver_init_ipc(void);

void uaserver_clear(struct uaserver_provider *provider, int num_provider);
void uaserver_clear_ipc(void);

int uaserver_start(void);
int uaserver_stop(void);

int uaserver_do_com(void);

struct ua_net_base *uaserver_get_net_base(void);

/** @} */

#endif /* end of include guard: UASERVER_INIT_H */

