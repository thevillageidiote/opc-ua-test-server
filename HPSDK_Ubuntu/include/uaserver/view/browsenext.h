/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef BROWSENEXT_H_YUB4UTTL
#define BROWSENEXT_H_YUB4UTTL

#include <uaserver/addressspace/addressspace.h>

struct ua_requestheader;
struct ua_browsenextrequest;
struct uasession_session;

/**
 * @internal
 * @brief Continuationpoint used for the browsenext service.
 *
 * The continuationpoint is defined as bytestring, for the browsenext service
 * it is structured as this struct. The continuationpoint is handled by the client
 * as opaque and the server only gets its own continuationspoints, so endianess is
 * not an issue. Furthermore the struct only has fixed size elements, so the
 * bytestring can be simply cast to this structure (after checking the length).
 */
struct uaserver_browsenext_continuationpoint {
    uint32_t src_node_id;   /* the src node is transmitted as registered node, so it is always numeric */
    uint32_t type_node_id;  /* the type node is transmitted as registered node, so it is always numeric */
    uint32_t max_results;
    uint32_t hash;            /* hash of the continuation point with the session id */
    ua_ref_t start_ref;
    uint16_t src_node_ns;
    uint16_t type_node_ns;
    uint8_t nodeclass_mask;   /* defined as uint32 but only 8 bits are used */
    uint8_t result_mask;      /* defined as uint32 but only 6 bits are used */
    uint8_t browse_direction; /* defined as enum but only 2 bits are used */
    bool include_subtypes;    /* optimization idea: move bool to unused bits of result mask */
};

int uasession_browsenext(struct ua_requestheader *head, struct ua_browsenextrequest *req, struct uasession_msg_ctxt *ctxt);

#endif /* end of include guard: BROWSENEXT_H_YUB4UTTL */

