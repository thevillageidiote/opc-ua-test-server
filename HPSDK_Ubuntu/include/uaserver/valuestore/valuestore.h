/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UASERVER_GLOBALSTORE_H_
#define _UASERVER_GLOBALSTORE_H_

#include <uabase/statuscode.h>
#include <uaserver/addressspace/addressspace.h>
#ifdef SUPPORT_FILE_IO
# include <platform/file.h>
#endif

/* forward declarations */
struct ua_indexrange;
struct ua_datavalue;

/**
 * @addtogroup valuestore
 * Functions and definitions for using the valuestore infrastructure of the SDK.
 * For store implementations provided by the SDK see:
 * - @ref ua_memorystore
 * - @ref ua_staticstore
 * @{
 */

/**
 * Get the value identified with valueindex from the store.
 * The function must copy the value in the provided ua_datavalue
 * struct. In case of an error while getting the value the statuscode
 * of the value be set to the corresponding bad statuscode.
 *
 * For an example how to implement this function see: @ref gettingstarted2_step02_01
 */
typedef void (*ua_valuestore_get)(void *store,
                             ua_node_t node,
                             unsigned int valueindex,
                             bool source_ts,
                             struct ua_indexrange *range,
                             unsigned int num_ranges,
                             struct ua_datavalue *value);

/**
 * Attach the value identified with valueindex to the store.
 * This function may take responsibility for the given ua_datavalue
 * struct and clear it. In case of an error a bad statuscode must
 * be returned.
 *
 * For an example how to implement this function see: @ref gettingstarted2_step02_02
 */
typedef ua_statuscode (*ua_valuestore_attach)(void *store,
                                      ua_node_t node,
                                      unsigned int valueindex,
                                      struct ua_indexrange *range,
                                      unsigned int num_ranges,
                                      struct ua_datavalue *value);

/**
 * @internal
 * @brief Internal storage structure for a valuestore.
 */
struct ua_valuestore_interface {
    void *store;
    ua_valuestore_get get_fct;
    ua_valuestore_attach attach_fct;
};

void ua_valuestore_interface_init(struct ua_valuestore_interface *store_interface);
int ua_valuestore_register_store(struct ua_valuestore_interface *store_if, uint8_t *storeindex);
int ua_valuestore_unregister_store(uint8_t storeindex);

struct ua_valuestore_interface *ua_valuestore_get_store(uint8_t storeindex);

#ifdef SUPPORT_FILE_IO
/**
 * Load the values for a store from a file. The function
 * Needs the store context and a filedescriptor to the opened
 * file to load the values from. It returns zero on success
 * or an negative errorcode on failure.
 */
typedef int (*ua_valuestore_from_fd)(void *store, ua_file_t f);

int ua_valuestore_from_fd_skip(void *dummy, ua_file_t f);
#endif

/** @} */

#endif /* _UASERVER_GLOBALSTORE_H_ */

