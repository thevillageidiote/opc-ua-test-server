/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef _UASERVER_STATICSTORE_H_
#define _UASERVER_STATICSTORE_H_

#include <platform/platform_config.h>
#ifdef SUPPORT_FILE_IO
# include <platform/file.h>
#endif
#include <uabase/datavalue.h>
#include <uabase/statuscode.h>
#include <uabase/indexrange.h>
#include <uabase/structure/timestampstoreturn.h>
#include <uaserver/addressspace/addressspace.h>

/**
 * @brief One element of the @ref ua_staticstore
 * @memberof ua_staticstore
 */
struct ua_staticstore_element {
    unsigned int length;
    const char *data;
};

/**
 * @ingroup valuestore
 * @struct ua_staticstore
 * Implementation of a store using preencoded values.
 */
struct ua_staticstore {
    const struct ua_staticstore_element *elements;
    unsigned int num_data;
    bool const_data;
    uint8_t storeidx;
};

int ua_staticstore_from_const_data(struct ua_staticstore *store, const struct ua_staticstore_element *elements, unsigned int num_data, uint8_t storeidx);
#ifdef SUPPORT_FILE_IO
int ua_staticstore_from_fd(void *staticstore, ua_file_t f);
#endif

void ua_staticstore_clear(struct ua_staticstore *store);
uint8_t ua_staticstore_get_storeindex(struct ua_staticstore *store);

#endif /* _UASERVER_STATICSTORE_H_ */

