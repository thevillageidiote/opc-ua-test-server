/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef MONITOREDITEM_H
#define MONITOREDITEM_H

#include <stdbool.h>
#include <util/ringqueue.h>
#include <uaserver/addressspace/addressspace.h>
#include <uabase/indexrange.h>
#include <uabase/structure/monitoringmode.h>
#include <uabase/structure/datachangetrigger.h>
#include <uabase/structure/timestampstoreturn.h>
#include <uabase/structure/monitoringparameters.h>
#include <uabase/datavalue.h>

#define UA_STATUSCODE_OVERFLOW_BIT 0x00000080
#define UA_STATUSCODE_DATAVALUE_BIT 0x00000400

struct ua_extensionobject;
struct ua_subscription;
struct ua_datavalue;
struct uasession_session;

/**
 * @ingroup uaserver
 * @struct ua_monitoreditem
 * @brief Structure to represent a monitoreditem in the server.
 */
struct ua_monitoreditem {
    struct ua_nodeid nodeid;    /**< nodeid of the item to monitor */
    uint32_t sampling_interval; /**< sampling interval can be adjusted by provider in add_item or modify_item */
    uint32_t user_data[2];      /**< user data can be used by provider */
    uint32_t attributeid;       /**< attributeid to monitor */
    struct ua_indexrange *range;   /**< array of requested indexranges for this item */
    unsigned int num_ranges;    /**< number of entries in the range array */
    uint32_t timeout_hint;      /**< timeout in ms an add/modify/remove may take, 0 indicates no timeout */
    enum ua_timestampstoreturn ts; /**< timestamps to return for the monitoreditem */
    ua_statuscode operation_result; /**< async result of the add/modify/remove operation */
    /* the id is persisted in the objectpool, so there must be at least sizeof(void *) before it */
    uint32_t id;                /**< server assigned id of the monitored item */
    /* below are the private members */
    bool discard_oldest;
    bool initial_value;
    uint32_t client_handle;
    enum ua_monitoringmode monitoring_mode;
    enum ua_datachangetrigger datachangetrigger;
    struct ua_datavalue last_value;
    struct ua_subscription *subscription;
    struct util_ringqueue value_queue;
};

int ua_monitoreditem_init(void);
void ua_monitoreditem_clear(void);

struct ua_monitoreditem *ua_monitoreditem_create(uint32_t queue_size);
void ua_monitoreditem_delete(struct ua_monitoreditem *item);
struct ua_monitoreditem *ua_monitoreditem_get(uint32_t id);

uint32_t ua_monitoreditem_queue_count(struct ua_monitoreditem *item);
struct ua_datavalue *ua_monitoreditem_get_value(struct ua_monitoreditem *item);
int ua_monitoreditem_new_value(struct ua_monitoreditem *item, struct ua_datavalue *value);
struct uasession_session *ua_monitoreditem_get_session(struct ua_monitoreditem *item);

ua_statuscode ua_monitoreditem_revise(struct ua_monitoringparameters *req_parameters,
                                      uint32_t publishing_interval,
                                      enum ua_datachangetrigger *datachangetrigger,
                                      uint32_t *rev_queue_size,
                                      uint32_t *rev_sampling_interval);

#endif /* end of include guard: MONITOREDITEM_H */

