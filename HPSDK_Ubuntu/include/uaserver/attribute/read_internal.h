/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef READ_INTERNAL_H_JLBWSTAJ
#define READ_INTERNAL_H_JLBWSTAJ

#include <uaserver/addressspace/addressspace.h>
#include <uabase/structure/timestampstoreturn.h>

struct ua_indexrange;
struct ua_string;
struct ua_datavalue;
struct uasession_session;

/** \addtogroup uaserver
 * @{
 */

void uaserver_read_internal(ua_node_t node,
    struct uasession_session *session,
    enum ua_timestampstoreturn ts,
    uint32_t attribute_id,
    struct ua_indexrange *index_range,
    unsigned int num_ranges,
    struct ua_datavalue *result);

/** @} */

#endif /* end of include guard: READ_INTERNAL_H_JLBWSTAJ */

