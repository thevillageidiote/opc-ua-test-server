/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UAPROVIDER_SERVER_H_OU0YQEOC
#define UAPROVIDER_SERVER_H_OU0YQEOC

#include <uabase/uatype_config.h>
#include <uaserver/addressspace/addressspace.h>
#include <uaserver/init.h>
#include <uabase/structure/timestampstoreturn.h>

/* forward declarations */
struct ua_indexrange;
struct ua_datavalue;

int uaprovider_server_init(struct uaprovider *ctx);

#ifdef ENABLE_SERVICE_READ
void uaprovider_server_read_value(ua_node_t node, double max_age, enum ua_timestampstoreturn ts, struct ua_indexrange *range, unsigned int num_ranges, struct ua_datavalue *result);
#endif

#endif /* end of include guard: UAPROVIDER_SERVER_H_OU0YQEOC */
