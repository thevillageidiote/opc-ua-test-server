/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UA_KEY_H_
#define _UA_KEY_H_

#include <stdlib.h>
#include <stdbool.h>

/**
 * @addtogroup crypto
 * @{
 */

/** Internal representation of a key in byte string format. */
struct crypto_key_plain {
    size_t len;          /**< Length of the data blob. */
    unsigned char *data; /**< Pointer to internal key representation. */
};

/** Describes several key encoding formats. */
enum crypto_key_format {
    crypto_key_format_none = 0, /**< Empty format placeholder. */
    crypto_key_format_pem  = 1, /**< PEM encoding.             */
    crypto_key_format_der  = 2, /**< DER.                      */
};

/** Describes the type of the contained key. */
enum crypto_key_type {
    crypto_key_type_none         = 0, /**< Empty key structure.                           */
    crypto_key_type_rsa_public   = 1, /**< Public key used for RSA crypto operations.     */
    crypto_key_type_rsa_private  = 2, /**< Private key used for RSA crypto operations.    */
    crypto_key_type_aes_encrypt  = 3, /**< Symmetric key used for AES encrypt operations. */
    crypto_key_type_aes_decrypt  = 4, /**< Symmetric key used for AES decrypt operations. */
    crypto_key_type_hmac_sign    = 5  /**< Key used for HMAC operations. */
};

/** Private/public key handle. */
struct crypto_key {
    void*                priv; /**< Pointer to backend key representation.     */
    enum crypto_key_type type; /**< Type of the key. @see enum crypto_key_type */
    bool                 free; /**< false, if priv must not be deleted.        */
};

/** Decode RSA key from PEM format.
 * @param pem    Buffer containing a PEM encoded key.
 * @param pemlen Length of the data in the buffer.
 * @param pwd    Buffer containing a optional password.
 * @param pwdlen Length of the password.
 * @param type   Type of the decoded key.
 * @param key   Handle to the decoded key.
 * @return Error Code
 */
int crypto_key_from_pem(
    const unsigned char *pem,
    size_t               pemlen,
    const unsigned char *pwd,
    size_t               pwdlen,
    enum crypto_key_type type,
    struct crypto_key   *key
);

/** Encode private key into PEM format.
 * The result is a zero terminated string.
 * @param key   Handle to the key to be encoded.
 * @param pem    Buffer to store the encoded key.
 * @param pemlen Length of the destination buffer and used buffer on return.
 * @return Error Code
 */
int crypto_key_to_pem(
    struct crypto_key *key,
    unsigned char     *pem,
    size_t            *pemlen
);

/** Create a random RSA key.
 * @param key  Pointer to the key structure to be set.
 * @param bits Required length of the key in bits.
 * @return Error Code
 */
int crypto_key_create_rsa_key(
    struct crypto_key *key,
    size_t             bits
);

/** Prepare key structure for use in symmetric encryption.
 * @param type       The type of key.
 * @param keydata    Buffer containing the key data.
 * @param keydatalen Length of the data in the key buffer.
 * @param key        Pointer to the key structure to be initialized.
 * @return Error Code
 */
int crypto_key_init(
    enum crypto_key_type type,
    const unsigned char *keydata,
    size_t               keydatalen,
    struct crypto_key   *key
);

/** Get the key length.
 * @param key Handle of the key.
 * @return Length of the key in bytes.
 */
size_t crypto_key_length(
    struct crypto_key *key
);

/** Release handle to key.
 * @param key Key handle to release.
 */
void crypto_key_clear(
    struct crypto_key *key
);

/** @} */

#endif /* _UA_KEY_H_ */

