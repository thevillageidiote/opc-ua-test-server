/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef _PKI_STORE_IMPL_H_
#define _PKI_STORE_IMPL_H_

#include <pki_store/pki_store_config.h>
#include <crypto/crypto.h>
#include <uabase/bytestring.h>

#define PKI_STORE_TYPE_FILE 1

/* PKI store capabilities */
#define PKI_STORE_SUPPORT_CERT 1
#define PKI_STORE_SUPPORT_KEY 1
#define PKI_STORE_SUPPORT_CRL 1
#define PKI_STORE_SUPPORT_VERIFY 1

/**
 * @addtogroup pki_store
 * @{
 */

/** Initialize PKI store management.
 * Must be called before any other PKI store function.
 */
int pki_store_init(void);

/** Clear PKI store management.
 * No further PKI store calls allowed after this call.
 */
int pki_store_clear(void);

/** Open the PKI store with the given id and settings and create directory layout if not existing.
 * Must be called before any other operation on a store with the given id.
 * @param config The configuration string for the store.
 * @param store Identifier of the store to be initialized.
 */
int pki_store_open(const char *config, uint32_t store);

/** Closes the store with the given id.
 * @param store Identifier of the store to be initialized.
 */
int pki_store_close(uint32_t store);

/** Convenience function to create SHA1 for use as certificate ID.
 * @param cert DER encoded certificate.
 * @param id Destination buffer for the SHA1 certificate id.
 * @memberof pki_store_file
*/
static inline int pki_store_make_cert_id(struct ua_bytestring *cert, unsigned char *id)
{
    return crypt_hash(crypto_alg_sha1, (const unsigned char *)cert->data, cert->len, id);
}

/** Convenience function to convert certificate ID to string representation.
 @param hash Certificate ID (SHA1) in binary representation
 @param dest Destination buffer of at least 41 bytes.
*/
int pki_store_sha1_to_string(const unsigned char *id, char *dest);

/** Convenience function to convert certificate ID from string to binary/API representation.
 @param hash Certificate ID in string representation (zero terminated).
 @param dest Destination buffer of at least 20 bytes for Certificate ID (SHA1) in binary representation.
*/
int pki_store_string_to_sha1(const char *src, unsigned char *id);

 /* @} */

#endif /* _PKI_STORE_IMPL_H_ */
