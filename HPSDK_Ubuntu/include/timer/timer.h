/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef _UATIMER_H_
#define _UATIMER_H_

#include <timer/config.h>
#include <stdbool.h>

#if defined(TIMER_BACKEND_SOFT)
# include "soft/timer_types.h"
#endif

/**
 * @addtogroup timer
 * @{
 * If timers are updated the elapsed time between their due time
 * and the time they were actually updated is not considered. So timers
 * won't shoot at a constant frequency if update isn't called exactly at
 * their due times, but they will always run for at least their configured
 * interval.
 *
 * A timer can be removed from inside the timer callback using one of the following options:
 * * Create the timer with the singleshot option, it will be removed after the
 * first shot automatically.
 * * Return -1 in the timer callback.
 * * Call the timer_remove() function.
 *
 * Note: Each of these is a valid option, but do not combine them!
 */

/**
 * Mode for newly added timers.
 * @see timer_add
 */
enum timer_mode {
    TIMER_DEFAULT = 0,    /**< Default: periodic timer */
    TIMER_SINGLESHOT = 1  /**< Singleshot timer */
};

/**
 * Initialize a timer base and allocate resources.
 * @param base The base to be initialized.
 * @param num_timers The maximum number of timers the base can hold.
 * @return Zero on success or errocode on failure.
 */
int timer_base_init(struct timer_base *base,
                    uint32_t           num_timers);

/**
 * Clear a timer base and free all its associated resources.
 * @param base The base to be cleared.
 */
void timer_base_clear(struct timer_base *base);

/**
 * Initialize and activate an emtpy timer element in the base.
 * @param base The timer base to add a new timer to.
 * @param id Output parameter that contains the id of the new timer (may be NULL).
 * @param ms The interval of the timer to shoot at in milliseconds. Zero is not allowed.
 * @param cb The callback function to be called when the timer shoots.
 * @param data The data passed to the callback function (may be NULL).
 * @param mode Timer mode like TIMER_DEFAULT, see @ref timer_mode.
 * @return Zero on success or errocode on failure.
 */
int timer_add(struct timer_base *base,
              uint32_t          *id,
              uint32_t           ms,
              timer_cb           cb,
              void              *data,
              uint8_t            mode);

/**
 * Activate the timer entry with \c id. The timer will resume shooting with the
 * properties it was added. The timer will not shoot before ms time has past.
 * @param base The timer base holding the timer to activate.
 * @param id The id of the timer to be activated.
 * @return Zero on success or errocode on failure.
 */
int timer_activate(struct timer_base *base,
                   uint32_t           id);

/**
 * Deactivate the timer entry with \c id. The timer will stop shooting, but its
 * resources will still be allocated.
 * Note: Calling deactivate and then activate is much faster than calling remove
 * and add for a timer.
 * @param base The timer base holding the timer to deactivate.
 * @param id The id of the timer to be deactivated.
 * @return Zero on success or errocode on failure.
 */
int timer_deactivate(struct timer_base *base,
                     uint32_t           id);

/**
 * Deactivate and remove a timer entry.
 * @param base The timer base holding the timer to remove.
 * @param id The id of the timer to be removed.
 * @param data Output parameter that returns the data added with timer_add (may be NULL).
 * @return Zero on success or errocode on failure.
 */
int timer_remove(struct timer_base *base,
                 uint32_t           id,
                 void             **data);

/**
 * Find and shoot all due timers in the base.
 * Looks at every timer and calls the callback if its due time has passed.
 * @param base Timer base whose timers need to be updated
 * @return ms till the next timer is due, or UINT32_MAX if there is none.
 */
uint32_t timer_update(struct timer_base *base);

/* @}*/

#endif /* _UATIMER_H_ */
