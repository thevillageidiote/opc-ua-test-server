/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef _UA_TIMER_TYPES_SOFT_H_
#define _UA_TIMER_TYPES_SOFT_H_

#include <stdint.h>
#include <util/bitmap.h>


/* forward declaration */
struct timer;

/**
 * @addtogroup timer
 * @{
 */

/**
 * Timer id, that is always considered invalid by the timer functions.
 */
#define TIMER_ID_INVALID UINT32_MAX

/**
 * Format of a timer callback.
 * @param elapsed Actually elapesd time since the last callback in miliseconds.
 * @param data The data that was added to the timer with timer_add.
 * @return Zero by default, -1 will remove the timer after the callback.
 */
typedef int timer_cb(uint64_t elapsed, void *data);

/** Timer management base structure. */
struct timer_base {
    uint32_t         ntimers;    /**< Number of currently used timers in the array. */
    uint32_t         max_timers; /**< Maximum number of timers in the array. */
    uint32_t         next;       /**< Time till next timer is due, set during timer_update(). */
    struct timer    *timers;     /**< Array of max_timers timers allocated by timer_base_init. */
    util_bitmap_t   *id_bitmap;  /**< Bitmap index of used entries in timers. */
};

/** @}*/

#endif /* _UA_TIMER_TYPES_SOFT_H_ */

