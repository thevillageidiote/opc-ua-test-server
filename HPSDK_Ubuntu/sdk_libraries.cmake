# SDK library configuration helper

include(MessageUtils)

# Use libuuid for GUID generation if available.
# Otherwise, a software fallback using rand() will be used.
# See src/platform/linux/pguid.c
if (UUID_FOUND)
    set(OS_LIBS ${OS_LIBS} uuid)
endif ()

# additional settings for lwIP backend
if (LIBUANET_BACKEND STREQUAL lwip)
    find_package(LwIP)
    set(OS_LIBS ${LWIP_LIBRARY} ${OS_LIBS})
    if (${CMAKE_SYSTEM_NAME} MATCHES "Windows")
        set(OS_LIBS ${OS_LIBS} ${LWIP_WIN_SUPPLEMENT})
    endif ()
endif ()

if (BUILD_LIBUACRYPTO)
    # setup the selected crypto library, it must be found else this is an error
    if (SECURITY_BACKEND STREQUAL openssl)
        if (NOT OPENSSL_FOUND)
            message(FATAL_ERROR "OpenSSL is required by this backend, but was not found.")
        endif ()
        # message(STATUS "CryptoTest: OpenSSL: ${OPENSSL_INCLUDE_DIR} ${OPENSSL_LIBRARIES}")
        set(OS_LIBS ${OS_LIBS} ${OPENSSL_LIBRARIES})
    elseif (SECURITY_BACKEND STREQUAL mbedtls)
        if (MBEDTLS_FOUND)
            if (MBEDTLS_REQUIRES_ZLIB)
                find_package(ZLIB)
                if (ZLIB_FOUND)
                    set(OS_LIBS ${OS_LIBS}  ${MBEDTLS_LIBRARY} ${MBEDX509_LIBRARY} ${MBEDCRYPTO_LIBRARY} ${ZLIB_LIBRARIES})
                else ()
                    message(FATAL_ERROR "Your mbedTLS version requires ZLIB which is not found.")
                endif ()
            else ()
                set(OS_LIBS ${OS_LIBS}  ${MBEDTLS_LIBRARY} ${MBEDX509_LIBRARY} ${MBEDCRYPTO_LIBRARY})
            endif ()
        else ()
            message(FATAL_ERROR "mbedTLS is required by this backend, but was not found.")
        endif ()
        #message(STATUS "CryptoTest: mbedTLS: ${MBEDTLS_INCLUDE_DIR} ${MBEDTLS_LIBRARY}")
    endif ()
endif ()

# unset libraries which are not enabled
if (NOT BUILD_LIBUACRYPTO)
    unset(UACRYPTO_LIBRARY)
endif ()
if (NOT BUILD_LIBUACRYPTO OR NOT BUILD_LIBUAPKI)
    unset(UAPKI_LIBRARY)
    unset(UAPKI_STORE_LIBRARY)
    unset(UAPKI_STORE_PROXY_LIBRARY)
    unset(UAPKI_STORE_STUB_LIBRARY)
    unset(UASECONV_PROXY_LIBRARY)
    unset(UASECONV_STUB_LIBRARY)
endif ()
if (NOT SUPPORT_FILE_IO)
    unset(UACONFIG_LIBRARY)
endif ()
if (NOT TRACE_ENABLED)
    unset(TRACE_LIBRARY)
endif ()

# create collection of SDK base libraries
set(SDK_BASE_LIBRARIES
    ${UATCP_LIBRARY}
    ${UASECONV_LIBRARY}
    ${UAENCODER_LIBRARY}
    ${UACONFIG_LIBRARY}
    ${UABASE_LIBRARY}
    ${NETWORK_LIBRARY}
    ${TIMER_LIBRARY}
    ${IPC_LIBRARY}
    ${MEMORY_LIBRARY}
    ${UACRYPTO_LIBRARY}
    ${UAPKI_LIBRARY}
    ${UAPKI_STORE_LIBRARY}
    ${UTIL_LIBRARY}
    ${TRACE_LIBRARY}
    ${CRASHHANDLER_LIBRARY}
    ${PLATFORM_LIBRARY})

# create collection of SDK server libraries
set(SDK_SERVER_LIBRARIES
    ${UASERVERPROVIDER_LIBRARY}
    ${UAINTERNALPROVIDER_LIBRARY}
    ${UASERVER_LIBRARY}
    ${UAPROVIDER_LIBRARY}
    ${ENCODER_PROXY_LIBRARY}
    ${ENCODER_STUB_LIBRARY}
    ${UASESSION_PROXY_LIBRARY}
    ${UASESSION_STUB_LIBRARY}
    ${UATCPMSG_PROXY_LIBRARY}
    ${UATCPMSG_STUB_LIBRARY}
    ${UAPKI_STORE_PROXY_LIBRARY}
    ${UAPKI_STORE_STUB_LIBRARY}
    ${UASECONV_PROXY_LIBRARY}
    ${UASECONV_STUB_LIBRARY})

# create collection of SDK client libraries
# TODO
