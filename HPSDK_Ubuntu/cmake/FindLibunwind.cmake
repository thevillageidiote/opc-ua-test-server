# Find the libunwind library
#
#  LIBUNWIND_FOUND       - True if libunwind was found.
#  LIBUNWIND_LIBRARIES   - The libraries needed to use libunwind
#  LIBUNWIND_INCLUDE_DIR - Location of unwind.h and libunwind.h

find_path(LIBUNWIND_INCLUDE_DIR libunwind.h)
find_path(UNWIND_INCLUDE_DIR unwind.h)

find_library(LIBUNWIND_GENERIC_LIBRARY "unwind")

# For some reason, we have to link to two libunwind shared object files:
# one arch-specific and one not.
if (CMAKE_SYSTEM_PROCESSOR MATCHES "^arm")
    set(LIBUNWIND_ARCH "arm")
elseif (CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64" OR CMAKE_SYSTEM_PROCESSOR STREQUAL "amd64")
    set(LIBUNWIND_ARCH "x86_64")
elseif (CMAKE_SYSTEM_PROCESSOR MATCHES "^i.86$")
    set(LIBUNWIND_ARCH "x86")
endif ()

if (LIBUNWIND_ARCH)
    find_library(LIBUNWIND_SPECIFIC_LIBRARY "unwind-${LIBUNWIND_ARCH}")
endif ()

find_package(PackageHandleStandardArgs)

find_package_handle_standard_args(libunwind
    REQUIRED_VARS
      LIBUNWIND_INCLUDE_DIR
      UNWIND_INCLUDE_DIR
      LIBUNWIND_GENERIC_LIBRARY
      LIBUNWIND_SPECIFIC_LIBRARY
    FAIL_MESSAGE
      "Could NOT find libunwind"
)

if (LIBUNWIND_FOUND)
    set(LIBUNWIND_LIBRARIES ${LIBUNWIND_GENERIC_LIBRARY} ${LIBUNWIND_SPECIFIC_LIBRARY} CACHE STRING "")
endif ()

mark_as_advanced(LIBUNWIND_INCLUDE_DIR
                 UNWIND_INCLUDE_DIR
                 LIBUNWIND_GENERIC_LIBRARY
                 LIBUNWIND_SPECIFIC_LIBRARY
                 LIBUNWIND_LIBRARIES)
