# Provides a macro for creating unittest executables using
# the C Unit Test Framework.
# Copyright (C) 2014 Gerhard Gappmeier
#
# Requirements:
# * you need to enable testing using 'enable_testing()' in your top-level
#   CMake file.
# * if you want to support CDash you need to add
#   'include(CTest)' in your top-level cmake file.
#
# Usage:
#  ADD_UNIT_TEST(<TestName> <Source>)
# Example:
#  ADD_UNIT_TEST(String teststring.c)
#
# This creates an executable named test_<TestName> and add a CMake test
# with the same name. Add also adds the test to the INSTALL target, so that
# 'make install' will install them into ${CMAKE_INSTALL_PREFIX}/bin. When
# running the tests using 'make test' it will also use this directory as the
# WORKING_DIRECTORY.
#
# There are two variables you can use to customize the build of unit test
# executables.
#
# * additional sources to compile with each test
#   set(TEST_ADDITIONAL <sources>)
# * libraries to link with each test
#   set(TEST_LIBS <libraries>)
#
# You can change the default working directory of the tests by overriding the
# variable TEST_WORKING_DIRECTORY.
# Note: The default dir may cause problems when using relative INSTALL_PREFIX
# like ../dist. Therefore it is recommended to use absolute paths like
# /usr/local.
#
# You can define paths to prepend to the PATH variable while running the test
# by setting TEST_ENVIRONMENT. On Windows, set this to a semicolon separated
# list, to a colon separated one on all other systems, e.g.:
# Windows: set(TEST_ENVIRONMENT "${TEST_ENVIRONMENT};${uastack_BINARY_DIR}")
# Other:   set(TEST_ENVIRONMENT "${TEST_ENVIRONMENT}:${uastack_BINARY_DIR}")
#
# If you are crosscompiling (CMAKE_CROSSCOMPILING set to ON) you can set the
# variable TEST_CROSSCOMPILE_TARGET to a host where the cross compiled test
# binaries can be run (e.g. root@192.168.10.2).
# The variable TEST_TARGET_DIR contains the path to the binaries on the target
# device.

# default value
set(TEST_WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}/../bin)
# default timeout of a test is 10s
set(DEFAULT_TIMEOUT 10)

# function for creating unit test executables
# @param testname The test name used for this test. This will be prefixed with
#   test_, so "foo" becomes "test_foo"
# @param timeout (optional) timeout for this test. Use this to increase the timeout
#   for certain tests. The default is 10s.
function (ADD_UNIT_TEST testname timeout)
    # strip testname from argument list
    list(REMOVE_AT ARGV 0)
    set(test_${testname}_SRCS ${ARGV1})
    if (${ARGC} GREATER 2)
        set(TEST_TIMEOUT ${ARGV2})
    else ()
        set(TEST_TIMEOUT ${DEFAULT_TIMEOUT})
    endif ()

    if (EXISTS ${testlib_SOURCE_DIR} AND EXISTS ${testlib_BINARY_DIR})
        include_directories(${testlib_SOURCE_DIR})
        include_directories(${testlib_BINARY_DIR})
        if (${CMAKE_SYSTEM_NAME} MATCHES "Windows")
            include_directories(${testlib_SOURCE_DIR}/win32)
            if (MSVC)
                include_directories(${testlib_SOURCE_DIR}/win32/compat)
            endif ()
        endif ()
    endif ()

    add_executable(test_${testname} ${BSP_SRCS} ${test_${testname}_SRCS} ${TEST_ADDITIONAL})
    target_link_libraries(test_${testname} testlib ${TEST_LIBS} ${SYSTEM_LIBS})

    install(TARGETS test_${testname} RUNTIME DESTINATION bin)

    if (NOT IS_ABSOLUTE ${TEST_WORKING_DIRECTORY})
        message(STATUS "Relative path passed to ADD_UNIT_TEST for test ${testname}, making it absolute:")
        message(STATUS "Configured value: ${TEST_WORKING_DIRECTORY}")
        set(TEST_WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/${TEST_WORKING_DIRECTORY}")
        get_filename_component(TEST_WORKING_DIRECTORY ${TEST_WORKING_DIRECTORY} ABSOLUTE)
        message(STATUS "New value: ${TEST_WORKING_DIRECTORY}")
    endif ()

    set(test_COMMAND test_${testname})
    if ((CMAKE_VERSION VERSION_EQUAL "2.8.10" OR CMAKE_VERSION VERSION_GREATER "2.8.10") AND CMAKE_DEBUG_POSTFIX)
        set_target_properties(test_${testname} PROPERTIES DEBUG_POSTFIX ${CMAKE_DEBUG_POSTFIX})
        # for multi-config generators, we use the executable installed to CMAKE_INSTALL_PREFIX/bin instead
        # of the one inside CMAKE_CURRENT_BINARY_DIR, so we have to append CMAKE_DEBUG_POSTFIX to the
        # command as needed
        if (NOT CMAKE_BUILD_TYPE)
            set(test_COMMAND test_${testname}$<$<CONFIG:Debug>:${CMAKE_DEBUG_POSTFIX}>)
        endif ()
    endif ()

    if (TEST_ENVIRONMENT)
        if (${CMAKE_SYSTEM_NAME} STREQUAL "Windows")
            # Replacing ; with \; is necessary here, otherwise TEST_ENVIRONMENT2 is treated as a list
            # http://www.cmake.org/pipermail/cmake/2009-May/029427.html
            string(REGEX REPLACE "([^\\\\]);" "\\1\\\\;" TEST_ENVIRONMENT2 "${TEST_ENVIRONMENT};$ENV{PATH}")
        else ()
            set(TEST_ENVIRONMENT2 "${TEST_ENVIRONMENT}:$ENV{PATH}")
        endif ()
    endif ()

    set(HAVETEST true)
    if (NOT CMAKE_CROSSCOMPILING AND NOT ${CMAKE_SYSTEM_NAME} MATCHES "^(QNX|vxWorks)$")
        # local/normal test execution
        add_test(NAME test_${testname}
                 COMMAND ${test_COMMAND} -s
                 WORKING_DIRECTORY ${TEST_WORKING_DIRECTORY})
        if (TEST_ENVIRONMENT2)
            set_tests_properties(test_${testname} PROPERTIES ENVIRONMENT "PATH=${TEST_ENVIRONMENT2}")
        endif ()
    elseif (CMAKE_CROSSCOMPILING AND ${CMAKE_SYSTEM_NAME} STREQUAL "Windows")
        # Testing mingw crosscompiled windows binary using Wine
        add_test(NAME test_${testname}
            COMMAND wine ${TEST_CROSSCOMPILE_TARGET} $<TARGET_FILE:test_${testname}> -s
                 WORKING_DIRECTORY ${TEST_WORKING_DIRECTORY})
        if (TEST_ENVIRONMENT2)
            set_tests_properties(test_${testname} PROPERTIES ENVIRONMENT "PATH=${TEST_ENVIRONMENT2}")
        endif ()
    elseif (CMAKE_CROSSCOMPILING AND PLATFORM_NAME STREQUAL "xmc4700")
        make_firmware(test_${testname})
        #add_test(NAME test_${testname} COMMAND cgdb -d ${GDB} -nh --command=${GDB_INIT_FILE} $<TARGET_FILE:test_${testname}>)
        add_test(NAME test_${testname} COMMAND ${GDB} -nh --batch $<TARGET_FILE:test_${testname}> --command=${GDB_INIT_FILE})
    elseif (CMAKE_CROSSCOMPILING AND TEST_CROSSCOMPILE_TARGET)
        # Remote execution via SSH
        add_test(NAME test_${testname}
            COMMAND ssh ${TEST_CROSSCOMPILE_TARGET} "cd ${TEST_TARGET_DIR}; ${TEST_TARGET_DIR}/$<TARGET_FILE_NAME:test_${testname}> -s"
                 WORKING_DIRECTORY ${TEST_WORKING_DIRECTORY})
    else ()
        # no test will be generated
        set(HAVETEST false)
    endif ()

    if (HAVETEST)
        set_target_properties(test_${testname} PROPERTIES FOLDER UnitTests)
        set_tests_properties(test_${testname} PROPERTIES TIMEOUT ${TEST_TIMEOUT})

        include(VSSetDebugConfig)
        vs_set_debug_config(TARGET test_${testname} INSTALL_DIR_EXECUTABLE WORKING_DIR "${CMAKE_INSTALL_PREFIX}/bin")
    endif ()
endfunction ()
