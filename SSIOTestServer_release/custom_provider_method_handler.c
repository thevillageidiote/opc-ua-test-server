/*
Declares the custom method call handler that processes the
method call request and calls the appropriate method
from g_methods[]
*/

#include <uaserver/addressspace/method.h>
#include <uaprovider/provider.h>
#include <uabase/identifiers.h>
#include <uabase/statuscodes.h>
#include <uabase/valuerank.h>
#include <uabase/nodeid.h>

#include <trace/trace.h>

#include "custom_provider_nodes.h"
#include "custom_provider_method_handler.h"
#include "numNodeDefinition.h"



#ifdef ENABLE_SERVICE_CALL

typedef ua_statuscode (*fctMethodCall)(const struct ua_callmethodrequest *req, struct ua_callmethodresult *res, struct ua_nodeid objectid);


//There are 3 methods in each port
//Initialised to NULL to allow the associate_function method to automatically
//assign indexes
static fctMethodCall g_methods[MAXASICNO * MAXPORTSPERASIC * 3 + 1] = { NULL };

/**
 * @brief calls methods of custom proiver
 *
 * @param ctx call context
 * @return void
 *
 */
/*! [custom_call_handler] */
void custom_provider_method_call(struct uaprovider_call_ctx *ctx)
{
    int i;
    struct ua_callrequest *req = ctx->req;
    struct ua_callresponse *res = ctx->res;
    ua_node_t method;
    uint16_t ns_index;
    unsigned int method_index;
    fctMethodCall fctmethod;


    for (i = 0; i < req->num_methods_to_call; i++) {

        ns_index = req->methods_to_call[i].method_id.nsindex;
        if (!uaprovider_contains_namespace(ctx->cur_provider, ns_index)) {
            /* skip node from other namespace */
            continue;
        }

        /* find the node handle */
        method = ua_node_find(&req->methods_to_call[i].method_id);
        if (method == UA_NODE_INVALID) {
            res->results[i].status_code = UA_SCBADNODEIDUNKNOWN;
            continue;
        }

#ifdef UA_AUTHORIZATION_SUPPORT
        /* check if the user is allowed to execute the method */
        res->results[i].status_code = ua_authorization_is_executable(method, &ctx->session->user_ctx);
        if (res->results[i].status_code != 0) continue;
#endif

        /* find the function to call */
        if (ua_method_get_index(method, &method_index) == 0 && method_index < countof(g_methods)) {
            fctmethod = g_methods[method_index];
            if (fctmethod) {
                /* call the function */
                res->results[i].status_code = fctmethod(&req->methods_to_call[i], &res->results[i], req->methods_to_call[i].object_id);
            } else {
                res->results[i].status_code = UA_SCBADINTERNALERROR;

            }
        } else {
            res->results[i].status_code = UA_SCBADINTERNALERROR;

        }
    }

    uaserver_call_complete(ctx);
}
/*! [custom_call_handler] */

/**
 * @brief Associates method to be called with method_node and stores it
 *        in the g_methods array under the appropriate identifier
 *
 * @param ctx call context
 * @return void
 *
 */
/*! [custom_call_handler] */

int custom_provider_init_method(ua_node_t method_node, fctMethodCall usrMethod )
{
    int ret, i;

    for (i=0;i<countof(g_methods); i++)
    {

        if (g_methods[i] == NULL)
        {
            //If index in g_methods is not used yet, store function there
            //and associate it with the method_node
            ret = ua_method_set_index(method_node, i);   
            if(ret!=0) return -1;                       
            g_methods[i] = usrMethod;
            return 0;
        }
    }
    //Return Error if method has not been assigned by the end of the for loop
    TRACE_ERROR(TRACE_FAC_PROVIDER, "Not enough memory in g_methods");
    return -1;

}

#endif /* ENABLE_SERVICE_CALL */
