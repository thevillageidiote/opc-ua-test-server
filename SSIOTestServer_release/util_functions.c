//Includes for String functions
#include <string.h>
#include <stdlib.h>
#include <uaserver/addressspace/addressspace.h>


//Includes for Node Searching
#include <uaserver/addressspace/reference.h>
#include <uabase/qualifiedname.h>
#include <uabase/nodeid.h>

//Includes for Trace
#include <trace/trace.h>

#include "util_functions.h"

int util_appendCharToString (char * input_string, size_t size, char c)
{

    int StringLength = strlen(input_string);
    //Check if there is space for one more character
    if (StringLength+1 <= size)
    {
        input_string[StringLength] = c;
        input_string[StringLength+1] = '\0';
        return 0;
    }
    else
    {
        return -1;
    }


}

uint16_t util_returnNofromString (const char * input_string)
{
    int numberLength = 2;
    char * temp = (char*) malloc((numberLength+1) * sizeof(char));
    temp[0]='\0';
    int count = 0;

    int number;
    
    while(input_string[count] != '\0')
    {
        //If character is a number
        if (input_string[count]<=57 && input_string[count] >=48)
        {
            util_appendCharToString (temp,numberLength+1,input_string[count]);
        }

        count ++;
    }

    number = atoi(temp);

    if(number <= 65535)
    {
        return (uint16_t) number;
    }
    else 
    {
        TRACE_ERROR(TRACE_FAC_PROVIDER, "Number Overflow");
        return 0;
    }

}




const char * util_splitString (const char * input_string, char delimiter, int index)
{

    int i,StringLength;
    int count = 0;
    const char *output_string;



    StringLength = strlen(input_string);
    char * temp = (char*) malloc((StringLength+1) * sizeof(char));

    for (i=0; i< index; i++)
    {
        //Initialise temp string to empty
        temp[0] = '\0';
        //Loop throught the string to check for delimiter
        //and store the string to temp
        while(input_string[count] != '\0' && input_string[count]!=delimiter)
        {
            util_appendCharToString(temp,StringLength,input_string[count]);
            count ++;
        }
        //increment past the delimiter encountered previously
        count ++;
    }
    output_string = temp;
    return output_string;
}



ua_node_t util_findNodefromInverseRef(ua_node_t input_node, uint16_t nsidx,const char * TargetBrowseName)
{

    int ret;
    ua_node_t output_node;
    ua_ref_t ref;

    struct ua_qualifiedname browsename = UA_QUALIFIEDNAME_INITIALIZER;
    struct ua_qualifiedname currentvalue = UA_QUALIFIEDNAME_INITIALIZER;

    //Set Qualified Name for Target Node Id
    ua_qualifiedname_set(&currentvalue,nsidx,TargetBrowseName);

    //Search Inverse References for output Node

    // Iterate through all inverse references of the Input to find output node
    ua_node_foreach_inv(ref,input_node)
    {
        output_node = ua_reference_source(ref);
        ret = ua_node_get_browsename(output_node, &browsename);
        if (ret != 0) continue;

        //Check if qualified names match
        if(ua_qualifiedname_compare(&browsename, &currentvalue) == 0)
        {
            return output_node;
            
        }

    }
    return UA_NODE_INVALID;
}

ua_node_t util_findNodefromForwardRef(ua_node_t input_node, uint16_t nsidx,const char * TargetBrowseName)
{

    int ret;
    ua_node_t output_node;
    ua_ref_t ref;


    struct ua_qualifiedname browsename = UA_QUALIFIEDNAME_INITIALIZER;
    struct ua_qualifiedname currentvalue = UA_QUALIFIEDNAME_INITIALIZER;

    //Set Qualified Name for Target Node Id
    ua_qualifiedname_set(&currentvalue,nsidx,TargetBrowseName);

    //Search Inverse References for output Node

    // Iterate through all inverse references of the Input to find output node
    ua_node_foreach(ref,input_node)
    {
        output_node = ua_reference_target(ref);
        ret = ua_node_get_browsename(output_node, &browsename);
        if (ret != 0) continue;

        //Check if qualified names match
        if(ua_qualifiedname_compare(&browsename, &currentvalue) == 0)
        {
            //TRACE_ERROR(TRACE_FAC_PROVIDER,"%s\t%s\n",ua_string_const_data(&currentvalue.name),ua_string_const_data(&browsename.name));
            return output_node; 
        }
    }
    return UA_NODE_INVALID;
}



static void util_deleteChildNodeAndReferencesRecursive(ua_node_t input_node,ua_node_t parent)
{
    ua_ref_t ref,prev_ref,temp_ref;
    ua_node_t ref_type;
    struct ua_nodeid nodeid;

    
    //Delete all references in port
    ref = ua_node_first_reference(input_node);
    
    //Iterate through all references in port to delete them
    while(ref != UA_REF_INVALID)
    {   
        //This first part is a recursive call to walk through and delete child nodes/references
        //Check if reference is HASCOMPONENT or HASPROPERTY 
        //and check if the reference source is the current input_node
        //This is so that only child nodes get deleted
        ref_type = ua_reference_type(ref);

        if((ref_type == UA_NODE_HASCOMPONENT || ref_type == UA_NODE_HASPROPERTY) 
            && ua_reference_source(ref) == input_node)
        {
            //Recursive call to remove all child nodes
            util_deleteChildNodeAndReferencesRecursive(ua_reference_target(ref),input_node);                  
        }      

        //printReference(ref);

        //Remove Reference and check if prev_ref returned is invalid (i.e. all other references have been deleted)
        //advance reference count first
        prev_ref = ref;
        ref = ua_node_next_reference(ref);
        ua_reference_remove(prev_ref,&temp_ref);               
    }

    //End of loop reached. Delete input_node and break loop
    ua_node_get_nodeid(input_node,&nodeid);
    TRACE_ERROR(TRACE_FAC_PROVIDER,"Deleting Node %s\n",ua_string_const_data(nodeid.identifier.string));
    ua_node_delete(input_node);

}



int util_deleteNodeRecursive(ua_node_t input_node,ua_node_t parent)
{

    ua_ref_t ref,prev_ref;

    struct ua_qualifiedname browsename = UA_QUALIFIEDNAME_INITIALIZER;
    struct ua_qualifiedname currentvalue = UA_QUALIFIEDNAME_INITIALIZER;
    
    //Iterate through all references in parent to delete references to the input_node
    //This is placed outside the recursion so that it is only called on the outermost node
    ua_node_get_browsename(input_node,&browsename);
    ua_node_foreach(ref,parent)
    {
        ua_node_get_browsename(ua_reference_target(ref),&currentvalue);

        if(ua_qualifiedname_compare(&browsename, &currentvalue) == 0)
        {
            ua_reference_remove(ref,&prev_ref);
        }
    }

    util_deleteChildNodeAndReferencesRecursive(input_node,parent);

}


