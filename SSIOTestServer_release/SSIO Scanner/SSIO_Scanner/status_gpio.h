/*
 * status_gpio.h
 *
 *  Created on: 18-Jan-2017
 *      Author: yocto
 */

#ifndef STATUS_GPIO_H_
#define STATUS_GPIO_H_

/* GPIO's for driving light stand status indicator */
/* GPIO2b[0..9] corresponds to pins 160..169 */
/*
#define LS_RED      164     GPIO2b[4]
#define LS_ORANGE   165     GPIO2b[5]
#define LS_GREEN    168     GPIO2b[8]
#define LS_SPARE    169     GPIO2b[9]*/

#define LOW  0
#define HIGH 1

#define IN  0
#define OUT 1

#define LINUX_PIN_GPIO2b 406

/* GPIO's for driving light stand status indicator */
/* We are not using  it for now */
#if 0

#define LS_RED      (LINUX_PIN_GPIO2b + 4)
#define LS_ORANGE   (LINUX_PIN_GPIO2b + 5)
#define LS_GREEN    (LINUX_PIN_GPIO2b + 8)
#define LS_SPARE    (LINUX_PIN_GPIO2b + 9)

#endif

/* On board status LED's on LCES board(Bestla)*/
#define LED_1_RED	18
#define LED_1_GREEN	17

#define LED_2_RED	13
#define LED_2_GREEN	14

/* Enum of LED */
typedef enum _STATUS_LED
{
	LED_ONE = 0,
	LED_TWO,
} STATUS_LED;

typedef enum _STATUS_LED_STATES
{
	LED_OFF = 0x0,
	LED_RED = 0x1,
	LED_GRN = 0x2,
	LED_ORA = 0x4,
} STATUS_LED_STATES;

typedef enum _LIGHTSTAND_STATES
{
	LIGHT_OFF = 0x0,
	LIGHT_ON,
} LIGHTSTAND_STATE;

void StatusGPIOInit(void);

#endif /* STATUS_GPIO_H_ */
