/*
 * data_store.h
 *
 *  Created on: 14-Jan-2017
 *      Author: Sasi
 */

#ifndef DATA_STORE_H_
#define DATA_STORE_H_

extern u_int16_t delay;

/* Type def of function configuration types */
enum _IO_TYPE
{
	NO_CONFIG = 0,
	AI_TYPE,
	AO_TYPE,
	DI_TYPE,
	DO_TYPE,
	MB_TYPE
};

enum _CTRL_TYPE
{
	NO_CTRL      = 0,
	CTRL_PID,
	CTRL_DO,
	CTRL_LAD_AND,
	CTRL_LAD_OR,
	CTRL_LAD_XOR,
	CTRL_LAD_NOT
};

/* IO Type is in lower 6 bits of Config */
#define IO_TYPE_MASK 0x3F

/* HART Flag is in bit 7 of Config */
#define HART_ENABLE_FLAG 0x40

#define SHARED_MEM_NAME "SSIOSHM"

#define SEM_NAME "SSIOSEM7"

/***** Shared DataStore Structure ******/

/*
 * NOTE This struct MUST be packed, but very careful checking needs to be done
 *      in order to ensure that all values align correctly,
 *      e.g. floats on DWORD boundary !!
 *
 *      This maps directly on to a 16 bit array for use as Modbus holding registers
 */

typedef struct __attribute__((__packed__)) _SSIO_NODE_DS
{
    /* 40001 = CONFIGUATION ATTRIBUTES */
	u_int16_t	ioType;			/* 40001 */
	u_int16_t	sci;			/* 40002 */
	float		loScale;		/* 40003 */
	float		hiScale;		/* 40005 */
	u_int16_t	pidEnable; 		/* 40007 */
	u_int16_t	pidSourceNode;	/* 40008 */
	float		pidSetpoint;	/* 40009 */
	float		pidPBAND;		/* 40011 */
	u_int16_t	hartEnable;		/* 40013 */
	u_int16_t	alarmEnable;	/* 40014 */
	float		loAlarmLimit;	/* 40015 */
	float		hiAlarmLimit;	/* 40017 */
	u_int16_t	stateAlarm;		/* 40019 */
	u_int16_t	ledStatus;		/* 40020 */
	/* 40022 = AI FUNCTION */
	float		aiValue;		/* 40021 */
	u_int16_t	aiStatus;		/* 40023 */
	u_int32_t	aiTimestamp;	/* 40024 */ // BCS timestamp variable changed to 32 bit
	u_int16_t	aiRawC;			/* 40025 */
	u_int16_t 	aiOSV;			/* 40026 */
	u_int16_t 	pad2_2;			/* 40027 */
	u_int16_t 	pad2_3;			/* 40028 */
	u_int16_t 	pad2_4;			/* 40029 */
	u_int16_t 	pad2_5;			/* 40030 */
	/* 40031 = AO FUNCTION */
	float		aoValue;		/* 40031 */
	u_int16_t	aoStatus;		/* 40033 */
	u_int32_t	aoTimestamp;	/* 40034 */ // BCS timestamp variable changed to 32 bit
	u_int16_t	aoRawC;  		/* 40035 */
	u_int16_t 	aoFSEnable;		/* 40036 */
	u_int16_t 	aoFSDelay;		/* 40037 */
	u_int16_t 	aoFSValue;		/* 40038 */
	u_int16_t 	aoFailsafe;		/* 40039 */
	u_int16_t 	pad3_6;			/* 40040 */
	/* 40041 = DI VALUE */
	u_int16_t	diValue;		/* 40041 */
	u_int16_t	diStatus;		/* 40042 */
	u_int32_t	diTimestamp;	/* 40043 */ // BCS timestamp variable changed to 32 bit
	u_int16_t	pad4_1;			/* 40044 */
	u_int16_t 	pad4_2;			/* 40045 */
	u_int16_t 	pad4_3;			/* 40046 */
	u_int16_t 	pad4_4;			/* 40047 */
	u_int16_t 	pad4_5;			/* 40048 */
	u_int16_t 	pad4_6;			/* 40049 */
	u_int16_t	pad4_7;			/* 40050 */
	/* 40051 = DO VALUE */
	u_int16_t	doValue;		/* 40051 */
	u_int16_t	doStatus;		/* 40052 */
	u_int32_t	doTimestamp;	/* 40053 */ // BCS timestamp variable changed to 32 bit
	u_int16_t	doFSEnable;		/* 40054 */
	u_int16_t 	doFSDelay;		/* 40055 */
	u_int16_t 	doFSValue;		/* 40056 */
	u_int16_t 	doFailsafe;		/* 40057 */
	u_int16_t 	pad5_5;			/* 40058 */
	u_int16_t 	pad5_6;			/* 40059 */
	u_int16_t 	pad5_7;			/* 40060 */
	/* 40061 = HART */
	float		hartPV;			/* 40061 */
	float		hartSV;			/* 40063 */
	float		hartTV;			/* 40065 */
	float		hartQV;			/* 40067 */
	u_int16_t 	hartStatus;		/* 40069 */
	u_int16_t 	pad6_1;			/* 40070 */
	/* 40071 = Control Out */
	float		ciValue;		/* 40071 */
	u_int16_t	ciStatus;		/* 40073 */
	u_int16_t	ciTimestamp;	/* 40074 */
	float 		pidError;		/* 40075 */
	u_int16_t	pidIncOpt;		/* 40077 */
	u_int16_t	doInvOpt;		/* 40078 */
	u_int16_t	ci_diValue;		/* 40079 */
	u_int16_t	pad7_6;			/* 40080 */
	/* 40081 = Peer to Peer data */
	float		ai_pValue;		/* 40081 */
	u_int16_t	ai_pStatus;		/* 40083 */
	u_int16_t	ai_pTimestamp;	/* 40084 */
	u_int16_t    di_pValue;      /* 40085 */
	u_int16_t    di_pStatus;     /* 40086 */
	u_int16_t    di_pTimestamp;  /* 40087 */
	u_int16_t    pad8_1;         /* 40088 */
	u_int16_t    pad8_2;         /* 40089 */
	u_int16_t    pad8_3;         /* 40090 */
	/* 40091 = Nanodac Modbus Data */
	float      channel1_PV;     /* 40091 */
	u_int16_t   pad9_1;          /* 40093 */
	u_int16_t   channel1_Status; /* 40094 */
	u_int16_t   pad9_2;          /* 40095 */
	u_int16_t   channel1_alarm1; /* 40096 */
	u_int16_t   pad9_3;          /* 40097 */
	u_int16_t   channel1_alarm2; /* 40098 */
	float      channel2_PV;     /* 40099 */
	u_int16_t   pad9_4;          /* 40101 */
	u_int16_t   channel2_Status; /* 40102 */
	u_int16_t   pad9_5;          /* 40103 */
	u_int16_t   channel2_alarm1; /* 40104 */
	u_int16_t   pad9_6;          /* 40105 */
	u_int16_t   channel2_alarm2; /* 40106 */
	float      channel3_PV;     /* 40107 */
	u_int16_t   pad9_7;          /* 40109 */
	u_int16_t   channel3_Status; /* 40110 */
	u_int16_t   pad9_8;          /* 40111 */
	u_int16_t   channel3_alarm1; /* 40112 */
	u_int16_t   pad9_9;          /* 40113 */
	u_int16_t   channel3_alarm2; /* 40114 */
	float      channel4_PV;     /* 40115 */
	u_int16_t   pad9_10;         /* 40117 */
	u_int16_t   channel4_Status; /* 40118 */
	u_int16_t   pad9_11;         /* 40119 */
	u_int16_t   channel4_alarm1; /* 40120 */
	u_int16_t   pad9_12;         /* 40121 */
	u_int16_t   channel4_alarm2; /* 40122 */
	u_int16_t   globalAlarmAck;  /* 40123 */

} SSIO_NODE_DS, *SSIO_NODE_DS_PTR;

/* THE ONE AND ONLY global data store */
extern SSIO_NODE_DS dataStore;

//extern SSIO_NODE_DS *shm;


/* Global data store locking functions */
int ds_init(u_int8_t iotype);
void ds_lock(void);
void ds_unlock(void);

#endif /* DATA_STORE_H_ */
