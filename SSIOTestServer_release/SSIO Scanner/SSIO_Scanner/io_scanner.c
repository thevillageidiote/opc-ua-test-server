/*
 * io_scanner.c
 *
 *  Created on: 14-Jan-2017
 *      Author: Sasi
 */
/* includes */


#include <stdio.h>
#include <linux/rtc.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/types.h>

#include "ssio_node.h"
#include "data_store.h"


/* Comment this if LED configuration is not enabled
 * For the intial development , this is not enabled so it is commented
 */

//#define LED_CONFIG_ENABLED

#define LOG_OUTPUT

/* Global Alarm status */
u_int8_t gInAlarm = FALSE;

/* Global IO Bad status */
u_int8_t gIoBad = FALSE;

/* Global In Control status */
u_int8_t gOnControl = FALSE;

/* Global Ladder OUT state */
u_int8_t gLadderState = FALSE;

/* GLOBAL DATA - RAW COUNTS LOW LIMIT AND SPAN BASED ON SCI TYPES */
/* SCI index    =         0,        1,        2,       3          */
float raw_low[] = { 0.0f, 0.0f, 1600.0f, 12800.0f };
float sptbl[] = { 64000.0f, 64000.0f, 62400.0f, 51200.0f };

#define GOOD        0x00
#define BAD_LOOR    0x01
#define BAD_HOOR    0x02

/* BCS modified to use 32 bit value to store the seconds as 16 bit values is not sufficient
 * But this requires change in the datastore structure and spi frame format.Currently only 
 * datastore is changed*/
/* Basic timestamping - use seconds since midnight from on-board battery backed RTC */

u_int32_t GetTimeStamp(void)
{

	int fd;
	struct rtc_time rtc_tm;
	int ret;
	u_int32_t timeloc;

	fd = open("/dev/rtc", O_RDONLY, 0);

	/*the ioctl command RTC_RD_TIME is used to read the current timer.	*/
	ret = ioctl(fd, RTC_RD_TIME, &rtc_tm);
	timeloc = rtc_tm.tm_sec + (rtc_tm.tm_min * 60) + (rtc_tm.tm_hour * 3600);
	/*printf("seconds %d\n",rtc_tm.tm_sec);
	printf("minutes %d\n",rtc_tm.tm_min);
	printf("hours %d\n",rtc_tm.tm_hour);	
	printf("time %d\n",timeloc);*/

	close(fd);
	return timeloc;
}

/* Test for Raw Counts value against valid range */
u_int16_t out_of_range(float raw_value, short sci, float osv)
{
    /* do Low Range check */
	if (raw_value <= raw_low[sci] - osv)
	{
		return BAD_LOOR;
	}
	/* do High Range check */
	else if (raw_value >= sptbl[sci] + raw_low[sci] + osv)
	{
		return BAD_HOOR;
	}
	else
	{
		return GOOD;
	}
}

/* Convert from raw counts to engineering units */
float conv_raw_to_eng(float value, /* value to be converted             */
	short scix,  /* index to the conversion algorithm */
	float span,  /* (high scale - low scale) /k       */
	float offset /* low scale value                   */)
{
	switch (scix)
	{
	case 0: /* no linearization */
		return (value);

	case 1: /* linear (0 - 64000) */
		return (float)((double) value * (double) span + (double) offset);

	case 2: /* linear (1600 - 64000) */
		return (float)(((double) value - 1600.0) * (double) span + (double) offset);

	case 3: /* linear (12800 - 64000) */
		return (float)(((double) value - 12800.0) * (double) span + (double) offset);

	}

	return value;
}

/* Function Block operations */
void AI_FUNCTION(A2L_MSG_PTR msg, u_int32_t timeStamp)
{
	/* Get Raw Counts as float from message */
	float pvRaw = (float)msg->a2lPVraw.lVal;	

	/* Save Raw Counts value for display */
	dataStore.aiRawC = (u_int16_t)msg->a2lPVraw.lVal;
	
	// BCS timestamp the rawcounts as scaling is done by BCSIOAPP
	dataStore.aiTimestamp = timeStamp;
		
	/* Calculate OSV and span based on SCI */
	float osv_raw = (float)(((double)dataStore.aiOSV / 100.0f) *(double)sptbl[dataStore.sci]);	
	float span = (float)(((double)dataStore.hiScale - (double)dataStore.loScale) / (double)sptbl[dataStore.sci]);
	
	/* Check for range error on RAWC */
	u_int16_t code = out_of_range(pvRaw, dataStore.sci, osv_raw);
	
	/* If we are not OOR on RAWC process the conversion (otherwise hold LGV) */
	if (code == GOOD)
	{
	    /* Convert from PV Raw Counts to Engineering Units */
		pvRaw = conv_raw_to_eng(pvRaw, dataStore.sci, span, dataStore.loScale);

		/* TODO Scaling  */

		/*** CHECK AND CLAMP IF VALUE IN LIMITS ***/
		float lscale = (float)((double)dataStore.loScale - (double)osv_raw);
		float hscale = (float)((double)dataStore.hiScale + (double)osv_raw);

		if (pvRaw <= lscale)
		{
			pvRaw = lscale;
			code = BAD_LOOR;
		}
		else
		{
			if (pvRaw >= hscale)
			{
				pvRaw = hscale;
				code = BAD_HOOR;
			}
		}

		/* ONLY process if clamped value is not OOR (otherwise hold LGV) */
		if (code == GOOD)
		{
		    /* TODO Filtering */

		    /* UPDATE POINT VALUE */
			dataStore.aiValue = pvRaw;
			

			/* Timestamp - TODO - use timestamp from ASIC ? */
			dataStore.aiTimestamp = timeStamp; //msg->a2lTimestamp.sVal;
		}
	}	

	/* Update block BAD status and limit indicators */
	if (code & BAD_LOOR)
	{
		/* Set Low Out of Range and BAD status */
		dataStore.aiStatus |= (PV_BAD | LIM_LOW);

		/* Clear High Out of Range */
		dataStore.aiStatus &= ~(LIM_HIGH);
	}

	else if (code & BAD_HOOR)
	{
		/* Set High Out of Range and BAD status */
		dataStore.aiStatus |= (PV_BAD | LIM_HIGH);

		/* Clear Low Out of Range */
		dataStore.aiStatus &= ~(LIM_LOW);
	}
	else
	{
		/* Clear Status */
		dataStore.aiStatus = 0;
	}

	/* Status OR in status from ASIC */
	dataStore.aiStatus |= (u_int16_t)msg->a2lStatus;

	/* Set global IO Status */
	if (dataStore.aiStatus != 0)
	{
		gIoBad = TRUE;
	}

	/* MEAS Alarming */
	if (dataStore.alarmEnable)
	{
	    /* Check if alarm limits exceeded */
		if ((dataStore.aiValue > dataStore.hiAlarmLimit) || (dataStore.aiValue < dataStore.loAlarmLimit))
		{
			gInAlarm = TRUE;
		}
		else
		{
			gInAlarm = FALSE;
		}
	}
	else
	{
		gInAlarm = FALSE;
	}
}

void AO_FUNCTION(L2A_MSG_PTR msg)
{
	/* Failsafe Counter */
	static u_int16_t fsCount = 0;	
	
	/* Test for OOS or BAD status */
	if (dataStore.aoStatus & (PV_OOS | PV_BAD))
	{
	    /* Increment Failsafe counter until we reach trigger */
		if (fsCount <= dataStore.aoFSDelay)
		{
			fsCount++;
		}

		/* Set global IO Status */
		gIoBad = TRUE;
	}
	else
	{
		fsCount = 0;
	}

	/* Check for FailSafe Enable */
	if (dataStore.aoFSEnable != 0)
	{
	    /* Check for FailSafe activation */
		if (fsCount > dataStore.aoFSDelay)
		{
		    /* We are in Fail Safe */
			dataStore.aoFailsafe = 0xFFFF; /* TRUE */

			/* Set Fail Save output value */
			msg->l2aPVraw = dataStore.aoFSValue;
			dataStore.aoRawC = dataStore.aoFSValue;

			/* TODO - what do we do about alarms in Failsafe ?? */
			gInAlarm = FALSE;

			return;
		}
	}
	else
	{
	    /* Reset Fail Safe counter if Failsafe is not enabled */
		fsCount = 0;
	}

	/*  We are not in fail safe - so do normal AO function */
	dataStore.aoFailsafe = 0x0000; /* FALSE */	

	/* Convert from Engineering Units to PV Raw Counts */
	switch (dataStore.sci)
	{
	/* TODO - Add SCI's as required */
	case 0: /* No conditioning */
		msg->l2aPVraw = (u_int16_t)dataStore.aoValue;
		
		/* Update to the Redis server only if it is initialized */
		//if (redis_initialized)
			//msg->l2aPVraw =  Redis_getAORawCount();		
		break;
	case 1: /* 0 to 64000 inverse linear */
		msg->l2aPVraw = (dataStore.aoValue - dataStore.loScale) * 64000 / (dataStore.hiScale - dataStore.loScale);
		break;
	case 2: /* 1600 to 64000 inverse linear */
		msg->l2aPVraw = ((dataStore.aoValue - dataStore.loScale) * 62400 / (dataStore.hiScale - dataStore.loScale)) + 1600;
		break;
	case 3: /* 12800 to 64000 inverse linear */
		msg->l2aPVraw = ((dataStore.aoValue - dataStore.loScale) * 51200 / (dataStore.hiScale - dataStore.loScale)) + 12800;
		break;
	default:
		msg->l2aPVraw = (u_int16_t)dataStore.aoValue;
	}

	/* Save calculated RAWC for display */
	dataStore.aoRawC = (u_int16_t)msg->l2aPVraw;

	/* Timestamp - TODO - mapping ? */
	msg->l2aTimestamp = dataStore.aoTimestamp;

	/* OUT Alarming */
	if (dataStore.alarmEnable)
	{
	    /* Check if alarm limits exceeded */
		if ((dataStore.aoValue > dataStore.hiAlarmLimit) || (dataStore.aoValue < dataStore.loAlarmLimit))
		{
			gInAlarm = TRUE;
		}
		else
		{
			gInAlarm = FALSE;
		}
	}
	else
	{
		gInAlarm = FALSE;
	}
}

void DI_FUNCTION(A2L_MSG_PTR msg, u_int32_t timeStamp)
{
	/* Status TODO - mapping ? */
	dataStore.diStatus = (u_int16_t)msg->a2lStatus;

	/* Set global IO Status */
	if (dataStore.diStatus != 0)
	{
		gIoBad = TRUE;
	}

	/* Convert from PV raw to Integer (Boolean) */
	if (msg->a2lPVraw.lVal)
	{
		dataStore.diValue = 0xFFFF; /* TRUE */
	}
	else
	{
		dataStore.diValue = 0x0000; /* FALSE */
	}	

	/* Timestamp - TODO Use timestamp from ASIC ? */
	dataStore.diTimestamp = timeStamp; //msg->a2lTimestamp.sVal;

	/* STATE Alarming */
	if (dataStore.alarmEnable)
	{
	    /* Check if in alarm state */
		if (dataStore.diValue != 0)
		{
			gInAlarm = dataStore.stateAlarm ? TRUE : FALSE;
		}
		else
		{
			gInAlarm = dataStore.stateAlarm ? FALSE : TRUE;
		}
	}
	else
	{
		gInAlarm = FALSE;
	}

}

void DO_FUNCTION(L2A_MSG_PTR msg)
{
	/* Failsafe Counter */
	static u_int16_t fsCount = 0;
	
	/* Test for OOS or BAD status */
	if (dataStore.doStatus & (PV_OOS | PV_BAD))
	{
	    /* Increment Failsafe counter until we reach trigger */
		if (fsCount <= dataStore.doFSDelay)
		{
			fsCount++;
		}

		/* Set global IO Status */
		gIoBad = TRUE;
	}
	else
	{
		fsCount = 0;
	}

	/* Check for FailSafe Enable */
	if (dataStore.doFSEnable != 0)
	{
	    /* Check for FailSafe activation */
		if (fsCount > dataStore.doFSDelay)
		{
		    /* We are in Fail Safe */
			dataStore.doFailsafe = 0xFFFF; /* TRUE */

			/* Set Fail Save output value */
			if (dataStore.doFSValue)
			{
				msg->l2aPVraw = 0xFFFF; /* TRUE */
			}
			else
			{
				msg->l2aPVraw = 0x0000; /* FALSE */
			}

			/* TODO - what do we do about alarms in Failsafe ?? */
			gInAlarm = FALSE;

			return;
		}
	}
	else
	{
	    /* Reset Fail Safe counter if Failsafe is not enabled */
		fsCount = 0;
	}

	/*  We are not in fail safe - so do normal AO function */
	dataStore.doFailsafe = 0x0000; /* FALSE */	
		
	/* Convert from Integer to PV Raw */
	if (dataStore.doValue)
	{
		msg->l2aPVraw = /*1;*/0xFFFF; /* TRUE */
	}
	else
	{
		msg->l2aPVraw = 0x0000; /* FALSE */
	}

	/* Timestamp - TODO - mapping ? */
	msg->l2aTimestamp = dataStore.aoTimestamp;

	/* STATE Alarming */
	if (dataStore.alarmEnable)
	{
	    /* Check if in alarm state */
		if (dataStore.doValue != 0)
		{
			gInAlarm = dataStore.stateAlarm ? TRUE : FALSE;
		}
		else
		{
			gInAlarm = dataStore.stateAlarm ? FALSE : TRUE;
		}
	}
	else
	{
		gInAlarm = FALSE;
	}
}

/* Do PID */
void PID_FUNCTION(L2A_MSG_PTR msg)
{
    /* Failsafe Counter */
	static u_int16_t fsCount = 0;

	/* Test for OOS or BAD status */
	if (dataStore.ai_pStatus & (PV_OOS | PV_BAD))
	{
	    /* Increment Failsafe counter until we reach trigger */
		if (fsCount <= dataStore.aoFSDelay)
		{
			fsCount++;
		}

		/* Set global IO Status */
		gIoBad = TRUE;
	}
	else
	{
		fsCount = 0;
	}

	/* Check for FailSafe Enable */
	if (dataStore.aoFSEnable != 0)
	{
	    /* Check for FailSafe activation */
		if (fsCount > dataStore.aoFSDelay)
		{
		    /* We are in Fail Safe */
			dataStore.aoFailsafe = 0xFFFF; /* TRUE */

			/* Set Fail Save output value */
			msg->l2aPVraw = dataStore.aoFSValue;
			dataStore.aoRawC = dataStore.aoFSValue;

			/* TODO - what do we do about alarms in Failsafe ?? */
			gInAlarm = FALSE;

			return;
		}
	}
	else
	{
	    /* Reset Fail Safe counter if Failsafe is not enabled */
		fsCount = 0;
	}

	/*  We are not in fail safe - so do normal AO function */
	dataStore.aoFailsafe = 0x0000; /* FALSE */

	/* Step 1 - Calculate and update PID Error (use double to avoid rounding !) */
	dataStore.pidError = (float)((double)dataStore.pidSetpoint - (double)dataStore.ai_pValue);

	/* Step 2 - Clamp PBAND */
	float clamped_pband = dataStore.pidPBAND;

	if (clamped_pband < 0.1F)
	{
		clamped_pband = 0.1F;
	}

	/* Step 3 - Calculate P Term */
	float p_term = (float)(((double)dataStore.pidError * 100.0) / (double)clamped_pband);

	/* Step 4 - Invert P Term (if configured) */
	if (dataStore.pidIncOpt)
	{
		p_term = -p_term;
	}

	/* Step 5 - Set clamped output */
	float holimit = dataStore.hiScale;
	float lolimit = dataStore.loScale;

	if (holimit < lolimit)
	{
		holimit = lolimit;
	}

	/* Check output against Lo Limit */
	if (p_term <= lolimit)
	{
		dataStore.ciValue = lolimit;

		/* Indicate Output Limited LOW */
		dataStore.ciStatus |= LO_LIMIT;
		dataStore.ciStatus &= ~(HO_LIMIT);
	}
	else if (p_term >= holimit)
	{
		dataStore.ciValue = holimit;

		/* Indicate Output Limited HIGH */
		dataStore.ciStatus |= HO_LIMIT;
		dataStore.ciStatus &= ~(LO_LIMIT);
	}
	else
	{
		dataStore.ciValue = p_term;

		/* Indicate Output Limited LOW */
		dataStore.ciStatus &= ~(LO_LIMIT);
		dataStore.ciStatus &= ~(HO_LIMIT);
	}

	    /* TODO - Additional status bits ?? */
	    //dataStore.ciStatus;

	/* Propagate time from AI input */
	dataStore.ciTimestamp = dataStore.ai_pTimestamp;

	/* Convert from Engineering Units to PV Raw Counts */
	switch (dataStore.sci)
	{
	/* TODO - Add SCI's as required */
	case 0: /* No conditioning */
		msg->l2aPVraw = (u_int16_t)dataStore.ciValue;
		break;
	case 1: /* 0 to 64000 inverse linear */
		msg->l2aPVraw = (dataStore.ciValue - dataStore.loScale) * 64000 / (dataStore.hiScale - dataStore.loScale);
		break;
	case 2: /* 1600 to 64000 inverse linear */
		msg->l2aPVraw = ((dataStore.ciValue - dataStore.loScale) * 62400 / (dataStore.hiScale - dataStore.loScale)) + 1600;
		break;
	case 3: /* 12800 to 64000 inverse linear */
		msg->l2aPVraw = ((dataStore.ciValue - dataStore.loScale) * 51200 / (dataStore.hiScale - dataStore.loScale)) + 12800;
		break;
	default:
		msg->l2aPVraw = (u_int16_t)dataStore.ciValue;
	}

	/* Save calculated RAWC for display */
	dataStore.aoRawC = (u_int16_t)msg->l2aPVraw;

	/* Timestamp */
	msg->l2aTimestamp = dataStore.ciTimestamp;

	/* OUT Alarming */
	if (dataStore.alarmEnable)
	{
	    /* Check if alarm limits exceeded */
		if ((dataStore.ciValue > dataStore.hiAlarmLimit) || (dataStore.ciValue < dataStore.loAlarmLimit))
		{
			gInAlarm = TRUE;
		}
		else
		{
			gInAlarm = FALSE;
		}
	}
	else
	{
		gInAlarm = FALSE;
	}
}

void DO_PEER_FUNCTION(L2A_MSG_PTR msg)
{
    /* Failsafe Counter */
	static u_int16_t fsCount = 0;

	/* Test for OOS or BAD status */
	if (dataStore.di_pStatus & (PV_OOS | PV_BAD))
	{
	    /* Increment Failsafe counter until we reach trigger */
		if (fsCount <= dataStore.doFSDelay)
		{
			fsCount++;
		}

		/* Set global IO Status */
		gIoBad = TRUE;
	}
	else
	{
		fsCount = 0;
	}

	/* Check for FailSafe Enable */
	if (dataStore.doFSEnable != 0)
	{
	    /* Check for FailSafe activation */
		if (fsCount > dataStore.doFSDelay)
		{
		    /* We are in Fail Safe */
			dataStore.doFailsafe = 0xFFFF; /* TRUE */

			/* Set Fail Save output value */
			if (dataStore.doFSValue)
			{
				msg->l2aPVraw = 0xFFFF; /* TRUE */
			}
			else
			{
				msg->l2aPVraw = 0x0000; /* FALSE */
			}

			/* TODO - what do we do about alarms in Failsafe ?? */
			gInAlarm = FALSE;

			return;
		}
	}
	else
	{
	    /* Reset Fail Safe counter if Failsafe is not enabled */
		fsCount = 0;
	}

	/*  We are not in fail safe - so do normal DO function */
	dataStore.doFailsafe = 0x0000; /* FALSE */

	/* Propagate digital state */
	if (dataStore.doInvOpt != 0)
	{
	    /* Inverted */
		if (dataStore.di_pValue == 0)
		{
			dataStore.ci_diValue = 0xFFFF;
		}
		else
		{
			dataStore.ci_diValue = 0x0000;
		}
	}
	else
	{
	    /* Non Inverted */
		dataStore.ci_diValue = dataStore.di_pValue;
	}

	/* Propagate time from DI input */
	dataStore.ciTimestamp = dataStore.di_pTimestamp;

	/* Convert from Integer to PV Raw */
	if (dataStore.ci_diValue)
	{
		msg->l2aPVraw = 0xFFFF; /* TRUE */
	}
	else
	{
		msg->l2aPVraw = 0x0000; /* FALSE */
	}

	/* Timestamp - TODO - mapping ? */
	msg->l2aTimestamp = dataStore.ciTimestamp;

	/* STATE Alarming on output */
	if (dataStore.alarmEnable)
	{
	    /* Check if in alarm state */
		if (dataStore.ci_diValue != 0)
		{
			gInAlarm = dataStore.stateAlarm ? TRUE : FALSE;
		}
		else
		{
			gInAlarm = dataStore.stateAlarm ? FALSE : TRUE;
		}
	}
	else
	{
		gInAlarm = FALSE;
	}
}

void LAD_AND_FUNCTION(L2A_MSG_PTR msg)
{
    /* Failsafe Counter */
	static u_int16_t fsCount = 0;

	/* Test for OOS or BAD status */
	if (dataStore.di_pStatus & (PV_OOS | PV_BAD))
	{
	    /* Increment Failsafe counter until we reach trigger */
		if (fsCount <= dataStore.doFSDelay)
		{
			fsCount++;
		}

		/* Set global IO Status */
		gIoBad = TRUE;
	}
	else
	{
		fsCount = 0;
	}

	    /* Check for FailSafe Enable */
	if (dataStore.doFSEnable != 0)
	{
	    /* Check for FailSafe activation */
		if (fsCount > dataStore.doFSDelay)
		{
		    /* We are in Fail Safe */
			dataStore.doFailsafe = 0xFFFF; /* TRUE */

			/* Set Fail Save output value */
			if (dataStore.doFSValue)
			{
				msg->l2aPVraw = 0xFFFF; /* TRUE */
			}
			else
			{
				msg->l2aPVraw = 0x0000; /* FALSE */
			}

			/* TODO - what do we do about alarms in Failsafe ?? */
			gInAlarm = FALSE;

			return;
		}
	}
	else
	{
	    /* Reset Fail Safe counter if Failsafe is not enabled */
		fsCount = 0;
	}

	/*  We are not in fail safe - so do normal AO function */
	dataStore.doFailsafe = 0x0000; /* FALSE */

	/* Perform logical AND on digital states */
	dataStore.ci_diValue = ((dataStore.di_pValue != 0) && (dataStore.doInvOpt != 0));

	/* Propagate time from DI input */
	dataStore.ciTimestamp = dataStore.di_pTimestamp;

	/* Convert from Integer to PV Raw */
	if (dataStore.ci_diValue)
	{
		msg->l2aPVraw = 0xFFFF; /* TRUE */

		gLadderState = TRUE;
	}
	else
	{
		msg->l2aPVraw = 0x0000; /* FALSE */

		gLadderState = FALSE;
	}

	/* No alarms for Ladder */
	gInAlarm = FALSE;

	/* Timestamp - TODO - mapping ? */
	msg->l2aTimestamp = dataStore.ciTimestamp;
}

void LAD_OR_FUNCTION(L2A_MSG_PTR msg)
{
    /* Failsafe Counter */
	static u_int16_t fsCount = 0;

	/* Test for OOS or BAD status */
	if (dataStore.di_pStatus & (PV_OOS | PV_BAD))
	{
	    /* Increment Failsafe counter until we reach trigger */
		if (fsCount <= dataStore.doFSDelay)
		{
			fsCount++;
		}

		/* Set global IO Status */
		gIoBad = TRUE;
	}
	else
	{
		fsCount = 0;
	}

	/* Check for FailSafe Enable */
	if (dataStore.doFSEnable != 0)
	{
	    /* Check for FailSafe activation */
		if (fsCount > dataStore.doFSDelay)
		{
		    /* We are in Fail Safe */
			dataStore.doFailsafe = 0xFFFF; /* TRUE */

			/* Set Fail Save output value */
			if (dataStore.doFSValue)
			{
				msg->l2aPVraw = 0xFFFF; /* TRUE */
			}
			else
			{
				msg->l2aPVraw = 0x0000; /* FALSE */
			}

			/* TODO - what do we do about alarms in Failsafe ?? */
			gInAlarm = FALSE;

			return;
		}
	}
	else
	{
	    /* Reset Fail Safe counter if Failsafe is not enabled */
		fsCount = 0;
	}

	/*  We are not in fail safe - so do normal AO function */
	dataStore.doFailsafe = 0x0000; /* FALSE */

	/* Perform logical OR on digital states */
	dataStore.ci_diValue = ((dataStore.di_pValue != 0) || (dataStore.doInvOpt != 0));

	/* Propagate time from DI input */
	dataStore.ciTimestamp = dataStore.di_pTimestamp;

	/* Convert from Integer to PV Raw */
	if (dataStore.ci_diValue)
	{
		msg->l2aPVraw = 0xFFFF; /* TRUE */

		gLadderState = TRUE;
	}
	else
	{
		msg->l2aPVraw = 0x0000; /* FALSE */

		gLadderState = FALSE;
	}

	/* Timestamp - TODO - mapping ? */
	msg->l2aTimestamp = dataStore.ciTimestamp;

	/* No alarms for Ladder */
	gInAlarm = FALSE;
}

void LAD_XOR_FUNCTION(L2A_MSG_PTR msg)
{
    /* Failsafe Counter */
	static u_int16_t fsCount = 0;

	/* Test for OOS or BAD status */
	if (dataStore.di_pStatus & (PV_OOS | PV_BAD))
	{
	    /* Increment Failsafe counter until we reach trigger */
		if (fsCount <= dataStore.doFSDelay)
		{
			fsCount++;
		}

		/* Set global IO Status */
		gIoBad = TRUE;
	}
	else
	{
		fsCount = 0;
	}

	    /* Check for FailSafe Enable */
	if (dataStore.doFSEnable != 0)
	{
	    /* Check for FailSafe activation */
		if (fsCount > dataStore.doFSDelay)
		{
		    /* We are in Fail Safe */
			dataStore.doFailsafe = 0xFFFF; /* TRUE */

			/* Set Fail Save output value */
			if (dataStore.doFSValue)
			{
				msg->l2aPVraw = 0xFFFF; /* TRUE */
			}
			else
			{
				msg->l2aPVraw = 0x0000; /* FALSE */
			}

			/* TODO - what do we do about alarms in Failsafe ?? */
			gInAlarm = FALSE;

			return;
		}
	}
	else
	{
	    /* Reset Fail Safe counter if Failsafe is not enabled */
		fsCount = 0;
	}

	 /*  We are not in fail safe - so do normal AO function */
	dataStore.doFailsafe = 0x0000; /* FALSE */

	/* Perform logical XOR on digital states */
	dataStore.ci_diValue = (!(dataStore.di_pValue != 0) != !(dataStore.doInvOpt != 0));

	/* Propagate time from DI input */
	dataStore.ciTimestamp = dataStore.di_pTimestamp;

	/* Convert from Integer to PV Raw */
	if (dataStore.ci_diValue)
	{
		msg->l2aPVraw = 0xFFFF; /* TRUE */

		gLadderState = TRUE;
	}
	else
	{
		msg->l2aPVraw = 0x0000; /* FALSE */

		gLadderState = FALSE;
	}

	/* Timestamp - TODO - mapping ? */
	msg->l2aTimestamp = dataStore.ciTimestamp;

	/* No alarms for Ladder */
	gInAlarm = FALSE;
}

void LAD_NOT_FUNCTION(L2A_MSG_PTR msg)
{
    /* Failsafe Counter */
	static u_int16_t fsCount = 0;

	/* Test for OOS or BAD status */
	if (dataStore.di_pStatus & (PV_OOS | PV_BAD))
	{
	    /* Increment Failsafe counter until we reach trigger */
		if (fsCount <= dataStore.doFSDelay)
		{
			fsCount++;
		}

		/* Set global IO Status */
		gIoBad = TRUE;
	}
	else
	{
		fsCount = 0;
	}

	/* Check for FailSafe Enable */
	if (dataStore.doFSEnable != 0)
	{
	    /* Check for FailSafe activation */
		if (fsCount > dataStore.doFSDelay)
		{
		    /* We are in Fail Safe */
			dataStore.doFailsafe = 0xFFFF; /* TRUE */

			/* Set Fail Save output value */
			if (dataStore.doFSValue)
			{
				msg->l2aPVraw = 0xFFFF; /* TRUE */
			}
			else
			{
				msg->l2aPVraw = 0x0000; /* FALSE */
			}

			/* TODO - what do we do about alarms in Failsafe ?? */
			gInAlarm = FALSE;

			return;
		}
	}
	else
	{
	    /* Reset Fail Safe counter if Failsafe is not enabled */
		fsCount = 0;
	}

	/*  We are not in fail safe - so do normal AO function */
	dataStore.doFailsafe = 0x0000; /* FALSE */

	/* Perform logical NOT on digital state */
	dataStore.ci_diValue = !(dataStore.di_pValue != 0);

	/* Propagate time from DI input */
	dataStore.ciTimestamp = dataStore.di_pTimestamp;

	/* Convert from Integer to PV Raw */
	if (dataStore.ci_diValue)
	{
		msg->l2aPVraw = 0xFFFF; /* TRUE */

		gLadderState = TRUE;
	}
	else
	{
		msg->l2aPVraw = 0x0000; /* FALSE */

		gLadderState = FALSE;
	}

	/* Timestamp - TODO - mapping ? */
	msg->l2aTimestamp = dataStore.ciTimestamp;

	/* No alarms for Ladder */
	gInAlarm = FALSE;
}

/* Update HART data values */
void ProcessHART(A2L_MSG_PTR msg)
{
	dataStore.hartPV = msg->a2lHARTPV.fVal;
	dataStore.hartSV = msg->a2lHARTSV.fVal;
	dataStore.hartTV = msg->a2lHARTTV.fVal;
	dataStore.hartQV = msg->a2lHARTQV.fVal;
	dataStore.hartStatus = (u_int16_t)msg->a2lHARTStatus;
}

/* Process the IO Scanner */
void DoIOScanner(int fd)
{
	/* SPI message buffers */
	static L2A_MSG txBuf = { 0 };
	static A2L_MSG rxBuf = { 0 };

	static u_int16_t lastConfig = NO_CONFIG;

	int i;

	u_int32_t timeStamp = GetTimeStamp();
	    
	/* Assume IO is GOOD unless anyone sets the flag */
	gIoBad = FALSE;

	/* Assume we are NOT running ladder unless anyone sets the flag */
	gLadderState = FALSE;
	
	/* This lock MUST be for the duration of the scan for data coherency */
	ds_lock();

	/* Detect change in IO Type configuration */
	if (lastConfig != dataStore.ioType)
	{
	    /* Reset data values if new state is IO Type = un-configured or if we are Modbus */
		if ((dataStore.ioType == NO_CONFIG) || (dataStore.ioType >= MB_TYPE))
		{
		    /* Set AI BAD / OOS with a value of zero */
			dataStore.aiValue = 0.0f;
			dataStore.aiStatus = (PV_BAD | PV_OOS);
			dataStore.aiTimestamp = timeStamp;
			dataStore.aiRawC = 0;

			/* Force AO out of failsafe and set output to zero */
			dataStore.aoRawC = 0;
			dataStore.aoFailsafe = 0;

			/* Set DO BAD / OOS with a state of FALSE */
			dataStore.diValue = 0;
			dataStore.diStatus = (PV_BAD | PV_OOS);
			dataStore.diTimestamp = timeStamp;

			/* Force DO out of failsafe */
			dataStore.doFailsafe = 0;

			/* Disable any alarms */
			gInAlarm = FALSE;
		}

		/* Update cached configuration state */
		lastConfig = dataStore.ioType;
	}
	
#ifdef LED_CONFIG_ENABLED
	/* Reset control LED to not configured if we are in NO_CONFIG or if control is disabled */
	if ((dataStore.ioType == NO_CONFIG) || ((dataStore.ioType != MB_TYPE) && (dataStore.pidEnable == 0)))
	{
	    StatusLedOrangeOn(LED_TWO);
	}
#endif

	/* BCS RDI DO NOT process I/O if I/O is not configured or if we are configured as a Modbus node */
	if ((dataStore.ioType == NO_CONFIG) || (dataStore.ioType >= MB_TYPE))
	{
		ds_unlock();
		printf("IO type not configured\n");
		return;
	}

	/* Update ASIC configured mode */
	txBuf.l2aConfig = dataStore.ioType;

	/* Set HART Enable flag into ASIC configuration */
	if (dataStore.hartEnable)
	{
		txBuf.l2aConfig |= HART_ENABLE_FLAG;
	}
	else
	{
	    /* Reset HART values if HART is disabled */
		dataStore.hartPV = 0.0f;
		dataStore.hartSV = 0.0f;
		dataStore.hartTV = 0.0f;
		dataStore.hartQV = 0.0f;
	}

	/* Perform either control OR direct AO/DO */
	if (dataStore.pidEnable != 0)
	{
	    /* Enable control flag */
		gOnControl = TRUE;

		/* Perform desired control action */
		switch (dataStore.pidEnable)
		{
		case CTRL_PID:
			PID_FUNCTION(&txBuf);
			break;
		case CTRL_DO:
			DO_PEER_FUNCTION(&txBuf);
			break;
		case CTRL_LAD_AND:
			LAD_AND_FUNCTION(&txBuf);
			break;
		case CTRL_LAD_OR:
			LAD_OR_FUNCTION(&txBuf);
			break;
		case CTRL_LAD_XOR:
			LAD_XOR_FUNCTION(&txBuf);
			break;
		case CTRL_LAD_NOT:
			LAD_NOT_FUNCTION(&txBuf);
			break;
		default: /* Output PV of zero for any other type */
			txBuf.l2aPVraw = 0;
			txBuf.l2aTimestamp = timeStamp;
			break;
		}
	}
	else
	{
	    /* Disable control flag */
		gOnControl = FALSE;

		/* Perform required function block operations for direct Output */
		switch (dataStore.ioType)
		{
		case AO_TYPE:
			AO_FUNCTION(&txBuf);
			break;
		case DO_TYPE:
			DO_FUNCTION(&txBuf);
			break;
		default: /* Output PV of zero for any other type */
			txBuf.l2aPVraw = 0;
			txBuf.l2aTimestamp = timeStamp;
			break;
		}
	}
	
	ds_unlock();

	/* Setup struct for passing message to UIO ASIC SPI driver task */
	UIO_ASIC_SPI_MSG msg;
	//msg.msgTaskId = taskIdSelf();
	msg.msgPtrTx = &txBuf;
	msg.msgTxLen = sizeof(L2A_MSG);
	msg.msgPtrRx = &rxBuf;
	msg.msgRxLen = sizeof(A2L_MSG);

	int ret = uiospi_transfer(msg, fd);

	if (ret != IOCTL_ERROR)
	{
		if (ret == NO_ERROR)
		{		    
			ds_lock();

			/* Perform required function block operations - if ASIC config matches our config !! */
			u_int8_t ioType = rxBuf.a2lConfig & IO_TYPE_MASK;

			if (ioType == dataStore.ioType)
			{
				switch (ioType)
				{
				case AI_TYPE:
					AI_FUNCTION(&rxBuf, timeStamp);
					break;
				case DI_TYPE:
					DI_FUNCTION(&rxBuf, timeStamp);
					break;
				}
			}
			else
			{
			    /* Handle LCES to ASIC IO Type mismatch - Set status of our configured type to BAD/OOS */
				switch (dataStore.ioType)
				{
				case AI_TYPE:
					dataStore.aiStatus |= (PV_BAD | PV_OOS);
					break;
				case DI_TYPE:
					dataStore.diStatus |= (PV_BAD | PV_OOS);
					break;
				}
			}

			/* Process HART (if enabled) */
			if (rxBuf.a2lConfig & HART_ENABLE_FLAG)
			{
				ProcessHART(&rxBuf);
			}
			
#ifdef LED_CONFIG_ENABLED

			/* Light stand RED follows IO Bad state */
			SetLightStandRedState(gIoBad ? LIGHT_ON : LIGHT_OFF);

			/* Light stand ORANGE follows alarm state */
			SetLightStandOrangeState((gInAlarm | gLadderState) ? LIGHT_ON : LIGHT_OFF);

			/* Light stand GREEN follows on control state */
			SetLightStandGreenState(gOnControl ? LIGHT_ON : LIGHT_OFF);

			/* Update LED status */
			dataStore.ledStatus = (GetLightStandStatus() << 9) | (gInAlarm << 8) | (GetLedStatus(LED_TWO) << 4) | GetLedStatus(LED_ONE);
#endif

			ds_unlock();
		}
		else
		{
#ifdef LED_CONFIG_ENABLED
		    /* FAILURE */
		    StatusLedRedOn(LED_ONE);
#endif

			ds_lock();

			/* Force input statuses to BAD / OOS */
			switch (dataStore.ioType)
			{
			case AI_TYPE:
				dataStore.aiStatus |= (PV_BAD | PV_OOS);
				break;
			case DI_TYPE:
				dataStore.diStatus |= (PV_BAD | PV_OOS);
				break;
			}

			/* Set IO Bad due to ASIC Communications failure */
			gIoBad = TRUE;
			
#ifdef LED_CONFIG_ENABLED

			/* Light stand RED follows IO Bad state */
			SetLightStandRedState(gIoBad ? LIGHT_ON : LIGHT_OFF);

			/* Light stand ORANGE follows alarm state */
			SetLightStandOrangeState((gInAlarm | gLadderState) ? LIGHT_ON : LIGHT_OFF);

			/* Light stand GREEN follows on control state */
			SetLightStandGreenState(gOnControl ? LIGHT_ON : LIGHT_OFF);

			/* Update LED status */
			dataStore.ledStatus = (GetLightStandStatus() << 9) | (gInAlarm << 8) | (GetLedStatus(LED_TWO) << 4) | GetLedStatus(LED_ONE);
#endif

			ds_unlock();

			/* TODO delay > 1mS and then retry ?? */
		}
	}
	else
	{
#ifdef LED_CONFIG_ENABLED
	    /* FAILURE - eventReceive error*/
	    StatusLedRedOn(LED_ONE);
#endif
	    /* Set IO Bad due to SPI interface Communications failure */
		gIoBad = TRUE;
		
#ifdef LED_CONFIG_ENABLED
		/* Light stand RED follows IO Bad state */
		SetLightStandRedState(gIoBad ? LIGHT_ON : LIGHT_OFF);

		/* Light stand ORANGE follows alarm state */
		SetLightStandOrangeState((gInAlarm | gLadderState) ? LIGHT_ON : LIGHT_OFF);

		/* Light stand GREEN follows on control state */
		SetLightStandGreenState(gOnControl ? LIGHT_ON : LIGHT_OFF);

		ds_lock();

		/* Update LED status */
		dataStore.ledStatus = (GetLightStandStatus() << 9) | (gInAlarm << 8) | (GetLedStatus(LED_TWO) << 4) | GetLedStatus(LED_ONE);

		ds_unlock();
#endif
	}
}





