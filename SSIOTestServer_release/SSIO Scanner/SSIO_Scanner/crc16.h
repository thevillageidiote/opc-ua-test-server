/*
 * crc16.h
 *
 *  Created on: 16-Jan-2017
 *      Author: yocto
 */

#ifndef CRC16_H_
#define CRC16_H_

#include <stdlib.h>
#include <stdint.h>

//extern u_int16_t const crc16_table[256];

extern u_int16_t crc16(const uint8_t *buffer, size_t len);

extern u_int16_t crc16_nt(const uint8_t *buffer, size_t len);



#endif /* CRC16_H_ */
