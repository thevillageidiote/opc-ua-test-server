#pragma once

#include "ParamRecord.h"
#include <BCCommon/AppDataAPI.h>

class MemGrid
{
private:	
	AppDataAPI *dataSvc;		// Pointer to the data service API
	string MemGridRecName;
	bool MemGrid_Initialized;
	u_int8_t MemGrid_IOtype;
	paramRecord<string> IOData;     // Contains the IO Value
	paramRecord<std::string> IOQual; // Contains the Quality value
	paramRecord<std::string> IOTime; // Contains the time value	
	
public:	
	
	MemGrid();
	virtual ~MemGrid();
	
	bool getInputs(std::string &dataRecord, std::map<string, string> &value);
	bool initialize(const char *url, const int port, const char *rkey, const u_int8_t IOtype);	
	bool SetDataRec(const int value, const int quality = 0, const int timestamp = 0);
	bool GetDataRec(int *value, int *quality, int *timestamp);
	bool setAttribute(const paramRecord<std::string>& param, map<string, string> &values);
	bool setAttributes(string& recordName, map<string, string> &values);
	int inscan();
	int outscan();
};

extern MemGrid *MemGridInst;