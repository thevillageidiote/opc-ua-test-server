#pragma once


static void error(const char *msg);
static void checkRet(int ret, const char *msg);
int initServerSocket(const char * socket_path);
int connectToClient(int sockfd);
int sendMessage(int sockfd, u_int16_t selector, u_int16_t data_value);


u_int16_t receiveMessage(int sockfd, u_int16_t * value);

u_int16_t obtainDataValue(u_int8_t IOtype);



/*
#define SERVER_RECEIVEVAL 1
#define SERVER_SENDVAL 2
#define SERVER_RECEIVECONFIG 3
#define SERVER_NODATA 0
*/

#define CLIENT_NODATA 0
#define CLIENT_READYTORECEIVE 1
#define CLIENT_SENDVAL 2
#define CLIENT_SENDCONFIG 3
#define CLIENT_RECEIVEVAL 4

#define SERVER_RECEIVED 1
#define SERVER_SENDVAL 2
#define SERVER_NODATA 0