#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sched.h>
#include <linux/spi/spidev.h>
#include <signal.h>
//#include "memgrid.h"
#ifdef __cplusplus
extern "C"
{
#endif
	
#include "ssio_node.h"
#include "status_gpio.h"
#include "data_store.h"
#include "socket_server.h"
	
#ifdef __cplusplus
}
#endif

#define CMD_LINE_ARGS_ENABLED

#define SCHED_PRIO_REALTIME 10

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

static void pabort(const char *s)
{
	perror(s);
	abort();
}


/*Redis Server default settings*/
static const char *memgrid_url = NULL;
static u_int32_t memgrid_port = 0;
static const char *memgrid_key = NULL;

/* IOtype default settings */
static u_int8_t IOtype = AI_TYPE;

/* SPI default settings */
static const char *device = "/dev/spidev32766.2";
static u_int8_t mode = SPI_MODE_0;
static u_int8_t bits = SPI_BITS_PER_MSG;
static u_int32_t speed = SPI_CLK_SPEED;
u_int16_t delay = 15;
u_int32_t frequency = 10000;

int i = 0;

#ifdef CMD_LINE_ARGS_ENABLED
static void print_usage(const char *prog)
{
	printf("Usage: %s [-DsbdlHOLC3]\n", prog);
	puts("  -D --device   device to use (default /dev/spidev1.1)\n"
	     "  -s --speed    max speed (Hz)\n"
	     "  -d --delay    delay (usec)\n"
		 "  -f --frequency    frequency (usec)\n"
		 "  -u --url      url\n"
		 "  -p --port     port\n"
		 "  -k --key      key\n"
		 "  -i --iotype   iotype\n"
		 "  -b --bpw      bits per word \n"
		 "  -l --loop     loopback\n"
		 "  -H --cpha     clock phase\n"
		 "  -O --cpol     clock polarity\n"
		 "  -L --lsb      least significant bit first\n"
		 "  -C --cs-high  chip select active high\n"
		 "  -3 --3wire    SI/SO signals shared\n");
	exit(1);
}

static void parse_opts(int argc, char *argv[])
{
	while (1) {
		static const struct option lopts[] = {
			{ "device", 1, 0, 'D' },
			{ "speed", 1, 0, 's' },
			{ "delay", 1, 0, 'd' },
			{ "frequency", 1, 0, 'f' },
			{ "url", 1, 0, 'u' },
			{ "port", 1, 0, 'p' },
			{ "key", 1, 0, 'k' },
			{ "iotype", 1, 0, 'i' },
			{ "bpw", 1, 0, 'b' },
			{ "loop", 0, 0, 'l' },
			{ "cpha", 0, 0, 'H' },
			{ "cpol", 0, 0, 'O' },
			{ "lsb", 0, 0, 'L' },
			{ "cs-high", 0, 0, 'C' },
			{ "3wire", 0, 0, '3' },
			{ "no-cs", 0, 0, 'N' },
			{ "ready", 0, 0, 'R' },
			{ NULL, 0, 0, 0 },
		};
		int c;

		c = getopt_long(argc, argv, "D:s:d:f:u:p:k:i:b:lHOLC3NR", lopts, NULL);

		if (c == -1)
			break;

		switch (c) {
		case 'D':
			device = optarg;
			break;
		case 's':
			speed = atoi(optarg);
			break;
		case 'd':
			delay = atoi(optarg);
			break;
		case 'f':
			frequency = atoi(optarg);
			break;
		case 'u':
			memgrid_url = optarg;
			break;
		case 'p':
			memgrid_port = atoi(optarg);
			break;
		case 'k':
			memgrid_key = optarg;
			break;
		case 'i':
			IOtype = atoi(optarg);
			break;				
		case 'b':
			bits = atoi(optarg);
			break;
		case 'l':
			mode |= SPI_LOOP;
			break;
		case 'H':
			mode |= SPI_CPHA;
			break;
		case 'O':
			mode |= SPI_CPOL;
			break;
		case 'L':
			mode |= SPI_LSB_FIRST;
			break;
		case 'C':
			mode |= SPI_CS_HIGH;
			break;
		case '3':
			mode |= SPI_3WIRE;
			break;
		case 'N':
			mode |= SPI_NO_CS;
			break;
		case 'R':
			mode |= SPI_READY;
			break;
		default:
			print_usage(argv[0]);
			break;
		}
	}
}
#endif


int main(int argc, char *argv[])
{
	int ret = 0;
	int fd;
	
#ifdef CMD_LINE_ARGS_ENABLED
	parse_opts(argc, argv);
#endif
	
	int sockfd,newsockfd;
	const char * server_pathname = "/home/root/socket";
	char server_buffer[256];
	u_int16_t value;

	
	
	sockfd = initServerSocket(server_pathname);
	
	newsockfd = connectToClient(sockfd);

	struct sched_param sp;
	
	/* set this process to real-time scheduling so it will pre-empt anything */
	sp.sched_priority = SCHED_PRIO_REALTIME;
	sched_setscheduler(0, SCHED_RR, &sp);

	fd = open(device, O_RDWR);
	if (fd < 0)
	{
		printf("Device open failed\n");
		return (-1);		
	}
		

	/*
	* spi mode
	*/
	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
	{
		printf("can't set spi mode\n");
		return (-1);
	}		

	ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
	if (ret == -1)
	{
		printf("can't get spi mode\n");
		return (-1);
	}		

	/*
	* bits per word
	*/
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
	{
		printf("can't set bits per word");
		return (-1);
	}		

	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
	{
		printf("can't get bits per word");
		return (-1);
	}		

	/*
	* max speed hz
	*/
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
	{
		printf("can't set max speed hz");
		return (-1);		
	}		

	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
	{
		pabort("can't get max speed hz");
		return (-1);
	}		

	printf("spi device:%s\n", device);
	printf("spi mode: %d\n", mode);
	printf("bits per word: %d\n", bits);
	printf("max speed: %d Hz (%d KHz)\n", speed, speed / 1000);
	printf("delay: %d\n", delay);
	printf("frequency(ms): %d\n", frequency);

	/* Currently LED status config is not supported */
	//StatusGPIO_LEDInit();
	    
	//MemGrid *MemGridInst = NULL;
	
	//MemGridInst = new MemGrid();
	
	//MemGridInst->initialize(memgrid_url, memgrid_port, memgrid_key, IOtype);
	
	/* Intialize the data store */
	if (ds_init(IOtype))
	{
		printf("data store initialization failed\n");
		return -1;
	}
	
	while (1)
	{
		// Update values of Ouput types from Memgrid to Datastore (global store)
		//MemGridInst->outscan();
		/* Perform the IO scanning */	
		
		u_int16_t selector = receiveMessage(newsockfd, &value);
		switch (selector)
		{
		case CLIENT_READYTORECEIVE:
			DoIOScanner(fd);
			value = obtainDataValue(dataStore.ioType);
			sendMessage(newsockfd, SERVER_SENDVAL, value);
			break;
		case CLIENT_SENDCONFIG:
			ds_lock();
			dataStore.ioType = (u_int8_t) value;
			ds_unlock();
			DoIOScanner(fd);
			sendMessage(newsockfd, SERVER_RECEIVED, 0);
			break;
		case CLIENT_SENDVAL:
			//Not supported yet
			/*
			switch (dataStore.ioType)
			{
			case AO_TYPE:
				//Still unsupported
				break;
			case DO_TYPE:
				ds_lock();
				dataStore.doValue = (u_int16_t) value;
				ds_unlock();
				break;
			default:
				break;
			}
			DoIOScanner(fd);
			sendMessage(newsockfd, SERVER_RECEIVED, 0);

			*/
			
			break;
		default:
			break;
		}	
		
		usleep(frequency);			
	}	
	
	close(newsockfd);
	close(fd);

	return ret;
}
	
