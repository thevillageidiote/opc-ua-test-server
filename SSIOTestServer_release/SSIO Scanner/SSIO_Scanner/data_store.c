
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/mman.h>

#include "ssio_node.h"
#include "data_store.h"

/* THE ONE AND ONLY global data store for this SSIO node */
SSIO_NODE_DS dataStore;

sem_t *mutex;
SSIO_NODE_DS *shm;


/* Initialize the data store */
int ds_init(u_int8_t iotype)
{
	int fd_shm;
	printf("Inside ds_init()\n");
	
	//create & initialize semaphore
	mutex = sem_open(SEM_NAME, O_CREAT, 0644, 1);
	if (mutex == SEM_FAILED)
	{
		printf("unable to create semaphore");
		sem_unlink(SEM_NAME);
		return -1;
	}
	/* BLCS RDI:Initial idea was to use the shared memory but later it was decided not use it be because of the design approach */
#if 0
	// Get shared memory
	if ((fd_shm = shm_open(SHARED_MEM_NAME, O_RDWR | O_CREAT, 0660)) == -1)
		error("shm_open");

	if (ftruncate(fd_shm, sizeof(SSIO_NODE_DS)) == -1)
		error("ftruncate");

	if ((shm = mmap(NULL,
		sizeof(SSIO_NODE_DS),
		PROT_READ | PROT_WRITE,
		MAP_SHARED,
		fd_shm,
		0)) == MAP_FAILED)
		error("mmap");
	memset(shm, sizeof(SSIO_NODE_DS), 0);
	
#endif

	/* Initialize to zero's on initial start-up */
	memset(&dataStore, sizeof(SSIO_NODE_DS), 0);
	
	/* Initialize the io type*/
	/* By default it is AI else it can be passed as command line argument with -i option */
	/* TODO: If redis database can be used to configure the io type, intialize it with NO_CONFIG(0) */
	dataStore.ioType = iotype;
	
	/* BLCS RDI:Data store intialisation */
	/* For initial BLCS Demo, HART is always disabled */
	dataStore.hartEnable = FALSE;	
	
	/* For initial BLCS Demo: Required for the AO config, we are not performing any scaling i.e rawcount and engineering units are same */
	dataStore.sci = 0;
	
	/* BLCS RDI:Currently the scanning is not done in LCES.The below code is not required */
	/* TODO: Code clean up required after the decision to remove the scaling inside the LCES is made*/
	
	/* Setup some useful defaults */
	dataStore.loScale = 0.0f;
	dataStore.hiScale = 100.0f;
	//dataStore.sci = 3;BLCS RDI: Comment it

	dataStore.loAlarmLimit = 25;
	dataStore.hiAlarmLimit = 33;
	dataStore.stateAlarm = 1;

	dataStore.pidSetpoint = 30.0f;
	dataStore.pidPBAND = 3.0f;

	dataStore.aiStatus = (PV_BAD | PV_OOS);
	dataStore.diStatus = (PV_BAD | PV_OOS);

	    /* Preset failsafe to ON */
	dataStore.aoFSEnable = 1;
	dataStore.aoFSValue = 64000;
	dataStore.aoFSDelay = 600;

	dataStore.doFSEnable = 1;
	dataStore.doFSValue = 0x0000;
	dataStore.doFSDelay = 600;

	/* Preset AI OSV to 2% */
	dataStore.aiOSV = 2.0f;


	/* TODO - this needs to initialize from flash or from NVRAM (any non-volatile storage !) */

	return 0;
}

/* Lock the data store */
inline void ds_lock(void)
{
    sem_wait(mutex);
}

/* Unlock the data store */
inline void ds_unlock(void)
{
	sem_post(mutex);
}
