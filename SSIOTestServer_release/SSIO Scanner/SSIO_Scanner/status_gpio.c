/*
 * status_gpio.c
 *
 *  Created on: 18-Jan-2017
 *      Author: yocto
 */
#include <stdio.h>
#include <stdlib.h>
//#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>

#include "status_gpio.h"

/* Global LED status for Modbus Server usage */
static STATUS_LED_STATES statusLedOneState = LED_OFF;
static STATUS_LED_STATES statusLedTwoState = LED_OFF;
static STATUS_LED_STATES lightStandState   = LED_OFF;

#define VALUE_MAX 45

/* GPIO's are required for driving light stand */
/* We are not using it for now */
#if 0
static int GPIOExport(int pin)
{
#define BUFFER_MAX 3
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;

	fd = open("/sys/class/gpio/export", O_WRONLY);
	if (-1 == fd) {
		printf("Failed to open export for writing!\n");
		return (-1);
	}

	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return (0);
}

static int GPIOUnexport(int pin)
{
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;

	fd = open("/sys/class/gpio/unexport", O_WRONLY);
	if (-1 == fd) {
		printf("Failed to open unexport for writing!\n");
		return (-1);
	}

	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return (0);
}

static int GPIODirection(int pin, int dir)
{
	static const char s_directions_str[]  = "in\0out";

#define DIRECTION_MAX 35
	char path[DIRECTION_MAX];
	int fd;

	snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		printf("Failed to open gpio direction for writing!\n");
		return (-1);
	}

	if (-1 == write(fd, &s_directions_str[IN == dir ? 0 : 3], IN == dir ? 2 : 3)) {
		printf("Failed to set direction!\n");
		return (-1);
	}

	close(fd);
	return (0);
}

static int GPIORead(int pin)
{

	char path[VALUE_MAX];
	char value_str[3];
	int fd;

	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_RDONLY);
	if (-1 == fd) {
		printf("Failed to open gpio value for reading!\n");
		return (-1);
	}

	if (-1 == read(fd, value_str, 3)) {
		printf("Failed to read value!\n");
		return (-1);
	}

	close(fd);

	return (atoi(value_str));
}

static int GPIOWrite(int pin, int value)
{
	static const char s_values_str[] = "01";

	char path[VALUE_MAX];
	int fd;

	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		printf("Failed to open gpio value for writing!\n");
		return (-1);
	}

	if (1 != write(fd, &s_values_str[LOW == value ? 0 : 1], 1)) {
		printf("Failed to write value!\n");
		return (-1);
	}

	close(fd);
	return (0);
}

void SetLightStandRedState(LIGHTSTAND_STATE state)
{
	if (state == LIGHT_ON)
	{
		lightStandState |= LED_RED;
		GPIOWrite(LS_RED, HIGH);
	}
	else
	{
		lightStandState &= ~(LED_RED);
		GPIOWrite(LS_RED, LOW);
	}
}

void SetLightStandOrangeState(LIGHTSTAND_STATE state)
{
	if (state == LIGHT_ON)
	{
		lightStandState |= LED_ORA;
		GPIOWrite(LS_ORANGE, HIGH);
	}
	else
	{
		lightStandState &= ~(LED_ORA);
		GPIOWrite(LS_ORANGE, LOW);
	}
}


void SetLightStandGreenState(LIGHTSTAND_STATE state)
{
	if (state == LIGHT_ON)
	{
		lightStandState |= LED_GRN;
		GPIOWrite(LS_GREEN, HIGH);
	}
	else
	{
		lightStandState &= ~(LED_GRN);
		GPIOWrite(LS_GREEN, LOW);
	}
}

/* Return LightStand status */
STATUS_LED_STATES GetLightStandStatus(void)
{
	return lightStandState;
}

#endif

/* On board status LED's on LCES board*/

static int LedInit(int pin)
{
	static const char s_values_str[] = "none";

	char path[VALUE_MAX];
	int fd;

	snprintf(path, VALUE_MAX, "/sys/class/leds/pl_gpio%d/trigger", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		printf("Failed to open gpio value for writing!\n");
		return (-1);
	}

	if (-1 == write(fd, &s_values_str[0], 4)) {
		printf("Failed to write value in LedInit!\n");
		return (-1);
	}

	close(fd);
	return (0);
}


static int LedWrite(int pin, int value)
{
	static const char s_values_str[] = "01";

	char path[VALUE_MAX];
	int fd;

	snprintf(path, VALUE_MAX, "/sys/class/leds/pl_gpio%d/brightness", pin);
	fd = open(path, O_WRONLY);
	if (-1 == fd) {
		printf("Failed to open gpio value for writing!\n");
		return (-1);
	}

	if (1 != write(fd, &s_values_str[LOW == value ? 0 : 1], 1)) {
		printf("Failed to write value!\n");
		return (-1);
	}

	close(fd);
	return (0);
}


/* Turn RED LED On */
void StatusLedRedOn(STATUS_LED led)
{
	if (led == LED_ONE)
	{
		statusLedOneState = LED_RED;
		LedWrite(LED_1_RED, HIGH);
		LedWrite(LED_1_GREEN, LOW);
	}
	else
	{
		statusLedTwoState = LED_RED;
		LedWrite(LED_2_RED, HIGH);
		LedWrite(LED_2_GREEN, LOW);
	}
}

/* Turn GREEN LED On */
void StatusLedGreenOn(STATUS_LED led)
{
	if (led == LED_ONE)
	{
		statusLedOneState = LED_GRN;
		LedWrite(LED_1_RED, LOW);
		LedWrite(LED_1_GREEN, HIGH);
	}
	else
	{
		statusLedTwoState = LED_GRN;
		LedWrite(LED_2_RED, LOW);
		LedWrite(LED_2_GREEN, HIGH);
	}
}

/* Turn ORANGE LED On */
void StatusLedOrangeOn(STATUS_LED led)
{
	if (led == LED_ONE)
	{
		statusLedOneState = LED_ORA;
		LedWrite(LED_1_RED, HIGH);
		LedWrite(LED_1_GREEN, HIGH);
	}
	else
	{
		statusLedTwoState = LED_ORA;
		LedWrite(LED_2_RED, HIGH);
		LedWrite(LED_2_GREEN, HIGH);
	}
}

/* Turn LED OFF */
void StatusLedOff(STATUS_LED led)
{
	if (led == LED_ONE)
	{
		statusLedOneState = LED_OFF;
		LedWrite(LED_1_RED, LOW);
		LedWrite(LED_1_GREEN, LOW);
	}
	else
	{
		statusLedTwoState = LED_OFF;
		LedWrite(LED_2_RED, LOW);
		LedWrite(LED_2_GREEN, LOW);
	}
}

/* Return LED status */
STATUS_LED_STATES GetLedStatus(STATUS_LED led)
{
	if (led == LED_ONE)
	{
		return statusLedOneState;
	}
	else
	{
		return statusLedTwoState;
	}
}



void StatusGPIO_LEDInit(void)
{
#if 0
	/*
	 * Enable GPIO pins
	 */
	if (-1 == GPIOExport(LS_RED) || -1 == GPIOExport(LS_ORANGE) || -1 == GPIOExport(LS_GREEN) || -1 == GPIOExport(LS_SPARE))
		printf("Enabling GPIO pins failed\n");

	/*
	 * Set GPIO directions
	 */
	if (-1 == GPIODirection(LS_RED, OUT) || -1 == GPIODirection(LS_ORANGE, OUT) || -1 == GPIODirection(LS_GREEN, OUT) || -1 == GPIODirection(LS_SPARE, OUT))
		printf("Enabling GPIO pins directions failed\n");
#endif

/* Initialization of on board status LED's on LCES board(Bestla) */
	LedInit(LED_1_RED);
	LedInit(LED_1_GREEN);
	LedInit(LED_2_RED);
	LedInit(LED_2_GREEN);

}

