#include <iostream>
#include <string>
#include <RedisClient.hpp>
#include <BCCommon/AppDataAPI.h>
#include "memgrid.h"
/*Link it as C as these interfaces are also called from the C files */
#ifdef __cplusplus
extern "C"
{
#endif
#include "data_store.h"
#ifdef __cplusplus
}
#endif

using namespace std;


//MemGrid *MemGridInst;
	
MemGrid::MemGrid()
{
	//cout << "Inside MemGrid::MemGrid\n";
	MemGrid_Initialized = false;
	IOData.value = "0";
	IOData.visibleName = "out";
	IOData.exposed = true;
	IOQual.value = "0";
	IOQual.visibleName = "quality";
	IOQual.exposed = true;
	IOTime.value = "0";
	IOTime.visibleName = "time"; 
	IOTime.exposed  = true;	
	dataSvc = NULL;
}

MemGrid::~MemGrid()
{
	delete dataSvc;	
}
	
/// @brief - Function for filling the output values into map named values
/// @param - const paramRecord<std::string>& param - reference of parameter for holding the output values
///        - map<string, string> &values - reference of map with key as string and value as output values
/// @return - true if successful
///           false if failure 
bool MemGrid::setAttribute(const paramRecord<std::string>& param, map<string, string> &values)
{
	//cout << "Inside MemGrid::setAttribute\n";
	if (param.exposed == true)
	{
		values[param.visibleName] = param.value;
		return true;
	}
	return false;
}
	
/// @brief - Function for writing SSIO values in redis
/// @param - std::string &recordName - Reference of string variable for holding the name of the DataRecord
///        - map<string, string> &values - Reference of the map containing the SSIO values
/// @return - true if successful
///           false if failure 
bool MemGrid::setAttributes(string& recordName, map<string, string> &values)
{
	//cout << "Inside MemGrid::setAttributes\n";
	// Now we do the data write
	if (dataSvc->setDataRecord(recordName, values) == false)
	{
		cout << "failed to write to the data store" << endl;
		return false;
	}
	
	return true;
}
	
	
	
/// @brief - Function for getting the data from redis for SSIO
/// @param - std::string &dataRecord - Reference of string variable for holding the DataRecord
///        - std::map<string, string> &values - Reference of the map for getting the value from Redis
/// @return - true if successful
///           false if failure 
bool MemGrid::getInputs(std::string &dataRecord, std::map<string, string> &values)
{
	//cout << "Inside MemGrid::getInputs\n";
	// perform the memory grid operations only if the memorygrid is initialized
	if (MemGrid_Initialized)
	{	
		// For now we will return all fields
		values.clear();

		// Now we do the data read
		if (dataSvc->getDataRecord(dataRecord, values) == false)
		{
			cout << "failed to read from the data store" << endl;
			return false;
		}
		return true;
	}
	else
	{
		return false;
	}
}

/// @brief - Function for Memory Grid intitialization
/// @param - const char *url - url of the MemGrid server
///        - int port - port of the MemGrid serer
///        - const char *rkey - key/hash name
/// @return - true if successful
///           false if failure 
	
bool MemGrid::initialize(const char *url, const int port, const char *rkey, const u_int8_t IOtype)
{
	//cout << "Inside MemGrid::initialize\n";
	if (url == NULL | rkey == NULL)
	{
		cout << "Memory grid intialization failed due to missing url or key(record name)\n";
		return false;
	}	
	std::string dsurl = url;
	std::string dskey = rkey;
	dataSvc = new AppDataAPI();
	// Initialize the data API
	MemGridRecName = dskey;
	MemGrid_IOtype = IOtype;
	cout << "Waiting for Memgrid connection\n";
	if (dataSvc->setEnvironment(dsurl, port, dskey) == false)
	{
		cout << "Initialization of MemGrid failed" << endl;
		return false;		
	}
	else
	{
		MemGrid_Initialized = true;		
		printf("Initialization of MemGrid successfull\n");
		return true;
	}		
}

/// @brief - Function for Setting the  Data Record
/// @param - int value - value of the IO
///        - int quality - quality of the IO
///        - int timestamp - timestamp of the IO
/// @return - true if successful
///           false if failure 
bool MemGrid::SetDataRec(const int value, const int quality, const int timestamp)
{
	//cout << "Inside MemGrid::SetDataRec\n";
	// perform the memory grid operations only if the memorygrid is initialized
	if (MemGrid_Initialized)
	{	
		map<string, string>valuesMap;
		IOData.value = to_string(value);
		// TODO: quality and time needs to be updated in the future
		IOQual.value = to_string(quality);
		IOTime.value = to_string(timestamp);
		
		/* Filling the map with key fields */
		setAttribute(IOData, valuesMap);
		setAttribute(IOQual, valuesMap);
		setAttribute(IOTime, valuesMap);
		
		/* Pushing the map against the Record Name SSIO.AO.RawCount into Redis */
		if (setAttributes(MemGridRecName, valuesMap) == false)
		{
			cout << "Unable to update data store with output values" << endl;
			return false;
		}
		return true;
	}
	else
	{
		return false;		
	}		
}

/// @brief - Function for Getting the  Data Record
/// @param - int *value - value of the IO
///        - int *quality - quality of the IO
///        - int *timestamp - timestamp of the IO
/// @return - true if successful
///           false if failure
bool MemGrid::GetDataRec(int *value, int *quality, int *timestamp)
{
	//cout << "Inside MemGrid::GetDataRec\n";
	map<string, string>values;
	if (getInputs(MemGridRecName, values))
	{
			
		/* Find the "out" key field */
		map<string, string>::iterator recordOut = values.find("out");				
		
		if (recordOut != values.end())
		{
			/* Get the value against the "out" key field; This is our rawcount value */
			*value = stod(recordOut->second);
		}
		else
		{
			/* Else case if "out" key field is not present */
			*value = 0.0;
		}
					
		/* Find the "quality" key field */
		map<string, string>::iterator recordqual = values.find("quality");				
		
		if (recordqual != values.end())
		{
			/* Get the value against the "quality" key field */
			*quality = stod(recordqual->second);				
		}
		else
		{
			/* Else case if "quality" key field is not present */
			*quality = 0.0;			
		}
					
		/* Find the "timestamp" field */
		map<string, string>::iterator recordtime = values.find("time");				
		
		if (recordtime != values.end())
		{
			/* Get the value against the "timestamp" key field */
			*timestamp = stod(recordtime->second);				
		}
		else
		{
			/* Else case if "timestamp" key field is not present */
			*timestamp = 0.0;				
		}
		return true;
	}
	return false;		
}

/// @brief - Function for updating the  Data Record based on IO config
/// @param - int *value - value of the IO
///        - int *quality - quality of the IO
///        - int *timestamp - timestamp of the IO
/// @return - true if successful
///           false if failure
int MemGrid::inscan()
{
	switch (MemGrid_IOtype)
	{
	case AI_TYPE:
		ds_lock();
		// Update the rawcount and timestamp to the memory grid.Currently quality is ignored(0)
		SetDataRec(dataStore.aiRawC, /*dataStore.aiStatus*/ 0, dataStore.aiTimestamp);
		// For DEBUG
		printf("AI value:% d\n", dataStore.aiRawC);
		ds_unlock();
		break;
		
	case DI_TYPE:	
		ds_lock();
		if (dataStore.diValue == 0xFFFF)
		{			
			// Update the divalue and timestamp to the memory grid.Currently quality is ignored(0)
			SetDataRec(1, /*dataStore.diStatus*/ 0, dataStore.diTimestamp);			
		}		
		else if (dataStore.diValue == 0x0000)
		{			
			// Update the divalue and timestamp to the memory grid.Currently quality is ignored(0)
			SetDataRec(0, /*dataStore.diStatus*/ 0, dataStore.diTimestamp);						
		}
		else
		{
			printf("DI value is updated incorrectly");
		}
		// For DEBUG
		printf("DI value:% d\n", dataStore.diValue);
		ds_unlock();
		break;
		
	default:
		break;			
	}
	return 0;	
}

/// @brief - Function for retieving the  Data Record based on IO config
/// @param - int *value - value of the IO
///        - int *quality - quality of the IO
///        - int *timestamp - timestamp of the IO
/// @return - true if successful
///           false if failure
int MemGrid::outscan()
{
	int value = 0, quality = 0, timestamp = 0;
	
	switch (MemGrid_IOtype)
	{
	case AO_TYPE:
		ds_lock();
		// Get the data from the memory grid.Currently quality is ignored
		GetDataRec(&value, &quality, &timestamp);
		dataStore.aoValue = (float)value;
		//dataStore.aoStatus = quality;
		dataStore.aoTimestamp = timestamp;
		// For DEBUG
		printf("dataStore.aoValue:%f\n", dataStore.aoValue);
		ds_unlock();
		break;
		
	case DO_TYPE:		
		ds_lock();
		// Update the dataRec from the memory grid.Currently quality is ignored
		GetDataRec(&value, &quality, &timestamp);
		dataStore.doValue = (u_int16_t)value;
		// dataStore.doStatus = quality;
		dataStore.doTimestamp = timestamp;
		// For DEBUG
		printf("DO value:%d\n", dataStore.doValue);
		ds_unlock();						
		break;
		
	default:
		break;			
	}
	return 0;
}
