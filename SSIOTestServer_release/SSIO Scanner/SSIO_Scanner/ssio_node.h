/*
 * ssio_node.h
 *
 *  Created on: 14-Jan-2017
 *      Author: yocto
 */

#ifndef SSIO_NODE_H_
#define SSIO_NODE_H_
#include <sys/types.h>

/** Status Bit Defines **/

/*  Bad (PV should not be used for control since out of range, OOS or rate-of-change fault) */
#define PV_BAD		0x01

/* Out-of-service (TBD) */
#define PV_OOS		0x02

/* Error/Uncertain (TBD) */
#define PV_ERROR	0x4

/* Limited High (PV above threshold limit) */
#define LIM_HIGH	0x08

/* Limited low (PV below threshold limit) */
#define LIM_LOW		0x10

/* Failsafe active */
#define FS_ACTIVE	0x20

/* Rate-of-change Fault */
#define ROC_FAULT	0x40

/* Output Limited High */
#define HO_LIMIT	0x80

/* Output Limited Low */
#define LO_LIMIT	0x100

#define TRUE 1
#define FALSE 0

/* Moved from vxbFdtRzn1dUIOAsicSPI.h*/

/* LCES to ASIC SPI transmit buffer - MUST be byte packed ! */
typedef struct __attribute__((__packed__)) lces_to_asic_msg
{
	u_int8_t   l2aConfig;
	u_int32_t  l2aPVraw;
	u_int16_t  l2aTimestamp;
} L2A_MSG, *L2A_MSG_PTR;

/* Union for converting byte values to WORD values */
typedef union a2l_word
{
	u_int16_t  sVal;
	struct
	{
		u_int8_t bVal1;
		u_int8_t bVal2;
	};
} A2L_WORD;

/* Union for converting byte values to DWORD / float values */
typedef union a2l_dword
{
	float     fVal;
	u_int32_t  lVal;
	struct
	{
		u_int8_t bVal1;
		u_int8_t bVal2;
		u_int8_t bVal3;
		u_int8_t bVal4;
	};
} A2L_DWORD;

/* ASIC to LCES SPI receive buffer - MUST be native packed / aligned */
typedef struct asic_to_lces_msg
{
	u_int8_t    a2lConfig;
	u_int8_t    a2lStatus;
	A2L_DWORD  a2lPVraw;
	A2L_WORD   a2lTimestamp;
	u_int8_t    a2lHARTStatus;
	A2L_DWORD  a2lHARTPV;
	A2L_DWORD  a2lHARTSV;
	A2L_DWORD  a2lHARTTV;
	A2L_DWORD  a2lHARTQV;
} A2L_MSG, *A2L_MSG_PTR;

/* Message Queue message definition for sending command to UIO ASIC SPI device driver */
typedef struct uio_asic_spi_msg
{
    /*TASK_ID     msgTaskId;TODO Sasi Remove it f not required */
	L2A_MSG_PTR msgPtrTx;
	u_int8_t     msgTxLen;
	A2L_MSG_PTR msgPtrRx;
	u_int8_t     msgRxLen;
} UIO_ASIC_SPI_MSG;

    /* Tuneable defines for SPI interface */
#define SPI_XFER_LEN      40      /* SPI transaction length, MUST be even value and needs to match agreed SPI transaction format ! */
#define SPI_CLK_SPEED     250000  /* 250kHz clock */
#define SPI_BITS_PER_MSG  16      /* Each SPI message is 16 bits in length */

    /* ASCII characters */
#define ASCII_STX   2
#define ASCII_ETX   3

//#include "RedisClient.hpp"

enum _SPI_ERROR
{
	NO_ERROR    = 0,
	IOCTL_ERROR,
	CRC_ERROR
};

void DoIOScanner(int fd);

#endif /* SSIO_NODE_H_ */
