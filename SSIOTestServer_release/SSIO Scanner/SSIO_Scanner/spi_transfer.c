/*
 * spi_transfer.c
 *
 *  Created on: 16-Jan-2017
 *      Author: Sasi
 */


#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include <sys/types.h>
#include <linux/spi/spidev.h>
#include "crc16.h"
#include "data_store.h"
#include "ssio_node.h"

//comment it if debug info not required
//#define SPI_DEBUG

//comment it if you want to transfer using a single block
#define SPI_MB_XFER

int spi_transfer(UIO_ASIC_SPI_MSG msg, int fd, int id);

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

u_int32_t crcfaiurecount = 0;
u_int32_t crcsuccesscount = 0;
u_int32_t msgcount = 0;

int uiospi_transfer(UIO_ASIC_SPI_MSG msg, int fd)
{
	u_int8_t txBuffer[SPI_XFER_LEN];
	u_int8_t rxBuffer[SPI_XFER_LEN] = { 0, };
#ifdef SPI_MB_XFER
	struct spi_ioc_transfer xfer1[SPI_XFER_LEN / 2];
#else
	struct spi_ioc_transfer xfer1;
#endif
	int i, j, ret;

	/* Clear the buffers and SPI transfer ctl structure */
#ifdef SPI_MB_XFER
	memset(xfer1, 0, sizeof xfer1);
#else
	memset(&xfer1, 0, sizeof xfer1);
#endif
	memset(txBuffer, 0, sizeof txBuffer);
	memset(rxBuffer, 0, sizeof rxBuffer);

    /*************************************************************************************************/
    /*  CONSTRUCT MESSAGE                                                                            */
    /*************************************************************************************************/

    /* Set STX header byte */
	txBuffer[0] = ASCII_STX;


	    /* Copy the message into the transmit buffer allowing a byte for STX header */
	memcpy(&txBuffer[1], (void*)msg.msgPtrTx, msg.msgTxLen);


	    /* Fill the transmit buffer remaining bytes with ETX */
	for (i = msg.msgTxLen + 1; i < SPI_XFER_LEN - 2; i++)
	{
		txBuffer[i] = ASCII_ETX;
	}

	    /* Calculate the CRC for the complete message, including the header and append to msg */
	*(u_int16_t*)&txBuffer[SPI_XFER_LEN - 2] = crc16((u_int8_t *)txBuffer, SPI_XFER_LEN - 2);

	    /*************************************************************************************************/
	    /*  TRANSMIT /  RECEIVE MESSAGE                                                                  */
	    /*************************************************************************************************/

	        /* Debug message to print the tx buffer before spi transfer */
#ifdef SPI_DEBUG
	printf("\nTx buffer before ioctl");
	for (ret = 0; ret < ARRAY_SIZE(txBuffer); ret++)
	{
		if (!(ret % 6))
			puts("");
		printf("%.2X ", txBuffer[ret]);
	}
	puts("");

	/* Debug message to print the rx buffer before spi transfer */
	printf("\nRx buffer before ioctl");
	for (ret = 0; ret < ARRAY_SIZE(txBuffer); ret++)
	{
		if (!(ret % 6))
			puts("");
		printf("%.2X ", rxBuffer[ret]);
	}
	puts("");
#endif

	/* TODO Fine tuning is required */
	/* This is required to send the SPI data as blocks as the ASIC does not have the capability to handle more than 1 word at once */
#ifdef SPI_MB_XFER
	for (i = 0; i < ((SPI_XFER_LEN / 2)); i++)
	{
		xfer1[i].tx_buf = (unsigned long)&txBuffer[i * 2];
		xfer1[i].len = 2;
		xfer1[i].rx_buf = (unsigned long)&rxBuffer[i * 2];
		xfer1[i].speed_hz = SPI_CLK_SPEED;
		/* cs_change not required for the last word as there are no more
		 * words to transfer
		 */
		if (i < (SPI_XFER_LEN / 2 - 1))
		{
			xfer1[i].cs_change = TRUE;
			xfer1[i].delay_usecs = delay;/*15 has 32% success 15~18 has better success*/
		}

	}

	ret = ioctl(fd, SPI_IOC_MESSAGE(SPI_XFER_LEN / 2), &xfer1);

#else
	xfer1.tx_buf = (unsigned long)&txBuffer;
	xfer1.len = 40;
	xfer1.rx_buf = (unsigned long)&rxBuffer;
	xfer1.speed_hz = SPI_CLK_SPEED;
	//xfer1.cs_change = TRUE;
	//xfer1.delay_usecs = 10;

	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &xfer1);

#endif
	/*increment the msg count after transmitting the data */
	msgcount++;
	if (ret < 0) {
		printf("SPI_IOCTL ERROR\n");
		return IOCTL_ERROR;
	}
	printf("TM: %d S:%d F:%d IO:%d\n", msgcount, crcsuccesscount, crcfaiurecount, dataStore.ioType);
#ifdef SPI_DEBUG
	printf("		TOTAL MESG: %d\n", msgcount);
	printf("		CRC SUCCESS: %d\n", crcsuccesscount);
	printf("		CRC FAILED: %d\n", crcfaiurecount);

		/* Debug message to print the rx buffer after spi transfer */
	printf("\nRx buffer after ioctl");
	for (ret = 0; ret < ARRAY_SIZE(rxBuffer); ret++)
	{
		if (!(ret % 6))
			puts("");
		printf("%.2X ", rxBuffer[ret]);
	}
#endif

    /* Zero the MSB of each WORD in Rx buffer to accomodate ASIC SPI MISO bug */
	for (i = 1; i < SPI_XFER_LEN; i += 2)
	{
		u_int8_t b = rxBuffer[i];
		rxBuffer[i] = b & 0x7F; /* Correct ASIC SPI madness :( !!! */
	}

	/* Manipulate message to accomodate fact that 16 bit CRC is received in TWO 16 bit transfers */
	//rxBuffer[SPI_XFER_LEN - 3] = rxBuffer[SPI_XFER_LEN - 2];
	/* Message manipulated as per the behavior with the Linux.This could be due to the MAD SPI!!!*/
	rxBuffer[SPI_XFER_LEN - 5] = rxBuffer[SPI_XFER_LEN - 4];


	/* CRC validate message including 7 bit byte identifer values - EXCLUDE starting STX word*/
	//if (crc16(&rxBuffer[4], SPI_XFER_LEN - 6) == 0)	
	/* Manipulated as per the behavior with the Linux.This could be due to the MAD SPI!!!*/
	if (crc16(&rxBuffer[2], SPI_XFER_LEN - 6) == 0)
	{
#ifdef SPI_DEBUG
		printf("Rx CRC success\n");
#endif
		crcsuccesscount++;
		/* changes have been done to the ported code from SSIO based on the the data received from ASIC
		 * due to the SPI comms triggered from Linux on LCES
		 */
		msg.msgPtrRx->a2lConfig = rxBuffer[3];
		msg.msgPtrRx->a2lStatus = rxBuffer[2];
		msg.msgPtrRx->a2lPVraw.bVal1 = rxBuffer[4];
		msg.msgPtrRx->a2lPVraw.bVal2 = rxBuffer[6];
		msg.msgPtrRx->a2lPVraw.bVal3 = rxBuffer[8];
		msg.msgPtrRx->a2lPVraw.bVal4 = rxBuffer[10];
		msg.msgPtrRx->a2lTimestamp.bVal1 = rxBuffer[12];
		msg.msgPtrRx->a2lTimestamp.bVal2 = rxBuffer[14];
		msg.msgPtrRx->a2lHARTStatus = rxBuffer[16];

		if (rxBuffer[19] == 'B')
		{
			msg.msgPtrRx->a2lHARTPV.bVal1 = rxBuffer[18];
			msg.msgPtrRx->a2lHARTPV.bVal2 = rxBuffer[20];
			msg.msgPtrRx->a2lHARTPV.bVal3 = rxBuffer[22];
			msg.msgPtrRx->a2lHARTPV.bVal4 = rxBuffer[24];

			msg.msgPtrRx->a2lHARTSV.bVal1 = rxBuffer[26];
			msg.msgPtrRx->a2lHARTSV.bVal2 = rxBuffer[28];
			msg.msgPtrRx->a2lHARTSV.bVal3 = rxBuffer[30];
			msg.msgPtrRx->a2lHARTSV.bVal4 = rxBuffer[32];
		}
		else
		{
			msg.msgPtrRx->a2lHARTTV.bVal1 = rxBuffer[18];
			msg.msgPtrRx->a2lHARTTV.bVal2 = rxBuffer[20];
			msg.msgPtrRx->a2lHARTTV.bVal3 = rxBuffer[22];
			msg.msgPtrRx->a2lHARTTV.bVal4 = rxBuffer[24];

			msg.msgPtrRx->a2lHARTQV.bVal1 = rxBuffer[26];
			msg.msgPtrRx->a2lHARTQV.bVal2 = rxBuffer[28];
			msg.msgPtrRx->a2lHARTQV.bVal3 = rxBuffer[30];
			msg.msgPtrRx->a2lHARTQV.bVal4 = rxBuffer[32];
		}

		/* Done with this message - notify caller SUCCESS */		        
		/*TODO proper handling required here */
		return NO_ERROR;
	}
	else
	{
	    /* Done with this message - notify caller FAILED */	    
		/*DEBUG*/
#ifdef SPI_DEBUG
		printf("Rx CRC failure\n");
#endif
		crcfaiurecount++;
		/*TODO proper handling required here */
		return CRC_ERROR;
	}

}

