#pragma once
//============================================================================
// Name        : ParamRecord.h
// Author      : SaratM
// Version     : 1.0
// Copyright   : Schneider Electric 2017
// Description :
//				This class is defined as a template class for a Parameter Record
//				Param Record is serializable name and value pair
//
// Jan 2017 - Initial implementation
//============================================================================

#include <string>

#include <cereal/cereal.hpp>

using namespace std;

template <typename T> 
	struct paramRecord 
	{ 
		T value; 
		string visibleName;
		bool exposed;
		
		paramRecord()
		{
			exposed = false;
		}
		template<class Archive>
			void serialize(Archive & ar)
			{
				ar(cereal::make_nvp("visibleName", visibleName));
				ar(cereal::make_nvp("value", value));
				ar(cereal::make_nvp("exposed", exposed));
			}
	}; 