

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

//datatypes for system calls
#include <sys/types.h> 
//definitions of structures needed for sockets
#include <sys/socket.h>
//constants and structures needed for UNIX DOMAIN Addresses addresses
#include <sys/un.h>
//Include to set file flag
#include <fcntl.h>
//Inlcude for error flags
#include <errno.h>

#include <arpa/inet.h>


#include "socket_server.h"
#include "data_store.h"



static void error(const char *msg)
{
	perror(msg);
	exit(1);
}

static void checkRet(int ret, const char *msg) 
{
	if (ret < 0) 
	{
		error(msg);
	}
}


int initServerSocket(const char * socket_path)
{
	int sockfd, rval, ret, done;
	
	
	struct sockaddr_un serv_addr;
	
	//Create a socket
	sockfd = socket(AF_UNIX, SOCK_STREAM, 0);	
	checkRet(sockfd, "Error opening socket");
	
	serv_addr.sun_family = AF_UNIX;
	strcpy(serv_addr.sun_path, socket_path);
	
	//Close any file residing on the socket before opening it just in case
	unlink(serv_addr.sun_path);
	
	//Bind socket
	ret = bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
	checkRet(ret, "Error on binding");

	//unlink(serv_addr.sun_path);
	
	return sockfd;
	
}

int connectToClient(int sockfd)
{ 
	socklen_t clilen;
	int newsockfd, ret;
	
	struct sockaddr_un client_addr;
	
	clilen = sizeof(client_addr);
	
	ret = listen(sockfd, 5);
	checkRet(ret, "ERROR on listening");
	
	newsockfd = accept(sockfd, (struct sockaddr *) &client_addr, &clilen);
	
	
	//Only one connection supported currently
	close(sockfd);
	
	//Dont need the below as accept will block till connection is found
	/*
	if (newsockfd == EAGAIN || newsockfd == EWOULDBLOCK)
	{
		//no connection, try again
		return -2;
	}
	*/
	
	return newsockfd;	
	
}


int sendMessage(int sockfd, u_int16_t selector, u_int16_t data_value)
{
	
	u_int16_t output_array[2];
	output_array[0] = selector;
	output_array[1] = data_value;

	write(sockfd, &output_array, sizeof(output_array));
	
}


u_int16_t receiveMessage(int sockfd, uint16_t * value)
{
	u_int16_t output_array[2];
	
	read(sockfd, &output_array, sizeof(output_array));

	*value = output_array[1];

	return output_array[0];
	
}

u_int16_t obtainDataValue(u_int8_t IOtype)
{
	
	u_int16_t value;
	
	switch (IOtype)
	{
	case AI_TYPE:
		ds_lock();
		value = dataStore.aiRawC;
		ds_unlock();
		return value;
		break;
		
	case DI_TYPE:
		ds_lock();
		value = dataStore.diValue;
		ds_unlock();	
		return value;
		break;
	
	default:
		break;			
	}
	
	return -1;
	
	
	
}


//Example main program
/*
int main()
{
	
	int sockfd, newsockfd, ret;
	int connected = 0;
	
	char buf[256];
	
	sockfd = initServerSocket("/home/lcesvp/socket");
	
	while (1)
	{
		
		if (!connected)
		{
			newsockfd = connectToClient(sockfd);
			if (newsockfd >= 0) connected = 1; 
		}
		else 
		{
			bzero(buf, sizeof(buf));
			ret = read(newsockfd, buf, sizeof(buf));
			
			if (ret < 0) {
				error("Error while reading stream");
				close(newsockfd);
			}
			else if (ret == 0) {
				printf("Connection being ended by client.\n");
				//close(newsockfd);
				break;
			}
			else {
				printf("Message Received. Message is: %s", buf);
			}
		}
		

	}
	
	
	
}
*/