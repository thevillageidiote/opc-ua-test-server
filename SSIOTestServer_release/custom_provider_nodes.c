/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#include <memory/memory.h>
#include <uabase/statuscodes.h>
#include <uabase/nodeid.h>
#include <uabase/variant.h>
#include <uabase/accesslevel.h>
#include <uabase/structure/nodeclass.h>
#include <uabase/structure/range.h>
#include <uabase/identifiers.h>
#include <uabase/valuerank.h>
#include <uaserver/addressspace/addressspace.h>
#include <uaserver/addressspace/object.h>
#include <uaserver/addressspace/variable.h>
#include <trace/trace.h>

#include <uabase/structure/argument.h>


#include "custom_provider.h"
#include "custom_provider_store.h"
#include "custom_provider_nodes.h"
#include "custom_provider_method_handler.h"
#include "numNodeDefinition.h"

#include "custom_provider_ASIC_nodes.h"




/*! [create_nodes] */
/* create nodes in the custom provider namespace */
int eval_pv_create_nodes(uint16_t nsidx)
{
    ua_node_t folder, node, ASICNode;
    struct ua_nodeid nodeid,refId;
    int ret,i;
    char ObjectName[15];


    //Find Folder Node
    folder = ua_node_find_numeric(nsidx,5002);


    ASICNode = ASIC_createASIC(nsidx, 1, folder);

    //ASIC_createAINPort(nsidx,4,ASICNode,3);

    ASIC_createAINPort(nsidx,ASICNode,1,4);

    //ASIC_modifyPort( nsidx, ASICNode, 4, 1);


    //ASIC_deletePort(nsidx,ASICNode,4);



    TRACE_ERROR(TRACE_FAC_PROVIDER,"Created %i nodes\n",NUM_NODES);


    return 0;
}
/*! [create_nodes] */


