/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef CUSTOM_PROVIDER_METHOD_H_KHQ7D1OP
#define CUSTOM_PROVIDER_METHOD_H_KHQ7D1OP

#include <uabase/uatype_config.h>
#include <uabase/service/callrequest.h>
#include <uabase/service/callresponse.h>


#ifdef ENABLE_SERVICE_CALL
struct uaprovider_call_ctx;

struct ua_callmethodrequest;
struct ua_callmethodresult;

struct ua_nodeid;

void custom_provider_method_call(struct uaprovider_call_ctx *ctx);
int custom_provider_init_method(ua_node_t method_node, ua_statuscode (*fctMethodCall)(const struct ua_callmethodrequest *req, struct ua_callmethodresult *res, struct ua_nodeid objectid) );

#endif

#endif /* end of include guard: CUSTOM_PROVIDER_METHOD_H_KHQ7D1OP */
