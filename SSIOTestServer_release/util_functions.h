

const char * util_splitString (const char * input_string, char delimiter, int index);
int util_appendCharToString (char * input_string, size_t size, char c);
uint16_t util_returnNofromString (const char * input_string);


ua_node_t util_findNodefromInverseRef(ua_node_t input_node, uint16_t nsidx,const char * TargetBrowseName);
ua_node_t util_findNodefromForwardRef(ua_node_t input_node, uint16_t nsidx,const char * TargetBrowseName);

int util_deleteNodeRecursive(ua_node_t input_node,ua_node_t parent);

