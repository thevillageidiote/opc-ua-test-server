#include <uabase/uatype_config.h>

#include <uabase/service/callrequest.h>
#include <uabase/service/callresponse.h>
struct uaprovider_call_ctx;

struct ua_callmethodrequest;
struct ua_callmethodresult;


ua_statuscode ASIC_addPort(const struct ua_callmethodrequest *req, struct ua_callmethodresult *res, struct ua_nodeid objectid);
int ASIC_createAddPortMethod(uint16_t nsidx, ua_node_t parent, uint16_t ASICNumber);
int ASIC_createDeletePortMethod(uint16_t nsidx, ua_node_t parent, uint16_t ASICNumber);
int ASIC_createModifyPortMethod(uint16_t nsidx, ua_node_t parent, uint16_t ASICNumber);
