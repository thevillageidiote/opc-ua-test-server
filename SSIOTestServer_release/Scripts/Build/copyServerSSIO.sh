#!/bin/bash


ssh root@192.168.1.63 "cd /home/root/data; if ! ls | grep 'OPCTestServer' ; then mkdir OPCTestServer ; fi"

#copy built uaserver_test_model to LCES2
scp ../../build_LCES2/uaserver_test_model root@192.168.1.63:/home/root/data/OPCTestServer


#copy Resources to LCES2
scp Resources/Info_Model_File/uio_asic_test_server_info_model.bin root@192.168.1.63:/home/root/data/OPCTestServer
scp Resources/groups root@192.168.1.63:/home/root/data/OPCTestServer
scp Resources/ns0.ua root@192.168.1.63:/home/root/data/OPCTestServer
scp Resources/passwd root@192.168.1.63:/home/root/data/OPCTestServer
scp Resources/settings.conf root@192.168.1.63:/home/root/data/OPCTestServer
scp Resources/users root@192.168.1.63:/home/root/data/OPCTestServer



