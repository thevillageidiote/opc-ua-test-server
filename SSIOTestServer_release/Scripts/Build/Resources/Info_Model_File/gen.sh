#!/bin/bash
# Generates a binary address space file from the xml file
INPUT=uio_asic_test_server_info_model.xml
OUTPUT=uio_asic_test_server_info_model.bin
NSIDX=2

./xml2bin -n $NSIDX -v $INPUT $OUTPUT

