[general]

# Maximum number of timers
# Note: timers are needed in several places of the SDK:
# *One timer is needed for the session management
# *For each subscription one timer is needed (subscription.num_subscriptions)
# *For each monitoreditem one timer is needed (subscription.num_monitoreditems)
#  (This only applies for the SDK default implementation, you may implement this different in your own provider)
# *If the demoprovider is used, it needs one timer for the simulation
num_timers = 10004

[ipc]

ipc_size = 16777216
heap_size = 8388608
num_queues = 10
num_messages = 100
num_services = 50

[network]

# Size of one buffer for serializing/deserializing UA messages
# Multiply with uatcpmsg.num_chunks for maximum message size
buffer_size = 65536
# Number of buffers serializing/deserializing UA messages
num_buffers = 100
# Maximum number of buffers for one tcp connection
num_buffers_per_connection = 50
# Maximum number of tcp connection from the same IP address
max_connections_per_ip = 10

[encoder]

# Maximum nesting deep of potentially recursive datastructures
max_call_depth = 10
# Maximum length of an ua_string that is encoded or decoded
max_string_length = 1000
# Maximum length of an ua_bytestring that is encoded or decoded
# Note: Certificates are also encoded as ua_bytestring
max_bytestring_length = 8000
# Maximum length for arrays to be encoded or decoded
max_array_length = 5000

[subscription]

# Maximum number of sessions, that can have subscriptions
num_sessions_with_subscriptions = 4
# Maximum queued publishrequests for one session
num_publishrequests_per_session = 4
# Maximum number of notificationmessages for one session (aka retransmission queue)
# Shall be at least twice as big as num_publishrequests_per_session
num_notificationmessages_per_session = 8
# Maximum number of subscriptions for one session
num_subscriptions_per_session = 2
# Maximum number of subscriptions
num_subscriptions = 8
# Maximum number of monitoreditems for one subscription
num_monitoreditems_per_subscription = 10000
# Maximum number of monitored items
num_monitoreditems = 10000


# Maximum queue size of values for one monitoreditem
# This value is requested by the client and revised to at most this value
max_monitoreditem_queue_size = 100
# Maximum number of values and events for one publishresponse
# This value is requested by the client and revised to at most this value
max_notifications_per_publish = 1000
# Minimum supported publishing interval (milliseconds)
min_publishing_interval = 10
# Maximum supported publishing interval (milliseconds)
max_publishing_interval = 10000
# Minimum supported sampling interval (milliseconds)
min_sampling_interval = 10
# Maximum supported sampling interval (milliseconds)
max_sampling_interval = 10000
# Minimum interval for subscripiton to time out (milliseconds)
min_lifetime = 1000
# Maximum interval for subscripiton to time out (milliseconds)
max_lifetime = 100000


[session]

# max number of sessions
num_sessions = 5
# max number of parallel service calls (should be same as uatcpmsg.num_ctxts)
num_calls = 50
# When true, the oldest, not connected session is deleted if num_sessions is reached
keep_spare_session = true

# max number of user supported for authentication
authentication_num_users = 10
# location of file to load users and passwords for authentication
authentication_passwd_file = passwd

# max number of user supported for authorization
authorization_num_users = 10
# location of file to load users for authorization
authorization_users_file = users
# max number of groups supported for authorization
authorization_num_groups = 10
# location of file to load groups for authorization
authorization_groups_file = groups


[uatcpmsg]

# max number of parallel client tcp connections
num_conns = 5
# number of listener sockets, one is needed for each endpoint
num_listeners = 1
# max number of parallel service calls (should be same as session.num_calls)
num_ctxts = 50
# max number of chunks per message; multiply with network.buffer_size to get max message size.
num_chunks = 4

[seconv]

# max number of parallel secure channels
num_channels = 10
# When true, the oldest channel without an activated session is deleted if num_channels is reached
keep_spare_channel = true

[pkistore]

pkistores/size = 1
# backend specific configuration string
# file backend: store_root_path;max_trusted;max_issuers;max_rejected;max_private
# memory backend: max_trusted;max_issuers;max_rejected;max_private
pkistores/0/config = "pki_store_0;5;5;5;5"

[endpoint]

# Array of certificates and private keys
# "certificate" location of the certificate
#    (file://[path] for file system; store://[sha1] for PKI store)
# "key" location of the private key for the "certificate"
#    (file://[path] for file system; store://[sha1_of_cert] for PKI store)
# "create" the "certificate" and "key" is created if they do not yet exist
# Note: the following options are only required if "create" is true
# "days_valid" number of days the created certificate is valid
# "algorithm" algorithm for creating the signature: sha1 or sha256
# "key_length" length of the private key to be created (in bit): 1024, 2048 or 4096
# "issuer_index" index of the key for signing the created certificate
certificates/size = 2
certificates/0/certificate = file://cert1024.der
certificates/0/key = file://private1024.key
certificates/0/create = true
certificates/0/days_valid = 365
certificates/0/algorithm = sha1
certificates/0/key_length = 1024
certificates/0/issuer_index = 0
certificates/1/certificate = file://cert2048.der
certificates/1/key = file://private2048.key
certificates/1/create = true
certificates/1/days_valid = 1448
certificates/1/algorithm = sha256
certificates/1/key_length = 2048
certificates/1/issuer_index = 1

# Array of user tokens
# "name" unique name for the token
# "type": Anonymous or Username
# "policy_id" encryption used for the password, only needed for Username tokens
user_tokens/size = 2
user_tokens/0/name = Anonymous_Token
user_tokens/0/type = Anonymous
user_tokens/1/name = Username_256_Token
user_tokens/1/type = Username
user_tokens/1/policy_id = http://opcfoundation.org/UA/SecurityPolicy#Basic256

# Array of security policies
# "policy_id" algorithms for encryption/signature to use
# "store" is the certificate store index in the "pkistore" section of this file.
# It configures the certificate store instance to use for storing and validating
# certificates received on this endpoint.
# "verification_flags" controls the certificate verification process.
# Possible values for verification_flags (combination by bitwise OR):
# IGNORE_TIMEINVALID             0x0001: ignore certificate time errors
# IGNORE_ISSUERTIMEINVALID       0x0002: ignore certificate time errors in issuer certificates
# IGNORE_REVOCATIONUNKNOWN       0x0004: ignore missing certificate crl
# IGNORE_ISSUERREVOCATIONUNKNOWN 0x0008: ignore missing issuer certificate crl
# IGNORE_UNTRUSTED               0x0010: ignore if a cert is untrusted
# CHECK_COMPLETE_CHAIN           0x0020: do not stop at the first error
# DISABLE_CRL_CHECK              0x0040: disable CRL check
# "max_verify_results" controls the dimension of the result array containing the number of ignored errors.
# "max_chain_length" sets the maximum certificate chain length.
# The "mode_" flags control the message security mode for each configured policy.
# "certificate" is the index in the certificates configuration section of the
# server certificate and key pair used for this policy.
security_policies/size = 4
security_policies/0/policy_id = http://opcfoundation.org/UA/SecurityPolicy#None
security_policies/0/store = 0
security_policies/0/ign_time = false
security_policies/0/ign_issuer_time = false
security_policies/0/ign_miss_crl = false
security_policies/0/ign_miss_issuer_crl = false
security_policies/0/ign_untrusted = false
security_policies/0/check_complete_chain = true
security_policies/0/disable_crl_check = false
security_policies/0/max_verify_results = 5
security_policies/0/max_chain_length = 3
security_policies/0/mode_sign = false
security_policies/0/mode_sign_and_encrypt = false
security_policies/0/mode_none = true
security_policies/0/certificate = -1
security_policies/1/policy_id = http://opcfoundation.org/UA/SecurityPolicy#Basic256
security_policies/1/store = 0
security_policies/1/ign_time = false
security_policies/1/ign_issuer_time = false
security_policies/1/ign_miss_crl = false
security_policies/1/ign_miss_issuer_crl = false
security_policies/1/ign_untrusted = false
security_policies/1/check_complete_chain = true
security_policies/1/disable_crl_check = false
security_policies/1/max_verify_results = 5
security_policies/1/max_chain_length = 3
security_policies/1/mode_sign = false
security_policies/1/mode_sign_and_encrypt = true
security_policies/1/mode_none = false
security_policies/1/certificate = 0
security_policies/2/policy_id = http://opcfoundation.org/UA/SecurityPolicy#Basic256Sha256
security_policies/2/store = 0
security_policies/2/ign_time = false
security_policies/2/ign_issuer_time = false
security_policies/2/ign_miss_crl = false
security_policies/2/ign_miss_issuer_crl = false
security_policies/2/ign_untrusted = false
security_policies/2/check_complete_chain = true
security_policies/2/disable_crl_check = false
security_policies/2/max_verify_results = 5
security_policies/2/max_chain_length = 3
security_policies/2/mode_sign = true
security_policies/2/mode_sign_and_encrypt = true
security_policies/2/mode_none = false
security_policies/2/certificate = 1
security_policies/3/policy_id = http://opcfoundation.org/UA/SecurityPolicy#Basic256
security_policies/3/store = 0
security_policies/3/ign_time = false
security_policies/3/ign_issuer_time = false
security_policies/3/ign_miss_crl = false
security_policies/3/ign_miss_issuer_crl = false
security_policies/3/ign_untrusted = false
security_policies/3/check_complete_chain = true
security_policies/3/disable_crl_check = false
security_policies/3/max_verify_results = 5
security_policies/3/max_chain_length = 3
security_policies/3/mode_sign = true
security_policies/3/mode_sign_and_encrypt = false
security_policies/3/mode_none = false
security_policies/3/certificate = 1

# Array of endpoints
# Note: at the moment only one endpoint is supported
# "endpoint_url" url given to clients to connect to the server
# "bind_address" IP to bind endpoint to (0.0.0.0 for all)
# "bind_port" port to bind endpoint to
# "security_policies" indices to the security_policies array for policies to use for this endpoint
# "user_tokens" indices to the user_tokens array for tokens to use for this endpoint
endpoints/size = 1
endpoints/0/endpoint_url = opc.tcp://[hostname]:4840
endpoints/0/bind_address = 0.0.0.0
endpoints/0/bind_port = 4840
endpoints/0/security_policies = 0,1,2,3
endpoints/0/user_tokens = 0,1

