/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#include <uabase/uatype_config.h>
#ifdef ENABLE_SERVICE_SUBSCRIPTION
#include <uaserver/monitoreditem/monitoreditem.h>
#include <uaserver/attribute/read_internal.h>
#include <uaserver/addressspace/variable.h>
#include <uabase/statuscodes.h>
#include <uabase/variant.h>
#include <uabase/datavalue.h>
#include <uabase/attributeid.h>
#include <uabase/structure/nodeclass.h>
#include <uaprovider/provider.h>
#include <memory/memory.h>
#include <timer/timer.h>
#include <trace/trace.h>

#include "custom_provider.h"
#include "custom_provider_read.h"
#include "custom_provider_subscription.h"

/*! [sampling_callback] */
/* callback for timer if sampling interval of monitored item is due */
static int custom_provider_timer_callback(uint64_t elapsed, void *data)
{
    struct ua_monitoreditem *item = data;
    struct uasession_session *session;
    struct ua_datavalue *dv;
    uint8_t storeidx = 0;
    ua_node_t node = ua_node_find(&item->nodeid);

    UA_UNUSED(elapsed);

    /* allocate the value */
    dv = IPC_ALLOC(dv);
    if (dv == NULL)
        return 0;
    ua_datavalue_init(dv);

    /* read the value */
    if (node == UA_NODE_INVALID) {
        dv->status = UA_SCBADNODEIDUNKNOWN;
    } else {
        session = ua_monitoreditem_get_session(item);
        if (session) {
            if (item->attributeid == UA_ATTRIBUTEID_VALUE) {
                ua_variable_get_store_index(node, &storeidx);
                if (storeidx != 0) {
                    uaserver_read_internal(node, session, item->ts, item->attributeid, item->range, item->num_ranges, dv);
                } else {
                    custom_provider_read_value(node, session, item->ts, item->range, item->num_ranges, dv);
                }
            } else {
                uaserver_read_internal(node, session, item->ts, item->attributeid, item->range, item->num_ranges, dv);
            }
        } else {
            dv->status = UA_SCBADSESSIONCLOSED;
        }
    }

    /* add the value to the monitored item */
    ua_monitoreditem_new_value(item, dv);
    return 0;
}
/*! [sampling_callback] */

/*! [add_item] */
ua_statuscode custom_provider_add_item(struct ua_monitoreditem *item, uint32_t max_items)
{
    int ret;
    uint32_t timer_id;
    uint8_t storeidx = 0;
    struct uasession_session *session;
    struct ua_datavalue *dv;
    ua_statuscode status;
    ua_node_t node;

    UA_UNUSED(max_items);

    /* lookup node handle */
    node = ua_node_find(&item->nodeid);
    if (node == UA_NODE_INVALID) return UA_SCBADNODEIDUNKNOWN;

    /* get session from monitored item */
    session = ua_monitoreditem_get_session(item);
    if (session == NULL) return UA_SCBADSESSIONCLOSED;

    /* allocate the initial value */
    dv = IPC_ALLOC(dv);
    if (dv == NULL)
        return UA_SCBADOUTOFMEMORY;
    ua_datavalue_init(dv);

    /* read the initial value */
    if (item->attributeid == UA_ATTRIBUTEID_VALUE) {
        ua_variable_get_store_index(node, &storeidx);
        if (storeidx != 0) {
            uaserver_read_internal(node, session, item->ts, item->attributeid, item->range, item->num_ranges, dv);
        } else {
            custom_provider_read_value(node, session, item->ts, item->range, item->num_ranges, dv);
        }
    } else {
        uaserver_read_internal(node, session, item->ts, item->attributeid, item->range, item->num_ranges, dv);
    }

    /* Check the result of the initial read. If bad the item is not created with few execptions:
     * INDEXRANGENODATA: a bigger array may be written to this value later
     * NOTREADABLE, USERACCESSDENIED: permissions of node or user may change
     */
    if (ua_statuscode_is_bad(dv->status)
        && dv->status != UA_SCBADNOTREADABLE
        && dv->status != UA_SCBADINDEXRANGENODATA
        && dv->status != UA_SCBADUSERACCESSDENIED) {

        status = dv->status;
        ua_datavalue_clear(dv);
        ipc_free(dv);
        return status;
    }

    /* add a timer to check the attribute periodically */
    ret = timer_add(NULL,
                    &timer_id,
                    item->sampling_interval,
                    custom_provider_timer_callback,
                    item,
                    TIMER_DEFAULT);

    if (ret != 0) {
        if (ret == UA_EBADOUTOFRESOURCE) {
            TRACE_ERROR(TRACE_FAC_APPLICATION, "%s: Not enough timers to create monitoreditem\n", __func__);
        }
        ua_datavalue_clear(dv);
        ipc_free(dv);
        return UA_SCBADRESOURCEUNAVAILABLE;
    }

    item->user_data[0] = timer_id;

    /* add the initial value */
    ua_monitoreditem_new_value(item, dv);

    /* set operation result, this can be done in subscribe, but we already know it will succeed */
    item->operation_result = 0;

    return 0;
}
/*! [add_item] */

/*! [remove_item] */
ua_statuscode custom_provider_remove_item(struct ua_monitoreditem *item, uint32_t max_items)
{
    int ret = timer_remove(NULL, item->user_data[0], NULL);
    if (ret != 0)
        return UA_SCBADINTERNALERROR;

    UA_UNUSED(max_items);

    /* set operation result, this can be done in subscribe, but we already know it will succeed */
    item->operation_result = 0;

    return 0;
}
/*! [remove_item] */

/*! [modify_item] */
ua_statuscode custom_provider_modify_item(struct ua_monitoreditem *item, uint32_t new_sampling_interval, uint32_t max_items)
{
    uint32_t timer_id;
    int ret;

    UA_UNUSED(max_items);

    /* add new timer */
    ret = timer_add(NULL,
            &timer_id,
            new_sampling_interval,
            custom_provider_timer_callback,
            item,
            TIMER_DEFAULT);

    if (ret == UA_EBADOUTOFRESOURCE) {
        TRACE_ERROR(TRACE_FAC_APPLICATION, "%s: Not enough timers to modify monitoreditem\n", __func__);
        return UA_SCBADRESOURCEUNAVAILABLE;
    } else if (ret != 0) {
        return UA_SCBADRESOURCEUNAVAILABLE;
    }

    /* update sampling interval in the item */
    item->sampling_interval = new_sampling_interval;

    /* remove old timer */
    timer_remove(NULL, item->user_data[0], NULL);

    /* store id of new timer in item */
    item->user_data[0] = timer_id;

    /* set operation result, this can be done in subscribe, but we already know it will succeed */
    item->operation_result = 0;

    return 0;
}
/*! [modify_item] */

/*! [subscribe] */
void custom_provider_subscribe(struct uaprovider_subscribe_ctx *ctx) {
    /* the item is added/removed in the sync functions completely, so nothing to do here */
    uaserver_subscribe_complete(ctx);
}
/*! [subscribe] */

#endif /* ENABLE_SERVICE_SUBSCRIPTION */
