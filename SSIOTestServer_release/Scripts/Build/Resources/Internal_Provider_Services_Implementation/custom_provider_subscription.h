/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef CUSTOM_PROVIDER_SUBSCRIPTION_H
#define CUSTOM_PROVIDER_SUBSCRIPTION_H

#include <uabase/uatype_config.h>

#ifdef ENABLE_SERVICE_SUBSCRIPTION
#include <uaprovider/provider.h>
#include <uaserver/monitoreditem/monitoreditem.h>

ua_statuscode custom_provider_add_item(struct ua_monitoreditem *item, uint32_t max_items);
ua_statuscode custom_provider_remove_item(struct ua_monitoreditem *item, uint32_t max_items);
ua_statuscode custom_provider_modify_item(struct ua_monitoreditem *item, uint32_t new_sampling_interval, uint32_t max_items);
void custom_provider_subscribe(struct uaprovider_subscribe_ctx *ctx);
#endif /* ENABLE_SERVICE_SUBSCRIPTION */

#endif /* end of include guard: CUSTOM_PROVIDER_SUBSCRIPTION_H */

