#!/bin/bash

#navigate into CMakeList folder
cd Resources/CMakeList_for_LCES2

rm ../../../../CMakeLists.txt

cp CMakeLists.txt ../../../../

#navigate out of the current folder
cd ../../../../

#Delete current build folder and build project again
rm -r build_LCES2

mkdir build_LCES2
cd build_LCES2

#Set environment to use ARM toolchain

source /opt/poky/2.0.2/environment-setup-cortexa7hf-vfp-vfpv4-d16-poky-linux-gnueabi 

#build with cmake and appropriate command line parameters

cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_INSTALL_PREFIX=$PWD -DCMAKE_BUILD_TYPE=Debug -DOPENSSL_CRYPTO_LIBRARY=/opt/poky/2.0.2/sysroots/cortexa7hf-vfp-vfpv4-d16-poky-linux-gnueabi/lib/libcrypto.so -DOPENSSL_INCLUDE_DIR=/opt/poky/2.0.2/sysroots/cortexa7hf-vfp-vfpv4-d16-poky-linux-gnueabi/usr/include/openssl -DOPENSSL_SSL_LIBRARY=/opt/poky/2.0.2/sysroots/cortexa7hf-vfp-vfpv4-d16-poky-linux-gnueabi/usr/lib/libssl.so ..


#make
make

#copy files using copyFiles.sh
#cd ../Scripts/Build
#sh copyFiles.sh

#copy Ubuntu CMake files back to original position
cd ../Scripts/Build/Resources/CMakeList_for_Ubuntu

rm ../../../../CMakeLists.txt

cp CMakeLists.txt ../../../../

cd ../../



sh copyServerSSIO.sh

sh SSIOserver.sh


exit 3

