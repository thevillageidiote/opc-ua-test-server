#!/bin/bash

if [ $# -eq 0 ] ; then 
echo "No arguments provided. Log will start with time interval of 1s"
SleepInterval=1
else
SleepInterval=$1
echo "Log will start with time interval of "$1"s"
fi

echo "press enter to exit the program"

cd ../../../build_ubuntu

./uaserver_test_model  &

uaServerPID=$!

cd ../Scripts/Test/Ubuntu

#Clear ps.log
> ps.log

#Start loop
while true ;

do 

#read ENTER key and breaks loop if ENTER is pressed
read -t 0.01 -r && { read -r; break; }

#print top usage to log

(top -n 1 -b -p $uaServerPID | tail -n 1 | awk '{print $9"\t"$10}' ) >> ps.log

sleep $SleepInterval

done

echo "written"

kill $uaServerPID

exit
