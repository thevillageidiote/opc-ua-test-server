/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef CUSTOM_PROVIDER_STORE_H
#define CUSTOM_PROVIDER_STORE_H

#include <stdint.h>
#include <stdbool.h>
#include <uabase/statuscode.h>
#include <uabase/datavalue.h>
#include <uabase/indexrange.h>
#include <uaserver/addressspace/addressspace.h>

#define PORTSTATE_OFF 0
#define PORTSTATE_ON 1

#define NO_CONFIG 0
#define AI_TYPE 1
#define AO_TYPE 2
#define DI_TYPE 3
#define DO_TYPE 4

#define VALUESELECT_PORTNO 0
#define VALUESELECT_PORTSTATE 1
#define VALUESELECT_PORTTYPE 2
#define VALUESELECT_SOCKFD 3


struct portConfig 
{

    struct ua_variant portNo;       //uint_16
    struct ua_variant portState;    //int32
    struct ua_variant portType;     //int32
    int sockfd;       //int32
    
};

/*! [store_initial] */
int custom_store_set_initial_value_double(unsigned int idx, double value) ;
/*! [store_initial] */

/*! [store_get] */
void custom_store_get_value(
        void *store,
        ua_node_t node,
        unsigned int idx,
        bool source_ts,
        struct ua_indexrange *range,
        unsigned int num_ranges,
        struct ua_datavalue *result);
/*! [store_get] */

/*! [store_attach] */
ua_statuscode custom_store_attach_value(
        void *store,
        ua_node_t node,
        unsigned int idx,
        struct ua_indexrange *range,
        unsigned int num_ranges,
        struct ua_datavalue *value);
/*! [store_attach] */


int custom_store_get_idx_for_portConfigList (int asicNo,int portNo,int valueSelect);
int custom_store_attach_portConfigValue (struct ua_variant value, ua_node_t node, int idx);
int custom_store_set_initial_value_ui16(unsigned int idx, uint16_t value) ;
int custom_store_set_initial_value_boolean(unsigned int idx, bool value) ;




#endif /* CUSTOM_PROVIDER_STORE_H */
