
#ifndef CUSTOM_PROVIDER_ASIC_NODES_H
#define CUSTOM_PROVIDER_ASIC_NODES_H

#define AINPORTTYPEID 1004
#define DINPORTTYPEID 1005
#define ASICTYPEID 1006


#define PORTCONFIGTYPE 1002
#define NODEFOLDERID 5002

ua_node_t ASIC_createASIC(uint16_t nsidx, uint16_t ASICNumber, ua_node_t parent);

int ASIC_deletePort(uint16_t nsidx, ua_node_t parent,uint16_t portNo);
int ASIC_modifyPort(uint16_t nsidx, ua_node_t asic_node, uint16_t portNo, int portState);


ua_node_t ASIC_createAINPort(uint16_t nsidx, ua_node_t asic_node, uint16_t ASICNo, uint16_t portNo );
ua_node_t ASIC_createDINPort(uint16_t nsidx, ua_node_t asic_node, uint16_t ASICNo, uint16_t portNo );



#endif