
/*
Contains functions that relate to creating Nodes in the Address Sppace of the Server
*/


#include <uabase/statuscodes.h>
#include <uabase/nodeid.h>
#include <uabase/variant.h>
#include <uabase/structure/nodeclass.h>
#include <uabase/identifiers.h>
#include <uaserver/addressspace/addressspace.h>
#include <uaserver/addressspace/object.h>
#include <uaserver/addressspace/variable.h>
#include <trace/trace.h>
#include <uabase/qualifiedname.h>



#include "custom_provider.h"
#include "custom_provider_ASIC_nodes.h"
#include "custom_provider_method_handler.h"
#include "custom_provider_ASIC_methods.h"
#include "util_functions.h"
#include "custom_provider_store.h"
#include "numNodeDefinition.h"

#include "socket_client.h"




extern struct portConfig g_portConfigList[MAXASICNO*MAXPORTSPERASIC];



ua_node_t ASIC_createASIC(uint16_t nsidx, uint16_t ASICNumber, ua_node_t parent)
{

    int ret;

    struct ua_string strnodeid;
    struct ua_nodeid nodeid;
    ua_node_t asic_node, asic_type, config_node;
    ua_ref_t ref;
    const char * displayname;

    struct ua_qualifiedname browsename = UA_QUALIFIEDNAME_INITIALIZER;
    struct ua_qualifiedname currentvalue = UA_QUALIFIEDNAME_INITIALIZER;

    //Find ASIC Type
    asic_type = ua_node_find_numeric(nsidx,ASICTYPEID);
    if (asic_type == UA_NODE_INVALID) return UA_NODE_INVALID;

    //Set string node id
    ua_string_init(&strnodeid);
    ua_string_snprintf(&strnodeid,14,"ASIC%02u",ASICNumber);
    displayname = ua_string_const_data(&strnodeid);
    ua_nodeid_set_string(&nodeid,nsidx,displayname);

    asic_node = ua_object_create_instance(&nodeid,asic_type,displayname,displayname);
    if (asic_node == UA_NODE_INVALID) return UA_NODE_INVALID;

    // add organises reference from parent to new asic
    ref = ua_reference_add(parent,asic_node,UA_NODE_ORGANIZES);
    if (ref == UA_REF_INVALID) return UA_NODE_INVALID;

    //Create browsename for Config Object
    ua_qualifiedname_set(&currentvalue,nsidx,"Config");

    // Iterate through all references of the newly created ASIC node to find config object
    ua_node_foreach(ref,asic_node)
    {
        config_node = ua_reference_target(ref);
        ret = ua_node_get_browsename(config_node, &browsename);
        if (ret != 0) continue;

        //Check if qualified names match
        if(ua_qualifiedname_compare(&browsename, &currentvalue) == 0)
        {

            //Create add/delete/modify port methods
            ret = ASIC_createAddPortMethod(nsidx,config_node,ASICNumber);
            if (ret != 0) {
                ua_qualifiedname_clear(&browsename);
                break;
            }

            ret = ASIC_createDeletePortMethod(nsidx,config_node,ASICNumber);
            if (ret != 0) {
                ua_qualifiedname_clear(&browsename);
                break;
            }


            ret = ASIC_createModifyPortMethod(nsidx,config_node,ASICNumber);
            if(ret!=0){
                ua_qualifiedname_clear(&browsename);
                break;
            }

        }

    }

    ua_qualifiedname_clear(&currentvalue);
    ua_string_clear(&strnodeid);
    ua_nodeid_clear(&nodeid);

    return asic_node;

}

ua_node_t ASIC_createPortConfig(uint16_t nsidx,ua_node_t port,ua_node_t asic_node, uint16_t portNo)
{

    ua_node_t port_config_type,port_config, config,operating_mode,port_no,port_state;
    struct ua_string strnodeid,temp;

    struct ua_nodeid nodeid;

    const char * browsename;
    const char * displayname;

    struct portConfig * currentPortConfig;

    int ret, sockfd,idx;
    uint16_t ASICNo;
    uint8_t tempval;

    //Find port_config_type
    port_config_type = ua_node_find_numeric(nsidx,PORTCONFIGTYPE);
    if(port_config_type == UA_NODE_INVALID){
        TRACE_ERROR(TRACE_FAC_PROVIDER,"Could not find Port Config Type");
        return UA_NODE_INVALID;
    }

    //Create Port Config
    ua_string_init(&strnodeid);
    ua_string_init(&temp);
    
    ua_string_snprintf(&temp,30,"Port%02u_Config",portNo);
    displayname = ua_string_const_data(&temp);

    ua_node_get_nodeid(asic_node,&nodeid);
    ASICNo = util_returnNofromString (ua_string_const_data(nodeid.identifier.string));

    ua_string_copy(&strnodeid,nodeid.identifier.string);
    ua_string_catf(&strnodeid,30,".Config.Port%02u_Config",portNo);
    browsename = ua_string_const_data(&strnodeid);

    ua_nodeid_set_string(&nodeid,nsidx, browsename);

    //Search for preexisting copies of Port Config first
    port_config = ua_node_find(&nodeid);
    if(port_config != UA_NODE_INVALID)
    {

        TRACE_ERROR(TRACE_FAC_PROVIDER,"Found pre-existing port config node. Will not create a new one. \n");
        return (ua_node_t) -2;
    }  

    else 
    {

        //Could not find a pre-existing port config. 
        //Create our own one
        port_config = ua_object_create_instance(&nodeid,port_config_type,browsename,displayname);
        if(port_config == UA_NODE_INVALID)
        {
            TRACE_ERROR(TRACE_FAC_PROVIDER,"Failed to create instance of Current Port Config");
            ua_string_clear(&strnodeid);
            ua_string_clear (&temp);
            return UA_NODE_INVALID;
        }
    
        //Find Config Port and add reference
        config = util_findNodefromForwardRef(asic_node,nsidx,"Config");
        if (config == UA_NODE_INVALID)
        {
            return UA_NODE_INVALID;
        } else 
        {
            ua_reference_add(config,port_config,UA_NODE_ORGANIZES);
        }

        //Find Operating Mode Variable and add reference
        operating_mode = util_findNodefromForwardRef(port_config,nsidx,"OperatingMode");
        if (operating_mode == UA_NODE_INVALID)
        {
            return UA_NODE_INVALID;
        } else 
        {
            //ua_reference_add(port,operating_mode,UA_NODE_HASPROPERTY);
        }

        port_no = util_findNodefromForwardRef(port_config,nsidx,"PortNo");
        port_state = util_findNodefromForwardRef(port_config,nsidx,"PortState");

        //Initialise values in portConfigList with default values
        currentPortConfig = &g_portConfigList[portNo-1];

        //Create a variant and attach it to the store.
        ua_variant_init(&(currentPortConfig -> portNo));
        ua_variant_set_uint16(&(currentPortConfig -> portNo), portNo);
        idx = custom_store_get_idx_for_portConfigList(ASICNo,portNo,VALUESELECT_PORTNO);
        ret = custom_store_attach_portConfigValue(currentPortConfig -> portNo, port_no,idx);
        if (ret < 0) TRACE_ERROR(TRACE_FAC_PROVIDER,"Could not store into node. idx = %i\n",idx);
        
        ua_variant_init(&(currentPortConfig -> portState));
        ua_variant_set_int32 (&(currentPortConfig -> portState),PORTSTATE_ON);
        idx = custom_store_get_idx_for_portConfigList(ASICNo,portNo,VALUESELECT_PORTSTATE);
        ret = custom_store_attach_portConfigValue(currentPortConfig -> portState, port_state,idx);
        if (ret < 0) TRACE_ERROR(TRACE_FAC_PROVIDER,"Could not store into node. idx = %i\n",idx);

        ua_variant_init(&(currentPortConfig -> portType));
        ua_variant_set_int32(&(currentPortConfig -> portType), AI_TYPE);
        idx = custom_store_get_idx_for_portConfigList(ASICNo,portNo,VALUESELECT_PORTTYPE);
        custom_store_attach_portConfigValue(currentPortConfig -> portType, operating_mode,idx);

        //Connect to SSIO Scanner server
        //This command will return an error if the SSIO Scanner application has not been started
        //as it cannot connect to the SSIO Scanner server
        sockfd = initClientSocket();
        ret = connectToServer(sockfd,socket_address);
        currentPortConfig -> sockfd = sockfd;

        /*
        //Find socket FD variable and add reference
        socket_fd = util_findNodefromForwardRef(port_config,nsidx,"Socket FD");
        if( socket_fd == UA_NODE_INVALID)
        {
            return UA_NODE_INVALID;
        } else
        {
            ua_reference_add(port,socket_fd,UA_NODE_HAS_PROPERTY);
        }
        */

        //
        ua_string_clear(&strnodeid);
        ua_string_clear (&temp);
        ua_nodeid_clear(&nodeid);

        return port_config;

    }
    
}





ua_node_t ASIC_createAINPort(uint16_t nsidx, ua_node_t asic_node, uint16_t ASICNo, uint16_t portNo )
{

    int ret, valueidx;
    struct ua_nodeid nodeid;
    ua_node_t port, portType, current_value, config_node,port_config;
    ua_ref_t ref;


    const char * displayname;
    const char * browsename;


    struct ua_string strnodeid,temp;

    // Find Analog In Port Type
    portType = ua_node_find_numeric(nsidx,AINPORTTYPEID);
    if(portType == UA_NODE_INVALID){
        TRACE_ERROR(TRACE_FAC_PROVIDER,"Could not find Analog in Port Type");
    }

    // Create Analog In Port
    ua_string_init(&strnodeid);
    ua_string_init(&temp);

    ua_string_snprintf(&temp,30,"Port%02u",portNo);
    displayname = ua_string_const_data(&temp);

    ua_node_get_nodeid(asic_node,&nodeid);
    ua_string_copy(&strnodeid,nodeid.identifier.string);
    ua_string_catf(&strnodeid,30,".Port%02u",portNo);
    browsename = ua_string_const_data(&strnodeid);

    ua_nodeid_set_string(&nodeid,nsidx, browsename);
    port = ua_object_create_instance(&nodeid,portType,browsename,displayname);

    if(port == UA_NODE_INVALID){
        TRACE_ERROR(TRACE_FAC_PROVIDER,"Failed to create instance of AIn Node");
        ua_string_clear(&strnodeid);
        return -1;
    }

    // add has component reference from parent to new port  
    ua_reference_add(asic_node,port,UA_NODE_HASCOMPONENT);

    //Add port config
    port_config = ASIC_createPortConfig(nsidx,port,asic_node,portNo);
    if( port_config == UA_NODE_INVALID) return UA_NODE_INVALID;
   
    //Find Current Value node to add Store reference
    current_value = util_findNodefromForwardRef(port,nsidx,"CurrentValue");
    
    //Calculate valueidx from ASICNo and Port No.
    valueidx = (ASICNo-1)*8+(portNo-1);
    
    //Set initial Value of current Val
    ret = custom_store_set_initial_value_ui16(valueidx,0);
    if(ret !=0 ) return UA_NODE_INVALID;

    //Write the storeindex to the node
    ret = ua_variable_set_store_index(current_value,g_custom_store_idx);
    if(ret !=0 ) return UA_NODE_INVALID;

    //Write the valueidx to the node
    ret = ua_variable_set_value_index(current_value,valueidx);
    if(ret !=0 ) return UA_NODE_INVALID;

    //End writing Store reference

    //Cleanup
    ua_string_clear(&strnodeid);
    ua_string_clear(&temp);
    ua_nodeid_clear(&nodeid);

    TRACE_ERROR(TRACE_FAC_PROVIDER,"Created AIN Port");

    return port;

}


ua_node_t ASIC_createDINPort(uint16_t nsidx, ua_node_t asic_node, uint16_t ASICNo, uint16_t portNo )
{

    int ret, valueidx;
    struct ua_nodeid nodeid;
    ua_node_t port, portType, current_value, config_node,port_config;
    ua_ref_t ref;


    const char * displayname;
    const char * browsename;


    struct ua_string strnodeid,temp;

    // Find Digital In Port Type
    portType = ua_node_find_numeric(nsidx,DINPORTTYPEID);
    if(portType == UA_NODE_INVALID){
        TRACE_ERROR(TRACE_FAC_PROVIDER,"Could not find Digital in Port Type");
    }

    // Create Digital In Port
    ua_string_init(&strnodeid);
    ua_string_init(&temp);

    ua_string_snprintf(&temp,30,"Port%02u",portNo);
    displayname = ua_string_const_data(&temp);

    ua_node_get_nodeid(asic_node,&nodeid);
    ua_string_copy(&strnodeid,nodeid.identifier.string);
    ua_string_catf(&strnodeid,30,".Port%02u",portNo);
    browsename = ua_string_const_data(&strnodeid);

    ua_nodeid_set_string(&nodeid,nsidx, browsename);
    port = ua_object_create_instance(&nodeid,portType,browsename,displayname);

    if(port == UA_NODE_INVALID){
        TRACE_ERROR(TRACE_FAC_PROVIDER,"Failed to create instance of DI_TYPE Node");
        ua_string_clear(&strnodeid);
        return -1;
    }

    // add has component reference from parent to new port  
    ua_reference_add(asic_node,port,UA_NODE_HASCOMPONENT);

    //Add port config
    port_config = ASIC_createPortConfig(nsidx,port,asic_node,portNo);
    if( port_config == UA_NODE_INVALID) return UA_NODE_INVALID;
   
    //Find Current Value node to add Store reference
    current_value = util_findNodefromForwardRef(port,nsidx,"CurrentValue");
    if (current_value == UA_NODE_INVALID) {
        TRACE_ERROR(TRACE_FAC_PROVIDER,"Can't find current val node");
    }

    //Calculate valueidx from ASICNo and Port No.
    valueidx = (ASICNo-1)*8+(portNo-1);
    
    //Set initial Value of current Val
    ret = custom_store_set_initial_value_boolean(valueidx,true);
    if(ret !=0 ) return UA_NODE_INVALID;

    //Write the storeindex to the node
    ret = ua_variable_set_store_index(current_value,g_custom_store_idx);
    if(ret !=0 ) return UA_NODE_INVALID;

    //Write the valueidx to the node
    ret = ua_variable_set_value_index(current_value,valueidx);
    if(ret !=0 ) return UA_NODE_INVALID;

    //End writing Store reference

    //Cleanup
    ua_string_clear(&strnodeid);
    ua_string_clear(&temp);
    ua_nodeid_clear(&nodeid);

    TRACE_ERROR(TRACE_FAC_PROVIDER,"Created DIN Port");

    return port;

}




int ASIC_deletePort(uint16_t nsidx, ua_node_t asic_node,uint16_t portNo)
{

    int ret;
    ua_node_t port,port_config,config_node;

    const char * portBrowseName; //= (char * ) malloc ((7)*sizeof(char));

    struct ua_string strnodeid;
    struct ua_nodeid nodeid;

    //Generate appropriate Port BrowseName

    ua_string_init(&strnodeid);

    ua_node_get_nodeid(asic_node,&nodeid);

    ua_string_copy(&strnodeid,nodeid.identifier.string);
    ua_string_catf(&strnodeid,30,".Port%02u",portNo);

    portBrowseName = ua_string_const_data(&strnodeid);
    TRACE_ERROR(TRACE_FAC_PROVIDER,"%s\n",portBrowseName);

    //Find Port from ASIC node 
    port = util_findNodefromForwardRef(asic_node,nsidx,portBrowseName);

    //delete it.
    util_deleteNodeRecursive(port,asic_node);

    //Cleanup
    ua_string_clear(&strnodeid);
    ua_nodeid_clear(&nodeid);


    return 0;
    

}



int ASIC_modifyPort(uint16_t nsidx, ua_node_t asic_node, uint16_t portNo, int portState)
{

    ua_node_t port, current_value, quality;
    ua_ref_t ref,prev_ref;

    const char * portBrowseName; 

    struct ua_string strnodeid;
    struct ua_nodeid ASICNodeId;

    uint16_t ASICNo, temp_value;
    int ret,sockfd;

    //Generate appropriate Browsename for the port and ASIC
    ua_string_init(&strnodeid);

    ua_node_get_nodeid(asic_node,&ASICNodeId);
    ASICNo = util_returnNofromString(ua_string_const_data(ASICNodeId.identifier.string));

    ua_string_copy(&strnodeid,ASICNodeId.identifier.string);
    ua_string_catf(&strnodeid,30,".Port%02u",portNo);
    portBrowseName = ua_string_const_data(&strnodeid);

    //Find and delete the port first
    port = util_findNodefromForwardRef(asic_node,nsidx,portBrowseName);
    util_deleteNodeRecursive(port,asic_node);   
    
    //Obtain sockfd from the port config list
    sockfd = g_portConfigList[portNo-1].sockfd;

    //create appropriate port based on portState
    switch (portState){
        //Analog In
        case AI_TYPE: 
            ret = ASIC_createAINPort(nsidx,asic_node,ASICNo,portNo);
            if(ret < 0){
                TRACE_ERROR(TRACE_FAC_PROVIDER,"Could not create AIN port");
            }
            
            //update portConfigList with appropriate portType
            g_portConfigList[portNo-1].portType.value.ui32 = AI_TYPE;

            //Initiate message sending process to send the config to the UIO ASIC
            sendMessage(sockfd,CLIENT_SENDCONFIG,AI_TYPE);
            receiveMessage(sockfd,&temp_value);
            break;
            
        case DI_TYPE: 
            ret = ASIC_createDINPort(nsidx,asic_node,ASICNo, portNo);
            if(ret < 0){
                TRACE_ERROR(TRACE_FAC_PROVIDER,"Could not create AIN port");
            }
                        
            //update portConfigList with appropriate portType
            g_portConfigList[portNo-1].portType.value.ui32 = DI_TYPE;

            //Initiate message sending process to send the config to the UIO ASIC            
            sendMessage(sockfd,CLIENT_SENDCONFIG,AI_TYPE);
            receiveMessage(sockfd,&temp_value);
            break;
        default:
            break;
    };

    //Cleanup
    ua_string_clear(&strnodeid);
    ua_nodeid_clear(&ASICNodeId);



}



