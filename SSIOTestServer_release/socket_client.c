#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
//#include <netinet/in.h>
#include <netdb.h> 

#include <arpa/inet.h>

#include <sys/time.h>
#include <time.h>



#include "socket_client.h"

const char * socket_address = "/home/root/socket";



void error(const char *msg)
{
    perror(msg);
    exit(0);
}

void checkRet(int ret,const char *msg)
{
    if(ret < 0){
        error(msg);
    }
}


int initClientSocket()
{

    int sockfd;

    sockfd = socket(AF_UNIX, SOCK_STREAM, 0);

    if (sockfd < 0) {
        error("ERROR opening socket");
    }

    return sockfd;
}

int connectToServer(int sockfd, const char * pathname)
{
    
    int ret;
    struct sockaddr_un serv_addr;
    
    serv_addr.sun_family = AF_UNIX;
    strcpy (serv_addr.sun_path,pathname);

    ret = connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr));
    
    if(ret < 0){
        close (sockfd);
        error("ERROR connecting to socket");
    }
    
    return ret;
}

void sendMessage(int sockfd,uint16_t selector,uint16_t data_value)
{

    uint16_t output_array[2];
    output_array[0] = selector;
    output_array[1] = data_value;

    write(sockfd,&output_array,sizeof(output_array));
}

int receiveMessage (int sockfd,  uint16_t *value)
{

    uint16_t output_array[2];


    //bzero(buffer,sizeof(buffer));
    read(sockfd,&output_array,sizeof(output_array));

    *value = output_array[1];

    return output_array[0];
    
}

void removeServer (int sockfd)
{
    close (sockfd);
}

/*
int main()
{


    struct timeval t_start,t_end;



    int sockfd,ret;

    char buffer[256];

    uint16_t value;

    sockfd = initClientSocket();
    ret = connectToServer(sockfd,"/home/root/socket");
    checkRet(ret,"Error connecting to socket");

    gettimeofday(&t_start,NULL);


    while(1)
    {


        sendAck(sockfd);

        ret = read(sockfd,&value,sizeof(value));

        if(ret > 0){
            printf("%i\n",value);
        }



        gettimeofday(&t_end,NULL);
        if ((t_end.tv_sec-t_start.tv_sec) > 100)
        {

	    printf("Ending Connection...");

            close(sockfd);
            break;

        }



    }

    return 0;




}
*/