

#define CLIENT_NODATA 0
#define CLIENT_READYTORECEIVE 1
#define CLIENT_SENDVAL 2
#define CLIENT_SENDCONFIG 3
#define CLIENT_RECEIVEVAL 4

#define SERVER_RECEIVED 1
#define SERVER_SENDVAL 2
#define SERVER_NODATA 0


extern const char * socket_address;

int initClientSocket();
int connectToServer(int sockfd, const char * pathname);
void removeServer (int sockfd);

void sendMessage(int sockfd,uint16_t selector,uint16_t data_value);
int receiveMessage (int sockfd,  uint16_t *value);

