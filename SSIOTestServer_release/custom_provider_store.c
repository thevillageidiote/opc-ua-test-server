/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#include <uabase/statuscodes.h>
#include <uaserver/addressspace/variable.h>

#include <trace/trace.h>

#include "custom_provider_store.h"
#include "numNodeDefinition.h"
#include "custom_provider.h"


/*! [store_values] */
struct ua_variant g_custom_provider_values[MAXASICNO * MAXPORTSPERASIC];
struct portConfig g_portConfigList[MAXASICNO * MAXPORTSPERASIC];
/*! [store_values] */

/*arrangement of *store* idx
[PORT0_VAL, PORT1_VAL,PORTN_VAL][PORT0_PORTNO, PORT0_PORTSTATE, PORT0_PORTTYPE, PORT0_SOCKFD ..... PORTN]


Actual corresponding layout in memory
[PORT0_VAL, PORT1_VAL,PORTN_VAL][port0_portConfig, port1_portConfig ... portN_portConfig]
[  g_custom_provider_values    ][                    g_portConfigList                   ]

Each portConfig is a structure containing the portNo, portState, portType and sockfd

*/


/*! [store_initial] */
int custom_store_set_initial_value_ui16(unsigned int idx, uint16_t value) 
{

    if (idx >= countof(g_custom_provider_values)) return -1;
    /* write value to the array */
    ua_variant_set_uint16(&g_custom_provider_values[idx],value);
    //g_custom_provider_values[idx].value.d = value;
    return 0;
}

int custom_store_set_initial_value_boolean(unsigned int idx, bool value) 
{

    if ((value == 1 ) || (value == 0) )
    {
        ua_variant_set_bool(&g_custom_provider_values[idx],value);
        return 0;
    }else 
    {
        return -1;
    }
    /* write value to the array */
    //g_custom_provider_values[idx].value.d = value;
}


/*! [store_initial] */

int custom_store_get_idx_for_portConfigList (int asicNo,int portNo,int valueSelect)
{

    int output;

    output = MAXASICNO*MAXPORTSPERASIC  + ((asicNo-1)*8 + (portNo-1))*4 + valueSelect;

    return output;
    //MAXASICNO*MAXPORTSPERASIC     --> offset for the values array
    //((asicNo-1)*8 + (portNo-1))*4 --> offset per port
    //valueSelect                   --> select which value to generate idx for

}

int custom_store_attach_portConfigValue (struct ua_variant value, ua_node_t node, int idx)
{

    int ret,temp,index,valueSelect;

    struct ua_variant * ua_variant_ptr;

    //Sanity check on idx bounds
    if (idx >= (countof(g_custom_provider_values)+ countof(g_portConfigList)*4))  return -1;
    
    //Collect index and valueSelect from the idx
    temp = idx - MAXASICNO*MAXPORTSPERASIC ;
    index = temp/4;
    valueSelect = temp%4;

    switch (valueSelect){
        case VALUESELECT_PORTNO:
            g_portConfigList[index].portNo = value;
            break;
        case VALUESELECT_PORTSTATE:
            g_portConfigList[index].portState = value;
            break;
        case VALUESELECT_PORTTYPE:
            g_portConfigList[index].portType = value;
            break;
        case VALUESELECT_SOCKFD: 
            TRACE_ERROR(TRACE_FAC_PROVIDER,"Cannot add variant into sockfd, which is of type int");
            return -1;
            //ua_variant_set_int32(&variant,g_portConfigList[*index].sockfd);
            //return &variant;
        default:
            break;
    }

    ret = ua_variable_set_store_index(node, g_custom_store_idx);
    if (ret != 0) return -1;
    ret = ua_variable_set_value_index(node, idx);
    if (ret != 0) return -1;

    return ret;


}


/*! [store_get] */
void custom_store_get_value(void *store, ua_node_t node, unsigned int idx, bool source_ts, struct ua_indexrange *range, unsigned int num_ranges, struct ua_datavalue *result)
{

    UA_UNUSED(node);
    UA_UNUSED(range);
    UA_UNUSED(store);

    /* check array bounds */
    if (idx >= (countof(g_custom_provider_values)+ countof(g_portConfigList)*4)) {
        result->status = UA_SCBADINTERNALERROR;
        return;
    }

    /* there are only scalar values in the store */
    if (num_ranges > 0) {
        result->status = UA_SCBADINDEXRANGENODATA;
        return;
    }


    //Check if Values array or portConfig array is being accessed

    if(idx < countof(g_custom_provider_values))
    {
        //Values array is being accessed.
        /* write value to result */

        result->value = g_custom_provider_values[idx];
    }


    //Port Config is being accessed.
    if(idx >= countof(g_custom_provider_values))
    {

        int index,temp,valueSelect;

        //result -> value = * (custom_store_idx_switch (idx,&index));

        //Collect index and valueSelect from the idx
        temp = idx - MAXASICNO*MAXPORTSPERASIC ;
        index = temp/4;
        valueSelect = temp%4;

        switch (valueSelect){
            case VALUESELECT_PORTNO:
                result -> value = g_portConfigList[index].portNo;
                break;
            case VALUESELECT_PORTSTATE:
                result -> value = g_portConfigList[index].portState;
                break;
            case VALUESELECT_PORTTYPE:
                result -> value = g_portConfigList[index].portType;
                break;
            case VALUESELECT_SOCKFD: 
                TRACE_ERROR(TRACE_FAC_PROVIDER,"Cannot add variant into sockfd, which is of type int");
                break;
                //ua_variant_set_int32(&variant,g_portConfigList[*index].sockfd);
                //return &variant;
            default:
                break;
        }

    }


    /* write value to result */

    /* add source timestamp if requested */
    if (source_ts) ua_datetime_now(&result->source_timestamp);

    /* set good statuscode */
    result->status = 0;
}
/*! [store_get] */

/*! [store_attach] */
ua_statuscode custom_store_attach_value(void *store, ua_node_t node, unsigned int idx, struct ua_indexrange *range, unsigned int num_ranges, struct ua_datavalue *value)
{
    UA_UNUSED(range);
    UA_UNUSED(store);
    UA_UNUSED(node);

    /* check array bounds */
    if (idx >= countof(g_custom_provider_values)) return UA_SCBADINTERNALERROR;

    /* there are only scalar values in the store */
    if (num_ranges > 0) return UA_SCBADWRITENOTSUPPORTED;

    /* write of status or timestamp is not supported */
    if (value->status != 0 || value->server_timestamp != 0 || value->source_timestamp != 0) {
        return UA_SCBADWRITENOTSUPPORTED;
    }


    /* write new value to the array */
    g_custom_provider_values[idx] = value->value;
    return 0;
}
/*! [store_attach] */
