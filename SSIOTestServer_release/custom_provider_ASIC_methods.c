
/*
Declares methods that are called by the ASIC during configuration
--AddPort
--DeletePort
--ModifyPort

*/



#include <uaserver/addressspace/addressspace.h>
#include <uabase/identifiers.h>
#include <uabase/statuscodes.h>

//Includes for creation of Methods and Arguments
#include <uabase/accesslevel.h>
#include <uabase/valuerank.h>
#include <uabase/structure/argument.h>
#include <uaserver/addressspace/variable.h>


//Include for error handling and debugging
#include <trace/trace.h>


#include "custom_provider_ASIC_methods.h"
#include "custom_provider_ASIC_nodes.h"
#include "custom_provider_method_handler.h"
#include "custom_provider.h"
#include "custom_provider_store.h"
#include "util_functions.h"


/*! [addPort_method] */
static ua_statuscode ASIC_addPortMethod(const struct ua_callmethodrequest *req, struct ua_callmethodresult *res, struct ua_nodeid objectid)
{

    int ret,portState;
    
    ua_node_t asic_node, config_node,port;
    uint16_t portNo,ASICNo;
    ua_ref_t ref;

    struct ua_string strnodeid;
    const char * ConfigNodeId;
    const char * ASICNodeId;

    /* input validation */
    if (req->num_input_arguments != 2) return UA_SCBADINVALIDARGUMENT;
    if (req->input_arguments[0].type != UA_VT_UINT16) return UA_SCBADINVALIDARGUMENT;
    if (req->input_arguments[1].type != UA_VT_INT32) return UA_SCBADINVALIDARGUMENT;


    //extract input arguments
    portNo = req->input_arguments[0].value.ui16;
    portState = req -> input_arguments[1].value.i32;

    //Find Config Node
    ua_string_init(&strnodeid);
    ConfigNodeId = ua_string_const_data(objectid.identifier.string);    
    config_node = ua_node_find_string(g_custom_provider_nsidx,ConfigNodeId);
    if(config_node == UA_NODE_INVALID) return UA_NODE_INVALID;

    //Find ASIC Node
    ASICNodeId = util_splitString(ConfigNodeId,'.',1);    
    asic_node = util_findNodefromInverseRef(config_node, g_custom_provider_nsidx,ASICNodeId);
    if(asic_node == UA_NODE_INVALID) return UA_SCBADNODEIDINVALID;

    ASICNo = util_returnNofromString(ASICNodeId);
    
    switch (portState){
        case AI_TYPE:
            //Create AINPort
            port = ASIC_createAINPort(g_custom_provider_nsidx,asic_node,ASICNo,portNo);
            break;
        case DI_TYPE:
            port = ASIC_createDINPort(g_custom_provider_nsidx,asic_node,ASICNo,portNo);
            break;
        default:
            break;
    }

    if (port == UA_NODE_INVALID) return UA_SCBADINTERNALERROR;    

    return 0;
}

int ASIC_createAddPortMethod(uint16_t nsidx, ua_node_t parent, uint16_t ASICNumber)
{

    int ret;

    //Declarations for node creation
    struct ua_string strnodeid;
    struct ua_nodeid nodeid, datatype_id;
    ua_node_t method_node, input_argument_node;
    ua_ref_t ref;
    const char * browsename;

    //Declarations for Argument init
    struct ua_argument args[2];
    struct ua_variant v;

    /*! [create_method_node] */

    //Set string node id
    ua_string_init(&strnodeid);
    ua_string_snprintf(&strnodeid,15,"ASIC%02u.AddPort",ASICNumber);
    browsename = ua_string_const_data(&strnodeid);
    ua_nodeid_set_string(&nodeid,nsidx,browsename);

    method_node = ua_node_create_with_attributes(
                      &nodeid,                        /* nodeid for the new node */
                      UA_NODECLASS_METHOD,            /* nodeclass of the new node */
                      nodeid.nsindex,                 /* ns index for browsename is same as nodeid */
                      browsename,                     /* browsename , NULL for same as displayname*/
                      "AddPort",                      /* displayname */
                      UA_NODE_INVALID,                /* the node has no typedefinition */
                      parent,                         /* parent node of the new node */
                      UA_NODE_HASCOMPONENT);          /* new node is referenced with hascomponent by parent */
    if (method_node == UA_NODE_INVALID) return -1;
    /*! [create_method_node] */


    /*! [create_input_argument_nodes] */
    //Set string node id
    ua_string_catf(&strnodeid,30,".InputArg");
    browsename = ua_string_const_data(&strnodeid);
    ua_nodeid_set_string(&nodeid,nsidx,browsename);

    input_argument_node = ua_node_create_with_attributes(
                &nodeid,                            /* nodeid for the new node */
                UA_NODECLASS_VARIABLE,              /* nodeclass of properties is variable */
                0,                                  /* ns index for browsename is namespace zero */
                "InputArguments",                   /* browsename ***Note*** BrowseName must be InputArguments exactly*/
                browsename,                         /* displayname */
                UA_NODE_PROPERTYTYPE,               /* the node has the typedefinition propertytype */
                method_node,                        /* parent of the new node is the method node */
                UA_NODE_HASPROPERTY);               /* new node is referenced with hasproperty by parent */
    if (input_argument_node == UA_NODE_INVALID) return -1;

    /*! [Initialise Arguments] */

    ua_nodeid_set_numeric(&datatype_id, 0, UA_ID_ARGUMENT);

    ret = ua_variable_set_attributes(input_argument_node, &datatype_id, UA_VALUERANK_ONEDIMENSION, UA_ACCESSLEVEL_CURRENTREAD, false);
    if (ret != 0) return ret;

    ua_argument_init(&args[0]);
    ua_string_attach_const(&args[0].name, "PortNumber");
    ua_string_attach_const(&args[0].description.text, "Specify a Port Number to add");
    ua_string_attach_const(&args[0].description.locale, "en-US");
    ua_nodeid_set_numeric(&args[0].data_type, 0, UA_ID_UINT16);
    args[0].value_rank = UA_VALUERANK_SCALAR;

    ua_argument_init(&args[1]);
    ua_string_attach_const(&args[1].name, "PortState");
    ua_string_attach_const(&args[1].description.text, "Specify a portState to modify to. 1 -- AIN, 2 -- AOUT, 3 -- DIN, 4 -- DOUT");
    ua_string_attach_const(&args[1].description.locale, "en-US");
    ua_nodeid_set_numeric(&args[1].data_type, 0, UA_ID_INT32);
    args[1].value_rank = UA_VALUERANK_SCALAR;

    ret = ua_variant_set_extensionobject_array(&v, args, 2, &datatype_id);
    if (ret != 0)
        return -1;

    /* attach the value to the node */
    ret = ua_memorystore_attach_new_value(&g_memorystore, &v, input_argument_node);
    if (ret != 0) {
        ua_variant_clear(&v);
        return -1;
    }

    ret = custom_provider_init_method(method_node,ASIC_addPortMethod);
    if (ret != 0) {
        ua_variant_clear(&v);
        TRACE_ERROR(TRACE_FAC_PROVIDER,"Return code is: %i",ret);

        return -1;
    }

    ua_variant_clear(&v);
    ua_string_clear(&strnodeid);
    ua_nodeid_clear(&nodeid);
    ua_nodeid_clear(&datatype_id);
    return 0;

}

/*! [addPort_method] */



/*! [deletePort_method] */
static ua_statuscode ASIC_deletePortMethod(const struct ua_callmethodrequest *req, struct ua_callmethodresult *res, struct ua_nodeid objectid)
{

    int ret;
    
    ua_node_t asic_node, config_node,AINPort;
    uint16_t portNo;
    ua_ref_t ref;

    struct ua_string strnodeid;
    const char * ConfigNodeId;
    const char * ASICNodeId;

    /* input validation */
    if (req->num_input_arguments != 1) return UA_SCBADINVALIDARGUMENT;
    if (req->input_arguments[0].type != UA_VT_UINT16) return UA_SCBADINVALIDARGUMENT;

    //extract input arguments
    portNo = req->input_arguments[0].value.ui16;

    //Find Config Node
    ua_string_init(&strnodeid);
    ConfigNodeId = ua_string_const_data(objectid.identifier.string);
    config_node = ua_node_find_string(g_custom_provider_nsidx,ConfigNodeId);
    if(config_node == UA_NODE_INVALID) return UA_NODE_INVALID;

    //Find ASIC Node
    ASICNodeId = util_splitString(ConfigNodeId,'.',1);    

    asic_node = util_findNodefromInverseRef(config_node, g_custom_provider_nsidx,ASICNodeId);
    if(asic_node == UA_NODE_INVALID) return UA_NODE_INVALID;
    
    ret = ASIC_deletePort(g_custom_provider_nsidx,asic_node,portNo);
    if (ret != 0) return UA_SCBADINTERNALERROR;

    //Cleanup
    ua_string_clear(&strnodeid);


    return 0;
}

int ASIC_createDeletePortMethod(uint16_t nsidx, ua_node_t parent, uint16_t ASICNumber)
{

    int ret;

    //Declarations for node creation
    struct ua_string strnodeid;
    struct ua_nodeid nodeid, datatype_id;
    ua_node_t method_node, input_argument_node;
    ua_ref_t ref;
    const char * browsename;

    //Declarations for Argument init
    struct ua_argument args[2];
    struct ua_variant v;

    /*! [create_method_node] */

    //Set string node id
    ua_string_init(&strnodeid);
    ua_string_snprintf(&strnodeid,20,"ASIC%02u.DeletePort",ASICNumber);
    browsename = ua_string_const_data(&strnodeid);
    ua_nodeid_set_string(&nodeid,nsidx,browsename);

    method_node = ua_node_create_with_attributes(
                      &nodeid,                        /* nodeid for the new node */
                      UA_NODECLASS_METHOD,            /* nodeclass of the new node */
                      nodeid.nsindex,                 /* ns index for browsename is same as nodeid */
                      browsename,                     /* browsename , NULL for same as displayname*/
                      "DeletePort",                      /* displayname */
                      UA_NODE_INVALID,                /* the node has no typedefinition */
                      parent,                         /* parent node of the new node */
                      UA_NODE_HASCOMPONENT);          /* new node is referenced with hascomponent by parent */
    if (method_node == UA_NODE_INVALID) return -1;
    /*! [create_method_node] */


    /*! [create_input_argument_nodes] */
    //Set string node id
    ua_string_catf(&strnodeid,50,".InputArg");
    browsename = ua_string_const_data(&strnodeid);
    ua_nodeid_set_string(&nodeid,nsidx,browsename);

    input_argument_node = ua_node_create_with_attributes(
                &nodeid,                            /* nodeid for the new node */
                UA_NODECLASS_VARIABLE,              /* nodeclass of properties is variable */
                0,                                  /* ns index for browsename is namespace zero */
                "InputArguments",                   /* browsename ***Note*** BrowseName must be InputArguments exactly*/
                browsename,                         /* displayname */
                UA_NODE_PROPERTYTYPE,               /* the node has the typedefinition propertytype */
                method_node,                        /* parent of the new node is the method node */
                UA_NODE_HASPROPERTY);               /* new node is referenced with hasproperty by parent */
    if (input_argument_node == UA_NODE_INVALID) return -1;

    /*! [Initialise Arguments] */

    ua_nodeid_set_numeric(&datatype_id, 0, UA_ID_ARGUMENT);

    ret = ua_variable_set_attributes(input_argument_node, &datatype_id, UA_VALUERANK_ONEDIMENSION, UA_ACCESSLEVEL_CURRENTREAD, false);
    if (ret != 0) return ret;

    ua_argument_init(&args[0]);
    ua_string_attach_const(&args[0].name, "PortNumber");
    ua_string_attach_const(&args[0].description.text, "Specify a Port Number to add");
    ua_string_attach_const(&args[0].description.locale, "en-US");
    ua_nodeid_set_numeric(&args[0].data_type, 0, UA_ID_UINT16);
    args[0].value_rank = UA_VALUERANK_SCALAR;

    ret = ua_variant_set_extensionobject_array(&v, args, 1, &datatype_id);
    if (ret != 0)
        return -1;

    /* attach the value to the node */
    ret = ua_memorystore_attach_new_value(&g_memorystore, &v, input_argument_node);
    if (ret != 0) {
        ua_variant_clear(&v);
        return -1;
    }

    ret = custom_provider_init_method(method_node,ASIC_deletePortMethod);
    if (ret != 0) {
        ua_variant_clear(&v);
        return -1;
    }

    ua_variant_clear(&v);
    ua_string_clear(&strnodeid);
    ua_nodeid_clear(&nodeid);
    ua_nodeid_clear(&datatype_id);
    return 0;
}

/*! [deletePort_method] */


/*! [modifyPort_method] */
static ua_statuscode ASIC_modifyPortMethod(const struct ua_callmethodrequest *req, struct ua_callmethodresult *res, struct ua_nodeid objectid)
{

    int ret,portState;
    
    ua_node_t asic_node, config_node,AINPort;
    uint16_t portNo;
    ua_ref_t ref;

    struct ua_string strnodeid;
    const char * ConfigNodeId;
    const char * ASICNodeId;




    /* input validation */
    if (req->num_input_arguments != 2) return UA_SCBADINVALIDARGUMENT;
    if (req->input_arguments[0].type != UA_VT_UINT16) return UA_SCBADINVALIDARGUMENT;
    if (req->input_arguments[1].type != UA_VT_INT32) return UA_SCBADINVALIDARGUMENT;

    //extract input arguments
    portNo = req->input_arguments[0].value.ui16;
    portState = req -> input_arguments[1].value.i32;

    //Find Config Node
    ua_string_init(&strnodeid);
    ConfigNodeId = ua_string_const_data(objectid.identifier.string);
    config_node = ua_node_find_string(g_custom_provider_nsidx,ConfigNodeId);
    if(config_node == UA_NODE_INVALID) return UA_NODE_INVALID;

    //Find ASIC Node
    ASICNodeId = util_splitString(ConfigNodeId,'.',1);    

    asic_node = util_findNodefromInverseRef(config_node, g_custom_provider_nsidx,ASICNodeId);
    if(asic_node == UA_NODE_INVALID) return UA_NODE_INVALID;

    ASIC_modifyPort(g_custom_provider_nsidx, asic_node, portNo,portState);
    if (ret != 0) return UA_SCBADINTERNALERROR;



    //Cleanup
    ua_string_clear(&strnodeid);


    return 0;
}


int ASIC_createModifyPortMethod(uint16_t nsidx, ua_node_t parent, uint16_t ASICNumber)
{

    int ret;

    //Declarations for node creation
    struct ua_string strnodeid;
    struct ua_nodeid nodeid, datatype_id;
    ua_node_t method_node, input_argument_node;
    ua_ref_t ref;
    const char * browsename;

    //Declarations for Argument init
    struct ua_argument args[2];
    struct ua_variant v;

    /*! [create_method_node] */

    //Set string node id
    ua_string_init(&strnodeid);
    ua_string_snprintf(&strnodeid,20,"ASIC%02u.ModifyPort",ASICNumber);
    browsename = ua_string_const_data(&strnodeid);
    ua_nodeid_set_string(&nodeid,nsidx,browsename);

    method_node = ua_node_create_with_attributes(
                      &nodeid,                        /* nodeid for the new node */
                      UA_NODECLASS_METHOD,            /* nodeclass of the new node */
                      nodeid.nsindex,                 /* ns index for browsename is same as nodeid */
                      browsename,                     /* browsename , NULL for same as displayname*/
                      "ModifyPort",                      /* displayname */
                      UA_NODE_INVALID,                /* the node has no typedefinition */
                      parent,                         /* parent node of the new node */
                      UA_NODE_HASCOMPONENT);          /* new node is referenced with hascomponent by parent */
    if (method_node == UA_NODE_INVALID) return -1;
    /*! [create_method_node] */


    /*! [create_input_argument_nodes] */
    //Set string node id
    ua_string_catf(&strnodeid,50,".InputArg");
    browsename = ua_string_const_data(&strnodeid);
    ua_nodeid_set_string(&nodeid,nsidx,browsename);

    input_argument_node = ua_node_create_with_attributes(
                &nodeid,                            /* nodeid for the new node */
                UA_NODECLASS_VARIABLE,              /* nodeclass of properties is variable */
                0,                                  /* ns index for browsename is namespace zero */
                "InputArguments",                   /* browsename ***Note*** BrowseName must be InputArguments exactly*/
                browsename,                         /* displayname */
                UA_NODE_PROPERTYTYPE,               /* the node has the typedefinition propertytype */
                method_node,                        /* parent of the new node is the method node */
                UA_NODE_HASPROPERTY);               /* new node is referenced with hasproperty by parent */
    if (input_argument_node == UA_NODE_INVALID) return -1;

    /*! [Initialise Arguments] */

    ua_nodeid_set_numeric(&datatype_id, 0, UA_ID_ARGUMENT);

    ret = ua_variable_set_attributes(input_argument_node, &datatype_id, UA_VALUERANK_ONEDIMENSION, UA_ACCESSLEVEL_CURRENTREAD, false);
    if (ret != 0) return ret;

    ua_argument_init(&args[0]);
    ua_string_attach_const(&args[0].name, "PortNumber");
    ua_string_attach_const(&args[0].description.text, "Specify a Port Number to add");
    ua_string_attach_const(&args[0].description.locale, "en-US");
    ua_nodeid_set_numeric(&args[0].data_type, 0, UA_ID_UINT16);
    args[0].value_rank = UA_VALUERANK_SCALAR;


    ua_argument_init(&args[1]);
    ua_string_attach_const(&args[1].name, "PortState");
    ua_string_attach_const(&args[1].description.text, "Specify a portState to modify to. 1 -- AIN, 2 -- AOUT, 3 -- DIN, 4 -- DOUT");
    ua_string_attach_const(&args[1].description.locale, "en-US");
    ua_nodeid_set_numeric(&args[1].data_type, 0, UA_ID_INT32);
    args[1].value_rank = UA_VALUERANK_SCALAR;

    ret = ua_variant_set_extensionobject_array(&v, args, 2, &datatype_id);
    if (ret != 0)
        return -1;

    /* attach the value to the node */
    ret = ua_memorystore_attach_new_value(&g_memorystore, &v, input_argument_node);
    if (ret != 0) {
        ua_variant_clear(&v);
        return -1;
    }

    ret = custom_provider_init_method(method_node,ASIC_modifyPortMethod);
    if (ret != 0) {
        ua_variant_clear(&v);
        return -1;
    }

    ua_variant_clear(&v);
    ua_string_clear(&strnodeid);
    ua_nodeid_clear(&nodeid);
    ua_nodeid_clear(&datatype_id);
    return 0;
}