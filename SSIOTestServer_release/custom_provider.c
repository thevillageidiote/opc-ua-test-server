/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#include <memory/memory.h>
#include <uaserver/addressspace/addressspace.h>
#include <uaserver/valuestore/valuestore.h>
#include <uaprovider/provider.h>

//Includes for persistence
#include <uaserver/addressspace/persistence.h>
#include <uaserver/valuestore/staticstore.h>


//Include for TRACE_ERROR debugging
#include <trace/trace.h>

//Include for util_error_lookup
#include <util/error_lookup.h>

//Include for SDK timers
#include <timer/timer.h>


#ifdef ENABLE_SERVICE_READ
#include <uaprovider/internal/uaprovider_internal_read.h>
#endif
#ifdef ENABLE_SERVICE_WRITE
#include <uaprovider/internal/uaprovider_internal_write.h>
#endif
#ifdef ENABLE_SERVICE_REGISTERNODES
#include <uaprovider/internal/uaprovider_internal_register.h>
#endif
#ifdef ENABLE_SERVICE_SUBSCRIPTION
#include <uaprovider/internal/uaprovider_internal_subscription.h>
#endif

#include "custom_provider_store.h"
#include "custom_provider_nodes.h"
#include "custom_provider.h"
#include "custom_provider_method_handler.h"
#include "numNodeDefinition.h"
#include "custom_provider_ASIC_nodes.h"
#include "socket_client.h"

/*! [nsidx_declaration] */
uint16_t g_custom_provider_nsidx;
/*! [nsidx_declaration] */

/*! [custom_store_idx] */
uint8_t g_custom_store_idx;
/*! [custom_store_idx] */

/*! [memory_store_declaration] */
// This memory store is used to store UA Variants such as the method arguments
static struct ua_variant g_memorystore_values[50];
struct ua_memorystore g_memorystore;
/*! [memory_store_declaration] */

/*! [static_store_declaration] !*/
static struct ua_staticstore g_staticstore;
/*! [static_store_declaration] !*/

extern struct portConfig g_portConfigList[MAXASICNO * MAXPORTSPERASIC];



//Declare timer
struct timer_base x_tmr_base;
uint32_t tmr_id = 0;

int timer_callback(uint64_t elapsed, void *data);

extern struct ua_variant g_custom_provider_values[50];


/*! [provider_cleanup] */
/* cleanup resources allocated by the custom provider */
static void eval_pv_cleanup(void)
{

    timer_remove(&x_tmr_base, tmr_id, NULL);
    timer_base_clear(&x_tmr_base);

    //Clear Stores
    ua_valuestore_unregister_store(g_custom_store_idx);
    ua_staticstore_clear(&g_staticstore);
    ua_memorystore_clear(&g_memorystore);


    //Clear values in g_portConfigList
    int i;
    for (i = 0; i< (MAXASICNO * MAXPORTSPERASIC); i++)
    {
        ua_variant_clear(&g_portConfigList[i].portNo);
        ua_variant_clear(&g_portConfigList[i].portState);
        ua_variant_clear(&g_portConfigList[i].portType);

        //Cleanup socket
        close(g_portConfigList[i].sockfd);
    }

    return;
}
/*! [provider_cleanup] */

/*! [provider_init] */
int eval_pv_init(struct uaprovider *ctx)
/*! [provider_init] */
{
    struct ua_valuestore_interface store_if;
    struct ua_addressspace_config config;
    uint16_t eval_pv_nsidx;
    int ret;

    TRACE_ERROR(TRACE_FAC_PROVIDER, "Creating dynamic address space based on uio_asic_test_server_info_model.bin.\n");
    /*! [loading_file] */
    /* reserve additional memory for dynamically created nodes */
    ua_memset(&config, 0, sizeof(config));
    config.max_variables  = 200; /* additional variables */
    config.max_objects    = 200; /* additional objects */
    config.max_references = 500; /* additional references */
    config.max_strings    = 500; /* additional strings */
    config.nsidx          = UA_NSIDX_INVALID; /* auto assign */

    TRACE_ERROR(TRACE_FAC_PROVIDER, "Loading NS from binary file\n");
    ret = ua_addressspace_load_file("uio_asic_test_server_info_model.bin", &g_staticstore, ua_staticstore_from_fd, &config);
    if (ret < 0) {
        TRACE_ERROR(TRACE_FAC_PROVIDER, "Loading NS from file failed: %i (%s)\n", ret, util_error_lookup(ret));
        return ret;
    }

    if (ret < 0) return ret;
    eval_pv_nsidx = (uint16_t) ret;
    g_custom_provider_nsidx = eval_pv_nsidx;



    /*! [register_addressspace] */

    /*! [register_nsidx] */
    /* register namespace index */
    ret = uaprovider_register_nsindex(ctx, eval_pv_nsidx);
    if (ret != 0) return ret;
    /*! [register_nsidx] */

    /*! [register_memorystore] */
    /* initialize memorystore context */
    ret = ua_memorystore_init(&g_memorystore, 0, g_memorystore_values, countof(g_memorystore_values));
    if (ret != 0) return ret;
    /*! [register_memorystore] */

    /*! [register_customstore] */
    /* register custom store */
    ua_valuestore_interface_init(&store_if);
    store_if.get_fct    = custom_store_get_value;
    store_if.attach_fct = custom_store_attach_value;
    ret = ua_valuestore_register_store(&store_if, &g_custom_store_idx);
    if (ret != 0) return ret;
    /*! [register_customstore] */

    /*! [call_create_nodes] */
    /* create nodes in the namespace */
    ret = eval_pv_create_nodes(eval_pv_nsidx);
    if (ret != 0) return ret;
    /*! [call_create_nodes] */

    //init timers
    ret = timer_base_init(&x_tmr_base, 1);
    ret = timer_add(&x_tmr_base, &tmr_id, 100, timer_callback, NULL, TIMER_DEFAULT);
    ret = timer_activate(&x_tmr_base, tmr_id);

    /*! [register_service_handler] */
    /* register service handler */
#ifdef ENABLE_SERVICE_READ
    ctx->read            = uaprovider_internal_read;
#endif
#ifdef ENABLE_SERVICE_WRITE
    ctx->write           = uaprovider_internal_write;
#endif
#ifdef ENABLE_SERVICE_REGISTERNODES
    ctx->registernodes   = uaprovider_internal_registernodes;
    ctx->unregisternodes = uaprovider_internal_unregisternodes;
#endif
#ifdef ENABLE_SERVICE_SUBSCRIPTION
    ctx->add_item        = uaprovider_internal_add_item;
    ctx->remove_item     = uaprovider_internal_remove_item;
    ctx->modify_item     = uaprovider_internal_modify_item;
    ctx->subscribe       = uaprovider_internal_subscribe;
#endif
    /*! [register_call_handler] */
#ifdef ENABLE_SERVICE_CALL
    ctx->call            = custom_provider_method_call;
#endif
    /*! [register_call_handler] */

    /*! [register_service_handler] */
    /*! [register_cleanup] */
    /* register cleanup function */
    ctx->cleanup         = eval_pv_cleanup;
    /*! [register_cleanup] */




    return ret;
}


int timer_callback(uint64_t elapsed, void *data)
{
    int i;
    static double test_time = 0;
    unsigned int input;
    struct portConfig * currentPortConfig;

    int sockfd,ret;
    uint16_t value;
    
    UA_UNUSED(data);

	test_time += (((double)elapsed)/1000);

    // for(i=0; i< MAXPORTSPERASIC; i++){

    // g_custom_provider_values[3].value.d = 10.0 * sin(2 * M_PI * 0.001 * test_time * (i+1));
    // }


    for ( i = 0; i < MAXPORTSPERASIC; i++)
    {

        currentPortConfig = &g_portConfigList[i];

        //if currentPortState is on
        if(currentPortConfig -> portState.value.i32 == 1)
        {
            switch(currentPortConfig -> portType.value.i32){
                case AI_TYPE:
                    
                    sockfd = currentPortConfig -> sockfd;

                    sendMessage(sockfd,CLIENT_READYTORECEIVE,0);

                    ret = receiveMessage(sockfd,&value);

                    if(ret == SERVER_SENDVAL){
                        g_custom_provider_values[i].value.ui16 = value;                    
                    }
                    break;
                
                case DI_TYPE:
                    sockfd = currentPortConfig -> sockfd;

                    sendMessage(sockfd,CLIENT_READYTORECEIVE,0);

                    ret = receiveMessage(sockfd,&value);

                    if(ret == SERVER_SENDVAL){
                        if(value == 0xFFFF){
                            g_custom_provider_values[i].value.b = true;
                        }
                        else if(value == 0x0000){
                            g_custom_provider_values[i].value.b = false;
                        }                    
                    }
                    break;
                    
                


                default:
                    break;
                    
            }
        }




    }


    return 0;
}
