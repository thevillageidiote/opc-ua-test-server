/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef FILESTORAGE_H_QZBEMZGR
#define FILESTORAGE_H_QZBEMZGR

#include <stdint.h>
#include <stdbool.h>
#include <platform/file.h>

int util_file_write_uint32(const uint32_t *pval, ua_file_t f);
int util_file_write_int32(const int32_t *pval, ua_file_t f);
int util_file_write_uint16(const uint16_t *pval, ua_file_t f);
int util_file_write_int16(const int16_t *pval, ua_file_t f);
int util_file_write_uint8(const uint8_t *pval, ua_file_t f);
int util_file_write_int8(const int8_t *pval, ua_file_t f);
int util_file_write_bool(const bool *pval, ua_file_t f);
int util_file_write_bytearray(const unsigned char *data, size_t len, ua_file_t f);
int util_file_write_string(const char *str, ua_file_t f);

int util_file_read_uint32(uint32_t *pval, ua_file_t f);
int util_file_read_int32(int32_t *pval, ua_file_t f);
int util_file_read_uint16(uint16_t *pval, ua_file_t f);
int util_file_read_int16(int16_t *pval, ua_file_t f);
int util_file_read_uint8(uint8_t *pval, ua_file_t f);
int util_file_read_int8(int8_t *pval, ua_file_t f);
int util_file_read_bool(bool *pval, ua_file_t f);
int util_file_read_bytearray(unsigned char *data, size_t len, ua_file_t f);
int util_file_read_string(char *str, size_t len, ua_file_t f);

int util_file_skip_bytearray(size_t len, ua_file_t f);
int util_file_skip_string(ua_file_t f);

#endif /* end of include guard: FILESTORAGE_H_QZBEMZGR */

