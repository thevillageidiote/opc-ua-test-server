/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef STRTOINT_H_ZLAFUM7Q
#define STRTOINT_H_ZLAFUM7Q

#include <stdint.h>

/**
 * @defgroup util_strtoint strtoint
 * @ingroup util
 * @brief String to integer conversion.
 * @{
 */

int util_strtoint8(const char *nptr, char **endptr, int base, int8_t *result);
int util_strtouint8(const char *nptr, char **endptr, int base, uint8_t *result);
int util_strtoint16(const char *nptr, char **endptr, int base, int16_t *result);
int util_strtouint16(const char *nptr, char **endptr, int base, uint16_t *result);
int util_strtoint32(const char *nptr, char **endptr, int base, int32_t *result);
int util_strtouint32(const char *nptr, char **endptr, int base, uint32_t *result);
int util_strtoint64(const char *nptr, char **endptr, int base, int64_t *result);
int util_strtouint64(const char *nptr, char **endptr, int base, uint64_t *result);

/** @} */

#endif /* end of include guard: STRTOINT_H_ZLAFUM7Q */

