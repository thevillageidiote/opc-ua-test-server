/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#include <platform/platform.h>
#include <common/errors.h>

#ifndef __POINTERLIST_H__
#define __POINTERLIST_H__

/**
 * @defgroup util_pointerset_group pointer_set
 * @ingroup util
 *
 * Array of pointers with simple management functionality.
 * Manages a set of void pointers and allows for easy iteration over its
 * elements. The internal array must be provided
 * to util_pointerset_init().
 * @{
 */

/**
 * The structured data type of the pointerset utility, see also @ref util_pointerset_group.
 * @see util_pointerset_group
 */
struct util_pointerset {
    void **elements;       /**< Managed array of void pointers. */
    uint16_t num_elements; /**< Current number of elements (last index + 1) */
    uint16_t max_elements; /**< Maximum number of elements */
};

/** Iterate backwards over all set elements with an iterator variable.
 * xIterator must be a pointer of type xIteratorType and is the iterator.
 * xIndex is the index variable.
 */
#define util_pointerset_iterate(xIndex, xList, xIteratorType, xIterator) \
    xIterator =  ((&xList)->num_elements < 1)?NULL:((xIteratorType)(&xList)->elements[(&xList)->num_elements - 1]); \
    for (xIndex = (&xList)->num_elements - 1; xIndex >= 0; xIndex--, xIterator = (xIndex >= 0)?((xIteratorType)(&xList)->elements[xIndex]):0)

/** Iterate backwards over all set elements. Elements have to be accessed with the index variable.
 */
#define util_pointerset_foreach(xIndex, xList) \
    for (xIndex = (&xList)->num_elements - 1; xIndex >= 0; --xIndex)

/** Initialize the pointerset structure with 0 element count and data as element array.
 * data must be large enough to hold the maximum number of elements possible during runtime.
 */
static inline int util_pointerset_init(struct util_pointerset *pointerset, void *data, uint16_t max_elements)
{
    pointerset->elements = data;
    pointerset->num_elements = 0;
    pointerset->max_elements = max_elements;
    return UA_EGOOD;
}

/** Add (append) a new element to the pointerset.
 * The new pointer will be appended to the end of the set. Adding the same pointer multiple times is possible.
 * However, make sure to remove the element as often as it has been added.
 * @return Zero on success or errorcode if capacity is exceeded.
 */
static inline int util_pointerset_addelement(struct util_pointerset *pointerset, void *element)
{
    if (pointerset->num_elements == pointerset->max_elements) return UA_EBADBUFTOOSMALL;
    pointerset->elements[pointerset->num_elements] = element;
    pointerset->num_elements++;
    return UA_EGOOD;
}

/** Remove the first element with the given pointervalue.
 * The last element in the set is moved into the newly freed slot to keep the set
 * compact for easy iteration. If more than one element contain the same value,
 * only the first is removed.
 */
static inline int util_pointerset_removeelement(struct util_pointerset *pointerset, void *element)
{
    uint16_t i;
    for (i = 0; i < pointerset->num_elements; i++) {
        if (pointerset->elements[i] == element) {
            pointerset->num_elements--;
            pointerset->elements[i] = pointerset->elements[pointerset->num_elements];
#ifdef _DEBUG
            pointerset->elements[pointerset->num_elements] = 0;
#endif /* _DEBUG */
            return 0;
        }
    }
    return UA_EBADNOTFOUND;
}

/** @} */

#endif /* end of include guard: __POINTERLIST_H__ */
