/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UTIL_LIST_H_
#define _UTIL_LIST_H_

/**
 * @defgroup util_list_group list
 * @ingroup util
 *
 * Double linked list embedded in a static array.
 * This way this list is very fast and doesn't need
 * dynamic memory allocation.
 * Unused array members are stored in a single linked list
 * as these are always accessed at the front.
 * It also supports index based access for known indeces,
 * returned by insert (Note: index n is NOT the n-th element,
 * it is just a handle for accessing the element).
 *
 * @{
 */

/*#define HAVE_LIST_DUMP*/
/** Identifier for an invalid index */
#define UTIL_LIST_NIL -1

/** Foreach loop for iterating over a list */
#define util_list_foreach(i, list) for (i = util_list_first(list); i != -1; i = util_list_next(list, i))

/* forward declaration */
struct util_list_el;

/**
 * Structure for list, see also @ref util_list_group.
 * @see util_list_group
 */
struct util_list {
    struct util_list_el *elements;
    int length;    /**< current list length */
    int alloclist; /**< anchor of alloc list */
    int freelist;  /**< anchor of free list */
    int last_element; /**< anchor of the last element */
};

int util_list_size(int num_elements);
void util_list_init(struct util_list *l, void *data, int size);
void *util_list_clear(struct util_list *l);

int util_list_insert(struct util_list *l, const void *x);
int util_list_insert_end(struct util_list *l, const void *x);
void *util_list_get(struct util_list *l, int index);
const void *util_list_get_const(struct util_list *l, int index);
void *util_list_remove(struct util_list *l, int index, int *new_index);
void *util_list_pop_front(struct util_list *l);
void *util_list_pop_end(struct util_list *l);
int util_list_first(struct util_list *l);
int util_list_last(struct util_list *l);
int util_list_next(struct util_list *l, int index);
int util_list_length(struct util_list *l);

#ifdef HAVE_LIST_DUMP
/* debug helper for dumping list contents and validating consistency. */
void util_list_dump(struct util_list *l);
#endif /* HAVE_LIST_DUMP */

/** @} */

#endif /* end of include guard: _UTIL_LIST_H_ */

