/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef _UTIL_HEAP_H_
#define _UTIL_HEAP_H_

#include <stdlib.h> /* size_t */

/**
 * @defgroup util_heap_group heap
 * @ingroup util
 * @brief A binary tree based min/max heap aka priority queue.
 *
 * The heap is a complete binary tree (all levels except the last one are full).
 * This allows to embedd the tree nodes into an simple array and omit the
 * left, right, parent pointers.
 * These relations can be calculated using the zero based array index i:
 * - parent: p = (i - 1) div 2 (integer division = floor function)
 * - left:   l = 2 * y + 1
 * - right:  r = 2 * y + 2
 *
 * Usage example:
 * @code
 * #include <stdio.h>
 * #include <memory/memory.h>
 *
 * // struct for elements to store inside the heap
 * struct sample_element {
 *     int prio;
 * };
 *
 * // compare function for sample_element (maximum heap)
 * int sample_compare(const void *a, const void *b)
 * {
 *     const struct sample_element *A = a;
 *     const struct sample_element *B = b;
 *
 *     if (A->prio < B->prio) return -1;
 *     if (A->prio > B->prio) return 1;
 *     return 0;
 * }
 *
 * int main(void)
 * {
 * struct util_heap heap;
 *
 * struct sample_element input = {2};
 * struct sample_element *output;
 *
 * // get the size in bytes for a heap with 10 elements
 * int size = util_heap_size(10);
 *
 * // allocte the required amount of memory
 * void *heapdata = ua_malloc(size);
 * if (heapdata == NULL) return -1;
 *
 * // initialize the heap
 * util_heap_init(&heap, sample_compare, heapdata, size);
 *
 * // add the element with priority 2
 * util_heap_enqueue(&heap, &input);
 *
 * // remove the element with priority 2
 * output = util_heap_dequeue(&heap);
 * if (output != NULL)
 *     printf("dequed element with priority %i\n", output->prio);
 *
 * // clear the heap structure
 * heapdata = util_heap_clear(&heap);
 *
 * // free the memory
 * ua_free(heapdata);
 *
 * return 0;
 * }
 * @endcode
 *
 * @{
 */

/** @brief Simple node structure for binary tree, see also @ref util_heap_group. */
struct util_heap_node {
    void *data;  /**< stores user data */
};

/**
 * @brief Comparison function that must be provided for the heap.
 *
 * Function capable of comparing the data added to heap by @ref util_heap_enqueue.
 * If the comparison function returns -1 for a < b the heap is a maximum heap.
 * If you change the comparison function to return -1 for a > b
 * the heap will become a minimum heap.
 */
typedef int (*util_heap_compar)(const void *a, const void *b);

/**
 * @brief The heap data structure, see also @ref util_heap_group.
 * @see util_heap_group
 */
struct util_heap {
    struct util_heap_node *nodes;
    unsigned int last;
    unsigned int num_elements;
};

size_t util_heap_size(unsigned int num_elements);
void util_heap_init(struct util_heap *heap, void *data, size_t size);
void *util_heap_clear(struct util_heap *heap);
int util_heap_count(struct util_heap *heap);

int util_heap_enqueue(struct util_heap *heap, void *data, util_heap_compar comp);
void *util_heap_dequeue(struct util_heap *heap, util_heap_compar comp);
void *util_heap_first(struct util_heap *heap);

/** @} */

#endif /* end of include guard: _UTIL_HEAP_H_ */

