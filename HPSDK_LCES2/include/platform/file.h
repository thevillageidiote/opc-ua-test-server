/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef FILE_H_3CPFP4LU
#define FILE_H_3CPFP4LU

#include <platform/platform.h>
#include <pfile_types.h>
#include <time.h>

/** buffer size of ua_filestream */
#define UA_STREAM_BUF_SIZE 1024

/** File open flags for ua_file_open and ua_filestream_open. */
enum ua_p_file_open_flags {
    UA_FOPEN_TEXT = 0,
    UA_FOPEN_BINARY = 1, /**< On POSIX platform BINARY makes no difference, other plaforms may tread text files differently than binary files. */
    UA_FOPEN_EXCLUSIVE = 2,
    UA_FOPEN_APPEND = 4,
    UA_FOPEN_READ = 8,
    UA_FOPEN_WRITE = 16,
    UA_FOPEN_READWRITE = UA_FOPEN_READ | UA_FOPEN_WRITE,
    UA_FOPEN_CREATE = 32, /**< Create the file if it does not exist. */
    UA_FOPEN_TRUNC = 64   /**< If the file already exists and is a regular file and the access mode allows writing (i.e., is O_RDWR or O_WRONLY) it will be truncated to length 0. */
};

/** POSIX like fstat structure.
 * Linux: http://linux.die.net/man/2/fstat
 * Windows: https://msdn.microsoft.com/en-us/library/aa246905%28v=vs.60%29.aspx
 */
struct ua_file_stat {
    ua_fs_dev_t     ua_dev;     /* ID of device containing file */
    ua_fs_ino_t     ua_ino;     /* inode number */
    ua_fs_mode_t    ua_mode;    /* protection */
    ua_fs_nlink_t   ua_nlink;   /* number of hard links */
    ua_fs_uid_t     ua_uid;     /* user ID of owner */
    ua_fs_gid_t     ua_gid;     /* group ID of owner */
    ua_fs_dev_t     ua_rdev;    /* device ID (if special file) */
    ua_fs_off_t     ua_size;    /* total size, in bytes */
    ua_fs_blksize_t ua_blksize; /* blocksize for file system I/O */
    ua_fs_blkcnt_t  ua_blocks;  /* number of 512B blocks allocated */
    time_t       ua_atime;   /* time of last access */
    time_t       ua_mtime;   /* time of last modification */
    time_t       ua_ctime;   /* time of last status change */
    time_t       ua_btime;   /* time of file creation(birth) */
};

/** file type used in ua_dir_entry */
enum ua_file_types {
    UA_FT_BLK = 1, /**< block device */
    UA_FT_CHR = 2, /**< character device */
    UA_FT_REG = 3, /**< regular file */
    UA_FT_DIR = 4, /**< directory */
    UA_FT_FIFO = 5, /**< named pipe */
    UA_FT_LNK = 6, /**< sym link */
    UA_FT_SOCK = 7, /**< UNIX domain socket */
    UA_FT_UNKNOWN = 8, /**< unknown file type */
    UA_FT_HIDDEN = 0x80, /**< hidden file (windows only) */
    UA_FT_SYSTEM = 0x40  /**< system file (windows only) */
};

/** ua directory entry.
 * @see ua_file_scandir
 */
struct ua_dir_entry {
    char d_name[UA_P_DNAME_LEN]; /* UA_P_DNAME_LEN is defined in pfile_types.h */
    unsigned char d_type;
};

/** whence type for ua_file_seek */
enum ua_file_whence {
    UA_FILE_SEEK_CUR = 0,
    UA_FILE_SEEK_SET,
    UA_FILE_SEEK_END
};

/* standard file io functions */
int ua_file_open(const char *filename, int flags, ua_file_t *file);
int ua_file_close(ua_file_t file);
int ua_file_touch(const char *filename);
int ua_file_stat(const char *filename, struct ua_file_stat *stat);
int ua_file_fstat(ua_file_t fd, struct ua_file_stat *stat);
int ua_file_read(ua_file_t file, void *ptr, size_t count, size_t *bytes_read);
int ua_file_write(ua_file_t file, const void *ptr, size_t count, size_t *bytes_written);
long ua_file_seek(ua_file_t file, long offset, enum ua_file_whence whence);
long ua_file_tell(ua_file_t file);

/* file text stream functions */
enum ua_filestream_status {
    UA_FSTREAM_CLOSED = 0,
    UA_FSTREAM_READ,
    UA_FSTREAM_WRITE,
    UA_FSTREAM_EOF,
    UA_FSTREAM_ERROR
};

struct ua_filestream {
    ua_file_t fd;  /**< file descripter */
    int pos; /**< read position */
    int num; /**< number of characters in buffer */
    enum ua_filestream_status status; /* stream status */
    char buf[UA_STREAM_BUF_SIZE];
};

int ua_filestream_open(struct ua_filestream *stream, const char *filename, int flags);
void ua_filestream_from_file(struct ua_filestream *stream, ua_file_t fd, enum ua_filestream_status mode);
int ua_filestream_close(struct ua_filestream *stream);
int ua_filestream_eof(struct ua_filestream *stream);
int ua_filestream_readline(struct ua_filestream *stream, char *line, unsigned int n);
int ua_filestream_printf(struct ua_filestream *stream, const char *fmt, ...) UA_FORMAT_ARGUMENT(2, 3);

/* file management functions */
typedef int (*filter_fct)(const struct ua_dir_entry *);
typedef int (*sort_fct)(const struct ua_dir_entry *, const struct ua_dir_entry *);

int ua_file_scandir(const char *parent_dir, struct ua_dir_entry **results, filter_fct filter, sort_fct sort);
int ua_file_delete(const char *filename);
int ua_file_rmdir(const char *filename);
int ua_file_delete_recursive(const char *filename);
int ua_file_mkdir(const char *filename);
int ua_file_mkpath(const char *path);
int ua_file_chdir(const char *path);
int ua_file_getcwd(char *path, size_t len);

#endif /* end of include guard: FILE_H_3CPFP4LU */
