/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef UA_PLATFORM_FRONTEND_MEMORY_H
#define UA_PLATFORM_FRONTEND_MEMORY_H

#include <stdlib.h> /* size_t */
#include <platform/platform_config.h>

/**
 * @defgroup platform_memory memory
 * @ingroup platform
 * Wrapper for common memory operations.
 *
 * In case a platform doesn't support standard malloc or has
 * an optimized version of memset it can be forwarded to the
 * custom implementation.
 *
 * @{
 */

#ifdef MEMORY_ENABLE_TRACE
#define ua_malloc(size) ua_malloc2(size, __FILE__, __LINE__)
#define ua_calloc(nmemb, size) ua_calloc2(nmemb, size, __FILE__, __LINE__)
#define ua_realloc(ptr, size) ua_realloc2(ptr, size, __FILE__, __LINE__)
#define ua_free(ptr) ua_free2(ptr, __FILE__, __LINE__)
#define ua_strdup(s) ua_strdup2(s, __FILE__, __LINE__)
void *ua_malloc2(size_t size, const char *file, int line);
void *ua_calloc2(size_t nmemb, size_t size, const char *file, int line);
void *ua_realloc2(void *ptr, size_t size, const char *file, int line);
void ua_free2(void *ptr, const char *file, int line);
char *ua_strdup2(const char *s, const char *file, int line);
#else
void *ua_malloc(size_t size);
void *ua_calloc(size_t nmemb, size_t size);
void *ua_realloc(void *ptr, size_t size);
void ua_free(void *ptr);
char *ua_strdup(const char *s);
#endif

void *ua_memset(void *s, int c, size_t n);
void *ua_memmove(void *dest, const void *src, size_t n);
void *ua_memcpy(void *dest, const void *src, size_t n);
void *ua_mempcpy(void *dest, const void *src, size_t n);
int ua_memcmp(const void *s1, const void *s2, size_t n);

/** @} */

#endif /* UA_PLATFORM_FRONTEND_MEMORY_H */
