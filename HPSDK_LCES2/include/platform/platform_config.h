/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef PLATFORM_CONFIG_H
#define PLATFORM_CONFIG_H

/* #undef CONFIG_THREADSAFE */

/* UUID library from util-linux package.
 * This library is available on almost all Linux systems,
 * but the header file is often in a separate package like uuid-dev.
 */
/* #undef HAVE_UUID */
#define HAVE_GETOPT

#define BUILD_LIBUACRYPTO
#ifdef BUILD_LIBUACRYPTO
# define HAVE_CRYPTO
#endif
#define BUILD_LIBUAPKI
#ifdef BUILD_LIBUAPKI
# define HAVE_PKI
#endif
#define SECURITY_BACKEND "openssl"
/* #undef MEMORY_ENABLE_TRACE */

/* when MEMORY_USE_SHM is set then the base memory area used for memory pools
 * is allocated in shared memory. This is necessary for IPC in multi-task configuration.
 * In single-task mode this can be undefined, and normal malloc is used.
 */
/* #undef MEMORY_USE_SHM */

/* generic UA file support */
#define SUPPORT_FILE_IO

#endif /* PLATFORM_CONFIG_H */

