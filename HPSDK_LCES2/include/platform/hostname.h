/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef HOSTNAME_H_W5HUZGO9
#define HOSTNAME_H_W5HUZGO9

#include <stdlib.h> /* size_t */

/**
 * @defgroup platform_hostname hostname
 * @ingroup platform
 *
 * Functionality for retrieving local hostname information.
 *
 * @{
 */

/**
 * Returns the local hostname.
 *
 * @param hostname Pointer to string which receives the hostname.
 * @param hostname_len Size of hostname in bytes.
 *
 * @return Zero on success. UA_EBADTRUNCATED if hostname_len is too short.
 * UA_EBADSYSCALL if underlying OS calls went wrong.
 */
int ua_get_hostname(char *hostname, size_t hostname_len);

/**
 * Returns the fully qualified domain name.
 *
 * @param fqdn Pointer to string which receives the FQDN.
 * @param fqdn_len Size of fqdn in bytes.
 *
 * @return Zero on success. UA_EBADTRUNCATED if hostname_len is too short.
 * UA_EBADSYSCALL if underlying OS calls went wrong.
 */
int ua_get_fqdn(char *fqdn, size_t fqdn_len);

/** @} */

#endif /* end of include guard: HOSTNAME_H_W5HUZGO9 */

