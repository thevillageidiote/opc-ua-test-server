/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _PMEMORY_H_
#define _PMEMORY_H_

#include <platform/platform_config.h>

#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>


#ifdef MEMORY_ENABLE_TRACE
/* call some malloc wrapper functions for logging file and line info */
void *ua_p_malloc(size_t size, const char *file, int line);
void ua_p_free(void *ptr, const char *file, int line);
void *ua_p_calloc(size_t nmemb, size_t size, const char *file, int line);
void *ua_p_realloc(void *ptr, size_t size, const char *file, int line);
char *ua_p_strdup(const char *s, const char *file, int line);
#else /* MEMORY_ENABLE_TRACE */
/* simply mapped to standard C functions. */
# define ua_p_malloc(size) malloc(size)
# define ua_p_free(ptr) free(ptr)
# define ua_p_calloc(nmemb, size) calloc(nmemb, size)
# define ua_p_realloc(ptr, size) realloc(ptr, size)
# define ua_p_strdup(s) strdup(s)
#endif /* MEMORY_ENABLE_TRACE */

#define ua_p_memset(s, c, n) memset(s, c, n)
#define ua_p_memmove(dest, src, n) memmove(dest, src, n)
#define ua_p_memcmp(s1, s2, n) memcmp(s1, s2, n)
#define ua_p_memcpy(dest, src, n) memcpy(dest, src, n)
#define ua_p_mempcpy(dest, src, n) mempcpy(dest, src, n)

#endif /* _PMEMORY_H_ */
