/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


/** \ingroup session
 * @{
 */

#ifndef _UASESSION_TYPES_H_
#define _UASESSION_TYPES_H_

#include <network/config.h>
#include <uabase/nodeid.h>
#include <util/map.h>
#include <util/pointerset.h>
#include <uabase/requestheader.h>
#include <uabase/bytestring.h>
#include <uabase/buffer.h>
#include <uabase/uatype_config.h>
#include <common/ref.h>
#include <uaprotocol/uaseconv/sechan.h>
#include <uaserver/server_config.h>
#ifdef UA_AUTHORIZATION_SUPPORT
# include "authorization/authorization.h"
#endif

/** Temporary configuration - should be moved to appconfig/endpoint_configuration_class */
/* session lifetime check interval in ms */
#define UASESSION_LIFETIME_CHECK_INTERVAL 5000
/* minimum session lifetime in ms */
#define UASESSION_LIFETIME_MIN            2*UASESSION_LIFETIME_CHECK_INTERVAL
/* maximum session lifetime in ms */
#define UASESSION_LIFETIME_MAX            5*UASESSION_LIFETIME_CHECK_INTERVAL

/** Event types of channel event notification callback. */
#define UASESSION_CHANNELEVENT_INVALID  0   /**< Unused/Reserved */
#define UASESSION_CHANNELEVENT_OPEN     1   /**< Communication channel has been opened. */
#define UASESSION_CHANNELEVENT_RENEW    2   /**< Communication channel has been renewed. */
#define UASESSION_CHANNELEVENT_TCPCLOSE 3   /**< Communication channel has lost its transport. */
#define UASESSION_CHANNELEVENT_CLOSE    4   /**< Communication channel has been closed. */

/* forward declaration */
struct ua_subscriptionbase;
struct uaprovider;

enum uasession_session_state {
    uasession_session_state_unused   = 0,   /**< Session not in use. */
    uasession_session_state_inactive = 1,   /**< In use but not yet activated. */
    uasession_session_state_active   = 2,   /**< Session in use and bound to channel. */
    uasession_session_state_closed   = 3    /**< Session is closed but not yet deleted. */
};

struct uasession_base;

#define SESSION_NONCE_LEN 32

/** \ingroup session
 * Session layer representation of a secure channel.
 */
struct uasession_channel {
    uint32_t                        channel_id;                      /**< ID of the secure channel which activated the session last. */
    uint32_t                        session_count;                   /**< Number of activated session on this channel */
    uint32_t                        policy_id;                       /**< ID of the security policy of this channel. */
    enum ua_messagesecuritymode     security_mode;                   /**< The security mode used by this channel. */
    ua_datetime                     end_of_life;                     /**< DateTime marking the end of life of this channel. */
    uint32_t                        footer_len;                      /**< The length of the space to be preserved during encoding. */
    size_t                          max_body_len;                    /**< The maximum body length of the response after encoding. */
    uint32_t                        max_chunks;                      /**< The maximum number of buffers to be used for encoding. */
    struct sechan_config           *sechan_config;                   /**< The endpoints securechannel configuration used by this channel. */
    bool                            connected;                       /**< TRUE if channel has active transport connection. */
    int                             prio;                            /**< Priority value for IPC queues. */
    unsigned int                    buffer_handle;                   /**< handle for buffer management */
};

/** \ingroup session
 * Object representing a OPC UA session.
 */
struct uasession_session {
    uint64_t                        lifetime;                        /**< Absolute session lifetime. */
    uint32_t                        session_id;                      /**< Session ID */
    uint32_t                        channel_id;                      /**< ID of the secure channel which activated the session last. */
    unsigned char                   server_nonce[SESSION_NONCE_LEN]; /**< Current server nonce. */
    enum uasession_session_state    session_state;                   /**< Current session state. */
    uint64_t                        last_access;                     /**< Tick count of last service call. */
    struct uasession_base          *base;                            /**< Session base managing the session object. */
#ifdef ENABLE_SERVICE_SUBSCRIPTION
    struct ua_subscriptionbase     *subscription_base;               /**< Subscription base managing subscriptions for the session. */
#endif
    struct ua_ref                   refcnt;                          /**< Reference counter. */
    int                             prio;
#ifdef UA_AUTHORIZATION_SUPPORT
    struct ua_user_ctx              user_ctx;                        /**< Information about the user represented by this session. */
#endif
#ifdef UA_AUTHENTICATION_SUPPORT
    uint8_t                         auth_policy;                     /**< Policy used to authenticate user, 0 is anonymous, others are defined by backend. */
#endif
#ifdef HAVE_PKI
    struct ua_bytestring            client_certificate;              /**< The clients application instance certificate. */
#endif
};

struct uasession_msg_ctxt;
typedef void (*req_clear_func_t)(void *req);
typedef void (*res_clear_func_t)(void *res);

/**  \ingroup session
 * Object representing a particular session call.
 */
struct uasession_msg_ctxt {
    struct uasession_session     *session;          /**< The session related to this service call. */
    struct ua_requestheader      *req_head;         /**< Request header. */
    void                         *req;              /**< Request body. */
    struct ua_responseheader     *res_head;         /**< Response header. */
    void                         *res;              /**< Response body. */
    struct uaprovider            *cur_provider;     /**< Current provider context, only used during provider mulitplexing */
    struct ua_ref                 refcnt;           /**< Reference counter. */
    req_clear_func_t              req_clear;        /**< Function pointer to function for clearing req. */
    res_clear_func_t              res_clear;        /**< Function pointer to function for clearing res. */
    uint32_t                      channel_id;       /**< Id of the channel which received this service call. */
    uint32_t                      footer_len;       /**< The length of the space to be preserved during encoding. */
    size_t                        max_body_len;     /**< The maximum body length of the response after encoding. */
    uint32_t                      max_chunks;       /**< The maximum number of buffers to be used for encoding. */
    struct ua_buffer             *recv_buf;         /**< Buffer chain containing the request. */
    uint32_t                      endpoint_id;      /**< Id of the endpoint this context is related to. */
    /* The following fields must not be accessed by service handlers! */
    void                         *channel_ctxt;     /**< Message context of the transport channel */
    struct uasession_base        *base;             /**< Session base managing the session object for this request. */
    unsigned int                  buffer_handle;    /**< handle for buffer management */
#ifdef _DEBUG
    int cnt;                                        /**< message context number for debugging purpose */
#endif
};

/** Free all data referenced by the context and the context itself.
 * First level of req and res will be freed if no clear function is registered.
 * @param msg_ctxt Context to be freed.
 * @return Error code.
 */
void uasession_msg_ctxt_delete(struct uasession_msg_ctxt *ctxt);

/**  \ingroup session
 * Managing base holding resources for sessions and session calls.
 */
struct uasession_base {
    const char             *url;                          /**< Endpoint URL */
    uint32_t                nSessions;                    /**< Max number of simultaneous sessions */
    uint32_t                nSessionCalls;                /**< Max number of simultaneous session calls. */
    unsigned int            last_session_id;              /**< Last used session id (time as initial value). */
    uint32_t                nChannels;                    /**< Max number of simultaneous channels */
    uint32_t                channels_total;               /**< Accumulated channel count. */
    struct util_map         channel_session_map;          /**< Map holding channel_id-channel_pointer key-value pairs. */
    struct uatcpmsg_base   *uatcpmsg_base;                /**< The tcp protocol base. */
    uint32_t                life_timer;                   /**< Handle of session and secure channel token lifetime supervision timer. */
    struct util_pointerset  session_list;                 /**< List of current sessions. */
    struct util_pointerset  channel_list;                 /**< List of current secure channels. */
};

/** Allocate all internal resources of a session base object.
 * Values of appconfig are accessed to dimension the capacity for sessions and call contexts.
 * @param base base to be initialized.
 * @return Error code.
 */
int uasession_base_init(struct uasession_base *base,
                        const char *url,
                        struct uatcpmsg_base *uatcpmsg_base);

/** Free all data referenced by the context and the context itself.
 * @param base base to be cleared.
 * @return Error code.
 */
int uasession_base_clear(struct uasession_base *base);

struct uasession_session *uasession_base_getsessionbyid(struct uasession_base *base,
                                                        unsigned int authentication_token);

void uasession_closesession_internal(struct uasession_session *session);

struct uasession_channel *uasession_base_getchannelbyid(struct uasession_base *base, uint32_t channel_id);

void uasession_session_delete(struct uasession_session *session);

int uasession_close_channel(uint32_t channel_id, uint32_t status_code);

#endif /* _UASESSION_TYPES */

/** @} */
