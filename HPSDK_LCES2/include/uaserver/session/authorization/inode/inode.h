/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


/**
 * @defgroup inode_authorization inode
 * @ingroup authorization
 * For more information on the inode backend
 * and usage examples see @ref authorization_inode.
 * @{
 */

#ifndef INODE_H
#define INODE_H

#include <stdint.h>
#include <uaserver/addressspace/addressspace.h>

/* mode defines */
#define UA_ATTRREADABLE       0x0001
#define UA_ATTRWRITABLE       0x0002

#define UA_EVENTREADABLE      0x0004
#define UA_EXECUTABLE         0x0008

#define UA_READABLE           0x00040000
#define UA_WRITABLE           0x00080000
#define UA_HISTORYWRITABLE    0x00100000

#define UA_HISTORYREADABLE    UA_READABLE
#define UA_HISTORYINSERT      UA_HISTORYWRITABLE
#define UA_HISTORYMODIFY      UA_HISTORYWRITABLE
#define UA_HISTORYDELETE      UA_HISTORYWRITABLE

#define UA_NUM_PERM_BITS 5

#define UA_OTHER_ATTRWRITABLE       UA_ATTRWRITABLE
#define UA_OTHER_WRITABLE           UA_WRITABLE
#define UA_OTHER_EXECUTABLE         UA_EXECUTABLE
#define UA_OTHER_HISTORYINSERT      UA_HISTORYINSERT
#define UA_OTHER_HISTORYMODIFY      UA_HISTORYMODIFY
#define UA_OTHER_HISTORYDELETE      UA_HISTORYDELETE
#define UA_OTHER_READABLE           UA_READABLE
#define UA_OTHER_HISTORYREADABLE    UA_HISTORYREADABLE
#define UA_OTHER_ATTRREADABLE       UA_ATTRREADABLE
#define UA_OTHER_EVENTREADABLE      UA_EVENTREADABLE
#define UA_OTHER_OBSERVATION        (UA_OTHER_READABLE | UA_OTHER_HISTORYREADABLE | UA_OTHER_ATTRREADABLE | UA_OTHER_EVENTREADABLE)
#define UA_OTHER_OPERATION          (UA_OTHER_OBSERVATION | UA_OTHER_WRITABLE | UA_OTHER_EXECUTABLE)
#define UA_OTHER_ALL                (UA_OTHER_OPERATION | UA_OTHER_ATTRWRITABLE | UA_OTHER_HISTORYINSERT | UA_OTHER_HISTORYMODIFY | UA_OTHER_HISTORYDELETE)

#define UA_GROUP_ATTRWRITABLE       (UA_OTHER_ATTRWRITABLE    << UA_NUM_PERM_BITS)
#define UA_GROUP_WRITABLE           (UA_OTHER_WRITABLE        << UA_NUM_PERM_BITS)
#define UA_GROUP_EXECUTABLE         (UA_OTHER_EXECUTABLE      << UA_NUM_PERM_BITS)
#define UA_GROUP_HISTORYINSERT      (UA_OTHER_HISTORYINSERT   << UA_NUM_PERM_BITS)
#define UA_GROUP_HISTORYMODIFY      (UA_OTHER_HISTORYMODIFY   << UA_NUM_PERM_BITS)
#define UA_GROUP_HISTORYDELETE      (UA_OTHER_HISTORYDELETE   << UA_NUM_PERM_BITS)
#define UA_GROUP_READABLE           (UA_OTHER_READABLE        << UA_NUM_PERM_BITS)
#define UA_GROUP_HISTORYREADABLE    (UA_OTHER_HISTORYREADABLE << UA_NUM_PERM_BITS)
#define UA_GROUP_ATTRREADABLE       (UA_OTHER_ATTRREADABLE    << UA_NUM_PERM_BITS)
#define UA_GROUP_EVENTREADABLE      (UA_OTHER_EVENTREADABLE   << UA_NUM_PERM_BITS)
#define UA_GROUP_OBSERVATION        (UA_GROUP_READABLE | UA_GROUP_HISTORYREADABLE | UA_GROUP_ATTRREADABLE | UA_GROUP_EVENTREADABLE)
#define UA_GROUP_OPERATION          (UA_GROUP_OBSERVATION | UA_GROUP_WRITABLE | UA_GROUP_EXECUTABLE)
#define UA_GROUP_ALL                (UA_GROUP_OPERATION | UA_GROUP_ATTRWRITABLE | UA_GROUP_HISTORYINSERT | UA_GROUP_HISTORYMODIFY | UA_GROUP_HISTORYDELETE)

#define UA_USER_ATTRWRITABLE        (UA_GROUP_ATTRWRITABLE    << UA_NUM_PERM_BITS)
#define UA_USER_WRITABLE            (UA_GROUP_WRITABLE        << UA_NUM_PERM_BITS)
#define UA_USER_EXECUTABLE          (UA_GROUP_EXECUTABLE      << UA_NUM_PERM_BITS)
#define UA_USER_HISTORYINSERT       (UA_GROUP_HISTORYINSERT   << UA_NUM_PERM_BITS)
#define UA_USER_HISTORYMODIFY       (UA_GROUP_HISTORYMODIFY   << UA_NUM_PERM_BITS)
#define UA_USER_HISTORYDELETE       (UA_GROUP_HISTORYDELETE   << UA_NUM_PERM_BITS)
#define UA_USER_READABLE            (UA_GROUP_READABLE        << UA_NUM_PERM_BITS)
#define UA_USER_HISTORYREADABLE     (UA_GROUP_HISTORYREADABLE << UA_NUM_PERM_BITS)
#define UA_USER_ATTRREADABLE        (UA_GROUP_ATTRREADABLE    << UA_NUM_PERM_BITS)
#define UA_USER_EVENTREADABLE       (UA_GROUP_EVENTREADABLE   << UA_NUM_PERM_BITS)
#define UA_USER_OBSERVATION         (UA_USER_READABLE | UA_USER_HISTORYREADABLE | UA_USER_ATTRREADABLE | UA_USER_EVENTREADABLE)
#define UA_USER_OPERATION           (UA_USER_OBSERVATION | UA_USER_WRITABLE | UA_USER_EXECUTABLE)
#define UA_USER_ALL                 (UA_USER_OPERATION | UA_USER_ATTRWRITABLE | UA_USER_HISTORYINSERT | UA_USER_HISTORYMODIFY | UA_USER_HISTORYDELETE)

#define UA_ALL_ENCRYPTION_REQUIRED  (0x1 << (3 * UA_NUM_PERM_BITS))


/** Maximum number of groups per user.
 * With 5 groups and uid and gid being 1 byte the structure
 * is 8 byte, which fits nicely into memory.
 */
#define UA_AUTHORIZATION_MAX_GROUPS_PER_USER 5

/** Datatype for the user id, can be increased to support more users */
typedef uint8_t ua_uid_t;
/** Datatype for the group id, can be increased to support more groups */
typedef uint8_t ua_gid_t;

/**
 * Permission information contained in every node.
 */
struct ua_perm_ctx {
    uint16_t mode; /**< Do NOT access this field directly! */
    ua_uid_t uid;
    ua_gid_t gid;
};

/**
 * Representation of a user in the server.
 */
struct ua_user_ctx {
    ua_uid_t uid;
    ua_gid_t num_groups;
    ua_gid_t groups[UA_AUTHORIZATION_MAX_GROUPS_PER_USER];
    uint8_t security_mode; /* holds enum ua_messagesecuritymode which would be 4 byte but uses only 2 bits */
};

int ua_inode_set_perm(ua_node_t node, ua_uid_t uid, ua_gid_t gid, uint32_t permissions);
int ua_inode_set_default_perm(ua_uid_t uid, ua_gid_t gid, uint32_t permissions);

int ua_inode_add_user(const char *username, ua_uid_t id);
int ua_inode_add_group(const char *groupname, ua_gid_t id);
int ua_inode_add_user_to_group(const char *username, const char *groupname);

int ua_inode_get_uid(const char *username, ua_uid_t *uid);
int ua_inode_get_gid(const char *groupname, ua_gid_t *gid);


#endif /* end of include guard: INODE_H */

/** @} */
