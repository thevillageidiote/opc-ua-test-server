/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


/**
 * @addtogroup authentication
 * Interface for implementing an authentication backend.
 *
 * The functions declared here must be implemented by every
 * authentication backend and are mainly intended for use by the SDK.
 *
 * Adding and Removing users is backend specific and thus implemented
 * in the backends like @ref internal_authentication.
 * @{
 */

#ifndef AUTHENTICATION_H
#define AUTHENTICATION_H

#include <uabase/statuscode.h>
#include <uabase/extensionobject.h>
#include <uabase/structure/signaturedata.h>
#include <uaserver/session/session.h>

/**
 * Initialize structures needed for managing user authentication.
 *
 * @param max_users Maximum number of users, that can be added.
 * @return 0 on success or errorcode on failure.
 */
int ua_authentication_init(uint32_t max_users);

/**
 * Clear structures needed for managing user authentication. All
 * users wil be removed and no user will be able to authenticate any
 * longer.
 */
void ua_authentication_clear(void);

/**
 * Authenticate a user who wants to activate a session.
 *
 * @param session The session to be activated.
 * @param channel The channel of the session. Note: This pointer is only valid
 * in the synchronous call.
 * @param token Token the user provides.
 * @param tokensignature Tokensignature of the user.
 * @param channel_changed Indicates if the channel changed between this activate
 * and the last activate. If true the backend has to make sure the same authentication
 * policy is used.
 * @param result Result of the authentication check.
 * @param username Identifier for the user being authenticated, must be set by the
 * authentication backend on success and is used for authorization.
 * @param ctxt Context to be passed to uasession_activatesesion_finish().
 * @return UA_EASYNC in case of async finish, for other return values
 * \c result will be evaluated.
 */
int ua_authenticate_user(struct uasession_session *session,
                      struct uasession_channel *channel,
                      struct ua_extensionobject *token,
                      struct ua_signaturedata *tokensignature,
                      bool channel_changed,
                      ua_statuscode *result,
                      struct ua_string *username,
                      void *ctxt);

#ifdef SUPPORT_FILE_IO
/**
 * Load user authentication information from a file.
 *
 * @param passwd_file Name of the file to load authentication
 * information from.
 * @return 0 on success or errorcode on failure.
 */
int ua_authentication_load_from_file(const char *passwd_file);
#endif

#endif /* end of include guard: AUTHENTICATION_H */

/** @}*/
