/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef SUBSCRIPTION_H
#define SUBSCRIPTION_H

#include <stdint.h>
#include <stdbool.h>
#include <util/ringqueue.h>
#include <uabase/statuscode.h>

struct ua_subscriptionbase;
struct uasession_msg_ctxt;
struct ua_monitoreditem;

enum ua_subscription_state
{
    ua_subscription_state_normal,
    ua_subscription_state_late,
    ua_subscription_state_keepalive,
};

struct ua_subscription
{
    bool publishing_enabled;
    bool message_sent;
    bool more_notifications;
    bool is_locked;
    uint8_t priority;
    uint32_t timer_id;
    uint32_t lifetime_count;
    uint32_t max_lifetime_count;
    uint32_t keep_alive_count;
    uint32_t max_keep_alive_count;
    uint32_t max_notifications_per_publish;
    uint32_t publishing_interval;
    uint32_t seq_num;
    uint32_t num_itmes;
    /* the id is persisted in the objectpool, so there must be at least sizeof(void *) before it */
    uint32_t id;
    enum ua_subscription_state state;
    struct ua_subscriptionbase *base;
    struct ua_monitoreditem *item_list[];
};


int ua_subscription_init(void);
void ua_subscription_clear(void);

struct ua_subscription *ua_subscription_create(struct ua_subscriptionbase *base, uint32_t publishing_interval);
void ua_subscription_delete(struct ua_subscription *sub);
struct ua_subscription *ua_subscription_get(struct ua_subscriptionbase *base, uint32_t id);
struct ua_subscription *ua_subscription_get_unsafe(uint32_t id);
void ua_subscription_disable_items(struct ua_subscription *sub);

bool ua_subscription_notifications_available(struct ua_subscription *sub);
ua_statuscode ua_subscription_add_monitoreditem(struct ua_subscription *sub, struct ua_monitoreditem *item);
int ua_subscription_remove_monitoreditem(struct ua_subscription *sub, struct ua_monitoreditem *item);

int ua_subscription_send_keepalive(struct ua_subscription *sub, struct uasession_msg_ctxt *ctx);
int ua_subscription_send_notification(struct ua_subscription *sub, struct uasession_msg_ctxt *ctx);
int ua_subscription_send_timeout(struct uasession_msg_ctxt *ctx, uint32_t sub_id);

ua_statuscode ua_subscription_revise(double req_publishing_interval,
                                     uint32_t req_keepalive_count,
                                     uint32_t req_lifetime_count,
                                     uint32_t req_max_notifications,
                                     uint32_t *rev_publishing_interval,
                                     uint32_t *rev_keepalive_count,
                                     uint32_t *rev_lifetime_count,
                                     uint32_t *rev_max_notifications);

int publish_timer_callback(uint64_t elapsed, void *data);

#endif /* end of include guard: SUBSCRIPTION_H */

/** @} */
