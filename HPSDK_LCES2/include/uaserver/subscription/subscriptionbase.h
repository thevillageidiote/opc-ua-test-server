/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef SUBSCRIPTION_BASE_H
#define SUBSCRIPTION_BASE_H

#include <stdint.h>
#include <util/ringqueue.h>
#include <util/list.h>

struct ua_subscription;
struct uasession_msg_ctxt;
struct ua_supernotification;
struct ua_publishresponse;
struct ua_publishrequest;
struct uasession_session;

struct ua_subscriptionbase
{
    uint16_t num_subs;
    uint32_t num_closed_subs;
    uint32_t *closed_subs;
    struct uasession_session *session;
    struct util_ringqueue req_queue;
    struct util_list notification_list;
    /* TODO: this should be a priority queue */
    struct ua_subscription *subscription_list[];
};

int ua_subscriptionbase_init(void);
void ua_subscriptionbase_clear(void);

struct ua_subscriptionbase *ua_subscriptionbase_create(struct uasession_session *session);
void ua_subscriptionbase_delete(struct ua_subscriptionbase *base);

/* subscription */
int ua_subscriptionbase_add_subscription(struct ua_subscriptionbase *base, struct ua_subscription *sub);
int ua_subscriptionbase_remove_subscription(struct ua_subscriptionbase *base, struct ua_subscription *sub);

/* notification */
int ua_subscriptionbase_add_notification(struct ua_subscriptionbase *base, struct ua_supernotification *sn);
struct ua_supernotification *ua_subscriptionbase_get_notification(struct ua_subscriptionbase *base, uint32_t subscription_id, uint32_t seq_number);
void ua_subscriptionbase_ack_notifications(struct ua_subscriptionbase *base, struct ua_publishrequest *req, struct ua_publishresponse *res);
void ua_subscriptionbase_create_acks(struct ua_subscriptionbase *base, struct ua_publishresponse *res);

/* publish request */
unsigned int ua_subscriptionbase_count_publish(struct ua_subscriptionbase *base);
int ua_subscriptionbase_enqueue_publish(struct ua_subscriptionbase *base, struct uasession_msg_ctxt *req);
int ua_subscriptionbase_enqueue_publish_front(struct ua_subscriptionbase *base, struct uasession_msg_ctxt *req);
struct uasession_msg_ctxt *ua_subscriptionbase_dequeue_publish(struct ua_subscriptionbase *base);

#endif /* end of include guard: SUBSCRIPTION_BASE_H */

/** @} */
