/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UASERVER_MEMORYSTORE_H_
#define _UASERVER_MEMORYSTORE_H_

#include <uabase/datavalue.h>
#include <uabase/statuscode.h>
#include <uabase/indexrange.h>
#include <uabase/structure/timestampstoreturn.h>
#include <uaserver/addressspace/addressspace.h>

/**
 * @ingroup valuestore
 * @struct ua_memorystore
 * Implementation of a store using in-memory values.
 *
 * A new memorystore must be initialized with a user provided array
 * of empty ua_variant structs using @ref ua_memorystore_init. The number of
 * elements in the array is the capacity of the store. Then values can be
 * added with @ref ua_memorystore_attach_new_value. This function also registers
 * the store with the given storeindex.
 *
 * For a full example how to use the memorystore see @ref server_gettingstarted2.
 */
struct ua_memorystore {
    struct ua_variant *values; /**< Array with values */
    unsigned int num_values;   /**< Number of currently used values */
    unsigned int max_values;   /**< Number of elements in the array */
    uint8_t storeidx;          /**< Index of the store */
};

int ua_memorystore_attach_new_value(struct ua_memorystore *store, struct ua_variant *value, ua_node_t node);
struct ua_variant *ua_memorystore_clear(struct ua_memorystore *store);
int ua_memorystore_init(struct ua_memorystore *store, uint8_t storeidx, struct ua_variant *values, unsigned int num_values);

uint8_t ua_memorystore_get_storeindex(struct ua_memorystore *store);


#endif /* _UASERVER_MEMORYSTORE_H_ */

