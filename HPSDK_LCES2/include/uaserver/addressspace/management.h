/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef UA_ADDR_MANAGEMENT_H
#define UA_ADDR_MANAGEMENT_H

#include <stdint.h>
#include <stdbool.h>

/* forward declarations */
struct mem_objectpool_static;

/**
 * @defgroup addr_management management
 * @ingroup addressspace
 * General addressspace management like register/unregister namespaces.
 * @{
 */

/** Possible index types for nodes in the addressspace */
enum ua_addressspace_index_type {
    UA_INDEX_BTREE,
    UA_INDEX_HASHTABLE,
    UA_INDEX_HASHTABLEQP
};


/* Initialize address space module. */
int ua_addressspace_init(void);
void ua_addressspace_clear(void);

struct ua_addressspace_config {
    uint16_t nsidx;
    char url[256];
    unsigned int max_variables;
    unsigned int max_objects;
    unsigned int max_methods;
    unsigned int max_variabletypes;
    unsigned int max_referencetypes;
    unsigned int max_objecttypes;
    unsigned int max_datatypes;
    unsigned int max_references;
    unsigned int max_views;
    unsigned int max_strings;
    unsigned int max_guids;
};

/* namespaces */
int ua_addressspace_register(
        const char *url,
        struct ua_addressspace_config *config,
        enum ua_addressspace_index_type type);

int ua_addressspace_register_static(
        const char *url,
        int hashtablesize,
        struct mem_objectpool_static *nodepool,
        struct mem_objectpool_static *varpool,
        struct mem_objectpool_static *vartypepool,
        struct mem_objectpool_static *reftypepool,
        struct mem_objectpool_static *refpool,
        struct mem_objectpool_static *stringpool,
        const char *stringtable[],
        struct mem_objectpool_static *guidpool,
        uint16_t nsidx,
        unsigned int num_placeholders,
        enum ua_addressspace_index_type type);

void ua_addressspace_unregister(uint16_t nsidx);

struct ua_addressspace *ua_addressspace_getaddr(uint16_t nsidx);
bool ua_addressspace_is_static(struct ua_addressspace *addr);

/* diagnostic */
void ua_addressspace_print_stat(uint16_t);

/** @} */

#endif /* UA_ADDR_MANAGEMENT_H */
