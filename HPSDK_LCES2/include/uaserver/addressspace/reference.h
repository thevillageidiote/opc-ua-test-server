/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef UA_ADDR_REFERENCE_H
#define UA_ADDR_REFERENCE_H

#include <stdint.h>

/**
 * @defgroup addr_reference reference
 * @ingroup addressspace
 * Create/delete/iterate through references
 * @{
 */

/** Handle for a reference in the addressspace */
typedef int32_t ua_ref_t;
/** Value of an invalid reference handle */
#define UA_REF_INVALID (ua_ref_t)-1
#define UA_NSIDX_INVALID (uint16_t)-1
#ifdef CODE_GENERATOR
# define UA_REF_PLACEHOLDER (ua_ref_t)-2
#endif

/* Return first forward reference */
ua_ref_t ua_node_first_reference(ua_node_t node);
/* Return next forward reference */
ua_ref_t ua_node_next_reference(ua_ref_t ref);
/* Return first reverse reference */
ua_ref_t ua_node_first_inv_reference(ua_node_t node);
/* Return next reverse reference */
ua_ref_t ua_node_next_inv_reference(ua_ref_t ref);
/* Count number of forward references and return result. */
unsigned int ua_node_forward_ref_count(ua_node_t node);
/* Count number of reverse references and return result. */
unsigned int ua_node_inverse_ref_count(ua_node_t node);
/** Foreach loop for iterating over all forward references of a node */
#define ua_node_foreach(ref, node) for (ref = ua_node_first_reference(node); ref != UA_REF_INVALID; ref = ua_node_next_reference(ref))
/** Foreach loop for iterating over all inverse references of a node */
#define ua_node_foreach_inv(ref, node) for (ref = ua_node_first_inv_reference(node); ref != UA_REF_INVALID; ref = ua_node_next_inv_reference(ref))

ua_ref_t ua_reference_add(ua_node_t src, ua_node_t dst, ua_node_t reftype);
ua_ref_t ua_reference_add0(ua_node_t src, ua_node_t dst, uint32_t reftypeid);

int ua_reference_remove(ua_ref_t ref, ua_ref_t *prev_ref);
ua_node_t ua_reference_target(ua_ref_t ref);
ua_node_t ua_reference_source(ua_ref_t ref);
ua_node_t ua_reference_type(ua_ref_t ref);

/** @} */

#endif /* UA_ADDR_REFERENCE_H */
