/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef UA_ADDR_PERSISTENCE_H
#define UA_ADDR_PERSISTENCE_H

#include <stdint.h>

#include <platform/platform_config.h>
#ifdef SUPPORT_FILE_IO
# include <platform/file.h>
# include "addressspace.h"

/* forward declarations */
struct ua_addressspace_config;

/** Additional cross reference info for binary files. */
struct ua_crossreference {
    ua_node_t src;
    ua_node_t dst;
    ua_node_t reftype;
};

/**
 * @defgroup addr_persistence persistence
 * @ingroup addressspace
 * Save/load namespaces to/from file.
 * @{
 */

int ua_addressspace_save_file(uint16_t nsidx, const char *filename, unsigned int num_xrefs, const struct ua_crossreference *xrefs);
int ua_addressspace_load_file(const char *filename, void *store, int (*store_from_fd)(void *store, ua_file_t f), struct ua_addressspace_config *add);
int ua_addressspace_load_file_info(const char *filename, struct ua_addressspace_config *info);
int ua_addressspace_save_fd(uint16_t nsidx, ua_file_t f, unsigned int num_xrefs, const struct ua_crossreference *xrefs);
int ua_addressspace_load_fd(ua_file_t f, void *store, int (*store_from_fd)(void *store, ua_file_t f), struct ua_addressspace_config *add);
int ua_addressspace_load_fd_info(ua_file_t f, struct ua_addressspace_config *info);

# ifdef CODE_GENERATOR

ua_ref_t ua_reference_add_placeholder(ua_node_t src);

int ua_addressspace_save_c_file(uint16_t nsidx, const char *filename);
int ua_addressspace_save_c_fd(uint16_t nsidx, struct ua_filestream *f);
# endif /* CODE_GENERATOR */

/** @} */

#endif /* SUPPORT_FILE_IO */
#endif /* UA_ADDR_PERSISTENCE_H */
