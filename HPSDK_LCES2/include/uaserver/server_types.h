/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef SERVER_TYPES_H_MLFW9XJV
#define SERVER_TYPES_H_MLFW9XJV

#include <appconfig.h>
#include <uaprotocol/uaseconv/seconv_types.h>
#include <uabase/structure/usertokentype.h>

/**
 * Identifies a certificate and key, does not contain the certificate
 * or key data.
 */
struct uaserver_certificate {
    const char *certificate;    /**< Certificate location specifier (file:// or store:// (only store for new certificates)) */
    const char *key;            /**< Key location specifier (file:// or store (identified by cert id)) */
    uint32_t    store;          /**< Store identifier */
    /* creation information - only required if certificate shall be generated automatically */
    uint32_t    days_valid;     /**< Number of days the created certificate shall be valid. */
    const char *algorithm;      /**< Signature algorithm used for signing the new certificate (sha1 or sha256) */
    uint32_t    key_length;     /**< Key length in bits of the new key pair. */
    uint32_t    issuer_index;   /**< Index specifying the issuer certificate (same as cert for self signed) */
    bool        create;         /**< true if certificate shall be generated if not found at above location */
};

struct uaserver_endpoint {
    const char *endpoint_url;             /**< endpoint url: e.g. "opc.tcp://hostname" */
    const char *bind_address;             /**< bind address: e.g. "0.0.0.0" for all IPs */
    uint32_t bind_port;                   /**< port number in host byte order */
    struct index_array security_policies; /**< Index array of security policies for this endpoint */
    struct index_array user_tokens;       /**< Index array of user token types for this endpoint */
};

struct uaserver_pkistore {
    const char *config;
};

struct uaserver_user_token {
    const char *name;
    enum ua_usertokentype type;
    enum ua_security_policy_id policy_id;
};

#endif /* end of include guard: SERVER_TYPES_H_MLFW9XJV */

