/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef BROWSE_H_YUB4UTTL
#define BROWSE_H_YUB4UTTL

#include <uaserver/addressspace/addressspace.h>

struct ua_browsedescription;
struct ua_browseresult;
struct ua_requestheader;
struct ua_browserequest;
struct ua_viewdescription;
struct uasession_session;
struct uasession_msg_ctxt;

void uaserver_browse_node(ua_node_t node,
        struct uasession_session *session,
        struct ua_browsedescription *browsdesc,
        struct ua_viewdescription *view,
        uint32_t max_results,
        ua_ref_t start_ref,
        struct ua_browseresult *result);

int uasession_browse(struct ua_requestheader *head, struct ua_browserequest *req, struct uasession_msg_ctxt *ctxt);

#endif /* end of include guard: BROWSE_H_YUB4UTTL */

