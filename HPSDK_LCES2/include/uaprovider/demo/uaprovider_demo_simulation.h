/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UAPROVIDER_DEMO_SIMULATION_H
#define UAPROVIDER_DEMO_SIMULATION_H

#include <uabase/string.h>
#include <uabase/datavalue.h>
#include <uabase/indexrange.h>
#include <uabase/structure/timestampstoreturn.h>
#include <uabase/uatype_config.h>
#include <uaprovider/provider.h>

int uaprovider_demo_simulation_init(void);
void uaprovider_demo_simulation_clear(void);

#ifdef ENABLE_SERVICE_CALL
void uaprovider_demo_call(struct uaprovider_call_ctx *ctx);
#endif

#endif /* end of include guard: UAPROVIDER_DEMO_SIMULATION_H */

