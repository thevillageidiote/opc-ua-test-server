/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef PROVIDER_H_MEI3KJ8Z
#define PROVIDER_H_MEI3KJ8Z

#include <common/ref.h>
#include <uaserver/session/session.h>
#include <uaserver/addressspace/addressspace.h>
#include <uabase/uatype_config.h>
#include <uabase/statuscode.h>

/**
 * @defgroup uaprovider uaprovider
 * @{
 */

/** Maximum number of namespaces per provider */
#define MAX_NAMESPACES_PER_PROVIDER 5

/* forward declaration */
struct ua_monitoreditem;

void uaprovider_node_delete(ua_node_t node, uint16_t nsidx);

#ifdef ENABLE_SERVICE_READ
/**
 * @brief Context given to the read service handler
 * @struct uaprovider_read_ctx
 */
struct uaprovider_read_ctx {
    struct uasession_session     *session;      /**< Session associated with the request */
    struct ua_requestheader      *req_head;     /**< Request header */
    struct ua_readrequest        *req;          /**< Request from the client */
    struct ua_responseheader     *res_head;     /**< Response header */
    struct ua_readresponse       *res;          /**< Response for the client */
    struct uaprovider            *cur_provider; /**< Context of the current provider */
    struct ua_ref                 refcnt;       /**< @internal Reference count of providers that have not finished yet */
};

struct uaprovider_read_ctx *uaprovider_read_ctx_init(struct uasession_msg_ctxt *session_msg_ctxt);
void uaprovider_read(struct uaprovider_read_ctx *ctx);
void uaserver_read_complete(struct uaprovider_read_ctx *ctx);
#endif

#ifdef ENABLE_SERVICE_WRITE
/**
 * @brief Context given to the write service handler
 * @struct uaprovider_write_ctx
 */
struct uaprovider_write_ctx {
    struct uasession_session     *session;      /**< Session associated with the request */
    struct ua_requestheader      *req_head;     /**< Request header */
    struct ua_writerequest       *req;          /**< Request from the client */
    struct ua_responseheader     *res_head;     /**< Response header */
    struct ua_writeresponse      *res;          /**< Response for the client */
    struct uaprovider            *cur_provider; /**< Context of the current provider */
    struct ua_ref                 refcnt;       /**< @internal Reference count of providers that have not finished yet */
};

struct uaprovider_write_ctx *uaprovider_write_ctx_init(struct uasession_msg_ctxt *session_msg_ctxt);
void uaprovider_write(struct uaprovider_write_ctx *ctx);
void uaserver_write_complete(struct uaprovider_write_ctx *ctx);
#endif

#ifdef ENABLE_SERVICE_REGISTERNODES
/**
 * @brief Context given to the registernodes service handler
 * @struct uaprovider_registernodes_ctx
 */
struct uaprovider_registernodes_ctx {
    struct uasession_session        *session;      /**< Session associated with the request */
    struct ua_requestheader         *req_head;     /**< Request header */
    struct ua_registernodesrequest  *req;          /**< Request from the client */
    struct ua_responseheader        *res_head;     /**< Response header */
    struct ua_registernodesresponse *res;          /**< Response for the client */
    struct uaprovider               *cur_provider; /**< Context of the current provider */
    struct ua_ref                    refcnt;       /**< @internal Reference count of providers that have not finished yet */
};

struct uaprovider_registernodes_ctx *uaprovider_registernodes_ctx_init(struct uasession_msg_ctxt *session_msg_ctxt);
void uaprovider_registernodes(struct uaprovider_registernodes_ctx *ctx);
void uaserver_registernodes_complete(struct uaprovider_registernodes_ctx *ctx);

/**
 * @brief Context given to the unregisternodes service handler
 * @struct uaprovider_unregisternodes_ctx
 */
struct uaprovider_unregisternodes_ctx {
    struct uasession_session         *session;      /**< Session associated with the request */
    struct ua_requestheader          *req_head;     /**< Request header */
    struct ua_unregisternodesrequest *req;          /**< Request from the client */
    struct ua_responseheader         *res_head;     /**< Response header */
    struct ua_unregisternodesresponse *res;         /**< Response for the client */
    struct uaprovider                *cur_provider; /**< Context of the current provider */
    struct ua_ref                     refcnt;       /**< @internal Reference count of providers that have not finished yet */
};

struct uaprovider_unregisternodes_ctx *uaprovider_unregisternodes_ctx_init(struct uasession_msg_ctxt *session_msg_ctxt);
void uaprovider_unregisternodes(struct uaprovider_unregisternodes_ctx *ctx);
void uaserver_unregisternodes_complete(struct uaprovider_unregisternodes_ctx *ctx);
#endif

#ifdef ENABLE_SERVICE_CALL
/**
 * @brief Context given to the call service handler
 * @struct uaprovider_call_ctx
 */
struct uaprovider_call_ctx {
    struct uasession_session     *session;      /**< Session associated with the request */
    struct ua_requestheader      *req_head;     /**< Request header */
    struct ua_callrequest        *req;          /**< Request from the client */
    struct ua_responseheader     *res_head;     /**< Response header */
    struct ua_callresponse       *res;          /**< Response for the client */
    struct uaprovider            *cur_provider; /**< Context of the current provider */
    struct ua_ref                 refcnt;       /**< @internal Reference count of providers that have not finished yet */
};

struct uaprovider_call_ctx *uaprovider_call_ctx_init(struct uasession_msg_ctxt *session_msg_ctxt);
void uaprovider_call(struct uaprovider_call_ctx *ctx);
void uaserver_call_complete(struct uaprovider_call_ctx *ctx);
#endif

#ifdef ENABLE_SERVICE_SUBSCRIPTION
/**
 * @brief Context given to the subscribe handler
 * @struct uaprovider_subscribe_ctx
 */
struct uaprovider_subscribe_ctx {
    struct ua_ref refcnt;
    void (*cb_fct) (void *cb_data);
    void *cb_data;
};

ua_statuscode uaprovider_add_item(struct ua_monitoreditem *item, uint32_t max_items);
ua_statuscode uaprovider_remove_item(struct ua_monitoreditem *item, uint32_t max_items);
ua_statuscode uaprovider_modify_item(struct ua_monitoreditem *item, uint32_t max_items, uint32_t new_sampling_interval);

struct uaprovider_subscribe_ctx *uaprovider_subscribe_ctx_create(void);
void uaprovider_subscribe_ctx_delete(struct uaprovider_subscribe_ctx *ctx);
void uaprovider_subscribe(struct uaprovider_subscribe_ctx *ctx);
void uaserver_subscribe_complete(struct uaprovider_subscribe_ctx *ctx);
#endif

#ifdef ENABLE_SERVICE_HISTORYREAD
/**
 * @brief Context given to the read service handler
 * @struct uaprovider_read_ctx
 */
struct uaprovider_historyread_ctx {
    struct uasession_session      *session;      /**< Session associated with the request */
    struct ua_requestheader       *req_head;     /**< Request header */
    struct ua_historyreadrequest  *req;          /**< Request from the client */
    struct ua_responseheader      *res_head;     /**< Response header */
    struct ua_historyreadresponse *res;          /**< Response for the client */
    struct uaprovider             *cur_provider; /**< Context of the current provider */
    struct ua_ref                  refcnt;       /**< @internal Reference count of providers that have not finished yet */
};

struct uaprovider_historyread_ctx *uaprovider_historyread_ctx_init(struct uasession_msg_ctxt *session_msg_ctxt);
void uaprovider_historyread(struct uaprovider_historyread_ctx *ctx);
void uaserver_historyread_complete(struct uaprovider_historyread_ctx *ctx);
#endif

/**
 * @struct uaprovider
 * @brief Provider context.
 * Struct with provider service handler functions and
 * namespaces the provider has registered.
 */
struct uaprovider {
    uint16_t nsindices[MAX_NAMESPACES_PER_PROVIDER]; /**< @internal Array of ns indices the provider has registered. */
    uint16_t num_nsindices;                          /**< @internal Number of elements in ns index array. */
    void (*cleanup)(void);                           /**< Function that is called when the provider is removed. */
    /** Node delete handler function. This function is called before the node gets
     * deleted. This gives you the chance to remove resources of underlying IO,
     * when the node gets deleted.
     */
    void (*node_delete)(ua_node_t node);
#ifdef ENABLE_SERVICE_READ
    void (*read)(struct uaprovider_read_ctx *ctx);   /**< Read service handler function */
#endif
#ifdef ENABLE_SERVICE_WRITE
    void (*write)(struct uaprovider_write_ctx *ctx); /**< Write service handler function */
#endif
#ifdef ENABLE_SERVICE_CALL
    void (*call)(struct uaprovider_call_ctx *ctx);   /**< Call service handler function */
#endif
#ifdef ENABLE_SERVICE_REGISTERNODES
    void (*registernodes)(struct uaprovider_registernodes_ctx *ctx);     /**< Register nodes service handler function */
    void (*unregisternodes)(struct uaprovider_unregisternodes_ctx *ctx); /**< Unregister nodes service handler function */
#endif
#ifdef ENABLE_SERVICE_SUBSCRIPTION
    ua_statuscode (*add_item)(struct ua_monitoreditem *item, uint32_t max_items);    /**< Add monitoreditem operation handler function */
    ua_statuscode (*remove_item)(struct ua_monitoreditem *item, uint32_t max_items); /**< Remove monitoreditem operation handler function */
    ua_statuscode (*modify_item)(struct ua_monitoreditem *item, uint32_t max_items, uint32_t new_sampling_interval); /**< Modify monitoreditem operation handler function */
    void (*subscribe)(struct uaprovider_subscribe_ctx *ctx);     /**< Subscribe operation handler function */
#endif
#ifdef ENABLE_SERVICE_HISTORYREAD
    void (*historyread)(struct uaprovider_historyread_ctx *ctx);   /**< HistoryRead service handler function */
#endif
};

/* Provider management */
/** Provider initialization function that must be implemented by each provider. */
typedef int (*uaprovider_init_func)(struct uaprovider *ctx);
int uaprovider_load(uaprovider_init_func init);
int uaprovider_unload(int hProvider);
int uaprovider_register_nsindex(struct uaprovider *ctx, uint16_t nsidx);
bool uaprovider_contains_namespace(struct uaprovider *ctx, uint16_t nsidx);

/** @} */

#endif /* end of include guard: PROVIDER_H_MEI3KJ8Z */

