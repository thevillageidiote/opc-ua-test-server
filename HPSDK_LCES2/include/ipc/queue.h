/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _QUEUE_H_
#define _QUEUE_H_

#include <ipc/ipc_config.h>

/* message types */
#define IPC_MSG_NOOP 0
#define IPC_MSG_QUIT 1
#define IPC_MSG_REGISTER 2
#define IPC_MSG_UNREGISTER 3
#define IPC_MSG_SERVICE_CALL 4

/* callback flag for service->type */
#define IPC_SERVICE_CALLBACK 0x80

/* well known queueid of master process.
 * all other queueid are registered dynamically.
 */
#define IPC_QUEUEID_MASTER 0

struct ipc_queue;

#define IPC_SYMBOL_MAX_LEN 16
#define IPC_CALLSTACK_MAX  3

struct ipc_message {
    int sender_id;
    int msg_id;
    unsigned int prio;    /**< heap sorts by prio */
    unsigned int counter; /**< 2nd sorting criteria for messages with same prio */
    void *param1;
    void *param2;
#ifdef BUILD_WITH_IPC_DIAG
    char callstack[IPC_CALLSTACK_MAX][IPC_SYMBOL_MAX_LEN];
    int callstack_depth;
#endif /* BUILD_WITH_IPC_DIAG */
};

struct ipc_queue *ipc_queue_create(void);
void ipc_queue_delete(struct ipc_queue *ipc_queue);
void ipc_queue_initialize(struct ipc_queue *ipc_queue);
void ipc_queue_clear(struct ipc_queue *ipc_queue);
int ipc_queue_put(struct ipc_queue *ipc_queue, struct ipc_message *msg);
int ipc_queue_get(struct ipc_queue *ipc_queue, struct ipc_message *msg);
int ipc_queue_take(struct ipc_queue *ipc_queue, struct ipc_message **msg);
int ipc_queue_tryget(struct ipc_queue *ipc_queue, struct ipc_message *msg);
int ipc_queue_trytake(struct ipc_queue *ipc_queue, struct ipc_message **msg);
int ipc_queue_timedget(struct ipc_queue *ipc_queue, struct ipc_message *msg, int ms_timeout);
int ipc_queue_timedtake(struct ipc_queue *ipc_queue, struct ipc_message **msg, int ms_timeout);
void ipc_queue_loop(struct ipc_queue *ipc_queue);
int ipc_queue_post_message(int sender, int receiver, int msg_id, int prio, void *param1, void *param2);
int ipc_queue_handle(struct ipc_queue *ipc_queue);
void *ipc_queue_lookup(int handle);
unsigned int ipc_queue_count(struct ipc_queue *ipc_queue);

#endif /* _QUEUE_H_ */
