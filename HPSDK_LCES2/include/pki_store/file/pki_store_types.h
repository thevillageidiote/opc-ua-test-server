/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


/*
 * This file was generated using CMake on .
 * Don't modify this file! All changes will be lost!
 * Use CMake to change the configuration.
 */

#ifndef _PKI_STORE_TYPES_H_
#define _PKI_STORE_TYPES_H_

#include "platform/platform.h"
#include "stdint.h"
#include "stdbool.h"
#include "pki/cert.h"

enum pki_store_loc {
    pki_store_loc_invld = 0,
    pki_store_loc_trstd = 1,
    pki_store_loc_issrs = 2,
    pki_store_loc_rjctd = 3,
    pki_store_loc_own = 4
};

struct pki_store_credentials {
    void *data;
    size_t len;
};

#include "pki_store/file/pki_store.h"
#include "proxystub/pki_store.h"

#endif /* _PKI_STORE_TYPES_H_ */
