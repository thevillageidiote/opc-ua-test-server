/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _DNSRSLV_H_
#define _DNSRSLV_H_

#include <network/uanetwork.h>

UA_BEGIN_EXTERN_C

/**
 * @addtogroup network
 * @{
 */

/**
 * Receive addresses after completion of asynchronous address resolution.
 * Only called if ua_net_resolve returned UA_NET_E_ASYNC.
 * @param iResult Result of the operation.
 * @param iNoOfAddresses Number of used slots in address array.
 * @param pAddresses Filled array passed into resolve function.
 * @param pvCallbackData User data passed into resolve function.
 */
typedef void (ua_net_resolve_callback)(int                 iResult,
                                       int                 iNoOfAddresses,
                                       struct ua_net_addr *pAddresses,
                                       void               *pvCallbackData);

UA_NET_API
int UA_NET_CCV ua_net_resolve(struct ua_net_base      *pNetBase,
                              const char              *sHost,
                              int                     *piNoOfAddresses,
                              struct ua_net_addr      *pAddresses,
                              ua_net_resolve_callback *pfCallback,
                              void                    *pvCallbackData);

/** @} */

UA_END_EXTERN_C

#endif /* _DNSRSLV_H_ */

