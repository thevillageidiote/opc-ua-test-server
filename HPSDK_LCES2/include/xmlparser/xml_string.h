/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef XML_STRING_H_SPBSGDIA
#define XML_STRING_H_SPBSGDIA

#include <stdbool.h>
#include <stddef.h>

#define XML_GOOD 0
#define XML_BAD  -1
#define XML_BAD_INVALID_ARG  -2
#define XML_BAD_TRUNCATED -3

/** Minimalistic string class for referencing strings in the XML document.
 * This parser is a non-extractive parser, so we don't copy strings.
 * This also means that these strings are NOT zero terminated.
 * For creating zero terminated strings you can use ua_string:
 * @code
 * struct xml_string *xml = xml_node_name(n);
 * struct ua_string string;
 * ua_string_setn(&string, xml_string_data(xml), xml_string_length(xml));
 * @endcode
 */
struct xml_string {
    const char *ptr;
    size_t len;
};

/** Macro for static initializers */
#define XML_STRING(str) { str, sizeof(str)-1 }

void xml_string_attach_const(struct xml_string *str, const char *text);
void xml_string_attachn_const(struct xml_string *str, const char *text, size_t len);
bool xml_string_is_empty(const struct xml_string *str);
bool xml_string_is_true(const struct xml_string *str);
int xml_string_compare(const struct xml_string *a, const struct xml_string *b);
int xml_string_compare_const(const struct xml_string *a, const char *c);
const char *xml_string_data(struct xml_string *str);
size_t xml_string_length(struct xml_string *str);
int xml_string_html_decode(const struct xml_string *str, char *dest, size_t len);
bool xml_string_contains_html_escaping(const struct xml_string *str);

#endif /* end of include guard: XML_STRING_H_SPBSGDIA */

