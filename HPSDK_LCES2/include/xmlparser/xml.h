/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef XML_H_I5TBR1KE
#define XML_H_I5TBR1KE

#include <stddef.h>
#include <xmlparser/xml_config.h>
#include <xmlparser/xml_string.h>

enum xml_node_type {
    XML_NODE_NONE = 0,
    XML_NODE_ELEMENT,
    XML_NODE_ATTRIBUTE,
    /* internal types */
    XML_NODE_TAG,
    XML_NODE_EMPTY_TAG,
    XML_NODE_CLOSING_TAG
};

struct xml_node {
    enum xml_node_type type;
    struct xml_string nsprefix;
    struct xml_string name;
    struct xml_string content;
    struct xml_doc *doc;
    struct xml_node *child, *sibling, *parent;
    int nsidx; /* namespace this tag belongs to */
    int xmlns; /* default namespace for this scops */
    int empty; /* content is empty */
#ifdef XML_ENABLE_DEBUG_INFO
    int line;
    int col;
    int alloc_num; /* node allocation number */
#endif
};

struct xml_ns_entry {
    struct xml_string prefix;
    struct xml_string uri;
};

struct xml_doc {
#ifdef XML_ENABLE_FILE_SUPPORT
    const char *buf;
    size_t size;
#endif /* XML_ENABLE_FILE_SUPPORT */
    struct xml_node *root;
    struct xml_ns_entry ns_table[XML_MAX_NAMESPACES];
    unsigned int ns_entries;
};

void xml_doc_init(struct xml_doc *doc);
void xml_doc_free(struct xml_doc *doc);

struct xml_node *xml_node_new(struct xml_doc *doc, enum xml_node_type type, int xmlns);
void xml_node_delete(struct xml_node *n);
int xml_parse(const char *buf, size_t len, struct xml_doc *doc);
#ifdef XML_ENABLE_FILE_SUPPORT
int xml_load(const char *filename, struct xml_doc *doc);
#endif /* XML_ENABLE_FILE_SUPPORT */

/* xml node getters */
struct xml_string *xml_node_namespace(struct xml_node *n);
struct xml_string *xml_node_name(struct xml_node *n);
struct xml_string *xml_node_content(struct xml_node *n);
/* xml node navigation */
bool xml_node_has_children(struct xml_node *n);
bool xml_node_has_attributes(struct xml_node *n);
/* generic node handling */
struct xml_node *xml_node_first_child(struct xml_node *n);
struct xml_node *xml_node_next_sibling(struct xml_node *n);
/* element handling */
size_t xml_node_element_count(struct xml_node *n);
struct xml_node *xml_node_first_element(struct xml_node *n);
struct xml_node *xml_node_next_element(struct xml_node *n);
struct xml_node *xml_node_get_element_by_name(struct xml_node *n, const char *name);
struct xml_node *xml_node_get_element_by_name_ns(struct xml_node *n, const char *name, const char *ns);
#define xml_foreach_element(n, p) for (n = xml_node_first_element(p); n != NULL; n = xml_node_next_element(n))
/* attribute handling */
size_t xml_node_attribute_count(struct xml_node *n);
struct xml_node *xml_node_first_attribute(struct xml_node *n);
struct xml_node *xml_node_next_attribute(struct xml_node *n);
struct xml_node *xml_node_get_attribute_by_name(struct xml_node *n, const char *name);
struct xml_node *xml_node_get_attribute_by_name_ns(struct xml_node *n, const char *name, const char *ns);
#define xml_foreach_attribute(n, p) for (n = xml_node_first_attribute(p); n != NULL; n = xml_node_next_attribute(n))

#endif /* end of include guard: XML_H_I5TBR1KE */

