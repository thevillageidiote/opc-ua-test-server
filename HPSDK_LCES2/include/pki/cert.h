/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UA_CERT_H_
#define _UA_CERT_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <crypto/key.h>
#include <pki/config.h>
#include <platform/memory.h>
#include <uabase/bytestring.h>

/**
 * @addtogroup pki_cert
 * @{
 */

/**
 * @defgroup certificate_validation_flags Certificate Validation Flags
 * Bitmask values for controlling the verification process of @ref pki_cert_verify.
 * @{
 */
/** Ignore certificate time errors. */
#define PKI_CERT_IGNORE_TIMEINVALID                 0x0001
/** Ignore certificate time errors in issuer certificates. */
#define PKI_CERT_IGNORE_ISSUERTIMEINVALID           0x0002
/** Ignore missing certificate crl. */
#define PKI_CERT_IGNORE_REVOCATIONUNKNOWN           0x0004
/** Ignore missing issuer certificate crl. */
#define PKI_CERT_IGNORE_ISSUERREVOCATIONUNKNOWN     0x0008
/** Ignore if a cert is untrusted. */
#define PKI_CERT_IGNORE_UNTRUSTED                   0x0010
/** Do not stop at the first error. */
#define PKI_CERT_CHECK_COMPLETE_CHAIN               0x0020
/** Disable CRL check. */
#define PKI_CERT_DISABLE_CRL_CHECK                  0x0040
/* @} defgroup */

/** List of trust list elements (trusted|issuers&certs|crls). */
struct pki_cert_trust_list {
    uint32_t num_elements;          /**< Number of elements in the list. */
    size_t *element_lengths;        /**< Array containing the lenght of each element_data. */
    unsigned char **element_datas;  /**< Array of addresses of the trust list elements. */
};

/** Certificate verification result. */
struct pki_cert_verification_result {
    int chain_depth;       /**< Position of the certificate in the chain. */
    uint32_t status;       /**< Verification status of the certificate. */
    int backend_code;      /**< Backend specific validation result code. */
    bool suppressed;       /**< True, if verification status was suppressed. */
};

/** X509 certificate handle. */
typedef void* pki_cert;

/** Holds all information about a certificate issuer or subject. */
struct pki_cert_identity
{
    char *organization;         /**< Name of the organization. */
    char *organization_unit;    /**< Name of the oranizational unit. */
    char *locality;             /**< Name of the locality. */
    char *state;                /**< Name of the state. */
    char *country;              /**< Two letter county code (ISO 3166-1). */
    char *common_name;          /**< The Common Name (i.e. OPC UA Application Name). */
    char *domain_component;     /**< The Domain Component (DC) of the certificate. */
};

/** Release all memory referenced by a pki_cert_identitiy structure. */
static inline void pki_cert_identity_clear(struct pki_cert_identity *id)
{
    if (id->organization) ua_free(id->organization);
    if (id->organization_unit) ua_free(id->organization_unit);
    if (id->locality) ua_free(id->locality);
    if (id->state) ua_free(id->state);
    if (id->country) ua_free(id->country);
    if (id->common_name) ua_free(id->common_name);
    if (id->domain_component) ua_free(id->domain_component);
    ua_memset(id, 0, sizeof(struct pki_cert_identity));
}

/** Holds all additional OPC UA relevant information of a certificate. */
struct pki_cert_info
{
    char   *uri;             /**< The URI of the OPC UA application. */
    char   *ip;              /**< Comma separated list of IP addresses */
    char   *dns;             /**< Comma separated list of hostnames / DNS names */
    char   *email;           /**< An email address. */
    time_t  validity_begin;  /**< Start of the validity period. */
    time_t  validity_end;    /**< End of the validity period. */
    char   *serial;          /**< The serial number of the certificate as hex octets */
};

/** Release all memory referenced by a pki_cert_info structure. */
static inline void pki_cert_info_clear(struct pki_cert_info *info)
{
    if (info->uri) ua_free(info->uri);
    if (info->ip) ua_free(info->ip);
    if (info->dns) ua_free(info->dns);
    if (info->email) ua_free(info->email);
    if (info->serial) ua_free(info->serial);
    ua_memset(info, 0, sizeof(struct pki_cert_info));
}

# include <pki/cert_ex.h>

/** Decode a single certificate from DER format.
 * @param der    Buffer containing a DER encoded certificate.
 * @param derlen Length of one encoded certificate in the buffer.
 * @param cert   Handle to the decoded certificate.
 * @return Error Code
 */
int pki_cert_from_der(
    const unsigned char *der,
    size_t               derlen,
    pki_cert            *cert
);

/** Check if certificate is valid (time, signature etc.).
 * @param cert_len Length in bytes of cert_data.
 * @param cert_data Array containing the DER encoded certificate to be verified.
 * @param verification_flags Bit mask of verification control flags (see @certificate_validation_flags).
 * @param trusted_certs Set of trusted application instance certificate and issuer certificates.
 * @param trusted_crls Set of trusted issuer CRLs.
 * @param issuer_certs Set of untrusted issuer certificates for chain completion.
 * @param issuer_crls Set of untrusted issuer CRLs.
 * @param cert_ok General verification result on return.
 * @param num_results Size of array results; number of used elements on return.
 * @param results Preallocated array for storing validation results.
 * @return Error Code; if bad, out parameters have undefined values.
 */
int pki_cert_verify(
    size_t                               cert_len,
    unsigned char                       *cert_data,
    uint32_t                             verification_flags,
    struct pki_cert_trust_list          *trusted_certs,
    struct pki_cert_trust_list          *trusted_crls,
    struct pki_cert_trust_list          *issuer_certs,
    struct pki_cert_trust_list          *issuer_crls,
    bool                                *cert_ok,
    unsigned int                        *num_results,
    struct pki_cert_verification_result *results
    );

/** Get handle to public key of a certificate.
 * The key becomes invalid when the certificate is released.
 * @param cert Handle of the certificate.
 * @param key  Pointer to the key handle memory.
 * @return Error Code.
 */
int pki_cert_get_public_key(
    pki_cert           cert,
    struct crypto_key *key
);

/** Get issuer or subject information from a certificate.
 * @param cert The cert to use.
 * @param issuer Set to 0 to get subject information, else issuer information.
 * @param cert_id Pointer to structure for storing the identity information. Contents must be freed.
 */
int pki_cert_get_identity(pki_cert                  cert,
                          unsigned char             issuer,
                          struct pki_cert_identity *cert_id);

/** Get basic X509 information from a certificate.
 * @param cert The cert to extract the data from.
 * @param cert_info Pointer to structure for storing the certificate information. Contents must be freed.
 */
int pki_cert_get_info(pki_cert              cert,
                      struct pki_cert_info *cert_info);


/** Release handle to certificate.
 * @param cert Certificate handle to release.
 */
void pki_cert_delete(
    pki_cert *cert
);

/** Get start positions of pnum_certs certificates.
 * The array certs should be long enough to hold the number of expected certificates.
 * @param chain Buffer containing one or more encoded certificates.
 * @param chain_size Number of bytes in chain.
 * @param pnum_certs Number of certs elements before call, number of used certs after call.
 * @param cert_lengths Array of sizes to store the lengths of the chain elements.
 * @param cert_datas Array of pointers to store the starting positions of the chain elements.
 * @return Error Code.
 */
int pki_cert_split_chain(
    unsigned char *chain,
    size_t chain_size,
    uint32_t *pnum_certs,
    size_t *cert_lengths,
    unsigned char **cert_datas);

/** @} addtogroup pki_cert */

#endif /* _UA_CERT_H_ */

