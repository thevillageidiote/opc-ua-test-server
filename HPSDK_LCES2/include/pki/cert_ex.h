/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UA_CERT_EX_H_
#define _UA_CERT_EX_H_

#include <stdlib.h>
#include <stdint.h>
#include <crypto/crypto.h>
#include <crypto/key.h>

/**
* @addtogroup pki_cert
* @{
*/

/** Identifiers for supported X509 extenstions */
enum pki_cert_extension
{
    pki_cert_extension_subject_alt_name         = 0,
    pki_cert_extension_basic_constraints        = 1,
    pki_cert_extension_netscape_comment         = 2,
    pki_cert_extension_subject_key_identifier   = 3,
    pki_cert_extension_authority_key_identifier = 4,
    pki_cert_extension_key_usage                = 5,
    pki_cert_extension_extended_key_usage       = 6
};

/** Creates a new cert based on given certificate data and returns it DER encoded.
 * @param cert_info UA Application information.
 * @param sub The identity of the cert owner.
 * @param sub_key The key pair of the cert. The public key part will be stored in the cert.
 * @param iss The identity of the cert iss.
 * @param iss_key The key pair of the cert iss. This is needed to sign the cert.
 * @param sign_alg Set the algorithm to be used for signing the new certificate.
 * @param der Buffer to encode the certificate into.
 * @param derlen Length of the destination buffer; used size on return.
 */
int pki_cert_create_der(const struct pki_cert_info     *cert_info,
                        const struct pki_cert_identity *sub,
                        const struct crypto_key        *sub_key,
                        const struct pki_cert_identity *iss,
                        const struct crypto_key        *iss_key,
                        enum crypto_hash_alg            sign_alg,
                        unsigned char                  *der,
                        size_t                         *derlen);

/** Creates a new cert based on given certificate data and returns it in internal format.
 * @param cert_info UA Application information.
 * @param sub The identity of the cert owner.
 * @param sub_key The key pair of the cert. The public key part will be stored in the cert.
 * @param iss The identity of the cert iss.
 * @param iss_key The key pair of the cert iss. This is needed to sign the cert.
 * @param sign_alg Set the algorithm to be used for signing the new certificate.
 * @param cert The created certificate in internal format.
 */
int pki_cert_create(const struct pki_cert_info     *cert_info,
                    const struct pki_cert_identity *sub,
                    const struct crypto_key        *sub_key,
                    const struct pki_cert_identity *iss,
                    const struct crypto_key        *iss_key,
                    enum crypto_hash_alg            sign_alg,
                    pki_cert                       *cert);

/** Creates a new certificate signing request based on given certificate data and returns it DER encoded.
 * @param cert_info UA Application information.
 * @param sub The identity of the cert owner.
 * @param sub_key The key pair of the cert. The public key part will be stored in the cert.
 * @param sign_alg Set the algorithm to be used for signing the new certificate.
 * @param der Buffer to encode the certificate into.
 * @param derlen Length of the destination buffer; used size on return.
 */
int pki_cert_create_csr_der(const struct pki_cert_info     *cert_info,
                            const struct pki_cert_identity *sub,
                            const struct crypto_key        *sub_key,
                            enum crypto_hash_alg            sign_alg,
                            unsigned char                  *der,
                            size_t                         *derlen);

/** Get extension from cert.
 * @param cert The cert to use.
 * @param ext The cert extension to get.
 * @param val Place to store the value of the specified extension.
 * @param vallen Length of the value buffer.
 */
int pki_cert_get_extension(pki_cert                cert,
                           enum pki_cert_extension ext,
                           unsigned char*          val,
                           size_t                  vallen);

/** @} addtogroup pki_cert */

#endif /* _UA_CERT_EX_H_ */

