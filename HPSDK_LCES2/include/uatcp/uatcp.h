/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef _UA_TCP_H_
#define _UA_TCP_H_

#define UA_TCP_CCV
#define UA_TCP_API

#include <stdint.h>
#include <stdbool.h>
#include <network/uanetwork.h>
#include <common/errors.h>

#define UA_TCP_TRANSPORTPROFILE_URI "http://opcfoundation.org/UA-Profile/Transport/uatcp-uasc-uabinary"

#ifdef UA_USE_SYNCHRONIZATION
/* placeholder for thread synchronization */
#define UA_TCP_LOCK void*
#define ua_tcp_lock(xConn)
#define ua_tcp_unlock(xConn)
#else /* UA_USE_SYNCHRONIZATION */
#define ua_tcp_lock(xConn)
#define ua_tcp_unlock(xConn)
#endif /* UA_USE_SYNCHRONIZATION */

/** Trace function signature */
typedef int ua_tcp_trace_hook(unsigned char a_uType, const char *a_sFormat, ...);

/** Register trace hook. */
void ua_tcp_set_trace_handler(ua_tcp_trace_hook *pfTrace);

/** Prototypes */
struct ua_tcp_connection;
struct ua_tcp_message;

/** The internal state of an ua tcp connection object. */
enum ua_tcp_connection_state
{
    ua_tcp_connection_state_Unused                 = 0,
    ua_tcp_connection_state_WaitingForHELHeader    = 1,
    ua_tcp_connection_state_WaitingForHELBody      = 2,
    ua_tcp_connection_state_SayingHello            = 3,
    ua_tcp_connection_state_WaitingForACK          = 4,
    ua_tcp_connection_state_WaitingForHeader       = 5,
    ua_tcp_connection_state_WaitingForBody         = 6,
    ua_tcp_connection_state_Connected              = 7,
    ua_tcp_connection_state_ShutDown               = 8,
    ua_tcp_connection_state_Listen                 = 9
};

/** Connection event types. */
#define UA_TCP_EVENT_NONE                0x00000000
#define UA_TCP_EVENT_ACCEPT              0x00000001
#define UA_TCP_EVENT_CONNECT             0x00000002
#define UA_TCP_EVENT_CLOSE               0x00000004
#define UA_TCP_EVENT_GET_BUFFER          0x00000008

/** Signature of a function called upon connection related events. */
typedef int (UA_TCP_CCV ua_tcp_event_callback)(struct ua_tcp_connection   *pConnection,
                                               unsigned int                uEventType,
                                               int                         iResult,
                                               void                       *pvCallbackData);

/** Represents an OPC UA TCP protocol connection. */
struct ua_tcp_connection
{
    struct ua_socket             tSocket;         /**< The socket to which the connection is assigned to. */
    struct ua_tcp_message       *pSendMessage;    /**< The current message to be sent. */
    struct ua_tcp_message       *pRecvMessage;    /**< The current message being received. */
    enum ua_tcp_connection_state eState;          /**< The state in which the connection is currently in. */
    unsigned char                bIsAccepted;     /**< True, if the connection was established by accepting a connect request. */
    uint32_t                     uRequestId;      /**< The highest request id used. */
    uint32_t                     uSeqNumSend;     /**< The highest sequence number used for sending. */
    uint32_t                     uSeqNumRecv;     /**< The highest sequence number received. */
    bool                         bSeqNumRecvValid;/**< The highest sequence number received is valid. */
    ua_tcp_event_callback       *pfCallback;      /**< Function for reporting connection events. */
    void                        *pvCallbackData;  /**< User data passed to callback function. */
    /* incoming messages */
    uint32_t                     nRecvMsgLen;     /**< Maximum message size for unencrypted body. */
    uint32_t                     nRecvBuffer;     /**< Largest chunk peer will send. */
    uint32_t                     nRecvChunkCount; /**< Max number of received chunks per message. */
    /* outgoing messages */
    uint32_t                     nSendMsgLen;     /**< Maximum message size for unencrypted body. */
    uint32_t                     nSendBuffer;     /**< Largest chunk allowed to send to peer. */
    uint32_t                     nSendChunkCount; /**< Max number of chunks per message. */
    int                          prio;            /**< connection priority. */
#ifdef UA_USE_SYNCHRONIZATION
    UA_TCP_CS                    Lock;
#endif /* UA_USE_SYNCHRONIZATION */
};

/** Signature of the function called upon message related events.
 * @param pConnection The connection the message belongs to.
 * @param pMessage The message object of this operation.
 * @param iResult A common result code.
 * @param uStatusCode A specific UA status code for the error if available. Undefined value if iResult is UA_EGOOD.
 */
typedef int (UA_TCP_CCV ua_tcp_message_callback)(struct ua_tcp_connection   *pConnection,
                                                 struct ua_tcp_message      *pMessage,
                                                 int                         iResult,
                                                 uint32_t                    uStatusCode);

/** Helper function to initialize the fields of a message context. */
#define ua_tcp_message_init(xMessage, xConnection, xBuffer, xCallback, xCallbackData) \
    (xMessage)->pBuffer = xBuffer; \
    (xMessage)->pConnection = xConnection; \
    (xMessage)->pfCallback = xCallback; \
    (xMessage)->pvCallbackData = xCallbackData; \
    (xMessage)->x_uRequestId = 0;

/** Context of an overlapped send/receive operation of an UA TCP message. */
struct ua_tcp_message
{
    /* set before starting operation (see ua_tcp_message_init) */

    struct ua_buffer          *pBuffer;        /**< Buffer chain head for sending and receiving the message. */
    struct ua_tcp_connection  *pConnection;    /**< The connection to which the message belongs. */
    ua_tcp_message_callback   *pfCallback;     /**< Function to be called when message operation is complete. */
    void                      *pvCallbackData; /**< User data passed into callback. */

    /* Implementation specific data - do not modify */

    struct ua_socket_op        x_tSockOp;      /**< Socket operation used for transmitting the data. */
    uint32_t                   x_uRequestId;   /**< Request ID set by the implementation. */
};

UA_TCP_API
int UA_TCP_CCV ua_tcp_connection_init(struct ua_net_base         *pNetBase,
                                      struct ua_tcp_connection   *pConnection,
                                      ua_tcp_event_callback      *pfCallback,
                                      void                       *pvCallbackData);

/** Reserved space for asymmetric security message (12 stands for 3*4 byte string length fields). */
#define UA_TCP_ASYMMETRIC_HEADER_LENGTH(xPolUriLength, xCertLength, xCertThumbLength)\
    (UA_TCP_MESSAGE_HEADER_LENGTH + 12 + (xPolUriLength>0?xPolUriLength:0) + (xCertLength>0?xCertLength:0) + (xCertThumbLength>0?xCertThumbLength:0) + UA_TCP_SEQUENCE_HEADER_LENGTH)

/** Contains the header elements of an open secure channel message. */
struct ua_tcp_opn_header {
    uint32_t       SecChannelId; /**< The secure channel id. */
    int32_t        PolUriLen;    /**< Length of the security policy URI string. */
    unsigned char *PolUri;       /**< Non-zero-terminated security policy URI. */
    int32_t        CertLen;      /**< Length of the Cert array. */
    unsigned char *Cert;         /**< Non-zero-terminated peer certificate. */
    int32_t        CertThumbLen; /**< Length of the certificate thumbprint. */
    unsigned char *CertThumb;    /**< Non-zero-terminated thumbprint of own certificate. */
};

UA_TCP_API
int UA_TCP_CCV ua_tcp_parse_opn_header(struct ua_tcp_message      *pMessage,
                                       struct ua_tcp_opn_header   *pMessageHeader);

UA_TCP_API
int UA_TCP_CCV ua_tcp_write_opn_header(struct ua_tcp_connection   *pConnection,
                                       struct ua_tcp_message      *pMessage,
                                       uint32_t                    uSecChannelId,
                                       int                         iPolUriLength,
                                       const unsigned char        *pchPolUri,
                                       int                         iCertLength,
                                       const unsigned char        *pchCert,
                                       int                         iCertThumbLength,
                                       const unsigned char        *pchCertThumb,
                                       uint32_t                    uRequestId);

/** Message header length consisting of MessageType/Final (i.e. MSFG), chunk length and secure channel id. (3*8 byte) */
#define UA_TCP_MESSAGE_HEADER_LENGTH 12

/** Symmetric security header length consisting of TokenId (4 byte) */
#define UA_TCP_SYMMETRIC_SECURITY_HEADER 4

/** Sequence header length consisting of sequence number and request id. (2*8 byte) */
#define UA_TCP_SEQUENCE_HEADER_LENGTH 8

/** Reserved space for symmetic security messages. (24 byte) */
#define UA_TCP_SYMMETRIC_HEADER_LENGTH (UA_TCP_MESSAGE_HEADER_LENGTH + UA_TCP_SYMMETRIC_SECURITY_HEADER + UA_TCP_SEQUENCE_HEADER_LENGTH)

/** Unencrypted header part of a symmetric encrypted message. (16 byte) */
#define UA_TCP_SYMMETRIC_HEADER_UNENCRYPTED_LENGTH (UA_TCP_MESSAGE_HEADER_LENGTH + UA_TCP_SYMMETRIC_SECURITY_HEADER)

/** Contains the header elements standard message. */
struct ua_tcp_msg_header {
    uint32_t SecChannelId; /**< The secure channel id. */
    uint32_t TokenId;      /**< The security token id. */
};

UA_TCP_API
int UA_TCP_CCV ua_tcp_parse_msg_header(struct ua_tcp_message      *pMessage,
                                       struct ua_tcp_msg_header   *pMessageHeader);

UA_TCP_API
int UA_TCP_CCV ua_tcp_write_msg_header(struct ua_tcp_connection   *pConnection,
                                       struct ua_tcp_message      *pMessage,
                                       uint32_t                    uSecChannelId,
                                       uint32_t                    uTokenId,
                                       uint32_t                   *puRequestId);

/** Contains the header elements of the sequence header. */
struct ua_tcp_seq_header {
    uint32_t SeqNo;        /**< The sequence number. */
    uint32_t RequestId;    /**< The request id. */
};

UA_TCP_API
int UA_TCP_CCV ua_tcp_parse_seq_header(struct ua_tcp_message      *pMessage,
                                       struct ua_tcp_seq_header   *pMessageHeader);

UA_TCP_API
int UA_TCP_CCV ua_tcp_update_recv_seqnum(struct ua_tcp_connection *pConnection,
                                         uint32_t                  uSeqNum);

/** Required space for error message */
#define UA_TCP_ERROR_MESSAGE_LENGTH(xReasonLen) (16 + xReasonLen)

/** Contains the elements of an error message. */
struct ua_tcp_err_message {
    uint32_t       Error;     /**< Numeric code of the reported error. */
    int32_t        ReasonLen; /**< Length of the error reason string. */
    unsigned char *Reason;    /**< Non-zero-terminated string containing the reason for the error. */
};

UA_TCP_API
int UA_TCP_CCV ua_tcp_parse_err_message(struct ua_tcp_message      *pMessage,
                                        struct ua_tcp_err_message  *pErrorMessage);

UA_TCP_API
int UA_TCP_CCV ua_tcp_write_err_message(struct ua_tcp_connection   *pConnection,
                                        struct ua_tcp_message      *pMessage,
                                        uint32_t                    uError,
                                        int32_t                     iReasonLen,
                                        const unsigned char        *sReason);

/** Required space for error message */
#define UA_TCP_CLO_MESSAGE_LENGTH 12

UA_TCP_API
int UA_TCP_CCV ua_tcp_parse_clo_message(struct ua_tcp_message      *pMessage,
                                        uint32_t                   *puSecChannelId);

UA_TCP_API
int UA_TCP_CCV ua_tcp_write_clo_message(struct ua_tcp_connection   *pConnection,
                                        struct ua_tcp_message      *pMessage,
                                        uint32_t                    uSecChannelId);

UA_TCP_API
int UA_TCP_CCV ua_tcp_listen(struct ua_tcp_connection   *pConnection,
                             struct ua_net_addr         *pLocalAddress);

UA_TCP_API
int UA_TCP_CCV ua_tcp_accept(struct ua_tcp_connection   *pListenConnection,
                             struct ua_tcp_connection   *pPeerConnection,
                             struct ua_tcp_message      *pRecvMessage,
                             struct ua_tcp_message      *pSendMessage);

UA_TCP_API
int UA_TCP_CCV ua_tcp_connect(struct ua_tcp_connection   *pConnection,
                              struct ua_net_addr         *pServerAddress,
                              unsigned int                uRcvBufLength,
                              unsigned int                uSndBufLength,
                              unsigned int                uMaxMessageLength,
                              int                         iHostnameLength,
                              const char                 *pchHostname,
                              struct ua_tcp_message      *pSendMessage,
                              struct ua_tcp_message      *pRecvMessage);

UA_TCP_API
int UA_TCP_CCV ua_tcp_send_message(struct ua_tcp_connection   *pConnection,
                                   struct ua_tcp_message      *pMessage);

UA_TCP_API
int UA_TCP_CCV ua_tcp_receive_message(struct ua_tcp_connection   *pConnection,
                                      struct ua_tcp_message      *pMessage);

UA_TCP_API
int UA_TCP_CCV ua_tcp_connection_close(struct ua_tcp_connection   *pConnection,
                                       bool                        bShutdown);

#endif /* _UA_TCP_H_ */
