/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UA_CRYPTO_H_
#define _UA_CRYPTO_H_

/**
 * @defgroup crypto crypto
 * @{
 */

#include <common/errors.h>
#include <crypto/key.h>
#include <stdlib.h> /* for size_t */
#include <stdint.h>

/** Null operation for all algorithms. */
#define crypto_alg_null 0

/** Enumeration of symmetric encryption algorithms. */
enum crypto_sym_alg {
    crypto_alg_aes_cbc      = 1, /**< AES encryption with cipher block chaining */
};

/** Enumeration of asymmetric encryption algorithms. */
enum crypto_asym_alg {
    crypto_alg_rsa          = 1, /**< RSA encryption */
};

/** Enumeration of message signing algorithms. */
enum crypto_sign_alg {
    crypto_alg_hmac_sha1    = 1, /**< hash-base message authentication using 160 bit SHA1 */
    crypto_alg_hmac_sha224  = 2, /**< hash-base message authentication using 224 bit SHA2 */
    crypto_alg_hmac_sha256  = 3, /**< hash-base message authentication using 256 bit SHA2 */
    crypto_alg_hmac_sha384  = 4, /**< hash-base message authentication using 384 bit SHA2 */
    crypto_alg_hmac_sha512  = 5, /**< hash-base message authentication using 512 bit SHA2 */
    crypto_alg_rsa_sha1     = 6, /**< RSA based signature using 160 bit SHA1 */
    crypto_alg_rsa_sha256   = 7  /**< RSA based signature using 256 bit SHA2 */
};

/** Enumeration of hash algorithms. */
enum crypto_hash_alg {
    crypto_alg_sha1         = 1, /**< 160 bit secure hash algorithm (SHA1) */
    crypto_alg_sha224       = 2, /**< 224 bit secure hash algorithm (SHA2) */
    crypto_alg_sha256       = 3, /**< 256 bit secure hash algorithm (SHA2)*/
    crypto_alg_sha384       = 4, /**< 384 bit secure hash algorithm (SHA2)*/
    crypto_alg_sha512       = 5, /**< 512 bit secure hash algorithm (SHA2)*/
};

/** Enumeration of padding algorithms. */
enum crypto_pad_alg {
    crypto_pad_oaep         = 1, /**< Optimal Asymmetric Encryption Padding */
    crypto_pad_pkcs1_v15    = 2, /**< PKCS1 padding v1.5 */
    crypto_pad_pkcs1_oaep   = 3, /**< PKCS1 OAEP */
    crypto_pad_x931         = 4  /**< X9.31 padding */
};

/** Size of AES cipher and plain text blocks in bytes. */
#define CRYPTO_AES_BLOCK_SIZE_BYTES 16

#ifdef HAVE_CRYPTO

/**
 * En-/decrypt buffer content using the requested algorithm.
 * @param alg_id Identifier of the crypto algorithm.
 * @param key    Encrytpion key.
 * @param iv     Initialization vector of size 16 bytes. (updated after use)
 * @param len    Length in bytes of the text to encrypt/decrypt.
 * @param in     Plain text.
 * @param out    Cipher text.
 */
int crypt_sym_encrypt(
    enum crypto_sym_alg  alg_id,
    struct crypto_key   *key,
    unsigned char       *iv,
    size_t               len,
    const unsigned char *in,
    unsigned char       *out
);

/**
 * En-/decrypt buffer content using the requested algorithm.
 * @param alg_id Identifier of the crypto algorithm.
 * @param key    Encrytpion key.
 * @param iv     Initialization vector of size 16 bytes. (updated after use)
 * @param len    Length in bytes of the text to encrypt/decrypt.
 * @param in     Cipher text.
 * @param out    Plain text.
 */
int crypt_sym_decrypt(
    enum crypto_sym_alg  alg_id,
    struct crypto_key   *key,
    unsigned char       *iv,
    size_t               len,
    const unsigned char *in,
    unsigned char       *out
);

/**
 * Sign the given data using HMAC and the given algorithm.
 * @param alg_id  HMAC algorithm to use.
 * @param signkey Key for hashing.
 * @param data    Data to sign.
 * @param datalen Length of the data to sign.
 * @param mac     Destination buffer of size maclen.
 * @param maclen  Size of the destination buffer.
 */
int crypt_sym_sign(
    enum crypto_sign_alg alg_id,
    struct crypto_key   *signkey,
    const unsigned char *data,
    size_t               datalen,
    unsigned char       *mac,
    size_t               maclen);

/**
 * Verify the correctness of the given signature.
 * @param alg_id  HMAC algorithm to use.
 * @param signkey Key for hashing.
 * @param data    Data to sign.
 * @param datalen Length of the data to sign.
 * @param mac     Buffer containing the signature to verify.
 * @param maclen  Length of the signature data to be verified.
 */
int crypt_sym_verify(
    enum crypto_sign_alg alg_id,
    struct crypto_key   *signkey,
    const unsigned char *data,
    size_t               datalen,
    unsigned char       *mac,
    size_t               maclen);

/**
 * Encrypt buffer content using the requested algorithm.
 * @param alg_id Identifier of the encryption algorithm.
 * @param pad_id Identifier of the padding algorithm.
 * @param pubkey The public key.
 * @param ctext  The plain text.
 * @param plen    Length of the plain text.
 * @param ptext  The cipher text.
 * @param clen   Length of the cipher text buffer; used space on return.
 */
int crypt_encrypt(
    enum crypto_asym_alg    alg_id,
    enum crypto_pad_alg     pad_id,
    struct crypto_key      *pubkey,
    const unsigned char    *ptext,
    size_t                  plen,
    unsigned char          *ctext,
    size_t                 *clen
);

/**
 * Decrypt buffer content using the requested algorithm.
 * @param alg_id  Identifier of the requested algorithm.
 * @param pad_id  Identifier of the padding algorithm.
 * @param privkey The private key.
 * @param ctext   The cipher text.
 * @param clen    Length of the cipher text.
 * @param ptext   The plain text.
 * @param plen    Length of the plain text buffer. Used length on return.
 */
int crypt_decrypt(
    enum crypto_asym_alg    alg_id,
    enum crypto_pad_alg     pad_id,
    struct crypto_key      *privkey,
    const unsigned char    *ctext,
    size_t                  clen,
    unsigned char          *ptext,
    size_t                 *plen
);

/**
 * Create cryptographic signature (i.e. RSA SHA1).
 * @param alg_id  Identifier of the requested algorithm.
 * @param privkey The private signing key.
 * @param data    Data to sign.
 * @param datalen Length in bytes of the data to sign.
 * @param md      The resulting message digest.
 * @param mdlen   Length of the buffer to store the message digest.
 */
int crypt_sign(
    enum crypto_sign_alg  alg_id,
    struct crypto_key    *privkey,
    const unsigned char  *data,
    size_t                datalen,
    unsigned char        *md,
    size_t                mdlen
);

    /**
 * Verify cryptographic signature (i.e. RSA SHA1).
 * @param alg_id  Identifier of the requested algorithm.
 * @param verkey  Key containing the public key component.
 * @param data    Signed data.
 * @param len Length in bytes of the signed data.
 * @param md      The message digest.
 * @param mdlen   Length of the buffer with the message digest.
 */
int crypt_verify(
    enum crypto_sign_alg alg_id,
    struct crypto_key   *verkey,
    const unsigned char *data,
    size_t               len,
    unsigned char       *md,
    size_t               mdlen
);

/**
 * Generate a hash value (i.e. SHA1).
 * @param alg_id Identifier of the requested algorithm.
 * @param data   Data to hash.
 * @param len    Length in bytes of the data to hash.
 * @param md     The resulting message digest.
 */
int crypt_hash(
    enum crypto_hash_alg  alg_id,
    const unsigned char  *data,
    size_t                len,
    unsigned char        *md
);

/**
 * Generate P-SHA hash value.
 * @param alg_id    Identifier of the requested algorithm (only sha1 and sha256 mandatory).
 * @param secret    Shared secret data.
 * @param secretlen Length in bytes of the shared secret.
 * @param seed      Label and seed data.
 * @param seedlen   Length in bytes of the label and seed. The max. supported size is 32 bytes.
 * @param out       Buffer of size outlen for storing the created hashes.
 * @param outlen    Length in bytes of the hash value to create. Must be a multiple of 32 (SHA256 length).
 */
int crypt_psha(
    enum crypto_hash_alg  alg_id,
    const unsigned char  *secret,
    size_t                secretlen,
    const unsigned char  *seed,
    size_t                seedlen,
    unsigned char        *out,
    size_t                outlen
);

/**
 * Create a random number of the requested size.
 * @param data  Buffer to fill with random bytes.
 * @param len   Size of the buffer to fill with random bytes.
 */
int crypt_random(
    unsigned char *data,
    size_t         len
);

/**
 * Create a random number of the requested size.
 * @param seed    Data for seeding the random number generator.
 * @param seedlen Length of seed.
 */
int crypt_random_seed(
    const unsigned char *seed,
    size_t               seedlen);

/**
 * Initialize the crypto library.
 * Must be called before any other call to this API.
 * @param seed    Data for seeding the entropy generator.
 * @param seedlen Length of seed.
 * @return Error Code
 */
int crypt_init(
    const unsigned char *seed,
    size_t               seedlen);

/**
 * Clean up resources of the crypto library.
 * @return Error Code
 */
void crypt_clear(void);

#endif /* HAVE_CRYPTO */

/** @} */

#endif /* _UA_CRYPTO_H_ */
