/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef TRACE_H_IEXXSWB9
#define TRACE_H_IEXXSWB9

#include <trace/trace_config.h>

#ifdef __GNUC__
/* fmt: index of the format string (starting from 1)
   va:  index of the first variable argument (starting from 1) */
# define TRACE_FORMAT_ARGUMENT(fmt, va) __attribute__ ((format (printf, fmt, va)))
#else /* __GNUC__ */
# define TRACE_FORMAT_ARGUMENT(fmt, va)
#endif /* __GNUC__ */

/**
 * @defgroup trace trace
 * @brief Writing trace messages.
 *
 * The SDK has multiple trace backends implemented, which can be selected using the
 * cmake switch TRACE_BACKEND. Each of them writes the trace to a different
 * location. To disable the trace completely the the cmake switch TRACE_ENABLED
 * must be disabled.
 *
 * Each trace message is written with a level to indicate the serverity of the message
 * and a facility to assign the message to a component.
 * Depending of the trace backend the trace messages may be prefixed with timestamps,
 * the output can be colored, etc. See \ref platform_trace for more general information
 * about the different trace backends.
 *
 * @{
 */

/* trace levels/categories */
/** Identifier for debug trace level: also logs internal variables/states, only useful for developers */
#define TRACE_LEVEL_DEBUG       1
/** Identifier for data trace level: logs process data (e.g. read/written values) */
#define TRACE_LEVEL_DATA        2
/** Identifier for info trace level: informational (e.g. app started, shutdown, connect, disconnect, subcription created, ...) */
#define TRACE_LEVEL_INFO        4
/** Identifier for function enter trace level */
#define TRACE_LEVEL_FUNC_ENTER  8
/** Identifier for function leave trace level */
#define TRACE_LEVEL_FUNC_LEAVE  16
/** Identifier for function trace level */
#define TRACE_LEVEL_FUNC        24
/** Identifier for notice trace level: normal but significant condition, e.g. authentication denied */
#define TRACE_LEVEL_NOTICE      32
/** Identifier for warning trace level. e.g. certificate may expire soon, only 1MB of memory left, etc. */
#define TRACE_LEVEL_WARNING     64
/** Identifier for error trace level: error conditions, e.g. file access denied, out of memory, etc. */
#define TRACE_LEVEL_ERROR       128
/** Identifier for insane trace level, produces a lot of output! This is used for cyclic events. */
#define TRACE_LEVEL_INSANE      256
/** Combination of all trace levels */
#define TRACE_LEVEL_ALL         511

/* trace facilities */
/** Identifier for platform facility */
#define TRACE_FAC_PLATFORM      1
/** Identifier for network facility */
#define TRACE_FAC_NETWORK       2
/** Identifier for crypto facility */
#define TRACE_FAC_CRYPTO        4
/** Identifier for IPC facility */
#define TRACE_FAC_IPC           8
/** Identifier for base facility */
#define TRACE_FAC_BASE          16
/** Identifier for memory facility */
#define TRACE_FAC_MEMORY        32
/** Identifier for UATCP facility */
#define TRACE_FAC_UATCP         64
/** Identifier for encoder facility */
#define TRACE_FAC_ENCODER       128
/** Identifier for session facility */
#define TRACE_FAC_SESSION       256
/** Identifier for provider facility */
#define TRACE_FAC_PROVIDER      512
/** Identifier for application facility */
#define TRACE_FAC_APPLICATION   1024
/** Identifier for addressspace facility */
#define TRACE_FAC_ADDRSPACE     2048
/** Identifier for timer facility */
#define TRACE_FAC_TIMER         4096
/** Identifier for timer facility */
#define TRACE_FAC_PKI           8192
/** Combination of all facilities */
#define TRACE_FAC_ALL           16383

#ifndef HAVE_TRACE
# define trace_openlog(level, facility) 0
# define trace_log(level, facility, format, ...)
# define trace_closelog()
# define trace_enabled(level) 0
# define TRACE_DEBUG(facility, format, ...)
# define TRACE_DATA(facility, format, ...)
# define TRACE_INFO(facility, format, ...)
# define TRACE_NOTICE(facility, format, ...)
# define TRACE_WARNING(facility, format, ...)
# define TRACE_ERROR(facility, format, ...)
# define TRACE_INSANE(facility, format, ...)
# define TRACE_ENTER(facility, funcname)
# define TRACE_RETURN(facility, funcname)
# define TRACE_RETURNERR(facility, funcname, result)
# define TRACE_ENTERD(facility, funcname)
# define TRACE_RETURND(facility, funcname)
# define TRACE_RETURNERRD(facility, funcname, result)
#else

/**
 * @brief Initialize the trace backend.
 * @param level_mask Bitwise OR of trace levels to trace.
 * @param facility_mask Bitwise OR of trace facilities to trace.
 * @return Zero on success or errorcode on failure
 */
int trace_openlog(int level_mask, int facility_mask);

/**
 * @brief Write to trace.
 * @param level Level to trace to.
 * @param facility Facility to trace to.
 * @param format Format specifier like for printf().
 */
void trace_log(int level, int facility, const char *format, ...) TRACE_FORMAT_ARGUMENT(3, 4);

/**
 * @brief Close the trace backend
 */
void trace_closelog(void);

/**
 * @brief Change the trace level and facility
 * @param level_mask New trace level mask.
 * @param facility_mask New trace facility mask.
 * @return Zero on success or errorcode on failure
 */
int trace_change_trace_level(int level_mask, int facility_mask);

/**
 * @brief Test if a certain trace level is enabled
 * @param level Trace level to test.
 * @return Zero if the specified trace level is disabled
 */
int trace_enabled(int level);

/**
 * @brief Add an indent to all further traces messages.
 */
void trace_indent(void);

/**
 * @brief Remove an indent from all furhter trace messages
 */
void trace_unindent(void);

/** Convenience define for tracing with debug level */
# define TRACE_DEBUG(facility, format, ...)   trace_log(TRACE_LEVEL_DEBUG, facility, format, ##__VA_ARGS__)
/** Convenience define for tracing with data level */
# define TRACE_DATA(facility, format, ...)    trace_log(TRACE_LEVEL_DATA, facility, format, ##__VA_ARGS__)
/** Convenience define for tracing with info level */
# define TRACE_INFO(facility, format, ...)    trace_log(TRACE_LEVEL_INFO, facility, format, ##__VA_ARGS__)
/** Convenience define for tracing with notic level */
# define TRACE_NOTICE(facility, format, ...)  trace_log(TRACE_LEVEL_NOTICE, facility, format, ##__VA_ARGS__)
/** Convenience define for tracing with warning level */
# define TRACE_WARNING(facility, format, ...) trace_log(TRACE_LEVEL_WARNING, facility, format, ##__VA_ARGS__)
/** Convenience define for tracing with error level */
# define TRACE_ERROR(facility, format, ...)   trace_log(TRACE_LEVEL_ERROR, facility, format, ##__VA_ARGS__)
/** Convenience define for tracing with insane level */
# define TRACE_INSANE(facility, format, ...)    trace_log(TRACE_LEVEL_INSANE, facility, format, ##__VA_ARGS__)
/** Trace with function enter level, adds an indent */
# define TRACE_ENTER(facility, funcname)      trace_log(TRACE_LEVEL_FUNC_ENTER, facility, "> %s\n", funcname); trace_indent()
/** Trace with function leave level, removes an indent */
# define TRACE_RETURN(facility, funcname)     trace_unindent(); trace_log(TRACE_LEVEL_FUNC_LEAVE, facility, "< %s\n", funcname)
/** Trace with function leave level, removes an indent and prints errorcode */
# define TRACE_RETURNERR(facility, funcname, result)  trace_unindent(); trace_log(TRACE_LEVEL_FUNC_LEAVE, facility, "< %s - return 0x%08X\n", funcname, result)
/** Trace function enter with insane level, adds an indent */
# define TRACE_ENTERD(facility, funcname)      trace_log(TRACE_LEVEL_INSANE, facility, "> %s\n", funcname); trace_indent()
/** Trace function leave with insane level, removes an indent */
# define TRACE_RETURND(facility, funcname)     trace_unindent(); trace_log(TRACE_LEVEL_INSANE, facility, "< %s\n", funcname)
/** Trace function leave with insane level, removes an indent and prints errorcode */
# define TRACE_RETURNERRD(facility, funcname, result)  trace_unindent(); trace_log(TRACE_LEVEL_INSANE, facility, "< %s - return 0x%08X\n", funcname, result)

#endif

/** @} */

#endif /* end of include guard: TRACE_H_IEXXSWB9 */

