/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef SECONV_H
#define SECONV_H 1

#include <stdbool.h>
#include <crypto/crypto.h>
#include <uabase/buffer.h>
#include <uabase/bytestring.h>
#include <uabase/structure/channelsecuritytoken.h>
#include <uabase/structure/messagesecuritymode.h>

/** Enumerates defined security policies. */
enum ua_security_policy_id {
    UA_SECURITY_POLICY_NONE           = 0,
    UA_SECURITY_POLICY_BASIC128RSA15  = 1,
    UA_SECURITY_POLICY_BASIC256       = 2,
    UA_SECURITY_POLICY_BASIC256SHA256 = 3
};

#define UA_SECURITY_POLICY_NONE_STRING              "http://opcfoundation.org/UA/SecurityPolicy#None"
#define UA_SECURITY_POLICY_BASIC128RSA15_STRING     "http://opcfoundation.org/UA/SecurityPolicy#Basic128Rsa15"
#define UA_SECURITY_POLICY_BASIC256_STRING          "http://opcfoundation.org/UA/SecurityPolicy#Basic256"
#define UA_SECURITY_POLICY_BASIC256SHA256_STRING    "http://opcfoundation.org/UA/SecurityPolicy#Basic256Sha256"

#define UA_SIGNATURE_ALGORITHMURI_RSASHA1           "http://www.w3.org/2000/09/xmldsig#rsa-sha1"
#define UA_SIGNATURE_ALGORITHMURI_RSASHA256         "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"

/** Contains all parameters defining a security policy. */
struct seconv_params {
    enum crypto_asym_alg asym_alg;          /**< The used asymmetric crypto algorithm. */
    enum crypto_pad_alg  asym_pad_alg;      /**< The used asymmetric crypto padding algorithm. */
    size_t               asym_key_min;      /**< The minimum size for asymmetric keys. */
    size_t               asym_key_max;      /**< The maximum size for asymmetric keys. */
    enum crypto_hash_alg key_deriv_alg;     /**< The id of the algorithm used for key derivation. */
    enum crypto_sym_alg  sym_alg;           /**< The used symmetric crypto algorithm. */
    size_t               sym_key_len;       /**< The used symmetric key length. */
    enum crypto_sign_alg asym_sign_alg;     /**< The used RSA signature algorithm. */
    enum crypto_sign_alg sym_sign_alg;      /**< The used HMAC signature algorithm. */
    size_t               sym_sign_key_len;  /**< The length of the derived signature key. */
    size_t               sym_sign_len;      /**< The length of the signature. */
    enum crypto_hash_alg cert_sign_alg;     /**< The used certificate signing algorithm. */
};

/** Holds security information for either sending or receiving (created by seconv_derive_keys()). */
#ifdef HAVE_CRYPTO
struct seconv_keyset {
    unsigned char     *iv;       /**< The initialization vector. */
    struct crypto_key  sym_key;  /**< The symmetric crypto key. */
    struct crypto_key  sign_key; /**< The signing key. */
};
#endif /* HAVE_CRYPTO */

enum seconv_channel_state {
    seconv_channel_state_unused      = 0, /**< Channel object is inactive and can be allocated. */
    seconv_channel_state_reserved    = 1, /**< Channel is allocated but not open, yet. (no communication) */
    seconv_channel_state_opening     = 2, /**< Channel is in process of being opened. (no communication) */
    seconv_channel_state_open        = 3, /**< Channel is open and can receive and send messages. */
    seconv_channel_state_notransport = 4, /**< Channel is open but has not transport. (no communication) */
    seconv_channel_state_retransport = 5, /**< Channel is in process of being bound to a new transport. (no communication) */
    seconv_channel_state_closing     = 6, /**< Channel is in process of being closed. (no communication) */
    seconv_channel_state_closed      = 7  /**< Channel is closed but cannot be allocated, yet. (no communication) */
};

/** Holds all security relevant parameters of a particular secure channel. */
struct seconv_channel {
    struct sechan_base             *base;                /**< The sechan base managing this channel. */
    struct sechan_config           *config;              /**< The security config used by this channel. */
    uint32_t                        channel_id;          /**< The unique id assigned to the channel. */
    enum seconv_channel_state       channel_state;       /**< Current state of the secure channel. */
    enum ua_security_policy_id      policy_id;           /**< The id of the security policy used on this channel. */
    enum ua_messagesecuritymode     security_mode;       /**< The applied security mode (none, sign, s&e). */
    unsigned char                  *peer_cert;           /**< The peer certificate in ASN1 encoding. */
    int32_t                         peer_cert_len;       /**< Length of the peer certificate blob. */
    unsigned char                   peer_cert_thumb[20]; /**< The thumbprint of the peer certificate (always 20 bytes). */
    int32_t                         peer_cert_thumb_len; /**< Length of the peer certificate thumbprint blob. */
    unsigned char                  *own_nonce;           /**< The locally created nonce. */
    int32_t                         own_nonce_len;       /**< Length of the locally created nonce. */
    char                           *peer_nonce;          /**< The remote created nonce. */
    int32_t                         peer_nonce_len;      /**< Length of the remote created nonce. */
#ifdef HAVE_CRYPTO
    struct crypto_key               pub_key;             /**< Peers public key. */
#endif /* HAVE_CRYPTO */
    int32_t                         ref_cnt;             /**< Reference counter. */
    void*                           transport_handle;    /**< Handle of the bound transport connection. */
    uint32_t                        num_tokens_total;    /**< Total number of security tokens generated for this channel. */
    /* Active token and key set. */
    struct ua_channelsecuritytoken  token_crnt;          /**< Currently active security token. */
#ifdef HAVE_CRYPTO
    struct seconv_keyset            send_keys_crnt;      /**< Keyset containing the encryption and signing keys. */
    struct seconv_keyset            recv_keys_crnt;      /**< Keyset containing the decryption and verification keys */
#endif /* HAVE_CRYPTO */
    /* Next token and key set. */
    struct ua_channelsecuritytoken  token_next;          /**< Security token to be used after the current one. */
#ifdef HAVE_CRYPTO
    struct seconv_keyset            send_keys_next;      /**< Keyset containing the send keys to be used after the current one. */
    struct seconv_keyset            recv_keys_next;      /**< Keyset containing the resv keys to be used after the current one */
#endif /* HAVE_CRYPTO */
};

/* Handle security policies. */
static inline size_t seconv_get_policy_id_length(unsigned int policy_id) {
    switch (policy_id) {
        case UA_SECURITY_POLICY_NONE: return sizeof(UA_SECURITY_POLICY_NONE_STRING)-1;
        case UA_SECURITY_POLICY_BASIC128RSA15: return sizeof(UA_SECURITY_POLICY_BASIC128RSA15_STRING)-1;
        case UA_SECURITY_POLICY_BASIC256: return sizeof(UA_SECURITY_POLICY_BASIC256_STRING)-1;
        case UA_SECURITY_POLICY_BASIC256SHA256: return sizeof(UA_SECURITY_POLICY_BASIC256SHA256_STRING)-1;
        default: return 0;
    };
}

static inline const unsigned char *seconv_get_policy_id_string(unsigned int policy_id) {
    switch (policy_id) {
        case UA_SECURITY_POLICY_NONE: return (const unsigned char*)UA_SECURITY_POLICY_NONE_STRING;
        case UA_SECURITY_POLICY_BASIC128RSA15: return (const unsigned char*)UA_SECURITY_POLICY_BASIC128RSA15_STRING;
        case UA_SECURITY_POLICY_BASIC256: return (const unsigned char*)UA_SECURITY_POLICY_BASIC256_STRING;
        case UA_SECURITY_POLICY_BASIC256SHA256: return (const unsigned char*)UA_SECURITY_POLICY_BASIC256SHA256_STRING;
        default: return 0;
    };
}

/**
 * Get the numeric policy id for the given policy URI.
 * @param policy_uri URI of the security policy.
 * @param policy_uri_len Length of the security policy URI.
 * @param policy_id The internal identifier of the policy.
 * @return 0 on success or errorcode on failure.
 */
int seconv_get_policy_id(unsigned char              *policy_uri,
                         int32_t                     policy_uri_len,
                         enum ua_security_policy_id *policy_id);

/**
 * Get the length of the message signature depending on the security policy.
 * @param policy_id Internal id of the security policy.
 * @return The requested length or zero if not found.
 */
size_t seconv_get_signature_size(uint32_t policy_id);

/**
 * Get the length of the message footer for the default buffer size.
 * @param policy_id Internal id of the security policy.
 * @param encrypt True if Sign&Encrypt, else Sign only.
 * @return The requested length.
 */
size_t seconv_get_max_footer_size(uint32_t policy_id, size_t buffer_size, bool encrypt);

#ifdef HAVE_CRYPTO
/**
 * Calculates the total size of an secured OPN message based on buffer position and header length.
 * @param channel Channel specific security parameters.
 * @param buffer Buffer containing data to sign and encrypt.
 * @param bodystart Begin of body data (end of header).
 * @param msglen Place to store the calculated size of the secured message.
 * @return Error Code
 */
int seconv_opn_msglen(struct seconv_channel      *channel,
                      struct ua_buffer           *uabuffer,
                      size_t                      bodystart,
                      size_t                     *msglen);

/**
 * Calculates the total size of an secured MSG message based on buffer position.
 * @param channel Channel specific security parameters.
 * @param buffer Buffer containing data to sign and encrypt.
 * @param msglen Place to store the calculated size of the secured message.
 * @return Error Code
 */
int seconv_msg_msglen(struct seconv_channel      *channel,
                      struct ua_buffer           *uabuffer,
                      size_t                     *msglen);

/**
 * Release memory referenced by the given keysets.
 * @param send_keys Sending keyset.
 * @param recv_keys Receiving keyset.
 * @return Nothing
 */
void seconv_clear_keysets(struct seconv_keyset send_keys,
                          struct seconv_keyset recv_keys);

#endif /* HAVE_CRYPTO */

#endif /* SECONV_H 1 */
