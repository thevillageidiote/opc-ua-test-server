/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef _SECHAN_H_
#define _SECHAN_H_

#include <network/config.h>
#include <uaprotocol/uaseconv/seconv_types.h>
#include <uabase/bytestring.h>
#include <uabase/structure/messagesecuritymode.h>
#include <uabase/structure/securitytokenrequesttype.h>
#include <util/pointerset.h>
#include <stdbool.h>
#include <appconfig.h>

/* Will be used simultaneously by uaseconv (required to get transport security related information)
   and uasession (basic management through service handling). Not clear yet, where this module should
   be put. Probably best included in transport with async interface between uasession. */

/* to be allocated in ipc */
struct sechan_cert {
    struct ua_bytestring             server_cert;           /**< The server certificate to be used for this policy. */
    unsigned char                    own_cert_thumb[20];    /**< SHA1 of the server certificate. */
#ifdef HAVE_CRYPTO
    struct crypto_key                server_priv_key;       /**< Private key of the server certificate. */
#endif
};

/** Represents a secure channel configuration in context of an endpoint. */
struct sechan_config {
    struct sechan_cert              *cert_ptr;              /**< Pointer to the certificate in the certificates array. */
    int32_t                          certificate;           /**< Index in the certificates array for this config, negative if no certificate. */
    uint32_t                         store;                 /**< PKI store identifier for certificate operation in context of this policy. */
    bool                             ign_time;              /**<  */
    bool                             ign_issuer_time;       /**<  */
    bool                             ign_miss_crl;          /**<  */
    bool                             ign_miss_issuer_crl;   /**<  */
    bool                             ign_untrusted;         /**<  */
    bool                             check_complete_chain;  /**<  */
    bool                             disable_crl_check;     /**<  */
    uint32_t                         max_verify_results;    /**< Max results reported when validating certificates in context of this policy. */
    uint32_t                         max_chain_length;      /**< Max number of certificates in a received chain. */
    enum ua_security_policy_id       policy_id;             /**< Identifier of the policy to use */
    bool                             mode_sign;             /**< true if message security mode sign is allowed. */
    bool                             mode_sign_and_encrypt; /**< true if message security mode sign and encrypt is allowed. */
    bool                             mode_none;             /**< true if message security mode none is allowed. */
};

/** Base object combining all elements for managing secure channels on an endpoint. */
struct sechan_base {
    uint32_t                    nConfigs;                     /**< Number of valid security policy configurations in configs. */
    struct sechan_config       *configs;                      /**< Array of allowed security policy configurations. */
    uint32_t                    channels_total;               /**< Total number of channels created. */
    uint32_t                    nChannels;                    /**< Max number of concurrent channels. */
    struct util_pointerset      channel_list;                 /**< List of current channels. */
};

int sechan_base_init(struct sechan_base *base,
                     uint32_t nchannels,
                     struct sechan_config *configs,
                     uint32_t nconfigs,
                     struct sechan_cert *certs,
                     uint32_t ncerts);

int sechan_base_clear(struct sechan_base *base);

uint32_t sechan_revise_lifetime(uint32_t requested_lifetime);

int sechan_create(struct sechan_base *base,
                  enum ua_security_policy_id policy_id,
                  uint32_t channel_id,
                  int32_t peer_cert_len,
                  unsigned char *peer_cert,
                  int32_t own_cert_thumb_len,
                  unsigned char *own_cert_thumb,
                  struct seconv_channel **channel);

int sechan_open(struct seconv_channel *channel,
                enum ua_securitytokenrequesttype request_type,
                enum ua_messagesecuritymode security_mode,
                struct ua_bytestring *client_nonce,
                uint32_t requested_lifetime,
                void* transport_handle);

int sechan_base_get_channel(struct sechan_base *base,
                            uint32_t channel_id,
                            struct seconv_channel **channel);

int sechan_base_get_channelconfig(struct sechan_base *base,
                                  enum ua_security_policy_id policy_id,
                                  struct sechan_config **config);

int sechan_close(struct seconv_channel *channel);

int sechan_setstate(struct seconv_channel *channel,
                    enum seconv_channel_state state);

int sechan_switch_token(struct seconv_channel *channel, uint32_t next_id);

int sechan_acquire(struct seconv_channel *channel);

int sechan_release(struct seconv_channel *channel);

#endif /* _SECHAN_H_ */
