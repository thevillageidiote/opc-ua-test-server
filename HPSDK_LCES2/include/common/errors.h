/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


/* Positive error codes */
#define UA_EGOOD                      0 /**< No error. All good. Be happy. */
#define UA_EASYNC                     1 /**< Operation completes asynchronously. */

/* Negative error codes */
#define UA_EBAD                      -1 /**< Generic fault. */
#define UA_EBADDISCONNECT            -2 /**< The connection was closed by the peer. */
#define UA_EBADSHUTDOWN              -3 /**< The connection was shut down by the peer. */
#define UA_EBADINVALIDARGUMENT       -4 /**< One or more of the supplied parameters were not valid. */
#define UA_EBADNOBUF                 -5 /**< Allocating a buffer failed. */
#define UA_EBADDECODING              -6 /**< An decoding error occured. */
#define UA_EBADENCODING              -7 /**< An encoding error occured. */
#define UA_EBADNOMEM                 -8 /**< Allocating memory failed */
#define UA_EBADNOTIMPL               -9 /**< The called function is not implemented fully or parially. */
#define UA_EBADHOSTUNKNOWN          -10 /**< The referenced host name could not be resolved. */
#define UA_EBADSYSCALL              -11 /**< An underlying system call failed. */
#define UA_EBADOPCANCELLED          -12 /**< The operation has been cancelled. */
#define UA_EBADOPTIMEOUT            -13 /**< The operation timed out. */
#define UA_EBADIOPENDING            -14 /**< There is already an operation pending. Call again after completion */
#define UA_EBADINVALIDSTATE         -15 /**< An object is not in the required state for the current operation. */
#define UA_EBADTRUNCATED            -16 /**< The result has been truncated. */
#define UA_EBADBUFTOOSMALL          -17 /**< The given destination buffer is too small. */
#define UA_EBADTOOMANYNESTEDCALLS   -18 /**< The operation would require too many iterations to succeed. */
#define UA_EBADNOTSUPPORTED         -19 /**< The requested operation is not supported. */
#define UA_EBADCERTIFICATINVALID    -20 /**< The certificate passed to the operation was not valid. */
#define UA_EBADNOTFOUND             -21 /**< A searched object could not be found. */
#define UA_EBADSIGNATURE            -22 /**< The operation failed because of a bad data signature. */
#define UA_EBADMISMATCH             -23 /**< The actual value didn't match the expected value. */
#define UA_EBADOUTOFRESOURCE        -24 /**< No more resources available. */
#define UA_EBADINUSE                -25 /**< Resource is still in use. */
#define UA_EBADAGAIN                -26 /**< Operation needs at last one more call to succeed. */
#define UA_EBADOUTOFRANGE           -27 /**< parameter is out of range. */
#define UA_EBADINTERNALERROR        -28 /**< Internal error. */
#define UA_EBADENCODINGLIMITSEXCEEDED -29 /**< Same as UA_SCBADENCODINGLIMITSEXCEEDED */
#define UA_EBADENCODINGTOOLARGE     -30 /**< Request or response would be too large if encoded. */
#define UA_EBADACCESSDENIED         -31 /**< Access denied. */
#define UA_EBADLOOP                 -32 /**< A loop exists in symbolic links encountered during resolution of the path argument. */
#define UA_EBADFILEEXISTS           -33 /**< The file or directory already exists. */
#define UA_EBADFILENOTFOUND         -34 /**< The specified file was not found. */
#define UA_EBADPATHNOTFOUND         -35 /**< The specified directory was not found. */
#define UA_EBADUNEXPECTED           -36 /**< For unexpected and unhandled errors to trigger general error handling. */
#define UA_EBADCOMMUNICATIONERROR   -37 /**< General error regarding the communication layer. */
