/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef UA_ATOMIC_H
#define UA_ATOMIC_H

#include <stdbool.h>

#if __STDC_VERSION__ >= 201112L
# ifdef __STDC_NO_ATOMICS__
#  error "Your C11 compiler is not required to provide stdatomic.h"
# else
//#  include <stdatomic.h>
# endif
#else
//# error "Your C compiler isn't providing C11."
#endif

/* Note: to use GCC atomic builtins on x86 you should define at least -march=486,
 * because 386 instruction sets do not support cmpxchg.
 * If you get an undefined reference error when linking you have most likely forgotten
 * to set this architecture. Default on GCC is still i386.
 * Today -march=i686 is a good option, as older CPUs are really rare.
 */

/* use compiler intrinsics if supported */
#if (__GNUC__ > 4) || ((__GNUC__ == 4) && (__GNUC_MINOR__ >= 1))
# if defined(__i486__) || defined(__i586__) || defined(__i686__)
   /* atomics seem to be available on x86 platorms, but not for i386 */
#  define GCC_HAS_ATOMIC_BUILTINS 1
# endif
# if defined(__powerpc__)
   /* atomics seem to be available on PPC platorms */
#  define GCC_HAS_ATOMIC_BUILTINS 1
# endif
# if defined(__ia64)
   /* atomics seem to be available on IA64 platorms */
#  define GCC_HAS_ATOMIC_BUILTINS 1
# endif
# if defined(__amd64__) || defined(__x86_64__)
   /* atomics seem to be available on 64bit linux */
#  define GCC_HAS_ATOMIC_BUILTINS 1
# endif
# if defined(__arm__)
   /* atomics seem to be available on 64bit linux */
#  define GCC_HAS_ATOMIC_BUILTINS 1
# endif
#endif

typedef struct {
    unsigned int counter;
} ua_atomic_t;

/* use compiler intrinsics if supported (aka atomic builtins) */
#if GCC_HAS_ATOMIC_BUILTINS

static inline void ua_atomic_set(ua_atomic_t *v, unsigned int value)
{
    /* Set should be atomic anyway when int fits into a single register */
    v->counter = value;
}

static inline unsigned int ua_atomic_inc(ua_atomic_t *v)
{
    /* atomic load, modify, store */
    return __sync_add_and_fetch(&v->counter, 1);
}

static inline unsigned int ua_atomic_dec(ua_atomic_t *v)
{
    /* atomic load, modify, store */
    return __sync_sub_and_fetch(&v->counter, 1);
}

/** ua_atomic_add_return - add and return
* @v: pointer of type atomic_t
* @i: integer value to add
*
* Atomically adds @i to @v and returns @v + @i
*/
static inline unsigned int ua_atomic_add_return(unsigned int i, ua_atomic_t *v)
{
    return __sync_add_and_fetch(&v->counter, i);
}

/** ua_atomic_sub_return - sub and return
* @v: pointer of type atomic_t
* @i: integer value to add
*
* Atomically adds @i from @v and returns @v - @i
*/
static inline unsigned int ua_atomic_sub_return(unsigned int i, ua_atomic_t *v)
{
    return __sync_sub_and_fetch(&v->counter, i);
}

/** subtract value from variable and test result.
 * Atomically subtracts i from v and returns true if the result is zero,
 * or false for all other cases.
 */
static inline unsigned int ua_atomic_sub_and_test(unsigned int i, ua_atomic_t *v)
{
    /* TODO: decrement and comparison is not atomic,
     * but in normal usecases as refcount there should nobody be left
     * to modify the refcnt when the final reference gets substracted.
     */
    __sync_sub_and_fetch(&v->counter, i);
    return (v->counter == 0);
}

static inline unsigned int ua_atomic_dec_and_test(ua_atomic_t *v)
{
    return ua_atomic_sub_and_test(1, v);
}

static inline int ua_atomic_compare_and_swap(int *ptr, int oldval, int newval)
{
    return __sync_val_compare_and_swap(ptr, oldval, newval);
}

static inline unsigned int ua_atomic_compare_and_swap_uint(unsigned int *ptr, unsigned int oldval, unsigned int newval)
{
    return __sync_val_compare_and_swap(ptr, oldval, newval);
}
#else
/* If you see this error, either write you own inline assembly routines
 * for your archticture, or use a newer GCC which has built-in compiler intrinsics for that.
 */
#  error "Atomic functions are not ported to non x86 architectures for GCC<4.1"
#endif

#endif /* end of include guard: UA_ATOMIC_H */

