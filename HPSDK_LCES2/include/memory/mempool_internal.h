/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef __MEMPOOL_INTERNAL_H__
#define __MEMPOOL_INTERNAL_H__

#include "mempool.h"

#ifdef MP_USE_INERNAL_MEMSET
/* Simple memset replacement.
 * Slow, unoptimized, but works.
 */
void *mempool_memset_internal(void *b, int c, size_t len);
#endif /* MP_USE_INERNAL_MEMSET */

/* Chunk methods */
static void chunk_set_size_and_free(struct malloc_chunk *chunk, size_t size);
static size_t chunk_get_size(struct malloc_chunk *chunk);
static struct malloc_chunk *chunk_get_previous(struct malloc_chunk *chunk);
static void chunk_set_previous(struct malloc_chunk *chunk, struct malloc_chunk *prev);
static struct malloc_chunk *chunk_get_next(struct malloc_chunk *chunk);
static void chunk_set_next(struct malloc_chunk *chunk, struct malloc_chunk *next);

#ifdef MP_DEBUG_MEMORY
static void chunk_set_bounds(struct malloc_chunk *chunk);
static void chunk_verify_bounds(struct malloc_chunk *chunk);
#endif /* TF_DEBUG_MEMORY */

/* Mempool methods */
int mempool_init(struct mempool *pool, unsigned char *data, size_t size);
static void mempool_push_chunk(struct mempool *pool, size_t iBin, struct malloc_chunk *chunk);
static struct malloc_chunk *mempool_pop_chunk(struct mempool *pool, size_t iBin);
static struct malloc_chunk *mempool_pop_chunk_with_size(struct mempool *pool, size_t iBin, size_t size);
static void mempool_remove_chunk(struct mempool *pool, size_t iBin, struct malloc_chunk *chunk);


#endif /* __MEMPOOL_INTERNAL_H__ */
