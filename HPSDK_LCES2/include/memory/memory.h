/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_MEMORY_H_
#define _UABASE_MEMORY_H_

#include <stdlib.h>
#include <platform/memory.h>
#include <memory/memory_config.h>
#include <memory/memory_layout.h>
#ifdef MEMORY_ENABLE_TRACE
# include <memory/mempool_trace.h>
#endif

/** @defgroup memory memory
 * @{
 */

void *pointer_align(void *ptr);

/** This rounds up the given size to a multiple of alignment. */
#define size_align(size, alignment) (((size) + (alignment) - 1) / (alignment) * (alignment))

/* IPC memory allocation.
 * This is performed in a shared memory area when IPC is enabled.
 * Otherwise this is mapped to internal memory allocation as seen above.
 */
#ifdef MEMORY_ENABLE_TRACE
#define ipc_malloc(size) memory_malloc(size, __FILE__, __LINE__)
#define ipc_free(ptr) memory_free(ptr, __FILE__, __LINE__)
#define ipc_calloc(nmemb, size) memory_calloc(nmemb, size, __FILE__, __LINE__)
#define ipc_realloc(ptr, size) memory_realloc(ptr, size, __FILE__, __LINE__)
#else /* MEMORY_ENABLE_TRACE */
/** Allocate dynamic memory from IPC memory */
#define ipc_malloc(size) memory_malloc(size)
/** Free dynamic memory from IPC memory */
#define ipc_free(ptr) memory_free(ptr)
/** Allocate zero initialized dynamic memory of \c nmemb * \c size from IPC memory */
#define ipc_calloc(nmemb, size) memory_calloc(nmemb, size)
/** Rellocate dynamic memory from IPC memory */
#define ipc_realloc(ptr, size) memory_realloc(ptr, size)
#endif /* MEMORY_ENABLE_TRACE */
/** Fill IPC memory with constant byte \c c */
#define ipc_memset(s, c, n) ua_memset(s, c, n)
char *ipc_strdup(const char *s);

/** On error (\c ret != 0) free dynamic IPC memory and set pointer to NULL */
#define IPC_CLEANUP_MEM_ON_ERROR(ret, ptr) \
    if (ret != 0) { \
        ipc_free(ptr); \
        ptr = NULL; \
    }

/** Free dynamic IPC memory and set pointer to NULL */
#define IPC_CLEANUP_MEM(ptr) ipc_free(ptr); ptr = NULL;

/* typesafe alloc macros (at least for GCC which supports typeof() */
#if defined(__GNUC__) && !defined(__STRICT_ANSI__)
# define IPC_ALLOC(var) (typeof(var))ipc_malloc(sizeof(*var))
# define IPC_ALLOC_ARRAY(var, num) (typeof(var))ipc_malloc(sizeof(*var)*(num))
# define IPC_CALLOC(var) (typeof(var))ipc_calloc(1, sizeof(*var))
# define IPC_CALLOC_ARRAY(var, num) (typeof(var))ipc_calloc(num, sizeof(*var))
# define IPC_FREE(var) ipc_free(var)
# if ((__GNUC__ > 4) && (__GNUC_MINOR__ >= 9))
#  define IPC_CLEAR(var) (typeof(var))ua_memset(var, 0, sizeof(*var))
#  define IPC_CLEAR_ARRAY(var, num) (typeof(var))ua_memset(var, 0, sizeof(*var)*(num))
#  define IPC_MEMSET(var, val) (typeof(var))ua_memset(var, val, sizeof(*var))
#  define IPC_MEMSET_ARRAY(var, val, num) (typeof(var))ua_memset(var, val, sizeof(*var)*(num))
# else
#  define IPC_CLEAR(var) ua_memset(var, 0, sizeof(*var))
#  define IPC_CLEAR_ARRAY(var, num) ua_memset(var, 0, sizeof(*var)*(num))
#  define IPC_MEMSET(var, val) ua_memset(var, val, sizeof(*var))
#  define IPC_MEMSET_ARRAY(var, val, num) ua_memset(var, val, sizeof(*var)*(num))
# endif
#else
/** Allocate dynamic IPC memory for a typed pointer \c var.
 * @code
 * struct ua_string *s = IPC_ALLOC(s);
 * @endcode
 */
# define IPC_ALLOC(var) ipc_malloc(sizeof(*var))
/** Allocate array of dynamic IPC memory for a typed pointer \c var.
 * @code
 * uint32_t *x = IPC_ALLOC_ARRAY(x, 2);
 * if (x == NULL) goto error;
 * x[0] = 35;
 * x[1] = 43;
 * @endcode
 */
# define IPC_ALLOC_ARRAY(var, num) ipc_malloc(sizeof(*var)*(num))
/** Allocate zero initialized dynamic IPC memory for a typed pointer \c var. */
# define IPC_CALLOC(var) ipc_calloc(1, sizeof(*var))
/** Allocate zero initialized array of dynamic IPC memory for a typed pointer \c var. */
# define IPC_CALLOC_ARRAY(var, num) ipc_calloc(num, sizeof(*var))
/** Free dynamic memory from IPC memory. */
# define IPC_FREE(var) ipc_free(var)
/** Set memory pointed to by typed pointer to zero */
# define IPC_CLEAR(var) ua_memset(var, 0, sizeof(*var))
/** Like @ref IPC_CLEAR but for arrays */
# define IPC_CLEAR_ARRAY(var, num) ua_memset(var, 0, sizeof(*var)*(num))
# define IPC_MEMSET(var, val) ua_memset(var, val, sizeof(*var))
# define IPC_MEMSET_ARRAY(var, val, num) ua_memset(var, val, sizeof(*var)*(num))
#endif

/** @} */

#endif /* end of include guard: _UABASE_MEMORY_H_ */

