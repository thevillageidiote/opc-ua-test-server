/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef MEMORY_CONFIG_H
#define MEMORY_CONFIG_H

/* #undef MEMORY_ENABLE_DEBUG */
/* #undef MEMORY_ENABLE_TRACK_USED_MEM */
/* #undef MEMORY_ENABLE_TRACE */
/* #undef MEMORY_ENABLE_STAT */
/* #undef MEMORY_THREADSAFE */
#ifdef MEMORY_ENABLE_TRACE
# define MEMORY_TRACE_LOG_SIZE 1024 /* should be a power of 2 to avoid alignment issues in the trace buffer */
#endif
/* when MEMORY_USE_SHM is set then the base memory area used for memory pools
 * is allocated in shared memory. This is necessary for IPC in multi-task configuration.
 * In single-task mode this can be undefined, and normal malloc is used.
 */
/* #undef MEMORY_USE_SHM */
/* the following switch is only for debugging purpose, not for productive usage. */
/* #undef MEMORY_OBJECTPOOL_USE_MALLOC */
/* the following switch is only for debugging purpose, not for productive usage. */
/* #undef MEMORY_MEMPOOL_USE_MALLOC */
/* object pool file support */
#define MEMORY_OBJECTPOOL_SUPPORT_FILE_IO

/* well known list of pool ids that always exist . */
/* memory pool id for shared memory heap */
#define POOLID_HEAP    0
/* memory pool id for message queues */
#define POOLID_QUEUE   1
/* memory pool id for ipc messages */
#define POOLID_MSG     2
/* memory pool id of services */
#define POOLID_SERVICE 3
/* memory pool id of buffers */
#define POOLID_BUFFER  4
/* memory pool id of sessions */
#define POOLID_SESSION  5
/* memory pool id of session service calls */
#define POOLID_SESSION_CALL 6
/* memory pool id of secure channels in the session map */
#define POOLID_SESSION_CHANNEL 7
/* memory pool id of UATCP message context */
#define POOLID_UATCPMSG_CTXT 8
/* memory pool id of UATCP connection */
#define POOLID_UATCPMSG_CONNECTION 9
/* memory pool id of subscriptionbases */
#define POOLID_SUBSCRIPTIONBASE 10
/* memory pool id of subscriptions */
#define POOLID_SUBSCRIPTION 11
/* memory pool id of notificationmessages aka supernotifications */
#define POOLID_NOTIFICATIONMESSAGE 12
/* memory pool id of monitoreditems */
#define POOLID_MONITOREDITEM 13
/* memory pool id of secure channels */
#define POOLID_SECURECHANNEL 14
/* memory pool id of secure channels */
#define POOLID_XMLPARSER 15
/* memory pool id of memory trace */
#define POOLID_MEM_TRACE  16
/* number of memory pools */
#define POOLID_NUM_POOLS  17

/* maximum service size used for memory pool POOLID_SERVICE */
#define MAX_SERVICE_SIZE 96

/* memory layout properties */
#define MEMORY_MAX_PROPS 10
#define MEMORY_PROP_BUFFERMGT 0
#define MEMORY_PROP_TRACE_LEVEL 1
#define MEMORY_PROP_TRACE_FACILITY 2
#define MEMORY_PROP_ENCODER_MAX_CALL_DEPTH 3
#define MEMORY_PROP_ENCODER_MAX_STRING_LEN 4
#define MEMORY_PROP_ENCODER_MAX_BYTESTRING_LEN 5
#define MEMORY_PROP_ENCODER_MAX_ARRAY_LEN 6

#if defined(MEMORY_USE_SHM) && !defined(MEMORY_THREADSAFE)
# error "You need to enable thread-safity when using share memory!"
#endif

#endif /* MEMORY_CONFIG_H */

