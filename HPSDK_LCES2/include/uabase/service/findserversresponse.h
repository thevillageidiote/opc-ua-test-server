/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_FINDSERVERSRESPONSE_H_
#define _UABASE_FINDSERVERSRESPONSE_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/structure/applicationdescription.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_findserversresponse
 *
 *  \var ua_findserversresponse::num_servers
 *  Number of elements in \ref ua_findserversresponse::servers.
 *
 *  \var ua_findserversresponse::servers
 *  List of Servers that meet criteria specified in the request.
 *
 *  This list is empty if no servers meet the criteria.
*/
struct ua_findserversresponse {
    int32_t num_servers;
    struct ua_applicationdescription *servers;
};

void ua_findserversresponse_init(struct ua_findserversresponse *t);
void ua_findserversresponse_clear(struct ua_findserversresponse *t);

#endif /* _UABASE_FINDSERVERSRESPONSE_H_ */

