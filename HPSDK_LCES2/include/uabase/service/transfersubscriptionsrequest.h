/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_TRANSFERSUBSCRIPTIONSREQUEST_H_
#define _UABASE_TRANSFERSUBSCRIPTIONSREQUEST_H_

#include <stdbool.h>
#include <stdint.h>
#include <uabase/base_config.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_transfersubscriptionsrequest
 *  Asynchronous call to transfer a subscription and its MonitoredItems from one
 *  Session to another.
 *
 *  For example, a Client may need to reopen a Session and then transfer its
 *  Subscriptions to that Session. It may also be used by one Client to take over a
 *  Subscription from another Client by transferring the Subscription to its
 *  Session.
 *
 *  The authenticationToken contained in the request header identifies the Session
 *  to which the Subscription and MonitoredItems shall be transferred. The Server
 *  shall validate that the Client of that Session is operating on behalf of the
 *  same user and that the potentially new Client supports the Profiles that are
 *  necessary for the Subscription. If the Server transfers the Subscription, it
 *  returns the sequence numbers of the NotificationMessages that are available for
 *  retransmission. The Client should acknowledge all messages in this list for
 *  which it will not request retransmission.
 *
 *  If the Server transfers the Subscription to the new Session, the Server shall
 *  issue a StatusChangeNotification notificationMessage with the status code
 *  Good_SubscriptionTransferred to the old Session.
 *
 *  \var ua_transfersubscriptionsrequest::num_subscription_ids
 *  Number of elements in \ref ua_transfersubscriptionsrequest::subscription_ids.
 *
 *  \var ua_transfersubscriptionsrequest::subscription_ids
 *  List of identifiers for the Subscriptions to be transferred to the new Client.
 *
 *  These identifiers are transferred from the primary Client to a backup Client
 *  via external mechanisms.
 *
 *  \var ua_transfersubscriptionsrequest::send_initial_values
 *  A boolean parameter indicating whether the first publish response should
 *  contain current values of all monitored items.
 *
 *  It has the following values: <dl> <dt>TRUE</dt> <dd>The first Publish response
 *  after the TransferSubscriptions call shall contain the current values of all
 *  Monitored Items in the Subscription where the Monitoring Mode is set to
 *  Reporting.</dd> <dt>FALSE</dt> <dd>The first Publish response after the
 *  TransferSubscriptions call shall contain only the value changes since the last
 *  Publish response was sent.</dd> </dl>
 *
 *  This parameter only applies to MonitoredItems used for monitoring Attribute
 *  changes.
*/
struct ua_transfersubscriptionsrequest {
    int32_t num_subscription_ids;
    uint32_t *subscription_ids;
    bool send_initial_values;
};

void ua_transfersubscriptionsrequest_init(struct ua_transfersubscriptionsrequest *t);
void ua_transfersubscriptionsrequest_clear(struct ua_transfersubscriptionsrequest *t);

#endif /* _UABASE_TRANSFERSUBSCRIPTIONSREQUEST_H_ */

