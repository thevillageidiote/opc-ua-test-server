/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_CREATESESSIONREQUEST_H_
#define _UABASE_CREATESESSIONREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/bytestring.h>
#include <uabase/string.h>
#include <uabase/structure/applicationdescription.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_createsessionrequest
 * Creates a new session with the server.
*/
struct ua_createsessionrequest {
    struct ua_applicationdescription client_description;
    struct ua_string server_uri;
    struct ua_string endpoint_url;
    struct ua_string session_name;
    struct ua_bytestring client_nonce;
    struct ua_bytestring client_certificate;
    double requested_session_timeout;
    uint32_t max_response_message_size;
};

void ua_createsessionrequest_init(struct ua_createsessionrequest *t);
void ua_createsessionrequest_clear(struct ua_createsessionrequest *t);

#endif /* _UABASE_CREATESESSIONREQUEST_H_ */

