/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_READREQUEST_H_
#define _UABASE_READREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/structure/readvalueid.h>
#include <uabase/structure/timestampstoreturn.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_readrequest
 *  Reads values and attributes asynchronously from OPC server.
 *
 *  The Read Service is used to read one or more attributes of one or more nodes.
 *  It allows also reading subsets or single elements of array values and to define
 *  a valid age of values to be returned to reduce the need for device reads. Like
 *  most other services, the read is optimized for bulk read operations and not for
 *  reading single Attribute values. Typically all Node Attributes are readable.
 *  For the Value Attribute the Read rights are indicated by the AccessLevel and
 *  UserAccessLevel Attribute of the Variable.
 *
 *  \var ua_readrequest::max_age
 *  Maximum age of the value to be read in milliseconds.
 *
 *  The age of the value is based on the difference between the ServerTimestamp and
 *  the time when the Server starts processing the request. For example if the
 *  Client specifies a maxAge of 500 milliseconds and it takes 100 milliseconds
 *  until the Server starts processing the request, the age of the returned value
 *  could be 600 milliseconds prior to the time it was requested.
 *
 *  If the Server has one or more values of an Attribute that are within the
 *  maximum age, it can return any one of the values or it can read a new value
 *  from the data source. The number of values of an Attribute that a Server has
 *  depends on the number of MonitoredItems that are defined for the Attribute. In
 *  any case, the Client can make no assumption about which copy of the data will
 *  be returned.
 *
 *  If the Server does not have a value that is within the maximum age, it shall
 *  attempt to read a new value from the data source.
 *
 *  If the Server cannot meet the requested maxAge, it returns its "best effort"
 *  value rather than rejecting the request. This may occur when the time it takes
 *  the Server to process and return the new data value after it has been accessed
 *  is greater than the specified maximum age.
 *
 *  If maxAge is set to 0, the Server shall attempt to read a new value from the
 *  data source.
 *
 *  If maxAge is set to the max Int32 value or greater, the Server shall attempt to
 *  get a cached value.
 *
 *  Negative values are invalid for maxAge.
 *
 *  \var ua_readrequest::ts
 *  An enumeration that specifies the Timestamps to be returned for each requested
 *  Variable Value Attribute. See \ref ua_timestampstoreturn for more information.
 *
 *  \var ua_readrequest::num_nodes
 *  Number of elements in \ref ua_readrequest::nodes.
 *
 *  \var ua_readrequest::nodes
 *  List of Nodes and their Attributes to read.
 *
 *  For each entry in this list, a StatusCode is returned, and if it indicates
 *  success, the Attribute Value is also returned.
*/
struct ua_readrequest {
    double max_age;
    enum ua_timestampstoreturn ts;
    int32_t num_nodes;
    struct ua_readvalueid *nodes;
};

void ua_readrequest_init(struct ua_readrequest *t);
void ua_readrequest_clear(struct ua_readrequest *t);

#endif /* _UABASE_READREQUEST_H_ */

