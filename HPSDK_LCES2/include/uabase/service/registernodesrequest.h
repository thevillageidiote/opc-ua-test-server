/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_REGISTERNODESREQUEST_H_
#define _UABASE_REGISTERNODESREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/nodeid.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_registernodesrequest
 *  Asynchronously registers nodes to create shortcuts in the server.
 *
 *  This service allows clients to optimize the cyclic access to nodes, for example
 *  for writing variable values or for calling methods. There are two levels of
 *  optimization.
 *
 *  The first level is to reduce the amount of data on the wire for addressing
 *  information. Since NodeIds are used for addressing in nodes and can be very
 *  long, a more optimized addressing method is desirable for cyclic use of nodes.
 *  Classic OPC provided the concept to create handles for items by adding them to
 *  a group. RegisterNodes provides a similar concept to create handles for nodes
 *  by returning a numeric NodeId that can be used in all functions accessing
 *  information from the server. The transport of numeric NodeIds is very efficient
 *  in the OPC UA binary protocol.
 *
 *  The second level of optimization is possible inside the server. Since the
 *  client is telling the server that it wants to use the node more frequently by
 *  registering the Node, the server is able to prepare everything that is possible
 *  to optimize the access to the node.
 *
 *  The handles returned by the server are only valid during the lifetime of the
 *  session that was used to register the nodes. Clients must call \ref
 *  ua_unregisternodesrequest if the node is no longer used, to free the resources
 *  used in the server for the optimization. This method should not be used to
 *  optimize the cyclic read of data, since OPC UA provides a much more optimized
 *  mechanism to subscribe for data changes.
 *
 *  Clients do not have to use the service and servers can simply implement the
 *  service only returning the same list of NodeIds that was passed in if there is
 *  no need to optimize the access.
 *
 *  \var ua_registernodesrequest::num_nodes
 *  Number of elements in \ref ua_registernodesrequest::nodes.
 *
 *  \var ua_registernodesrequest::nodes
 *  A list of NodeIds to register that the client has retrieved through browsing,
 *  querying or in some other manner.
*/
struct ua_registernodesrequest {
    int32_t num_nodes;
    struct ua_nodeid *nodes;
};

void ua_registernodesrequest_init(struct ua_registernodesrequest *t);
void ua_registernodesrequest_clear(struct ua_registernodesrequest *t);

#endif /* _UABASE_REGISTERNODESREQUEST_H_ */

