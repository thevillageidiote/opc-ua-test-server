/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_CREATESUBSCRIPTIONRESPONSE_H_
#define _UABASE_CREATESUBSCRIPTIONRESPONSE_H_

#include <stdint.h>
#include <uabase/base_config.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_createsubscriptionresponse
 *
 *  \var ua_createsubscriptionresponse::subscription_id
 *  The Server-assigned identifier for the Subscription.
 *
 *  This identifier shall be unique for the entire Server, not just for the
 *  Session, in order to allow the Subscription to be transferred to another
 *  Session using the TransferSubscriptions service.
 *
 *  \var ua_createsubscriptionresponse::revised_publishing_interval
 *  The actual publishing interval that the Server will use, expressed in
 *  milliseconds.
 *
 *  The Server should attempt to honor the Client request for this parameter, but
 *  may negotiate this value up or down to meet its own constraints.
 *
 *  \var ua_createsubscriptionresponse::revised_lifetime_count
 *  The lifetime of the Subscription shall be a minimum of three times the
 *  keep-alive interval negotiated by the Server.
 *
 *  \var ua_createsubscriptionresponse::revised_max_keep_alive_count
 *  The actual maximum keep-alive count.
 *
 *  The Server should attempt to honor the Client request for this parameter, but
 *  may negotiate this value up or down to meet its own constraints.
*/
struct ua_createsubscriptionresponse {
    uint32_t subscription_id;
    double revised_publishing_interval;
    uint32_t revised_lifetime_count;
    uint32_t revised_max_keep_alive_count;
};

void ua_createsubscriptionresponse_init(struct ua_createsubscriptionresponse *t);
void ua_createsubscriptionresponse_clear(struct ua_createsubscriptionresponse *t);

#endif /* _UABASE_CREATESUBSCRIPTIONRESPONSE_H_ */

