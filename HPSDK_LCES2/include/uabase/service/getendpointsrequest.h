/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_GETENDPOINTSREQUEST_H_
#define _UABASE_GETENDPOINTSREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/string.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_getendpointsrequest
 *  Asynchronous call to get the Endpoints supported by a Server and all of the
 *  configuration information required to establish a SecureChannel and a Session.
 *
 *  This Service shall not require any message security, but it may require
 *  transport layer security.
 *
 *  A Client may reduce the number of results returned by specifying filter
 *  criteria based on LocaleIds and Transport Profile URIs. The Server returns an
 *  empty list if no Endpoints match the criteria specified by the client.
 *
 *  A Server may support multiple security configurations for the same Endpoint. In
 *  this situation, the Server shall return separate \ref ua_endpointdescription
 *  records for each available configuration.
 *
 *  Clients should treat each of these configurations as distinct Endpoints, even
 *  if the physical URL happens to be the same.
 *
 *  The security configuration for an Endpoint has four components:
 *
 *  - Server application instance certificate
 *  - Message Security Mode
 *  - Security Policy
 *  - Supported User Identity Tokens
 *
 *  The ApplicationInstanceCertificate is used to secure the OpenSecureChannel
 *  request. The MessageSecurityMode and the SecurityPolicy tell the Client how to
 *  secure messages sent via the SecureChannel. The UserIdentityTokens tell the
 *  client which type of user credentials shall be passed to the Server in the
 *  ActivateSession request.
 *
 *  If the securityPolicyUri is NONE and none of the UserTokenPolicies requires
 *  encryption, the Client shall ignore the ApplicationInstanceCertificate.
 *
 *  Each EndpointDescription also specifies a URI for the Transport Profile that
 *  the Endpoint supports. The Transport Profiles specify information such as
 *  message encoding format and protocol version and are defined in Part 7 of the
 *  OPC UA Specification. Clients shall fetch the Server’s SoftwareCertificates if
 *  they want to discover the complete list of Profiles supported by the Server.
 *
 *  Messages are secured by applying standard cryptography algorithms to the
 *  messages before they are sent over the network. The exact set of algorithms
 *  used depends on the SecurityPolicy for the Endpoint. Part 7 of the OPC UA
 *  Specification defines Profiles for common SecurityPolicies and assigns a unique
 *  URI to them. It is expected that applications have built in knowledge of the
 *  SecurityPolicies that they support. As a result, only the Profile URI for the
 *  SecurityPolicy is specified in the EndpointDescription. A Client cannot connect
 *  to an Endpoint that does not support a SecurityPolicy that it recognizes.
 *
 *  An EndpointDescription may specify that the message security mode is NONE. This
 *  configuration is not recommended unless the applications are communicating on a
 *  physically isolated network where the risk of intrusion is extremely small. If
 *  the message security is NONE, it is possible for Clients to deliberately or
 *  accidentally hijack Sessions created by other Clients.
 *
 *  A Server may have multiple HostNames. For this reason, the Client shall pass
 *  the URL it used to connect to the Endpoint to this Service. The implementation
 *  of this Service shall use this information to return responses that are
 *  accessible to the Client via the provided URL.
 *
 *  This Service can be used without security and it is therefore vulnerable to
 *  Denial Of Service (DOS) attacks. A Server should minimize the amount of
 *  processing required to send the response for this Service. This can be achieved
 *  by preparing the result in advance. The Server should also add a short delay
 *  before starting processing of a request during high traffic conditions.
 *
 *  Some of the EndpointDescriptions returned in a response shall specify the
 *  Endpoint information for a Gateway Server that can be used to access another
 *  Server. In these situations, the gatewayServerUri is specified in the
 *  EndpointDescription and all security checks used to verify certificate s shall
 *  use the gatewayServerUri instead of the serverUri.
 *
 *  To connect to a Server via the gateway, the Client shall first establish a
 *  SecureChannel with the Gateway Server. Then the Client shall call the
 *  CreateSession service and pass the serverUri specified in the
 *  EndpointDescription to the Gateway Server. The Gateway Server shall then
 *  connect to the underlying Server on behalf of the Client.
 *
 *  \var ua_getendpointsrequest::endpoint_url
 *  The network address that the Client used to access the Discovery Endpoint.
 *
 *  The Server uses this information for diagnostics and to determine what URLs to
 *  return in the response.
 *
 *  The Server should return a suitable default URL if it does not recognize the
 *  HostName in the URL.
 *
 *  \var ua_getendpointsrequest::num_locale_ids
 *  Number of elements in \ref ua_getendpointsrequest::locale_ids.
 *
 *  \var ua_getendpointsrequest::locale_ids
 *  List of locales to use.
 *
 *  Specifies the locale to use when returning human readable strings.
 *
 *  If the server supports more than one of the requested locales, the server shall
 *  use the locale that appears first in this list. If the server does not support
 *  any of the requested locales, it chooses an appropriate default locale.
 *
 *  The server chooses an appropriate default locale if this list is empty.
 *
 *  \var ua_getendpointsrequest::num_profile_uris
 *  Number of elements in \ref ua_getendpointsrequest::profile_uris.
 *
 *  \var ua_getendpointsrequest::profile_uris
 *  List of Transport Profile that the returned Endpoints shall support.
 *
 *  Part 7 of the OPC UA Specification defines URIs for the Transport Profiles.
 *
 *  All Endpoints are returned if the list is empty.
*/
struct ua_getendpointsrequest {
    struct ua_string endpoint_url;
    int32_t num_locale_ids;
    struct ua_string *locale_ids;
    int32_t num_profile_uris;
    struct ua_string *profile_uris;
};

void ua_getendpointsrequest_init(struct ua_getendpointsrequest *t);
void ua_getendpointsrequest_clear(struct ua_getendpointsrequest *t);

#endif /* _UABASE_GETENDPOINTSREQUEST_H_ */

