/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_SETMONITORINGMODEREQUEST_H_
#define _UABASE_SETMONITORINGMODEREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/structure/monitoringmode.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_setmonitoringmoderequest
 *  Asynchronous call to set the monitoring mode for a list of monitored items.
 *
 *  This Service is used to set the monitoring mode for one or more MonitoredItems
 *  of a Subscription. Setting the mode to DISABLED causes all queued notifications
 *  to be deleted.
 *
 *  \var ua_setmonitoringmoderequest::subscription_id
 *  The Server-assigned identifier for the Subscription used to qualify the
 *  monitoredItemIds.
 *
 *  \var ua_setmonitoringmoderequest::monitoring_mode
 *  The monitoring mode to be set for the MonitoredItems.
 *
 *  \var ua_setmonitoringmoderequest::num_monitored_item_ids
 *  Number of elements in \ref ua_setmonitoringmoderequest::monitored_item_ids.
 *
 *  \var ua_setmonitoringmoderequest::monitored_item_ids
 *  List of Server-assigned IDs for the MonitoredItems.
*/
struct ua_setmonitoringmoderequest {
    uint32_t subscription_id;
    enum ua_monitoringmode monitoring_mode;
    int32_t num_monitored_item_ids;
    uint32_t *monitored_item_ids;
};

void ua_setmonitoringmoderequest_init(struct ua_setmonitoringmoderequest *t);
void ua_setmonitoringmoderequest_clear(struct ua_setmonitoringmoderequest *t);

#endif /* _UABASE_SETMONITORINGMODEREQUEST_H_ */

