/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_OPENSECURECHANNELRESPONSE_H_
#define _UABASE_OPENSECURECHANNELRESPONSE_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/bytestring.h>
#include <uabase/structure/channelsecuritytoken.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_opensecurechannelresponse
 * Creates a secure channel with a server.
*/
struct ua_opensecurechannelresponse {
    uint32_t server_protocol_version;
    struct ua_channelsecuritytoken security_token;
    struct ua_bytestring server_nonce;
};

void ua_opensecurechannelresponse_init(struct ua_opensecurechannelresponse *t);
void ua_opensecurechannelresponse_clear(struct ua_opensecurechannelresponse *t);

#endif /* _UABASE_OPENSECURECHANNELRESPONSE_H_ */

