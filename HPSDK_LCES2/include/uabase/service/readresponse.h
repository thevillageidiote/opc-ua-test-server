/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_READRESPONSE_H_
#define _UABASE_READRESPONSE_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/datavalue.h>
#include <uabase/diagnosticinfo.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_readresponse
 *
 *  \var ua_readresponse::num_results
 *  Number of elements in \ref ua_readresponse::results.
 *
 *  \var ua_readresponse::results
 *  List of read results contained in an array of DataValue structures.
 *
 *  The DataValue consists of <dl> <dt>Value</dt> <dd>The value of the read node
 *  and attribute combination</dd> <dt>StatusCode</dt> <dd>This parameter is used
 *  to indicate the conditions under which the value was generated, and thereby can
 *  be used as an indicator of the usability of the value.</dd>
 *  <dt>SourceTimestamp</dt> <dd> Reflects the UTC timestamp that was applied to
 *  the value by the data source. It is only available for Value attributes.</dd>
 *  <dt>ServerTimestamp</dt> <dd>Reflects the time that the Server received the
 *  value or knew it to be accurate.</dd> </dl>
 *
 *  The size and order of this list matches the size and order of the request
 *  parameter. There is one entry in this list for each Node contained in the
 *  parameter.
 *
 *  \var ua_readresponse::num_diag_infos
 *  Number of elements in \ref ua_readresponse::diag_infos.
 *
 *  \var ua_readresponse::diag_infos
 *  List of diagnostic information.
 *
 *  The size and order of this list matches the size and order of the \ref
 *  ua_ReadRequest::nodes_to_read request parameter. There is one entry in this
 *  list for each Node contained in the \ref ua_ReadRequest::nodes_to_read
 *  parameter. This list is empty if diagnostics information was not requested in
 *  the request header or if no diagnostic information was encountered in
 *  processing of the request.
*/
struct ua_readresponse {
    int32_t num_results;
    struct ua_datavalue *results;
#ifdef ENABLE_DIAGNOSTICS
    int32_t num_diag_infos;
    struct ua_diagnosticinfo *diag_infos;
#endif
};

void ua_readresponse_init(struct ua_readresponse *t);
void ua_readresponse_clear(struct ua_readresponse *t);

#endif /* _UABASE_READRESPONSE_H_ */

