/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_ACTIVATESESSIONREQUEST_H_
#define _UABASE_ACTIVATESESSIONREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/extensionobject.h>
#include <uabase/string.h>
#include <uabase/structure/signaturedata.h>
#include <uabase/structure/signedsoftwarecertificate.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_activatesessionrequest
 * Activates a session with the server.
*/
struct ua_activatesessionrequest {
    struct ua_signaturedata client_signature;
    int32_t num_client_software_certificates;
    struct ua_signedsoftwarecertificate *client_software_certificates;
    int32_t num_locale_ids;
    struct ua_string *locale_ids;
    struct ua_extensionobject user_identity_token;
    struct ua_signaturedata user_token_signature;
};

void ua_activatesessionrequest_init(struct ua_activatesessionrequest *t);
void ua_activatesessionrequest_clear(struct ua_activatesessionrequest *t);

#endif /* _UABASE_ACTIVATESESSIONREQUEST_H_ */

