/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_DELETESUBSCRIPTIONSRESPONSE_H_
#define _UABASE_DELETESUBSCRIPTIONSRESPONSE_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/diagnosticinfo.h>
#include <uabase/statuscode.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_deletesubscriptionsresponse
 *
 *  \var ua_deletesubscriptionsresponse::num_results
 *  Number of elements in \ref ua_deletesubscriptionsresponse::results.
 *
 *  \var ua_deletesubscriptionsresponse::results
 *  List of StatusCodes for the Subscriptions to delete.
 *
 *  The size and order of the list matches the size and order of the
 *  subscriptionIds request parameter.
 *
 *  \var ua_deletesubscriptionsresponse::num_diag_infos
 *  Number of elements in \ref ua_deletesubscriptionsresponse::diag_infos.
 *
 *  \var ua_deletesubscriptionsresponse::diag_infos
 *  List of diagnostic information for the Subscriptions to delete.
 *
 *  The size and order of the list matches the size and order of the
 *  subscriptionIds request parameter. This list is empty if diagnostics
 *  information was not requested in the request header or if no diagnostic
 *  information was encountered in processing of the request.
*/
struct ua_deletesubscriptionsresponse {
    int32_t num_results;
    ua_statuscode *results;
#ifdef ENABLE_DIAGNOSTICS
    int32_t num_diag_infos;
    struct ua_diagnosticinfo *diag_infos;
#endif
};

void ua_deletesubscriptionsresponse_init(struct ua_deletesubscriptionsresponse *t);
void ua_deletesubscriptionsresponse_clear(struct ua_deletesubscriptionsresponse *t);

#endif /* _UABASE_DELETESUBSCRIPTIONSRESPONSE_H_ */

