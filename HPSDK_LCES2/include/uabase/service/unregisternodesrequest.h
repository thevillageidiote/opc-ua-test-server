/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_UNREGISTERNODESREQUEST_H_
#define _UABASE_UNREGISTERNODESREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/nodeid.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_unregisternodesrequest
 *  Asynchronously unregisters nodes to delete shortcuts in the server.
 *
 *  This Service is used to unregister NodeIds that have been obtained via the
 *  RegisterNodes service.
 *
 *  UnregisterNodes does not validate the NodeIds from the request. Servers shall
 *  simply unregister NodeIds that are known as registered NodeIds. Any NodeIds
 *  that are in the list, but are not registered NodeIds are simply ignored.
 *
 *  \var ua_unregisternodesrequest::num_nodes
 *  Number of elements in \ref ua_unregisternodesrequest::nodes.
 *
 *  \var ua_unregisternodesrequest::nodes
 *  A list of NodeIds that have been obtained via the RegisterNodes service.
*/
struct ua_unregisternodesrequest {
    int32_t num_nodes;
    struct ua_nodeid *nodes;
};

void ua_unregisternodesrequest_init(struct ua_unregisternodesrequest *t);
void ua_unregisternodesrequest_clear(struct ua_unregisternodesrequest *t);

#endif /* _UABASE_UNREGISTERNODESREQUEST_H_ */

