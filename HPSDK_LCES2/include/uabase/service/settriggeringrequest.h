/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_SETTRIGGERINGREQUEST_H_
#define _UABASE_SETTRIGGERINGREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_settriggeringrequest
 *  Asynchronously changes the triggering for a monitored item.
 *
 *  This service is used to create and delete triggering links for a triggering
 *  item. The triggering item and the items to report shall belong to the same
 *  subscription.
 *
 *  Each triggering link links a triggering item to an item to report. Each link is
 *  represented by the MonitoredItem id for the item to report. An error code is
 *  returned if this id is invalid.
 *
 *  \var ua_settriggeringrequest::subscription_id
 *  The Server-assigned identifier for the subscription that contains the
 *  triggering item and the items to report.
 *
 *  \var ua_settriggeringrequest::triggering_item_id
 *  Server-assigned ID for the MonitoredItem used as the triggering item.
 *
 *  \var ua_settriggeringrequest::num_links_to_add
 *  Number of elements in \ref ua_settriggeringrequest::links_to_add.
 *
 *  \var ua_settriggeringrequest::links_to_add
 *  The list of Server-assigned IDs of the items to report that are to be added as
 *  triggering links.
 *
 *  The list of linksToRemove is processed before the linksToAdd.
 *
 *  \var ua_settriggeringrequest::num_links_to_remove
 *  Number of elements in \ref ua_settriggeringrequest::links_to_remove.
 *
 *  \var ua_settriggeringrequest::links_to_remove
 *  The list of Server-assigned IDs of the items to report for the triggering links
 *  to be deleted.
 *
 *  The list of linksToRemove is processed before the linksToAdd.
*/
struct ua_settriggeringrequest {
    uint32_t subscription_id;
    uint32_t triggering_item_id;
    int32_t num_links_to_add;
    uint32_t *links_to_add;
    int32_t num_links_to_remove;
    uint32_t *links_to_remove;
};

void ua_settriggeringrequest_init(struct ua_settriggeringrequest *t);
void ua_settriggeringrequest_clear(struct ua_settriggeringrequest *t);

#endif /* _UABASE_SETTRIGGERINGREQUEST_H_ */

