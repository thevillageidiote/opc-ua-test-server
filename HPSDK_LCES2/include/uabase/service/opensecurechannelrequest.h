/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_OPENSECURECHANNELREQUEST_H_
#define _UABASE_OPENSECURECHANNELREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/bytestring.h>
#include <uabase/structure/messagesecuritymode.h>
#include <uabase/structure/securitytokenrequesttype.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_opensecurechannelrequest
 * Creates a secure channel with a server.
*/
struct ua_opensecurechannelrequest {
    uint32_t client_protocol_version;
    enum ua_securitytokenrequesttype request_type;
    enum ua_messagesecuritymode security_mode;
    struct ua_bytestring client_nonce;
    uint32_t requested_lifetime;
};

void ua_opensecurechannelrequest_init(struct ua_opensecurechannelrequest *t);
void ua_opensecurechannelrequest_clear(struct ua_opensecurechannelrequest *t);

#endif /* _UABASE_OPENSECURECHANNELREQUEST_H_ */

