/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_CREATEMONITOREDITEMSREQUEST_H_
#define _UABASE_CREATEMONITOREDITEMSREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/structure/monitoreditemcreaterequest.h>
#include <uabase/structure/timestampstoreturn.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_createmonitoreditemsrequest
 *  Asynchronous call to create a list of monitored items in the subscription.
 *
 *  This service is used to create and add one or more MonitoredItems to a
 *  Subscription. A MonitoredItem is deleted automatically by the Server when the
 *  Subscription is deleted. Deleting a MonitoredItem causes its entire set of
 *  triggered item links to be deleted, but has no effect on the MonitoredItems
 *  referenced by the triggered items.
 *
 *  Calling the CreateMonitoredItems service repetitively to add a small number of
 *  MonitoredItems each time may adversely affect the performance of the Server.
 *  Instead, Clients should add a complete set of MonitoredItems to a Subscription
 *  whenever possible.
 *
 *  When a user adds a monitored item that the user is denied read access to, the
 *  add operation for the item shall succeed and the bad status Bad_NotReadable or
 *  Bad_UserAccessDenied shall be returned in the Publish response. This is the
 *  same behaviour for the case where the access rights are changed after the call
 *  to CreateMonitoredItems. If the access rights change to read rights, the Server
 *  shall start sending data for the MonitoredItem. The same procedure shall be
 *  applied for an IndexRange that does not deliver data for the current value but
 *  could deliver data in the future.
 *
 *  Monitored Nodes can be removed from the AddressSpace after the creation of a
 *  MonitoredItem. This does not affect the validity of the MonitoredItem but a
 *  Bad_NodeIdUnknown shall be returned in the Publish response.
 *
 *  The return diagnostic info setting in the request header of the
 *  CreateMonitoredItems or the last ModifyMonitoredItems Service is applied to the
 *  Monitored Items and is used as the diagnostic information settings when sending
 *  notifications in the Publish response.
 *
 *  Illegal request values for parameters that can be revised do not generate
 *  errors. Instead, the server will choose default values and indicate them in the
 *  corresponding revised parameter.
 *
 *  \var ua_createmonitoreditemsrequest::subscription_id
 *  The Server-assigned identifier for the Subscription that will report
 *  Notifications for this MonitoredItem.
 *
 *  \var ua_createmonitoreditemsrequest::ts
 *  An enumeration that specifies the timestamp Attributes to be transmitted for
 *  each MonitoredItem.
 *
 *  When monitoring Events, this applies only to Event fields that are of type
 *  DataValue.
 *
 *  \var ua_createmonitoreditemsrequest::num_items_to_create
 *  Number of elements in \ref ua_createmonitoreditemsrequest::items_to_create.
 *
 *  \var ua_createmonitoreditemsrequest::items_to_create
 *  A list of MonitoredItems to be created and assigned to the specified
 *  Subscription.
*/
struct ua_createmonitoreditemsrequest {
    uint32_t subscription_id;
    enum ua_timestampstoreturn ts;
    int32_t num_items_to_create;
    struct ua_monitoreditemcreaterequest *items_to_create;
};

void ua_createmonitoreditemsrequest_init(struct ua_createmonitoreditemsrequest *t);
void ua_createmonitoreditemsrequest_clear(struct ua_createmonitoreditemsrequest *t);

#endif /* _UABASE_CREATEMONITOREDITEMSREQUEST_H_ */

