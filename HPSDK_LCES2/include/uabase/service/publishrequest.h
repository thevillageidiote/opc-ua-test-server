/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_PUBLISHREQUEST_H_
#define _UABASE_PUBLISHREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/structure/subscriptionacknowledgement.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_publishrequest
 *  Request the server to return NotificationMessages and acknowledge received
 *  NotificationMessages.
 *
 *  This Service is used for two purposes. First, it is used to acknowledge the
 *  receipt of NotificationMessages for one or more Subscriptions. Second, it is
 *  used to request the Server to return a NotificationMessage or a keep-alive
 *  message. Since Publish requests are not directed to a specific Subscription,
 *  they may be used by any Subscription.
 *
 *  Client strategies for issuing Publish requests may vary depending on the
 *  networking delays between the Client and the Server. In many cases, the Client
 *  may wish to issue a Publish request immediately after creating a Subscription,
 *  and thereafter, immediately after receiving a Publish response.
 *
 *  In other cases, especially in high latency networks, the Client may wish to
 *  pipeline Publish requests to ensure cyclic reporting from the Server.
 *  Pipelining involves sending more than one Publish request for each Subscription
 *  before receiving a response. For example, if the network introduces a delay
 *  between the Client and the Server of five seconds and the publishing interval
 *  for a Subscription is one second, then the Client will have to issue Publish
 *  requests every second instead of waiting for a response to be received before
 *  sending the next request.
 *
 *  A server should limit the number of active Publish requests to avoid an
 *  infinite number since it is expected that the Publish requests are queued in
 *  the Server. But a Server shall accept more queued Publish requests than created
 *  Subscriptions. It is expected that a Server supports several Publish requests
 *  per Subscription. When a Server receives a new Publish request that exceeds its
 *  limit it shall dequeue the oldest Publish request and return a response with
 *  the result set to Bad_TooManyPublishRequests. If a Client receives this Service
 *  result for a Publish request it shall not issue another Publish request before
 *  one of its outstanding Publish requests is returned from the Server.
 *
 *  Clients can limit the size of Publish responses with the
 *  maxNotificationsPerPublish parameter passed to the CreateSubscription Service.
 *  However, this could still result in a message that is too large for the Client
 *  or Server to process. In this situation, the Client will find that either the
 *  SecureChannel goes into a fault state and needs to be reestablished or the
 *  Publish response returns an error and calling the Republish Service also
 *  returns an error. If either situation occurs then the Client will have to
 *  adjust its message processing limits or the parameters for the Subscription
 *  and/or MonitoredItems.
 *
 *  The return diagnostic info setting in the request header of the
 *  CreateMonitoredItems or the last ModifyMonitoredItems Service is applied to the
 *  Monitored Items and is used as the diagnostic information settings when sending
 *  Notifications in the Publish response.
 *
 *  \var ua_publishrequest::num_subscription_acknowledgements
 *  Number of elements in \ref ua_publishrequest::subscription_acknowledgements.
 *
 *  \var ua_publishrequest::subscription_acknowledgements
 *  The list of acknowledgements for one or more Subscriptions.
 *
 *  This list may contain multiple acknowledgements for the same Subscription
 *  (multiple entries with the same).
*/
struct ua_publishrequest {
    int32_t num_subscription_acknowledgements;
    struct ua_subscriptionacknowledgement *subscription_acknowledgements;
};

void ua_publishrequest_init(struct ua_publishrequest *t);
void ua_publishrequest_clear(struct ua_publishrequest *t);

#endif /* _UABASE_PUBLISHREQUEST_H_ */

