/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_TRANSLATEBROWSEPATHSTONODEIDSREQUEST_H_
#define _UABASE_TRANSLATEBROWSEPATHSTONODEIDSREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/structure/browsepath.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_translatebrowsepathstonodeidsrequest
 *  Asynchronously translates a browse path to a NodeId.
 *
 *  This service is used to request that the Server translates one or more browse
 *  paths to NodeIds. Each browse path is constructed of a starting node and a
 *  RelativePath. The specified starting Node identifies the node from which the
 *  RelativePath is based. The RelativePath contains a sequence of ReferenceTypes
 *  and BrowseNames.
 *
 *  One purpose of this service is to allow programming against type definitions.
 *  Since BrowseNames shall be unique in the context of type definitions, a Client
 *  may create a browse path that is valid for a type definition and use this path
 *  on instances of the type. For example, an ObjectType “Boiler” may have a
 *  “HeatSensor” Variable as InstanceDeclaration. A graphical element programmed
 *  against the “Boiler” may need to display the Value of the “HeatSensor”. If the
 *  graphical element would be called on “Boiler1”, an instance of “Boiler”, it
 *  would need to call this Service specifying the NodeId of “Boiler1” as starting
 *  node and the BrowseName of the “HeatSensor” as browse path. The Service would
 *  return the NodeId of the “HeatSensor” of “Boiler1” and the graphical element
 *  could subscribe to its Value attribute.
 *
 *  If a node has multiple targets with the same BrowseName, the Server shall
 *  return a list of NodeIds. However, since one of the main purposes of this
 *  service is to support programming against type definitions, the NodeId of the
 *  node based on the type definition of the starting node is returned as the first
 *  NodeId in the list.
 *
 *  \var ua_translatebrowsepathstonodeidsrequest::num_browse_paths
 *  Number of elements in \ref ua_translatebrowsepathstonodeidsrequest::browse_paths.
 *
 *  \var ua_translatebrowsepathstonodeidsrequest::browse_paths
 *  A list of browse paths for which NodeIds are being requested.
*/
struct ua_translatebrowsepathstonodeidsrequest {
    int32_t num_browse_paths;
    struct ua_browsepath *browse_paths;
};

void ua_translatebrowsepathstonodeidsrequest_init(struct ua_translatebrowsepathstonodeidsrequest *t);
void ua_translatebrowsepathstonodeidsrequest_clear(struct ua_translatebrowsepathstonodeidsrequest *t);

#endif /* _UABASE_TRANSLATEBROWSEPATHSTONODEIDSREQUEST_H_ */

