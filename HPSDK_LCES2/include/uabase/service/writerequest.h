/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_WRITEREQUEST_H_
#define _UABASE_WRITEREQUEST_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/structure/writevalue.h>

/**
 *  \ingroup ua_base_service
 *  \struct ua_writerequest
 *  Asynchronously writes variable values to an OPC UA Server.
 *
 *  This service is used to write values to one or more attributes of one or more
 *  Nodes. For constructed attribute values whose elements are indexed, such as an
 *  array, this service allows Clients to write the entire set of indexed values as
 *  a composite, to write individual elements or to write ranges of elements of the
 *  composite.
 *
 *  The values are written to the data source, such as a device, and the service
 *  does not return until it writes the values or determines that the value cannot
 *  be written. In certain cases, the Server will successfully write to an
 *  intermediate system or Server, and will not know whether the data source was
 *  updated properly. In these cases, the Server should report a success code that
 *  indicates that the write was not verified. In the cases where the Server is
 *  able to verify that it has successfully written to the data source, it reports
 *  an unconditional success.
 *
 *  The order the operations are processed in the Server is not defined and depends
 *  on the different data sources and the internal Server logic. If an attribute
 *  and node combination is contained in more than one operation, the order of the
 *  processing is undefined. If a Client requires sequential processing, the Client
 *  needs separate service calls.
 *
 *  It is possible that the Server may successfully write some attributes, but not
 *  others. Rollback is the responsibility of the Client.
 *
 *  If a Server allows writing of attributes with the DataType LocalizedText, the
 *  Client can add or overwrite the text for a locale by writing the text with the
 *  associated LocaleId. Writing a null string for the text for a locale shall
 *  delete the String for that locale. Writing a null string for the locale and a
 *  non-null string for the text is setting the text for an invariant locale.
 *  Writing a null string for the text and a null string for the locale shall
 *  delete the entries for all locales. If a Client attempts to write a locale that
 *  is either syntactically invalid or not supported, the Server returns
 *  Bad_LocaleNotSupported.
 *
 *  \var ua_writerequest::num_nodes
 *  Number of elements in \ref ua_writerequest::nodes.
 *
 *  \var ua_writerequest::nodes
 *  List of Nodes and their Attributes to write.
*/
struct ua_writerequest {
    int32_t num_nodes;
    struct ua_writevalue *nodes;
};

void ua_writerequest_init(struct ua_writerequest *t);
void ua_writerequest_clear(struct ua_writerequest *t);

#endif /* _UABASE_WRITEREQUEST_H_ */

