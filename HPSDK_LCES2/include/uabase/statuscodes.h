/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



/**
 * @defgroup ua_statuscodes ua_statuscodes
 * @ingroup ua_base
 * @{
 */

#define UA_SCBAD 0x80000000 /**< Generic bad status code. */
#define UA_SCBADUNEXPECTEDERROR 0x80010000 /**< An unexpected error occurred. */
#define UA_SCBADINTERNALERROR 0x80020000 /**< An internal error occurred as a result of a programming or configuration error. */
#define UA_SCBADOUTOFMEMORY 0x80030000 /**< Not enough memory to complete the operation. */
#define UA_SCBADRESOURCEUNAVAILABLE 0x80040000 /**< An operating system resource is not available. */
#define UA_SCBADCOMMUNICATIONERROR 0x80050000 /**< A low level communication error occurred. */
#define UA_SCBADENCODINGERROR 0x80060000 /**< Encoding halted because of invalid data in the objects being serialized. */
#define UA_SCBADDECODINGERROR 0x80070000 /**< Decoding halted because of invalid data in the stream. */
#define UA_SCBADENCODINGLIMITSEXCEEDED 0x80080000 /**< The message encoding/decoding limits imposed by the stack have been exceeded. */
#define UA_SCBADREQUESTTOOLARGE 0x80B80000 /**< The request message size exceeds limits set by the server. */
#define UA_SCBADRESPONSETOOLARGE 0x80B90000 /**< The response message size exceeds limits set by the client. */
#define UA_SCBADUNKNOWNRESPONSE 0x80090000 /**< An unrecognized response was received from the server. */
#define UA_SCBADTIMEOUT 0x800A0000 /**< The operation timed out. */
#define UA_SCBADSERVICEUNSUPPORTED 0x800B0000 /**< The server does not support the requested service. */
#define UA_SCBADSHUTDOWN 0x800C0000 /**< The operation was cancelled because the application is shutting down. */
#define UA_SCBADSERVERNOTCONNECTED 0x800D0000 /**< The operation could not complete because the client is not connected to the server. */
#define UA_SCBADSERVERHALTED 0x800E0000 /**< The server has stopped and cannot process any requests. */
#define UA_SCBADNOTHINGTODO 0x800F0000 /**< There was nothing to do because the client passed a list of operations with no elements. */
#define UA_SCBADTOOMANYOPERATIONS 0x80100000 /**< The request could not be processed because it specified too many operations. */
#define UA_SCBADTOOMANYMONITOREDITEMS 0x80DB0000 /**< The request could not be processed because there are too many monitored items in the subscription. */
#define UA_SCBADDATATYPEIDUNKNOWN 0x80110000 /**< The extension object cannot be (de)serialized because the data type id is not recognized. */
#define UA_SCBADCERTIFICATEINVALID 0x80120000 /**< The certificate provided as a parameter is not valid. */
#define UA_SCBADSECURITYCHECKSFAILED 0x80130000 /**< An error occurred verifying security. */
#define UA_SCBADCERTIFICATETIMEINVALID 0x80140000 /**< The Certificate has expired or is not yet valid. */
#define UA_SCBADCERTIFICATEISSUERTIMEINVALID 0x80150000 /**< An Issuer Certificate has expired or is not yet valid. */
#define UA_SCBADCERTIFICATEHOSTNAMEINVALID 0x80160000 /**< The HostName used to connect to a Server does not match a HostName in the Certificate. */
#define UA_SCBADCERTIFICATEURIINVALID 0x80170000 /**< The URI specified in the ApplicationDescription does not match the URI in the Certificate. */
#define UA_SCBADCERTIFICATEUSENOTALLOWED 0x80180000 /**< The Certificate may not be used for the requested operation. */
#define UA_SCBADCERTIFICATEISSUERUSENOTALLOWED 0x80190000 /**< The Issuer Certificate may not be used for the requested operation. */
#define UA_SCBADCERTIFICATEUNTRUSTED 0x801A0000 /**< The Certificate is not trusted. */
#define UA_SCBADCERTIFICATEREVOCATIONUNKNOWN 0x801B0000 /**< It was not possible to determine if the Certificate has been revoked. */
#define UA_SCBADCERTIFICATEISSUERREVOCATIONUNKNOWN 0x801C0000 /**< It was not possible to determine if the Issuer Certificate has been revoked. */
#define UA_SCBADCERTIFICATEREVOKED 0x801D0000 /**< The certificate has been revoked. */
#define UA_SCBADCERTIFICATEISSUERREVOKED 0x801E0000 /**< The issuer certificate has been revoked. */
#define UA_SCBADCERTIFICATECHAININCOMPLETE 0x810D0000 /**< The certificate chain is incomplete. */
#define UA_SCBADUSERACCESSDENIED 0x801F0000 /**< User does not have permission to perform the requested operation. */
#define UA_SCBADIDENTITYTOKENINVALID 0x80200000 /**< The user identity token is not valid. */
#define UA_SCBADIDENTITYTOKENREJECTED 0x80210000 /**< The user identity token is valid but the server has rejected it. */
#define UA_SCBADSECURECHANNELIDINVALID 0x80220000 /**< The specified secure channel is no longer valid. */
#define UA_SCBADINVALIDTIMESTAMP 0x80230000 /**< The timestamp is outside the range allowed by the server. */
#define UA_SCBADNONCEINVALID 0x80240000 /**< The nonce does appear to be not a random value or it is not the correct length. */
#define UA_SCBADSESSIONIDINVALID 0x80250000 /**< The session id is not valid. */
#define UA_SCBADSESSIONCLOSED 0x80260000 /**< The session was closed by the client. */
#define UA_SCBADSESSIONNOTACTIVATED 0x80270000 /**< The session cannot be used because ActivateSession has not been called. */
#define UA_SCBADSUBSCRIPTIONIDINVALID 0x80280000 /**< The subscription id is not valid. */
#define UA_SCBADREQUESTHEADERINVALID 0x802A0000 /**< The header for the request is missing or invalid. */
#define UA_SCBADTIMESTAMPSTORETURNINVALID 0x802B0000 /**< The timestamps to return parameter is invalid. */
#define UA_SCBADREQUESTCANCELLEDBYCLIENT 0x802C0000 /**< The request was cancelled by the client. */
#define UA_SCBADTOOMANYARGUMENTS 0x80E50000 /**< Too many arguments were provided. */
#define UA_SCGOODSUBSCRIPTIONTRANSFERRED 0x002D0000 /**< The subscription was transferred to another session. */
#define UA_SCGOODCOMPLETESASYNCHRONOUSLY 0x002E0000 /**< The processing will complete asynchronously. */
#define UA_SCGOODOVERLOAD 0x002F0000 /**< Sampling has slowed down due to resource limitations. */
#define UA_SCGOODCLAMPED 0x00300000 /**< The value written was accepted but was clamped. */
#define UA_SCBADNOCOMMUNICATION 0x80310000 /**< Communication with the data source is defined, but not established, and there is no last known value available. */
#define UA_SCBADWAITINGFORINITIALDATA 0x80320000 /**< Waiting for the server to obtain values from the underlying data source. */
#define UA_SCBADNODEIDINVALID 0x80330000 /**< The syntax of the node id is not valid. */
#define UA_SCBADNODEIDUNKNOWN 0x80340000 /**< The node id refers to a node that does not exist in the server address space. */
#define UA_SCBADATTRIBUTEIDINVALID 0x80350000 /**< The attribute is not supported for the specified Node. */
#define UA_SCBADINDEXRANGEINVALID 0x80360000 /**< The syntax of the index range parameter is invalid. */
#define UA_SCBADINDEXRANGENODATA 0x80370000 /**< No data exists within the range of indexes specified. */
#define UA_SCBADDATAENCODINGINVALID 0x80380000 /**< The data encoding is invalid. */
#define UA_SCBADDATAENCODINGUNSUPPORTED 0x80390000 /**< The server does not support the requested data encoding for the node. */
#define UA_SCBADNOTREADABLE 0x803A0000 /**< The access level does not allow reading or subscribing to the Node. */
#define UA_SCBADNOTWRITABLE 0x803B0000 /**< The access level does not allow writing to the Node. */
#define UA_SCBADOUTOFRANGE 0x803C0000 /**< The value was out of range. */
#define UA_SCBADNOTSUPPORTED 0x803D0000 /**< The requested operation is not supported. */
#define UA_SCBADNOTFOUND 0x803E0000 /**< A requested item was not found or a search operation ended without success. */
#define UA_SCBADOBJECTDELETED 0x803F0000 /**< The object cannot be used because it has been deleted. */
#define UA_SCBADNOTIMPLEMENTED 0x80400000 /**< Requested operation is not implemented. */
#define UA_SCBADMONITORINGMODEINVALID 0x80410000 /**< The monitoring mode is invalid. */
#define UA_SCBADMONITOREDITEMIDINVALID 0x80420000 /**< The monitoring item id does not refer to a valid monitored item. */
#define UA_SCBADMONITOREDITEMFILTERINVALID 0x80430000 /**< The monitored item filter parameter is not valid. */
#define UA_SCBADMONITOREDITEMFILTERUNSUPPORTED 0x80440000 /**< The server does not support the requested monitored item filter. */
#define UA_SCBADFILTERNOTALLOWED 0x80450000 /**< A monitoring filter cannot be used in combination with the attribute specified. */
#define UA_SCBADSTRUCTUREMISSING 0x80460000 /**< A mandatory structured parameter was missing or null. */
#define UA_SCBADEVENTFILTERINVALID 0x80470000 /**< The event filter is not valid. */
#define UA_SCBADCONTENTFILTERINVALID 0x80480000 /**< The content filter is not valid. */
#define UA_SCBADFILTEROPERATORINVALID 0x80C10000 /**< An unregognized operator was provided in a filter. */
#define UA_SCBADFILTEROPERATORUNSUPPORTED 0x80C20000 /**< A valid operator was provided, but the server does not provide support for this filter operator. */
#define UA_SCBADFILTEROPERANDCOUNTMISMATCH 0x80C30000 /**< The number of operands provided for the filter operator was less then expected for the operand provided. */
#define UA_SCBADFILTEROPERANDINVALID 0x80490000 /**< The operand used in a content filter is not valid. */
#define UA_SCBADFILTERELEMENTINVALID 0x80C40000 /**< The referenced element is not a valid element in the content filter. */
#define UA_SCBADFILTERLITERALINVALID 0x80C50000 /**< The referenced literal is not a valid value. */
#define UA_SCBADCONTINUATIONPOINTINVALID 0x804A0000 /**< The continuation point provide is longer valid. */
#define UA_SCBADNOCONTINUATIONPOINTS 0x804B0000 /**< The operation could not be processed because all continuation points have been allocated. */
#define UA_SCBADREFERENCETYPEIDINVALID 0x804C0000 /**< The operation could not be processed because all continuation points have been allocated. */
#define UA_SCBADBROWSEDIRECTIONINVALID 0x804D0000 /**< The browse direction is not valid. */
#define UA_SCBADNODENOTINVIEW 0x804E0000 /**< The node is not part of the view. */
#define UA_SCBADSERVERURIINVALID 0x804F0000 /**< The ServerUri is not a valid URI. */
#define UA_SCBADSERVERNAMEMISSING 0x80500000 /**< No ServerName was specified. */
#define UA_SCBADDISCOVERYURLMISSING 0x80510000 /**< No DiscoveryUrl was specified. */
#define UA_SCBADSEMPAHOREFILEMISSING 0x80520000 /**< The semaphore file specified by the client is not valid. */
#define UA_SCBADREQUESTTYPEINVALID 0x80530000 /**< The security token request type is not valid. */
#define UA_SCBADSECURITYMODEREJECTED 0x80540000 /**< The security mode does not meet the requirements set by the Server. */
#define UA_SCBADSECURITYPOLICYREJECTED 0x80550000 /**< The security policy does not meet the requirements set by the Server. */
#define UA_SCBADTOOMANYSESSIONS 0x80560000 /**< The server has reached its maximum number of sessions. */
#define UA_SCBADUSERSIGNATUREINVALID 0x80570000 /**< The user token signature is missing or invalid. */
#define UA_SCBADAPPLICATIONSIGNATUREINVALID 0x80580000 /**< The signature generated with the client certificate is missing or invalid. */
#define UA_SCBADNOVALIDCERTIFICATES 0x80590000 /**< The client did not provide at least one software certificate that is valid and meets the profile requirements for the server. */
#define UA_SCBADIDENTITYCHANGENOTSUPPORTED 0x80C60000 /**< The Server does not support changing the user identity assigned to the session. */
#define UA_SCBADREQUESTCANCELLEDBYREQUEST 0x805A0000 /**< The request was cancelled by the client with the Cancel service. */
#define UA_SCBADPARENTNODEIDINVALID 0x805B0000 /**< The parent node id does not to refer to a valid node. */
#define UA_SCBADREFERENCENOTALLOWED 0x805C0000 /**< The reference could not be created because it violates constraints imposed by the data model. */
#define UA_SCBADNODEIDREJECTED 0x805D0000 /**< The requested node id was reject because it was either invalid or server does not allow node ids to be specified by the client. */
#define UA_SCBADNODEIDEXISTS 0x805E0000 /**< The requested node id is already used by another node. */
#define UA_SCBADNODECLASSINVALID 0x805F0000 /**< The node class is not valid. */
#define UA_SCBADBROWSENAMEINVALID 0x80600000 /**< The browse name is invalid. */
#define UA_SCBADBROWSENAMEDUPLICATED 0x80610000 /**< The browse name is not unique among nodes that share the same relationship with the parent. */
#define UA_SCBADNODEATTRIBUTESINVALID 0x80620000 /**< The node attributes are not valid for the node class. */
#define UA_SCBADTYPEDEFINITIONINVALID 0x80630000 /**< The type definition node id does not reference an appropriate type node. */
#define UA_SCBADSOURCENODEIDINVALID 0x80640000 /**< The source node id does not reference a valid node. */
#define UA_SCBADTARGETNODEIDINVALID 0x80650000 /**< The target node id does not reference a valid node. */
#define UA_SCBADDUPLICATEREFERENCENOTALLOWED 0x80660000 /**< The reference type between the nodes is already defined. */
#define UA_SCBADINVALIDSELFREFERENCE 0x80670000 /**< The server does not allow this type of self reference on this node. */
#define UA_SCBADREFERENCELOCALONLY 0x80680000 /**< The reference type is not valid for a reference to a remote server. */
#define UA_SCBADNODELETERIGHTS 0x80690000 /**< The server will not allow the node to be deleted. */
#define UA_SCUNCERTAINREFERENCENOTDELETED 0x40BC0000 /**< The server was not able to delete all target references. */
#define UA_SCBADSERVERINDEXINVALID 0x806A0000 /**< The server index is not valid. */
#define UA_SCBADVIEWIDUNKNOWN 0x806B0000 /**< The view id does not refer to a valid view node. */
#define UA_SCBADVIEWTIMESTAMPINVALID 0x80C90000 /**< The view timestamp is not available or not supported. */
#define UA_SCBADVIEWPARAMETERMISMATCH 0x80CA0000 /**< The view parameters are not consistent with each other. */
#define UA_SCBADVIEWVERSIONINVALID 0x80CB0000 /**< The view version is not available or not supported. */
#define UA_SCUNCERTAINNOTALLNODESAVAILABLE 0x40C00000 /**< The list of references may not be complete because the underlying system is not available. */
#define UA_SCGOODRESULTSMAYBEINCOMPLETE 0x00BA0000 /**< The server should have followed a reference to a node in a remote server but did not. The result set may be incomplete. */
#define UA_SCBADNOTTYPEDEFINITION 0x80C80000 /**< The provided Nodeid was not a type definition nodeid. */
#define UA_SCUNCERTAINREFERENCEOUTOFSERVER 0x406C0000 /**< One of the references to follow in the relative path references to a node in the address space in another server. */
#define UA_SCBADTOOMANYMATCHES 0x806D0000 /**< The requested operation has too many matches to return. */
#define UA_SCBADQUERYTOOCOMPLEX 0x806E0000 /**< The requested operation requires too many resources in the server. */
#define UA_SCBADNOMATCH 0x806F0000 /**< The requested operation has no match to return. */
#define UA_SCBADMAXAGEINVALID 0x80700000 /**< The max age parameter is invalid. */
#define UA_SCBADSECURITYMODEINSUFFICIENT 0x80E60000 /**< The operation is not permitted over the current secure channel. */
#define UA_SCBADHISTORYOPERATIONINVALID 0x80710000 /**< The history details parameter is not valid. */
#define UA_SCBADHISTORYOPERATIONUNSUPPORTED 0x80720000 /**< The server does not support the requested operation. */
#define UA_SCBADINVALIDTIMESTAMPARGUMENT 0x80BD0000 /**< The defined timestamp to return was invalid. */
#define UA_SCBADWRITENOTSUPPORTED 0x80730000 /**< The server not does support writing the combination of value, status and timestamps provided. */
#define UA_SCBADTYPEMISMATCH 0x80740000 /**< The value supplied for the attribute is not of the same type as the attribute's value. */
#define UA_SCBADMETHODINVALID 0x80750000 /**< The method id does not refer to a method for the specified object. */
#define UA_SCBADARGUMENTSMISSING 0x80760000 /**< The client did not specify all of the input arguments for the method. */
#define UA_SCBADTOOMANYSUBSCRIPTIONS 0x80770000 /**< The server has reached its  maximum number of subscriptions. */
#define UA_SCBADTOOMANYPUBLISHREQUESTS 0x80780000 /**< The server has reached the maximum number of queued publish requests. */
#define UA_SCBADNOSUBSCRIPTION 0x80790000 /**< There is no subscription available for this session. */
#define UA_SCBADSEQUENCENUMBERUNKNOWN 0x807A0000 /**< The sequence number is unknown to the server. */
#define UA_SCBADMESSAGENOTAVAILABLE 0x807B0000 /**< The requested notification message is no longer available. */
#define UA_SCBADINSUFFICIENTCLIENTPROFILE 0x807C0000 /**< The Client of the current Session does not support one or more Profiles that are necessary for the Subscription. */
#define UA_SCBADSTATENOTACTIVE 0x80BF0000 /**< The sub-state machine is not currently active. */
#define UA_SCBADTCPSERVERTOOBUSY 0x807D0000 /**< The server cannot process the request because it is too busy. */
#define UA_SCBADTCPMESSAGETYPEINVALID 0x807E0000 /**< The type of the message specified in the header invalid. */
#define UA_SCBADTCPSECURECHANNELUNKNOWN 0x807F0000 /**< The SecureChannelId and/or TokenId are not currently in use. */
#define UA_SCBADTCPMESSAGETOOLARGE 0x80800000 /**< The size of the message specified in the header is too large. */
#define UA_SCBADTCPNOTENOUGHRESOURCES 0x80810000 /**< There are not enough resources to process the request. */
#define UA_SCBADTCPINTERNALERROR 0x80820000 /**< An internal error occurred. */
#define UA_SCBADTCPENDPOINTURLINVALID 0x80830000 /**< The Server does not recognize the QueryString specified. */
#define UA_SCBADREQUESTINTERRUPTED 0x80840000 /**< The request could not be sent because of a network interruption. */
#define UA_SCBADREQUESTTIMEOUT 0x80850000 /**< Timeout occurred while processing the request. */
#define UA_SCBADSECURECHANNELCLOSED 0x80860000 /**< The secure channel has been closed. */
#define UA_SCBADSECURECHANNELTOKENUNKNOWN 0x80870000 /**< The token has expired or is not recognized. */
#define UA_SCBADSEQUENCENUMBERINVALID 0x80880000 /**< The sequence number is not valid. */
#define UA_SCBADPROTOCOLVERSIONUNSUPPORTED 0x80BE0000 /**< The applications do not have compatible protocol versions. */
#define UA_SCBADCONFIGURATIONERROR 0x80890000 /**< There is a problem with the configuration that affects the usefulness of the value. */
#define UA_SCBADNOTCONNECTED 0x808A0000 /**< The variable should receive its value from another variable, but has never been configured to do so. */
#define UA_SCBADDEVICEFAILURE 0x808B0000 /**< There has been a failure in the device/data source that generates the value that has affected the value. */
#define UA_SCBADSENSORFAILURE 0x808C0000 /**< There has been a failure in the sensor from which the value is derived by the device/data source. */
#define UA_SCBADOUTOFSERVICE 0x808D0000 /**< The source of the data is not operational. */
#define UA_SCBADDEADBANDFILTERINVALID 0x808E0000 /**< The deadband filter is not valid. */
#define UA_SCUNCERTAINNOCOMMUNICATIONLASTUSABLEVALUE 0x408F0000 /**< Communication to the data source has failed. The variable value is the last value that had a good quality. */
#define UA_SCUNCERTAINLASTUSABLEVALUE 0x40900000 /**< Whatever was updating this value has stopped doing so. */
#define UA_SCUNCERTAINSUBSTITUTEVALUE 0x40910000 /**< The value is an operational value that was manually overwritten. */
#define UA_SCUNCERTAININITIALVALUE 0x40920000 /**< The value is an initial value for a variable that normally receives its value from another variable. */
#define UA_SCUNCERTAINSENSORNOTACCURATE 0x40930000 /**< The value is at one of the sensor limits. */
#define UA_SCUNCERTAINENGINEERINGUNITSEXCEEDED 0x40940000 /**< The value is outside of the range of values defined for this parameter. */
#define UA_SCUNCERTAINSUBNORMAL 0x40950000 /**< The value is derived from multiple sources and has less than the required number of Good sources. */
#define UA_SCGOODLOCALOVERRIDE 0x00960000 /**< The value has been overridden. */
#define UA_SCBADREFRESHINPROGRESS 0x80970000 /**< This Condition refresh failed, a Condition refresh operation is already in progress. */
#define UA_SCBADCONDITIONALREADYDISABLED 0x80980000 /**< This condition has already been disabled. */
#define UA_SCBADCONDITIONALREADYENABLED 0x80CC0000 /**< This condition has already been enabled. */
#define UA_SCBADCONDITIONDISABLED 0x80990000 /**< Property not available, this condition is disabled. */
#define UA_SCBADEVENTIDUNKNOWN 0x809A0000 /**< The specified event id is not recognized. */
#define UA_SCBADEVENTNOTACKNOWLEDGEABLE 0x80BB0000 /**< The event cannot be acknowledged. */
#define UA_SCBADDIALOGNOTACTIVE 0x80CD0000 /**< The dialog condition is not active. */
#define UA_SCBADDIALOGRESPONSEINVALID 0x80CE0000 /**< The response is not valid for the dialog. */
#define UA_SCBADCONDITIONBRANCHALREADYACKED 0x80CF0000 /**< The condition branch has already been acknowledged. */
#define UA_SCBADCONDITIONBRANCHALREADYCONFIRMED 0x80D00000 /**< The condition branch has already been confirmed. */
#define UA_SCBADCONDITIONALREADYSHELVED 0x80D10000 /**< The condition has already been shelved. */
#define UA_SCBADCONDITIONNOTSHELVED 0x80D20000 /**< The condition is not currently shelved. */
#define UA_SCBADSHELVINGTIMEOUTOFRANGE 0x80D30000 /**< The shelving time not within an acceptable range. */
#define UA_SCBADNODATA 0x809B0000 /**< No data exists for the requested time range or event filter. */
#define UA_SCBADBOUNDNOTFOUND 0x80D70000 /**< No data found to provide upper or lower bound value. */
#define UA_SCBADBOUNDNOTSUPPORTED 0x80D80000 /**< The server cannot retrieve a bound for the variable. */
#define UA_SCBADDATALOST 0x809D0000 /**< Data is missing due to collection started/stopped/lost. */
#define UA_SCBADDATAUNAVAILABLE 0x809E0000 /**< Expected data is unavailable for the requested time range due to an un-mounted volume, an off-line archive or tape, or similar reason for temporary unavailability. */
#define UA_SCBADENTRYEXISTS 0x809F0000 /**< The data or event was not successfully inserted because a matching entry exists. */
#define UA_SCBADNOENTRYEXISTS 0x80A00000 /**< The data or event was not successfully updated because no matching entry exists. */
#define UA_SCBADTIMESTAMPNOTSUPPORTED 0x80A10000 /**< The client requested history using a timestamp format the server does not support (i.e requested ServerTimestamp when server only supports SourceTimestamp). */
#define UA_SCGOODENTRYINSERTED 0x00A20000 /**< The data or event was successfully inserted into the historical database. */
#define UA_SCGOODENTRYREPLACED 0x00A30000 /**< The data or event field was successfully replaced in the historical database. */
#define UA_SCUNCERTAINDATASUBNORMAL 0x40A40000 /**< The value is derived from multiple values and has less than the required number of Good values. */
#define UA_SCGOODNODATA 0x00A50000 /**< No data exists for the requested time range or event filter. */
#define UA_SCGOODMOREDATA 0x00A60000 /**< The data or event field was successfully replaced in the historical database. */
#define UA_SCBADAGGREGATELISTMISMATCH 0x80D40000 /**< The requested number of Aggregates does not match the requested number of NodeIds. */
#define UA_SCBADAGGREGATENOTSUPPORTED 0x80D50000 /**< The requested Aggregate is not support by the server. */
#define UA_SCBADAGGREGATEINVALIDINPUTS 0x80D60000 /**< The aggregate value could not be derived due to invalid data inputs. */
#define UA_SCBADAGGREGATECONFIGURATIONREJECTED 0x80DA0000 /**< The aggregate configuration is not valid for specified node. */
#define UA_SCGOODDATAIGNORED 0x00D90000 /**< The request pecifies fields which are not valid for the EventType or cannot be saved by the historian. */
#define UA_SCBADREQUESTNOTALLOWED 0x80E40000 /**< The request was rejected by the server because it did not meet the criteria set by the server. */
#define UA_SCGOODEDITED 0x00DC0000 /**< The value does not come from the real source and has been edited by the server. */
#define UA_SCGOODPOSTACTIONFAILED 0x00DD0000 /**< There was an error in execution of these post-actions. */
#define UA_SCUNCERTAINDOMINANTVALUECHANGED 0x40DE0000 /**< The related EngineeringUnit has been changed but the Variable Value is still provided based on the previous unit. */
#define UA_SCGOODDEPENDENTVALUECHANGED 0x00E00000 /**< A dependent value has been changed but the change has not been applied to the device. */
#define UA_SCBADDOMINANTVALUECHANGED 0x80E10000 /**< The related EngineeringUnit has been changed but this change has not been applied to the device. The Variable Value is still dependent on the previous unit but its status is currently Bad. */
#define UA_SCUNCERTAINDEPENDENTVALUECHANGED 0x40E20000 /**< A dependent value has been changed but the change has not been applied to the device. The quality of the dominant variable is uncertain. */
#define UA_SCBADDEPENDENTVALUECHANGED 0x80E30000 /**< A dependent value has been changed but the change has not been applied to the device. The quality of the dominant variable is Bad. */
#define UA_SCGOODCOMMUNICATIONEVENT 0x00A70000 /**< The communication layer has raised an event. */
#define UA_SCGOODSHUTDOWNEVENT 0x00A80000 /**< The system is shutting down. */
#define UA_SCGOODCALLAGAIN 0x00A90000 /**< The operation is not finished and needs to be called again. */
#define UA_SCGOODNONCRITICALTIMEOUT 0x00AA0000 /**< A non-critical timeout occurred. */
#define UA_SCBADINVALIDARGUMENT 0x80AB0000 /**< One or more arguments are invalid. */
#define UA_SCBADCONNECTIONREJECTED 0x80AC0000 /**< Could not establish a network connection to remote server. */
#define UA_SCBADDISCONNECT 0x80AD0000 /**< The server has disconnected from the client. */
#define UA_SCBADCONNECTIONCLOSED 0x80AE0000 /**< The network connection has been closed. */
#define UA_SCBADINVALIDSTATE 0x80AF0000 /**< The operation cannot be completed because the object is closed, uninitialized or in some other invalid state. */
#define UA_SCBADENDOFSTREAM 0x80B00000 /**< Cannot move beyond end of the stream. */
#define UA_SCBADNODATAAVAILABLE 0x80B10000 /**< No data is currently available for reading from a non-blocking stream. */
#define UA_SCBADWAITINGFORRESPONSE 0x80B20000 /**< The asynchronous operation is waiting for a response. */
#define UA_SCBADOPERATIONABANDONED 0x80B30000 /**< The asynchronous operation was abandoned by the caller. */
#define UA_SCBADEXPECTEDSTREAMTOBLOCK 0x80B40000 /**< The stream did not return all data requested (possibly because it is a non-blocking stream). */
#define UA_SCBADWOULDBLOCK 0x80B50000 /**< Non blocking behaviour is required and the operation would block. */
#define UA_SCBADSYNTAXERROR 0x80B60000 /**< A value had an invalid syntax. */
#define UA_SCBADMAXCONNECTIONSREACHED 0x80B70000 /**< The operation could not be finished because all available connections are in use. */

/** @} */

