/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_REQUESTHEADER_H_
#define _UABASE_REQUESTHEADER_H_

#include <uabase/nodeid.h>
#include <uabase/string.h>
#include <uabase/datetime.h>

/**
 * @ingroup ua_base
 * @brief Common parameters for all requests submitted on a Session.
 */
struct ua_requestheader {
    /** The secret Session identifier used to verify that the request
     *  is associated with the Session. */
    struct ua_nodeid authentication_token;
    /** The time the Client sent the request.
     *
     * The parameter is only used for diagnostic and logging purposes
     * in the server.*/
    ua_datetime timestamp;
    /** A handle associated with the request. This client defined handle can
     * be used to cancel the request. It is also returned in the response. */
    uint32_t request_handle;
    /** This timeout in milliseconds is used in the Client side
     * Communication Stack to set the timeout on a per-call base.
     *
     * For a Server this timeout is only a hint and can be used to
     * cancel long running operations to free resources. If the Server
     * detects a timeout, he can cancel the operation by sending the
     * Service result Bad_Timeout. The Server should wait at minimum
     * the timeout after he received the request before cancelling the
     * operation.
     *
     * The value of 0 indicates no timeout.*/
    uint32_t timeout_hint;
#ifdef ENABLE_DIAGNOSTICS
    /** A bit mask that identifies the types of vendor-specific diagnostics to
     * be returned in diagnosticInfo response parameters.
     *
     * The value of this parameter may consist of zero, one or more of
     * the following values. No value indicates that diagnostics are
     * not to be returned.
     *
     * Bit Value   | Diagnostics to return
     * ------------|--------------------------------
     * 0x0000 0001 | ServiceLevel/SymbolicId
     * 0x0000 0002 | ServiceLevel/LocalizedText
     * 0x0000 0004 | ServiceLevel/AdditionalInfo
     * 0x0000 0008 | ServiceLevel/Inner StatusCode
     * 0x0000 0010 | ServiceLevel/Inner Diagnostics
     * 0x0000 0020 | OperationLevel/SymbolicId
     * 0x0000 0040 | OperationLevel/LocalizedText
     * 0x0000 0080 | OperationLevel/AdditionalInfo
     * 0x0000 0100 | OperationLevel/Inner StatusCode
     * 0x0000 0200 | OperationLevel/Inner Diagnostics
     *
     * Each of these values is composed of two components, level and
     * type, as described below. If none are requested, as indicated
     * by a 0 value, or if no diagnostic information was encountered
     * in processing of the request, then diagnostics information is
     * not returned.
     *
     * Level:<br/>
     * <dl>
     *     <dt>ServiceLevel</dt>
     *     <dd>return diagnostics in the diagnosticInfo of the
     *     Service.</dd>
     *     <dt>OperationLevel</dt>
     *     <dd>return diagnostics in the diagnosticInfo defined for
     *     individual operations requested in the Service.</dd>
     * </dl>
     * Type:<br/>
     * <dl>
     *     <dt>SymbolicId</dt>
     *     <dd>return a namespace-qualified, symbolic identifier for
     *     an error or condition. The maximum length of this
     *     identifier is 32 characters.</dd>
     *     <dt>LocalizedText</dt>
     *     <dd>return up to 256 bytes of localized text that describes
     *     the symbolic id.</dd>
     *     <dt>AdditionalInfo</dt>
     *     <dd>return a byte string that contains additional
     *     diagnostic information, such as a memory image. The format
     *     of this byte string is vendor-specific, and may depend on
     *     the type of error or condition encountered.</dd>
     *     <dt>InnerStatusCode</dt>
     *     <dd>return the inner StatusCode associated with the
     *     operation or Service.</dd>
     *     <dt>InnerDiagnostics</dt>
     *     <dd>return the inner diagnostic info associated with the
     *     operation or Service. The contents of the inner diagnostic
     *     info structure are determined by other bits in the
     *     mask. Note that setting this bit could cause multiple
     *     levels of nested diagnostic info structures to be
     *     returned.</dd>
     * </dl> */
    uint32_t return_diagnostics;
#endif
#ifdef UA_SUPPORT_AUTDIT
    /** An identifier that identifies the Client’s security audit log entry
     * associated with this request.
     *
     * An empty string value means that this parameter is not used.
     *
     * The AuditEntryId typically contains who initiated the action
     * and from where it was initiated. The AuditEventId is included
     * in the AuditEvent to allow human readers to correlate an Event
     * with the initiating action.*/
    struct ua_string audit_entry_id;
#endif
    /* additionalHeader: reserverd for future use */
};

void ua_requestheader_init(struct ua_requestheader *h);
void ua_requestheader_clear(struct ua_requestheader *h);

#endif /* _UABASE_REQUESTHEADER_H_ */
