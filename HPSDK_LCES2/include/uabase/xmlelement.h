/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef UABASE_XMLELEMENT_H
#define UABASE_XMLELEMENT_H

#include <uabase/base_config.h>
#include <stdint.h>
#include "bytestring.h"

/**  @ingroup ua_base
 * @struct ua_xmlelement
 * An XmlElement contains an XML fragment in UTF8 encoding.
 *
 */
struct ua_xmlelement {
    struct ua_bytestring content;
};

/**
 * @memberof ua_xmlelement
 */
static inline void ua_xmlelement_init(struct ua_xmlelement *xml)
{
    ua_bytestring_init(&xml->content);
}

/**
 * @memberof ua_xmlelement
 */
static inline void ua_xmlelement_clear(struct ua_xmlelement *xml)
{
    ua_bytestring_clear(&xml->content);
}

/**
 * @memberof ua_xmlelement
 */
static inline int ua_xmlelement_compare(const struct ua_xmlelement *a, const struct ua_xmlelement *b)
{
    return ua_bytestring_compare(&a->content, &b->content);
}

/**
 * @memberof ua_xmlelement
 */
static inline int ua_xmlelement_copy(struct ua_xmlelement *dst, const struct ua_xmlelement *src)
{
    return ua_bytestring_copy(&dst->content, &src->content);
}

#ifdef ENABLE_TO_STRING
/**
 * @memberof ua_xmlelement
 */
static inline int ua_xmlelement_to_string(const struct ua_xmlelement *xml, struct ua_string *dst)
{
    return ua_bytestring_to_string(&xml->content, dst);
}
#endif

#ifdef ENABLE_FROM_STRING
/**
 * @memberof ua_xmlelement
 */
static inline int ua_xmlelement_from_string(struct ua_xmlelement *xml, const struct ua_string *src)
{
    return ua_bytestring_from_string(&xml->content, src);
}
#endif



#endif /* UABASE_XMLELEMENT_H */

