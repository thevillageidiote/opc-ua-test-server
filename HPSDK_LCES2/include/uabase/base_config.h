/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#ifndef BASE_CONFIG_H
#define BASE_CONFIG_H

#define PRODUCTFULLNAME    "embeddedstack"
#define SDK_VERSION        "1.0.0"
#define SDK_VERSION_SUFFIX " BETA"
#define      BUILDNUMBER        "1"

/* #undef CONFIG_THREADSAFE */

#define ENABLE_TO_STRING
#define ENABLE_FROM_STRING
/* #undef MEMORY_USE_SHM */
/* #undef ENABLE_DIAGNOSTICS */

#define ENABLE_VARIANT_ARRAY
/* #undef ENABLE_VARIANT_MATRIX */
/* #undef ENABLE_VARIANT_DATAVALUE */

/* optional attributes */
#define UA_SUPPORT_DESCRIPTION
#define UA_SUPPORT_HISTORIZING

/* map cmake options to defines used in code */
#ifdef ENABLE_VARIANT_ARRAY
# define UA_SUPPORT_ARRAY_IN_VARIANT
#endif
#ifdef ENABLE_VARIANT_MATRIX
# define UA_SUPPORT_MATRIX_IN_VARIANT
#endif
#ifdef ENABLE_VARIANT_DATAVALUE
# define UA_SUPPORT_DATAVALUE_IN_VARIANT
#endif

#endif /* BASE_CONFIG_H */

