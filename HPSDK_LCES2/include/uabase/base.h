/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UABASE_BASE_H
#define _UABASE_BASE_H

#include <platform/platform.h>
#include <stdint.h>
#include <stdbool.h>

/**
 * @defgroup ua_base_base ua_base
 * @ingroup ua_base
 * @brief General base library functionality.
 * @{
 */

/* forward declarations */
struct ua_encoder_context;
struct ua_decoder_context;

/** Interface for encode functions */
typedef int (*encode_t)(struct ua_encoder_context *, const void *);
/** Interface for decode functions */
typedef int (*decode_t)(struct ua_decoder_context *, void *);
/** Interface for compare functions */
typedef int (*compare_t)(const void *, const void *);
/** Interface for copy functions */
typedef int (*copy_t)(void *, const void *);
/** Interface for clear functions */
typedef void (*clear_t)(void *);

/* generated files call the uint8 byte */
#define ua_byte_compare(a, b) ua_uint8_compare(a, b)
#define ua_sbyte_compare(a, b) ua_int8_compare(a, b)

#define INT_COMPARE(name, type) static inline int ua_##name##_compare(const type *a, const type *b) \
{ \
    if (*a == *b) return 0; \
    if (*a < *b) return -1; \
    return 1; \
}

static inline int ua_boolean_compare(const bool *a, const bool *b)
{
    if (*a == *b) return 0;
    if (*a == false) return -1;
    return 1;
}

INT_COMPARE(int8, int8_t)
INT_COMPARE(uint8, uint8_t)
INT_COMPARE(int16, int16_t)
INT_COMPARE(uint16, uint16_t)
INT_COMPARE(int32, int32_t)
INT_COMPARE(uint32, uint32_t)
INT_COMPARE(int64, int64_t)
INT_COMPARE(uint64, uint64_t)
INT_COMPARE(float, float)
INT_COMPARE(double, double)
INT_COMPARE(int, int)

int ua_base_init(void);
void ua_base_cleanup(void);

/** @} */

#endif /* _UABASE_BASE_H */
