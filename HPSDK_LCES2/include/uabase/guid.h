/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef UABASE_GUID_H
#define UABASE_GUID_H

#include <uabase/base_config.h>
#include <stdint.h>
#include <stdlib.h> /* for size_t */
#include <platform/platform.h>

struct ua_string;

/** @ingroup ua_base
 * A 16-byte globally unique identifier.
 *
 * Its layout is shown in the following table:
 *
 * Component | Data Type
 * ----------|----------
 * Data1     | UInt32
 * Data2     | UInt16
 * Data3     | UInt16
 * Data4     | Byte[8]
 *
 * Guid values may be represented as a string in this form:<br/>
 * <tt>\<Data1\>-\<Data2\>-\<Data3\>-\<Data4[0:1]\>-\<Data4[2:7]\></tt>
 *
 * Where `Data1` is 8 characters wide, `Data2` and `Data3` are 4
 * characters wide and each Byte in `Data4` is 2 characters wide. Each
 * value is formatted as a hexadecimal number padded zeros. A typical
 * Guid value would look like this when formatted as a string:
 *
 * `C496578A-0DFE-4b8f-870A-745238C6AEAE`
 */
struct ua_guid {
    uint32_t data1;    /**< Data1 field. */
    uint16_t data2;    /**< Data2 field. */
    uint16_t data3;    /**< Data3 field. */
    uint8_t  data4[8]; /**< Data4 array. */
};

/**
 * Initializer for null guids.
 * @relates ua_guid
 */
#define UA_GUID_INITIALIZER { 0, 0, 0, {0} }

void ua_guid_init(struct ua_guid *g);
void ua_guid_clear(struct ua_guid *g);

int ua_guid_create(struct ua_guid *g);

int ua_guid_compare(const struct ua_guid *a, const struct ua_guid *b) UA_PURE_FUNCTION;
int ua_guid_copy(struct ua_guid *dst, const struct ua_guid *src);

#ifdef ENABLE_TO_STRING
int ua_guid_to_string(const struct ua_guid *src, struct ua_string *dst);
int ua_guid_snprintf(char *dst, size_t size, const struct ua_guid *src);
#endif /* ENABLE_TO_STRING */
#ifdef ENABLE_FROM_STRING
int ua_guid_from_string(struct ua_guid *dst, const struct ua_string *src);
#endif /* ENABLE_FROM_STRING */

#endif /* UABASE_GUID_H */
