/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_DATETIME_H_
#define _UABASE_DATETIME_H_

#include <uabase/base_config.h>
#include <stdint.h>
#include <time.h>

/**
 * @defgroup ua_base_datetime ua_datetime
 * This Built-in DataType defines a Gregorian calendar date.
 *
 * It is 64-bit signed integer representing the number of 100
 * nanoseconds intervals since 1601-01-01 00:00:00. This is the same
 * as the WIN32 FILETIME type.
 *
 * @see util_utctime
 *
 * @ingroup ua_base
 * @{
 */

/** Opaque ua_datetime.
 * Opaque means that you cannot make any assumption if the type's contents.
 * So don't access any fields directly. Always used the provided accessor
 * functions.
 */
typedef uint64_t ua_datetime;

struct ua_string;

void ua_datetime_init(ua_datetime *dt);
void ua_datetime_clear(ua_datetime *dt);

int ua_datetime_compare(const ua_datetime *a, const ua_datetime *b);

int ua_datetime_now(ua_datetime *dt);

#define ua_datetime_copy(dst, src) *(dst) = *(src)

time_t ua_datetime_to_time_t(const ua_datetime *dt);
void ua_datetime_from_time_t(ua_datetime *dt, const time_t *t);

#ifdef ENABLE_TO_STRING
int ua_datetime_to_string(const ua_datetime *dt, struct ua_string *dst);
int ua_datetime_snprintf(char *dst, size_t size, const ua_datetime *dt);
#endif /* ENABLE_TO_STRING */

#ifdef ENABLE_FROM_STRING
int ua_datetime_from_string(ua_datetime *dt, const struct ua_string *src);
#endif /* ENABLE_FROM_STRING */

/** @}*/

#endif /* _UABASE_DATETIME_H_ */

