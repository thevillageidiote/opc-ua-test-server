/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_EXTENSIONOBJECT_INTERNAL_H_
#define _UABASE_EXTENSIONOBJECT_INTERNAL_H_

#include "extensionobject.h"

#define UA_EXTENSIONOBJECT_NS_TABLE_SIZE 16

/* Map to store nsindex as key and function pointers as value. */
extern struct util_map g_extensionobject_map;

/* One entry in the g_extensionobject_map */
struct ua_extensionobject_ns_entry {
    ua_extensionobject_lookup_type_t lookup_type;
    void *type_data;
    ua_extensionobject_lookup_encoding_t lookup_encoding;
    void *encoding_data;
};

struct ua_extensionobject_base_properties {
    uint32_t type_id;
    uint32_t bin_enc_id; /* binary encoding id */
    size_t size;
    compare_t compare;
    copy_t copy;
    clear_t clear;
};

struct ua_extensionobject_encoder_properties {
    decode_t decode;
    encode_t encode;
};

void ua_extensionobject_set_ns0_encoder(const struct ua_extensionobject_encoder_properties *encoder_table);

int ua_extensionobject_ns_table_init(int num_elements);
void ua_extensionobject_ns_table_clear(void);

int ua_extensionobject_lookup_properties(const struct ua_nodeid *type_id, struct ua_extensionobject_properties *prop);
int ua_extensionobject_lookup_properties_by_encoding(const struct ua_nodeid *encoding_id, struct ua_extensionobject_properties *prop);

#endif /* _UABASE_EXTENSIONOBJECT_INTERNAL_H_ */
