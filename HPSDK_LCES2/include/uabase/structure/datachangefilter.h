/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_DATACHANGEFILTER_H_
#define _UABASE_DATACHANGEFILTER_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/structure/datachangetrigger.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_datachangefilter
*/
struct ua_datachangefilter {
    enum ua_datachangetrigger trigger;
    uint32_t deadband_type;
    double deadband_value;
};

void ua_datachangefilter_init(struct ua_datachangefilter *t);
void ua_datachangefilter_clear(struct ua_datachangefilter *t);
int ua_datachangefilter_compare(const struct ua_datachangefilter *a, const struct ua_datachangefilter *b) UA_PURE_FUNCTION;
int ua_datachangefilter_copy(struct ua_datachangefilter *dst, const struct ua_datachangefilter *src);

#endif /* _UABASE_DATACHANGEFILTER_H_ */

