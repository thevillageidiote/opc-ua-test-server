/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_EVENTFIELDLIST_H_
#define _UABASE_EVENTFIELDLIST_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/variant.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_eventfieldlist
*/
struct ua_eventfieldlist {
    uint32_t client_handle;
    int32_t num_event_fields;
    struct ua_variant *event_fields;
};

void ua_eventfieldlist_init(struct ua_eventfieldlist *t);
void ua_eventfieldlist_clear(struct ua_eventfieldlist *t);
int ua_eventfieldlist_compare(const struct ua_eventfieldlist *a, const struct ua_eventfieldlist *b) UA_PURE_FUNCTION;
int ua_eventfieldlist_copy(struct ua_eventfieldlist *dst, const struct ua_eventfieldlist *src);

#endif /* _UABASE_EVENTFIELDLIST_H_ */

