/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_AXISINFORMATION_H_
#define _UABASE_AXISINFORMATION_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/localizedtext.h>
#include <uabase/structure/axisscaleenumeration.h>
#include <uabase/structure/euinformation.h>
#include <uabase/structure/range.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_axisinformation
 *  Defines the information for auxiliary axis for ArrayItemType Variables.
 *
 *  There are three typical uses of this structure:
 *
 *  1. The step between points is constant and can be predicted using the range
 *  information and the number of points. In this case, axisSteps can be set to
 *  NULL.
 *
 *  2. The step between points is not constant, but remains the same for a long
 *  period of time (from acquisition to acquisition for example). In this case,
 *  axisSteps contains the value of each step on the axis.
 *
 *  3. The step between points is not constant and changes at every update. In this
 *  case, a type like XYArrayType shall be used and axisSteps is set to NULL.
 *
 *  When the steps in the axis are constant, axisSteps may be set to “Null” and in
 *  this case, the Range limits are used to compute the steps. The number of steps
 *  in the axis comes from the parent ArrayItem.ArrayDimensions.
 *
 *  \var ua_axisinformation::engineering_units
 *  Holds the information about the engineering units for a given axis.
 *
 *  \var ua_axisinformation::eurange
 *  Limits of the range of the axis
 *
 *  \var ua_axisinformation::title
 *  User readable axis title.
 *
 *  This is useful when the units are \%. The Title may be e.g. “Particle size
 *  distribution”
 *
 *  \var ua_axisinformation::axis_scale_type
 *  LINEAR, LOG, LN, defined by AxisSteps (see \ref ua_axisscaleenumeration).
 *
 *  \var ua_axisinformation::num_axis_steps
 *  Number of elements in \ref ua_axisinformation::axis_steps.
 *
 *  \var ua_axisinformation::axis_steps
 *  Specific value of each axis step.
 *
 *  May be set to “Null” if not used.
*/
struct ua_axisinformation {
    struct ua_euinformation engineering_units;
    struct ua_range eurange;
    struct ua_localizedtext title;
    enum ua_axisscaleenumeration axis_scale_type;
    int32_t num_axis_steps;
    double *axis_steps;
};

void ua_axisinformation_init(struct ua_axisinformation *t);
void ua_axisinformation_clear(struct ua_axisinformation *t);
int ua_axisinformation_compare(const struct ua_axisinformation *a, const struct ua_axisinformation *b) UA_PURE_FUNCTION;
int ua_axisinformation_copy(struct ua_axisinformation *dst, const struct ua_axisinformation *src);

#endif /* _UABASE_AXISINFORMATION_H_ */

