/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_ATTRIBUTEOPERAND_H_
#define _UABASE_ATTRIBUTEOPERAND_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/nodeid.h>
#include <uabase/string.h>
#include <uabase/structure/relativepath.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_attributeoperand
 *  Attribute of a node in the address space.
 *
 *  \var ua_attributeoperand::node_id
 *  NodeId of a Node from the type system.
 *
 *  \var ua_attributeoperand::alias
 *  An optional parameter used to identify or refer to an alias.
 *
 *  An alias is a symbolic name that can be used to alias this operand and use it
 *  in other locations in the filter structure.
 *
 *  \var ua_attributeoperand::browse_path
 *  Browse path relative to the node identified by the nodeId parameter.
 *
 *  \var ua_attributeoperand::attribute_id
 *  ID of the attribute.
 *
 *  This shall be a valid.
 *
 *  \var ua_attributeoperand::index_range
 *  This parameter is used to identify a single element of an array or a single
 *  range of indexes for an array.
 *
 *  The first element is identified by index 0 (zero). This parameter is not used
 *  if the specified attribute is not an array. However, if the specified attribute
 *  is an array and this parameter is not used, then all elements are to be
 *  included in the range. The parameter is null if not used.
*/
struct ua_attributeoperand {
    struct ua_nodeid node_id;
    struct ua_string alias;
    struct ua_relativepath browse_path;
    uint32_t attribute_id;
    struct ua_string index_range;
};

void ua_attributeoperand_init(struct ua_attributeoperand *t);
void ua_attributeoperand_clear(struct ua_attributeoperand *t);
int ua_attributeoperand_compare(const struct ua_attributeoperand *a, const struct ua_attributeoperand *b) UA_PURE_FUNCTION;
int ua_attributeoperand_copy(struct ua_attributeoperand *dst, const struct ua_attributeoperand *src);

#endif /* _UABASE_ATTRIBUTEOPERAND_H_ */

