/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_EVENTFILTERRESULT_H_
#define _UABASE_EVENTFILTERRESULT_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/diagnosticinfo.h>
#include <uabase/statuscode.h>
#include <uabase/structure/contentfilterresult.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_eventfilterresult
 *  This is the MonitoringFilterResult associated with the EventFilter
 *  MonitoringFilter.
 *
 *  \var ua_eventfilterresult::num_select_clause_results
 *  Number of elements in \ref ua_eventfilterresult::select_clause_results.
 *
 *  \var ua_eventfilterresult::select_clause_results
 *  List of status codes for the elements in the select clause.
 *
 *  The size and order of the list matches the size and order of the elements in
 *  the selectClauses request parameter. The Server returns null for unavailable or
 *  rejected Event fields.
 *
 *  \var ua_eventfilterresult::num_select_clause_diag_infos
 *  Number of elements in \ref ua_eventfilterresult::select_clause_diag_infos.
 *
 *  \var ua_eventfilterresult::select_clause_diag_infos
 *  A list of diagnostic information for individual elements in the select clause.
 *
 *  The size and order of the list matches the size and order of the elements in
 *  the selectClauses request parameter. This list is empty if diagnostics
 *  information was not requested in the request header or if no diagnostic
 *  information was encountered in processing of the select clauses.
 *
 *  \var ua_eventfilterresult::where_clause_result
 *  Any results associated with the whereClause request parameter.
*/
struct ua_eventfilterresult {
    int32_t num_select_clause_results;
    ua_statuscode *select_clause_results;
#ifdef ENABLE_DIAGNOSTICS
    int32_t num_select_clause_diag_infos;
    struct ua_diagnosticinfo *select_clause_diag_infos;
#endif
    struct ua_contentfilterresult where_clause_result;
};

void ua_eventfilterresult_init(struct ua_eventfilterresult *t);
void ua_eventfilterresult_clear(struct ua_eventfilterresult *t);
int ua_eventfilterresult_compare(const struct ua_eventfilterresult *a, const struct ua_eventfilterresult *b) UA_PURE_FUNCTION;
int ua_eventfilterresult_copy(struct ua_eventfilterresult *dst, const struct ua_eventfilterresult *src);

#endif /* _UABASE_EVENTFILTERRESULT_H_ */

