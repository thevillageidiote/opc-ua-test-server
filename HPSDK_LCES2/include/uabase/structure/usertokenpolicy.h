/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_USERTOKENPOLICY_H_
#define _UABASE_USERTOKENPOLICY_H_

#include <platform/platform.h>
#include <uabase/base_config.h>
#include <uabase/string.h>
#include <uabase/structure/usertokentype.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_usertokenpolicy
 *  Specifies a UserIdentityToken that a Server will accept.
 *
 *  \var ua_usertokenpolicy::policy_id
 *  An identifier for the UserTokenPolicy assigned by the Server.
 *
 *  The Client specifies this value when it constructs a UserIdentityToken that
 *  conforms to the policy.
 *
 *  This value is only unique within the context of a single Server.
 *
 *  \var ua_usertokenpolicy::token_type
 *  The type of user identity token required.
 *
 *  A tokenType of ANONYMOUS indicates that the Server does not require any user
 *  identification. In this case the Client application instance certificate is
 *  used as the user identification.
 *
 *  \var ua_usertokenpolicy::issued_token_type
 *  A URI for the type of token.
 *
 *  Part 6 of the OPC UA Specification defines URIs for common issued token types.
 *  Vendors may specify their own token.
 *
 *  This field may only be specified if TokenType is IssuedToken.
 *
 *  \var ua_usertokenpolicy::issuer_endpoint_url
 *  An optional URL for the token issuing service.
 *
 *  The meaning of this value depends on the IssuedTokenType.
 *
 *  \var ua_usertokenpolicy::security_policy_uri
 *  The security policy to use when encrypting or signing the UserIdentityToken
 *  when it is passed to the Server in the ActivateSession request.
 *
 *  The security policy for the SecureChannel is used if this value is omitted.
*/
struct ua_usertokenpolicy {
    struct ua_string policy_id;
    enum ua_usertokentype token_type;
    struct ua_string issued_token_type;
    struct ua_string issuer_endpoint_url;
    struct ua_string security_policy_uri;
};

void ua_usertokenpolicy_init(struct ua_usertokenpolicy *t);
void ua_usertokenpolicy_clear(struct ua_usertokenpolicy *t);
int ua_usertokenpolicy_compare(const struct ua_usertokenpolicy *a, const struct ua_usertokenpolicy *b) UA_PURE_FUNCTION;
int ua_usertokenpolicy_copy(struct ua_usertokenpolicy *dst, const struct ua_usertokenpolicy *src);

#endif /* _UABASE_USERTOKENPOLICY_H_ */

