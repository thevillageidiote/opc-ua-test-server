/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_IDTYPE_H
#define _UABASE_IDTYPE_H

#include <uabase/base_config.h>

/**
 * @defgroup ua_base_idtype ua_idtype
 * @ingroup ua_base_enums
 * @brief
 *  An enumeration that identifies the IdType of a NodeId.
 * @{
 */

/** \enum ua_idtype
 *  An enumeration that identifies the IdType of a NodeId.
 *
 *  \var UA_IDTYPE_NUMERIC
 *  Numeric value
 *
 *  \var UA_IDTYPE_STRING
 *  String value
 *
 *  \var UA_IDTYPE_GUID
 *  Globally Unique Identifier
 *
 *  \var UA_IDTYPE_OPAQUE
 *  Namespace specific format
*/
enum ua_idtype
{
    UA_IDTYPE_NUMERIC = 0,
    UA_IDTYPE_STRING = 1,
    UA_IDTYPE_GUID = 2,
    UA_IDTYPE_OPAQUE = 3
};

#ifdef ENABLE_TO_STRING
const char* ua_idtype_to_string(enum ua_idtype idtype);
#endif /* ENABLE_TO_STRING */

/** @}*/

#endif /*_UABASE_IDTYPE_H*/
