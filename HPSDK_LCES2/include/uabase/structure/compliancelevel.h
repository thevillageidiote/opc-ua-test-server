/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_COMPLIANCELEVEL_H
#define _UABASE_COMPLIANCELEVEL_H

#include <uabase/base_config.h>

/**
 * @defgroup ua_base_compliancelevel ua_compliancelevel
 * @ingroup ua_base_enums
 * @brief
 *  Specifies the compliance level of the profile.
 * @{
 */

/** \enum ua_compliancelevel
 *  Specifies the compliance level of the profile.
 *
 *  \var UA_COMPLIANCELEVEL_UNTESTED
 *  The profiled capability has not been tested successfully
 *
 *  \var UA_COMPLIANCELEVEL_PARTIAL
 *  The profiled capability has been partially tested and has passed critical
 *  tests, as defined by the certifying authority.
 *
 *  \var UA_COMPLIANCELEVEL_SELFTESTED
 *  The profiled capability has been successfully tested using a self-test system
 *  authorized by the certifying authority.
 *
 *  \var UA_COMPLIANCELEVEL_CERTIFIED
 *  The profiled capability has been successfully tested by a testing organisation
 *  authorized by the certifying authority.
*/
enum ua_compliancelevel
{
    UA_COMPLIANCELEVEL_UNTESTED = 0,
    UA_COMPLIANCELEVEL_PARTIAL = 1,
    UA_COMPLIANCELEVEL_SELFTESTED = 2,
    UA_COMPLIANCELEVEL_CERTIFIED = 3
};

#ifdef ENABLE_TO_STRING
const char* ua_compliancelevel_to_string(enum ua_compliancelevel compliancelevel);
#endif /* ENABLE_TO_STRING */

/** @}*/

#endif /*_UABASE_COMPLIANCELEVEL_H*/
