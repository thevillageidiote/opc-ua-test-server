/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_CHANNELSECURITYTOKEN_H_
#define _UABASE_CHANNELSECURITYTOKEN_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/datetime.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_channelsecuritytoken
 * The token that identifies a set of keys for an active secure channel.
*/
struct ua_channelsecuritytoken {
    uint32_t channel_id;
    uint32_t token_id;
    ua_datetime created_at;
    uint32_t revised_lifetime;
};

void ua_channelsecuritytoken_init(struct ua_channelsecuritytoken *t);
void ua_channelsecuritytoken_clear(struct ua_channelsecuritytoken *t);
int ua_channelsecuritytoken_compare(const struct ua_channelsecuritytoken *a, const struct ua_channelsecuritytoken *b) UA_PURE_FUNCTION;
int ua_channelsecuritytoken_copy(struct ua_channelsecuritytoken *dst, const struct ua_channelsecuritytoken *src);

#endif /* _UABASE_CHANNELSECURITYTOKEN_H_ */

