/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_APPLICATIONDESCRIPTION_H_
#define _UABASE_APPLICATIONDESCRIPTION_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/localizedtext.h>
#include <uabase/string.h>
#include <uabase/structure/applicationtype.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_applicationdescription
 *  Specifies an application that is available.
 *
 *  \var ua_applicationdescription::application_uri
 *  The globally unique identifier for the application instance.
 *
 *  This URI is used as ServerUri in Services if the application is a Server.
 *
 *  \var ua_applicationdescription::product_uri
 *  The globally unique identifier for the product.
 *
 *  \var ua_applicationdescription::application_name
 *  A localized descriptive name for the application.
 *
 *  \var ua_applicationdescription::application_type
 *  The type of application
 *
 *  See \ref ua_applicationtype
 *
 *  \var ua_applicationdescription::gateway_server_uri
 *  A URI that identifies the Gateway Server associated with the DiscoveryUrls.
 *
 *  This value is not specified if the Server can be accessed directly.
 *
 *  This field is not used if the ApplicationType is Client.
 *
 *  \var ua_applicationdescription::discovery_profile_uri
 *  A URI that identifies the discovery profile supported by the URLs provided.
 *
 *  This field is not used if the ApplicationType is client.
 *
 *  \var ua_applicationdescription::num_discovery_urls
 *  Number of elements in \ref ua_applicationdescription::discovery_urls.
 *
 *  \var ua_applicationdescription::discovery_urls
 *  A list of URLs for the discovery Endpoints provided by the application.
 *
 *  If the ApplicationType is Client, this field shall contain an empty list.
*/
struct ua_applicationdescription {
    struct ua_string application_uri;
    struct ua_string product_uri;
    struct ua_localizedtext application_name;
    enum ua_applicationtype application_type;
    struct ua_string gateway_server_uri;
    struct ua_string discovery_profile_uri;
    int32_t num_discovery_urls;
    struct ua_string *discovery_urls;
};

void ua_applicationdescription_init(struct ua_applicationdescription *t);
void ua_applicationdescription_clear(struct ua_applicationdescription *t);
int ua_applicationdescription_compare(const struct ua_applicationdescription *a, const struct ua_applicationdescription *b) UA_PURE_FUNCTION;
int ua_applicationdescription_copy(struct ua_applicationdescription *dst, const struct ua_applicationdescription *src);

#endif /* _UABASE_APPLICATIONDESCRIPTION_H_ */

