/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_AGGREGATEFILTER_H_
#define _UABASE_AGGREGATEFILTER_H_

#include <platform/platform.h>
#include <uabase/base_config.h>
#include <uabase/datetime.h>
#include <uabase/nodeid.h>
#include <uabase/structure/aggregateconfiguration.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_aggregatefilter
*/
struct ua_aggregatefilter {
    ua_datetime start_time;
    struct ua_nodeid aggregate_type;
    double processing_interval;
    struct ua_aggregateconfiguration aggregate_configuration;
};

void ua_aggregatefilter_init(struct ua_aggregatefilter *t);
void ua_aggregatefilter_clear(struct ua_aggregatefilter *t);
int ua_aggregatefilter_compare(const struct ua_aggregatefilter *a, const struct ua_aggregatefilter *b) UA_PURE_FUNCTION;
int ua_aggregatefilter_copy(struct ua_aggregatefilter *dst, const struct ua_aggregatefilter *src);

#endif /* _UABASE_AGGREGATEFILTER_H_ */

