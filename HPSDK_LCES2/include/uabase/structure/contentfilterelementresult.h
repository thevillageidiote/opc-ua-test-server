/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_CONTENTFILTERELEMENTRESULT_H_
#define _UABASE_CONTENTFILTERELEMENTRESULT_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/diagnosticinfo.h>
#include <uabase/statuscode.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_contentfilterelementresult
 *  A structure that is defined as the type of the elementResults parameter of the
 *  ContentFilterResult structure.
 *
 *  The size and order of the list matches the size and order of the elements in
 *  the ContentFilter parameter.
 *
 *  \var ua_contentfilterelementresult::status_code
 *  The status code for a single element.
 *
 *  \var ua_contentfilterelementresult::num_operand_status_codes
 *  Number of elements in \ref ua_contentfilterelementresult::operand_status_codes.
 *
 *  \var ua_contentfilterelementresult::operand_status_codes
 *  A list of status codes for the operands in an element.
 *
 *  The size and order of the list matches the size and order of the operands in
 *  the ContentFilterElement. This list is empty if no operand errors occurred.
 *
 *  \var ua_contentfilterelementresult::num_operand_diag_infos
 *  Number of elements in \ref ua_contentfilterelementresult::operand_diag_infos.
 *
 *  \var ua_contentfilterelementresult::operand_diag_infos
 *  A list of diagnostic information for the operands in an element.
 *
 *  The size and order of the list matches the size and order of the operands in
 *  the ContentFilterElement. This list is empty if diagnostics information was not
 *  requested in the request header or if no diagnostic information was encountered
 *  in processing of the operands. A list of diagnostic information for individual
*/
struct ua_contentfilterelementresult {
    ua_statuscode status_code;
    int32_t num_operand_status_codes;
    ua_statuscode *operand_status_codes;
#ifdef ENABLE_DIAGNOSTICS
    int32_t num_operand_diag_infos;
    struct ua_diagnosticinfo *operand_diag_infos;
#endif
};

void ua_contentfilterelementresult_init(struct ua_contentfilterelementresult *t);
void ua_contentfilterelementresult_clear(struct ua_contentfilterelementresult *t);
int ua_contentfilterelementresult_compare(const struct ua_contentfilterelementresult *a, const struct ua_contentfilterelementresult *b) UA_PURE_FUNCTION;
int ua_contentfilterelementresult_copy(struct ua_contentfilterelementresult *dst, const struct ua_contentfilterelementresult *src);

#endif /* _UABASE_CONTENTFILTERELEMENTRESULT_H_ */

