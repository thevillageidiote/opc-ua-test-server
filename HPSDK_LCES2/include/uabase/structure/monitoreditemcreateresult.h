/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_MONITOREDITEMCREATERESULT_H_
#define _UABASE_MONITOREDITEMCREATERESULT_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/extensionobject.h>
#include <uabase/statuscode.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_monitoreditemcreateresult
 *  A structure that is defined as the type of the results parameter of the
 *  CreateMonitoredItems service.
 *
 *  \var ua_monitoreditemcreateresult::status_code
 *  StatusCode for the MonitoredItem to create.
 *
 *  \var ua_monitoreditemcreateresult::monitored_item_id
 *  Server-assigned ID for the MonitoredItem.
 *
 *  This ID is unique within the Subscription, but might not be unique within the
 *  Server or Session. This parameter is present only if the statusCode indicates
 *  that the MonitoredItem was successfully created.
 *
 *  \var ua_monitoreditemcreateresult::revised_sampling_interval
 *  The actual sampling interval that the Server will use.
 *
 *  This value is based on a number of factors, including capabilities of the
 *  underlying system. The Server shall always return a revisedSamplingInterval
 *  that is equal or higher than the requestedSamplingInterval. If the requested
 *  samplingInterval is higher than the maximum sampling interval supported by the
 *  Server, the maximum sampling interval is returned.
 *
 *  \var ua_monitoreditemcreateresult::revised_queue_size
 *  The actual queue size that the Server will use.
 *
 *  \var ua_monitoreditemcreateresult::filter_result
 *  Contains any revised parameter values or error results associated with the
 *  MonitoringFilter specified in the request.
 *
 *  This parameter may be omitted if no errors occurred. The MonitoringFilterResult
 *  parameter type is an extensible parameter type.
*/
struct ua_monitoreditemcreateresult {
    ua_statuscode status_code;
    uint32_t monitored_item_id;
    double revised_sampling_interval;
    uint32_t revised_queue_size;
    struct ua_extensionobject filter_result;
};

void ua_monitoreditemcreateresult_init(struct ua_monitoreditemcreateresult *t);
void ua_monitoreditemcreateresult_clear(struct ua_monitoreditemcreateresult *t);
int ua_monitoreditemcreateresult_compare(const struct ua_monitoreditemcreateresult *a, const struct ua_monitoreditemcreateresult *b) UA_PURE_FUNCTION;
int ua_monitoreditemcreateresult_copy(struct ua_monitoreditemcreateresult *dst, const struct ua_monitoreditemcreateresult *src);

#endif /* _UABASE_MONITOREDITEMCREATERESULT_H_ */

