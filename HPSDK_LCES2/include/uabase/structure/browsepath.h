/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_BROWSEPATH_H_
#define _UABASE_BROWSEPATH_H_

#include <platform/platform.h>
#include <uabase/base_config.h>
#include <uabase/nodeid.h>
#include <uabase/structure/relativepath.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_browsepath
 *  A structure that is defined as the type of the browsePaths parameter of the
 *  TranslateBrowsePathsToNodeIds service.
 *
 *  \var ua_browsepath::starting_node
 *  NodeId of the starting Node for the browse path.
 *
 *  \var ua_browsepath::relative_path
 *  The path to follow from the startingNode.
 *
 *  The last element in the relativePath shall always have a targetName specified.
 *  This further restricts the definition of the RelativePath type. The Server
 *  shall return Bad_BrowseNameInvalid if the targetName is missing.
*/
struct ua_browsepath {
    struct ua_nodeid starting_node;
    struct ua_relativepath relative_path;
};

void ua_browsepath_init(struct ua_browsepath *t);
void ua_browsepath_clear(struct ua_browsepath *t);
int ua_browsepath_compare(const struct ua_browsepath *a, const struct ua_browsepath *b) UA_PURE_FUNCTION;
int ua_browsepath_copy(struct ua_browsepath *dst, const struct ua_browsepath *src);

#endif /* _UABASE_BROWSEPATH_H_ */

