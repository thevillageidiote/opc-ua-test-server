/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_TRANSFERRESULT_H_
#define _UABASE_TRANSFERRESULT_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/statuscode.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_transferresult
 *  A structure that is defined as the type of the results parameter of the
 *  TransferSubscriptions service.
 *
 *  \var ua_transferresult::status_code
 *  StatusCode for each Subscription to be transferred.
 *
 *  \var ua_transferresult::num_available_sequence_numbers
 *  Number of elements in \ref ua_transferresult::available_sequence_numbers.
 *
 *  \var ua_transferresult::available_sequence_numbers
 *  A list of sequence number ranges that identify NotificationMessages that are in
 *  the Subscription’s retransmission queue.
 *
 *  This parameter is null if the transfer of the Subscription failed.
*/
struct ua_transferresult {
    ua_statuscode status_code;
    int32_t num_available_sequence_numbers;
    uint32_t *available_sequence_numbers;
};

void ua_transferresult_init(struct ua_transferresult *t);
void ua_transferresult_clear(struct ua_transferresult *t);
int ua_transferresult_compare(const struct ua_transferresult *a, const struct ua_transferresult *b) UA_PURE_FUNCTION;
int ua_transferresult_copy(struct ua_transferresult *dst, const struct ua_transferresult *src);

#endif /* _UABASE_TRANSFERRESULT_H_ */

