/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_VIEWDESCRIPTION_H_
#define _UABASE_VIEWDESCRIPTION_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/datetime.h>
#include <uabase/nodeid.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_viewdescription
 *  Specifies a View.
 *
 *  \var ua_viewdescription::view_id
 *  NodeId of the View to Query. A null value indicates the entire AddressSpace.
 *
 *  \var ua_viewdescription::timestamp
 *  The time date desired.
 *
 *  The corresponding version is the one with the closest previous creation
 *  timestamp. Either the Timestamp or the viewVersion parameter may be set by a
 *  Client, but not both. If ViewVersion is set this parameter shall be null.
 *
 *  \var ua_viewdescription::view_version
 *  The version number for the View desired.
 *
 *  When Nodes are added to or removed from a View, the value of a View’s
 *  ViewVersion Property is updated. Either the Timestamp or the viewVersion
 *  parameter may be set by a Client, but not both. The ViewVersion Property is
 *  defined in Part 3. If timestamp is set this parameter shall be 0. The current
 *  view is used if timestamp is null and viewVersion is 0.
*/
struct ua_viewdescription {
    struct ua_nodeid view_id;
    ua_datetime timestamp;
    uint32_t view_version;
};

void ua_viewdescription_init(struct ua_viewdescription *t);
void ua_viewdescription_clear(struct ua_viewdescription *t);
int ua_viewdescription_compare(const struct ua_viewdescription *a, const struct ua_viewdescription *b) UA_PURE_FUNCTION;
int ua_viewdescription_copy(struct ua_viewdescription *dst, const struct ua_viewdescription *src);

#endif /* _UABASE_VIEWDESCRIPTION_H_ */

