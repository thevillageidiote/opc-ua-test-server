/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_ELEMENTOPERAND_H_
#define _UABASE_ELEMENTOPERAND_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_elementoperand
 *  Provides the linking to subelements within a \ref ua_contentfilter.
 *
 *  The link is in the form of an integer that is used to index into the array of
 *  elements contained in the \ref ua_contentfilter. An index is considered valid
 *  if its value is greater than the element index it is part of and it does not
 *  reference a non-existent element. Clients shall construct filters in this way
 *  to avoid circular and invalid references. Servers should protect against
 *  invalid indexes by verifying the index prior to using it.
 *
 *  \var ua_elementoperand::index
 *  Index into the element array.
*/
struct ua_elementoperand {
    uint32_t index;
};

void ua_elementoperand_init(struct ua_elementoperand *t);
void ua_elementoperand_clear(struct ua_elementoperand *t);
int ua_elementoperand_compare(const struct ua_elementoperand *a, const struct ua_elementoperand *b) UA_PURE_FUNCTION;
int ua_elementoperand_copy(struct ua_elementoperand *dst, const struct ua_elementoperand *src);

#endif /* _UABASE_ELEMENTOPERAND_H_ */

