/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_SUBSCRIPTIONACKNOWLEDGEMENT_H_
#define _UABASE_SUBSCRIPTIONACKNOWLEDGEMENT_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_subscriptionacknowledgement
 *  A structure that is defined as the type of the subscriptionAcknowledgements
 *  parameter of the Publish service.
 *
 *  \var ua_subscriptionacknowledgement::subscription_id
 *  The Server assigned identifier for a Subscription.
 *
 *  \var ua_subscriptionacknowledgement::sequence_number
 *  The sequence number being acknowledged.
 *
 *  The Server may delete the message with this sequence number from its
 *  retransmission queue.
*/
struct ua_subscriptionacknowledgement {
    uint32_t subscription_id;
    uint32_t sequence_number;
};

void ua_subscriptionacknowledgement_init(struct ua_subscriptionacknowledgement *t);
void ua_subscriptionacknowledgement_clear(struct ua_subscriptionacknowledgement *t);
int ua_subscriptionacknowledgement_compare(const struct ua_subscriptionacknowledgement *a, const struct ua_subscriptionacknowledgement *b) UA_PURE_FUNCTION;
int ua_subscriptionacknowledgement_copy(struct ua_subscriptionacknowledgement *dst, const struct ua_subscriptionacknowledgement *src);

#endif /* _UABASE_SUBSCRIPTIONACKNOWLEDGEMENT_H_ */

