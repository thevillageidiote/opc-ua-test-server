/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_MONITORINGPARAMETERS_H_
#define _UABASE_MONITORINGPARAMETERS_H_

#include <platform/platform.h>
#include <stdbool.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/extensionobject.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_monitoringparameters
 *  Parameters that define the monitoring characteristics of a MonitoredItem.
 *
 *  \var ua_monitoringparameters::client_handle
 *  Client-supplied id of the MonitoredItem.
 *
 *  This id is used in Notifications generated for the list Node.
 *
 *  \var ua_monitoringparameters::sampling_interval
 *  The interval in milliseconds that defines the fastest rate at which the
 *  MonitoredItem(s) should be accessed and evaluated.
 *
 *  The value 0 indicates that the Server should use the fastest practical rate.
 *
 *  The value -1 indicates that the default sampling interval defined by the
 *  publishing interval of the Subscription is requested. A different sampling
 *  interval is used if the publishing interval is not a supported sampling
 *  interval. Any negative number is interpreted as -1. The sampling interval is
 *  not changed if the publishing interval is changed by a subsequent call to the
 *  ModifySubscription Service.
 *
 *  The Server uses this parameter to assign the MonitoredItems to a sampling
 *  interval that it supports.
 *
 *  The assigned interval is provided in the revisedSamplingInterval parameter. The
 *  Server shall always return a revisedSamplingInterval that is equal or higher
 *  than the requested samplingInterval. If the requested samplingInterval is
 *  higher than the maximum sampling interval supported by the Server, the maximum
 *  sampling interval is returned.
 *
 *  \var ua_monitoringparameters::filter
 *  A filter used by the Server to determine if the MonitoredItem should generate a
 *  Notification.
 *
 *  If not used, this parameter is null. The MonitoringFilter parameter type is an
 *  extensible parameter type. It specifies the types of filters that can be used.
 *
 *  \var ua_monitoringparameters::queue_size
 *  The requested size of the MonitoredItem queue.
 *
 *  The following values have special meaning for data monitored items:
 *
 *  <dl> <dt>0 or 1</dt> <dd>the server returns the default queue size which shall
 *  be 1 as revisedQueueSize for data monitored items. The queue has a single
 *  entry, effectively disabling queuing.</dd> </dl> For values larger than one a
 *  first-in-first-out queue is to be used. The Server may limit the size in
 *  revisedQueueSize. In the case of a queue overflow, the Overflow bit (flag) in
 *  the InfoBits portion of the DataValue statusCode is set in the new value.
 *
 *  The following values have special meaning for event monitored items:
 *
 *  <dl> <dt>0</dt> <dd>the Server returns the default queue size for Event
 *  Notifications as revisedQueueSize for event monitored items.</dd> <dt>1</dt>
 *  <dd>the Server returns the minimum queue size the Server requires for Event
 *  Notifications as revisedQueueSize.</dd> <dt>MaxUInt32</dt> <dd>the Server
 *  returns the maximum queue size that the Server can support for Event
 *  Notifications as revisedQueueSize.</dd> </dl>
 *
 *  If a Client chooses a value between the minimum and maximum settings of the
 *  Server the value shall be returned in the revisedQueueSize. If the requested
 *  queueSize is outside the minimum or maximum, the Server shall return the
 *  corresponding bounding value.
 *
 *  In the case of a queue overflow, an Event of the type
 *  EventQueueOverflowEventType is generated.
 *
 *  \var ua_monitoringparameters::discard_oldest
 *  A boolean parameter that specifies the discard policy when the queue is full
 *  and a new Notification is to be enqueued.
 *
 *  It has the following values: <dl> <dt>TRUE</dt> <dd>the oldest (first)
 *  Notification in the queue is discarded. The new Notification is added to the
 *  end of the queue.</dd> <dt>FALSE</dt> <dd>the new Notification is discarded.
 *  The queue is unchanged.</dd> </dl>
*/
struct ua_monitoringparameters {
    uint32_t client_handle;
    double sampling_interval;
    struct ua_extensionobject filter;
    uint32_t queue_size;
    bool discard_oldest;
};

void ua_monitoringparameters_init(struct ua_monitoringparameters *t);
void ua_monitoringparameters_clear(struct ua_monitoringparameters *t);
int ua_monitoringparameters_compare(const struct ua_monitoringparameters *a, const struct ua_monitoringparameters *b) UA_PURE_FUNCTION;
int ua_monitoringparameters_copy(struct ua_monitoringparameters *dst, const struct ua_monitoringparameters *src);

#endif /* _UABASE_MONITORINGPARAMETERS_H_ */

