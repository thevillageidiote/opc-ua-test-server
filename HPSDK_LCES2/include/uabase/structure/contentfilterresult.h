/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_CONTENTFILTERRESULT_H_
#define _UABASE_CONTENTFILTERRESULT_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/diagnosticinfo.h>
#include <uabase/structure/contentfilterelementresult.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_contentfilterresult
 *  A structure that contains any errors associated with the filter.
 *
 *  \var ua_contentfilterresult::num_element_results
 *  Number of elements in \ref ua_contentfilterresult::element_results.
 *
 *  \var ua_contentfilterresult::element_results
 *  A list of results for individual elements in the filter.
 *
 *  See \ref ua_contentfilterelementresult.
 *
 *  \var ua_contentfilterresult::num_element_diag_infos
 *  Number of elements in \ref ua_contentfilterresult::element_diag_infos.
 *
 *  \var ua_contentfilterresult::element_diag_infos
 *  A list of diagnostic information for individual elements in the filter.
 *
 *  The size and order of the list matches the size and order of the elements in
 *  the filter request parameter. This list is empty if diagnostics information was
 *  not requested in the request header or if no diagnostic information was
 *  encountered in processing of the elements.
*/
struct ua_contentfilterresult {
    int32_t num_element_results;
    struct ua_contentfilterelementresult *element_results;
#ifdef ENABLE_DIAGNOSTICS
    int32_t num_element_diag_infos;
    struct ua_diagnosticinfo *element_diag_infos;
#endif
};

void ua_contentfilterresult_init(struct ua_contentfilterresult *t);
void ua_contentfilterresult_clear(struct ua_contentfilterresult *t);
int ua_contentfilterresult_compare(const struct ua_contentfilterresult *a, const struct ua_contentfilterresult *b) UA_PURE_FUNCTION;
int ua_contentfilterresult_copy(struct ua_contentfilterresult *dst, const struct ua_contentfilterresult *src);

#endif /* _UABASE_CONTENTFILTERRESULT_H_ */

