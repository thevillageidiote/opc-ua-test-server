/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_BROWSEDESCRIPTION_H_
#define _UABASE_BROWSEDESCRIPTION_H_

#include <platform/platform.h>
#include <stdbool.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/nodeid.h>
#include <uabase/structure/browsedirection.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_browsedescription
 *  A structure defined as the type of the parameter nodesToBrowse of the Browse
 *  service.
 *
 *  \var ua_browsedescription::node_id
 *  NodeId of the Node to be browsed.
 *
 *  If a view is provided, it shall include this Node.
 *
 *  \var ua_browsedescription::browse_direction
 *  An enumeration that specifies the direction of References to follow.
 *
 *  The returned references do indicate the direction the Server followed in the
 *  isForward parameter of the ReferenceDescription.
 *
 *  Symmetric references are always considered to be in forward direction,
 *  therefore the isForward flag is always set to TRUE and symmetric references are
 *  not returned if browseDirection is set to INVERSE_1.
 *
 *  \var ua_browsedescription::reference_type_id
 *  Specifies the NodeId of the ReferenceType to follow.
 *
 *  Only instances of this ReferenceType or its subtypes are returned.
 *
 *  If not specified, all References are returned and includeSubtypes is ignored.
 *
 *  \var ua_browsedescription::include_subtypes
 *  Indicates whether subtypes of the ReferenceType should be included in the
 *  browse.
 *
 *  If TRUE, then instances of referenceTypeId and all of its subtypes are
 *  returned.
 *
 *  \var ua_browsedescription::node_class_mask
 *  Specifies the NodeClasses of the TargetNodes.
 *
 *  Only TargetNodes with the selected NodeClasses are returned. The NodeClasses
 *  are assigned the following bits:
 *
 *  Bit  | Node Class
 *  -----|---------------
 *  0    | Object
 *  1    | Variable
 *  2    | Method
 *  3    | ObjectType
 *  4    | VariableType
 *  5    | ReferenceType
 *  6    | DataType
 *  7    | View
 *
 *  If set to zero, then all NodeClasses are returned.
 *
 *  \var ua_browsedescription::result_mask
 *  Specifies the fields in the ReferenceDescription structure that should be
 *  returned.
 *
 *  The fields are assigned the following bits:
 *
 *  Bit  | Result
 *  -----|----------------
 *  0    | ReferenceType
 *  1    | IsForward
 *  2    | NodeClass
 *  3    | BrowseName
 *  4    | DisplayName
 *  5    | TypeDefinition
*/
struct ua_browsedescription {
    struct ua_nodeid node_id;
    enum ua_browsedirection browse_direction;
    struct ua_nodeid reference_type_id;
    bool include_subtypes;
    uint32_t node_class_mask;
    uint32_t result_mask;
};

void ua_browsedescription_init(struct ua_browsedescription *t);
void ua_browsedescription_clear(struct ua_browsedescription *t);
int ua_browsedescription_compare(const struct ua_browsedescription *a, const struct ua_browsedescription *b) UA_PURE_FUNCTION;
int ua_browsedescription_copy(struct ua_browsedescription *dst, const struct ua_browsedescription *src);

#endif /* _UABASE_BROWSEDESCRIPTION_H_ */

