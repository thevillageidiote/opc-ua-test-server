/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_REFERENCEDESCRIPTION_H_
#define _UABASE_REFERENCEDESCRIPTION_H_

#include <platform/platform.h>
#include <stdbool.h>
#include <uabase/base_config.h>
#include <uabase/expandednodeid.h>
#include <uabase/localizedtext.h>
#include <uabase/nodeid.h>
#include <uabase/qualifiedname.h>
#include <uabase/structure/nodeclass.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_referencedescription
 *  Reference parameters returned for the Browse Service.
 *
 *  \var ua_referencedescription::reference_type_id
 *  NodeId of the ReferenceType that defines the Reference.
 *
 *  \var ua_referencedescription::is_forward
 *  If the value is TRUE, the Server followed a forward Reference. If the value is
 *  FALSE, the Server followed an inverse Reference.
 *
 *  \var ua_referencedescription::node_id
 *  NodeId of the TargetNode as assigned by the Server identified by the Server
 *  index.
 *
 *  If the serverIndex indicates that the TargetNode is a remote Node, then the
 *  nodeId shall contain the absolute namespace URI. If the TargetNode is a local
 *  Node the nodeId shall contain the namespace index.
 *
 *  \var ua_referencedescription::browse_name
 *  The BrowseName of the TargetNode.
 *
 *  \var ua_referencedescription::display_name
 *  The DisplayName of the TargetNode.
 *
 *  \var ua_referencedescription::node_class
 *  NodeClass of the TargetNode.
 *
 *  \var ua_referencedescription::type_definition
 *  Type definition NodeId of the TargetNode.
 *
 *  Type definitions are only available for the NodeClasses object and Variable.
 *  For all other NodeClasses a null NodeId shall be returned.
*/
struct ua_referencedescription {
    struct ua_nodeid reference_type_id;
    bool is_forward;
    struct ua_expandednodeid node_id;
    struct ua_qualifiedname browse_name;
    struct ua_localizedtext display_name;
    enum ua_nodeclass node_class;
    struct ua_expandednodeid type_definition;
};

void ua_referencedescription_init(struct ua_referencedescription *t);
void ua_referencedescription_clear(struct ua_referencedescription *t);
int ua_referencedescription_compare(const struct ua_referencedescription *a, const struct ua_referencedescription *b) UA_PURE_FUNCTION;
int ua_referencedescription_copy(struct ua_referencedescription *dst, const struct ua_referencedescription *src);

#endif /* _UABASE_REFERENCEDESCRIPTION_H_ */

