/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_NOTIFICATIONMESSAGE_H_
#define _UABASE_NOTIFICATIONMESSAGE_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/datetime.h>
#include <uabase/extensionobject.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_notificationmessage
 *
 *  \var ua_notificationmessage::sequence_number
 *  The Message that contains one or more Notifications.
 *
 *  \var ua_notificationmessage::publish_time
 *  The sequence number of the NotificationMessage.
 *
 *  \var ua_notificationmessage::num_notification_data
 *  Number of elements in \ref ua_notificationmessage::notification_data.
 *
 *  \var ua_notificationmessage::notification_data
 *  The list of NotificationData structures.
 *
 *  The NotificationData parameter type is an extensible parameter type. It
 *  specifies the types of Notifications that can be sent.
 *
 *  Notifications of the same type should be grouped into one NotificationData
 *  element. If a Subscription contains MonitoredItems for events and data, this
 *  array should have not more than 2 elements. If the Subscription contains
 *  MonitoredItems only for data or only for events, the array size should always
 *  be one for this Subscription.
*/
struct ua_notificationmessage {
    uint32_t sequence_number;
    ua_datetime publish_time;
    int32_t num_notification_data;
    struct ua_extensionobject *notification_data;
};

void ua_notificationmessage_init(struct ua_notificationmessage *t);
void ua_notificationmessage_clear(struct ua_notificationmessage *t);
int ua_notificationmessage_compare(const struct ua_notificationmessage *a, const struct ua_notificationmessage *b) UA_PURE_FUNCTION;
int ua_notificationmessage_copy(struct ua_notificationmessage *dst, const struct ua_notificationmessage *src);

#endif /* _UABASE_NOTIFICATIONMESSAGE_H_ */

