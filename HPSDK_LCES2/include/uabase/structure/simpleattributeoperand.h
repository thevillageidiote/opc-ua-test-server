/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_SIMPLEATTRIBUTEOPERAND_H_
#define _UABASE_SIMPLEATTRIBUTEOPERAND_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/nodeid.h>
#include <uabase/qualifiedname.h>
#include <uabase/string.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_simpleattributeoperand
 *  A simplified form of the \ref ua_attributeoperand.
 *
 *  All of the rules that apply to the \ref ua_attributeoperand also apply to the
 *  SimpleAttributeOperand.
 *
 *  \var ua_simpleattributeoperand::type_definition_id
 *  NodeId of a TypeDefinitionNode.
 *
 *  This parameter restricts the operand to instances of the TypeDefinitionNode or
 *  one of its subtypes.
 *
 *  \var ua_simpleattributeoperand::num_browse_path
 *  Number of elements in \ref ua_simpleattributeoperand::browse_path.
 *
 *  \var ua_simpleattributeoperand::browse_path
 *  A relative path to a Node.
 *
 *  This parameter specifies a relative path using a list of browse names instead
 *  of the \ref ua_relativepath structure used in the \ref ua_attributeoperand. The
 *  list of browse names is equivalent to a relative path that specifies forward
 *  references which are subtypes of the HierarchicalReferences reference type.
 *
 *  All nodes followed by the browsePath shall be of the NodeClass object or
 *  variable.
 *
 *  If this list is empty, the node is the instance of the TypeDefinition.
 *
 *  \var ua_simpleattributeoperand::attribute_id
 *  ID of the Attribute.
 *
 *  The Value attribute shall be supported by all Servers. The support of other
 *  attributes depends on requirements set in Profiles or other parts of this
 *  specification.
 *
 *  \var ua_simpleattributeoperand::index_range
 *  This parameter is used to identify a single element of an array, or a single
 *  range of indexes for an array.
 *
 *  The first element is identified by index 0 (zero).
 *
 *  This parameter is ignored if the selected node is not a variable or the value
 *  of a variable is not an array.
 *
 *  The parameter is null if not specified.
 *
 *  All values in the array are used if this parameter is not specified.
*/
struct ua_simpleattributeoperand {
    struct ua_nodeid type_definition_id;
    int32_t num_browse_path;
    struct ua_qualifiedname *browse_path;
    uint32_t attribute_id;
    struct ua_string index_range;
};

void ua_simpleattributeoperand_init(struct ua_simpleattributeoperand *t);
void ua_simpleattributeoperand_clear(struct ua_simpleattributeoperand *t);
int ua_simpleattributeoperand_compare(const struct ua_simpleattributeoperand *a, const struct ua_simpleattributeoperand *b) UA_PURE_FUNCTION;
int ua_simpleattributeoperand_copy(struct ua_simpleattributeoperand *dst, const struct ua_simpleattributeoperand *src);

#endif /* _UABASE_SIMPLEATTRIBUTEOPERAND_H_ */

