/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_READVALUEID_H_
#define _UABASE_READVALUEID_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/nodeid.h>
#include <uabase/qualifiedname.h>
#include <uabase/string.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_readvalueid
 *  Identifier for an item to read or to monitor.
 *
 *  \var ua_readvalueid::node_id
 *  NodeId of a Node.
 *
 *  \var ua_readvalueid::attribute_id
 *  Id of the Attribute. This shall be a valid Attribute id.
 *
 *  The IntegerIds for the Attributes are defined in NodeAttributesMask.
 *
 *  \var ua_readvalueid::index_range
 *  This parameter is used to identify a single element of an array, or a single
 *  range of indexes for arrays.
 *
 *  If a range of elements is specified, the values are returned as a composite.
 *  The first element is identified by index 0 (zero).
 *
 *  This parameter is null if the specified Attribute is not an array. However, if
 *  the specified Attribute is an array, and this parameter is null, then all
 *  elements are to be included in the range.
 *
 *  \var ua_readvalueid::data_encoding
 *  This parameter specifies the BrowseName of the DataTypeEncoding that the Server
 *  should use when returning the Value Attribute of a Variable. It is an error to
 *  specify this parameter for other Attributes.
 *
 *  A Client can discover what DataTypeEncodings are available by following the
 *  HasEncoding Reference from the DataType Node for a Variable.
 *
 *  OPC UA defines BrowseNames which Servers shall recognize even if the DataType
 *  Nodes are not visible in the Server address space. These BrowseNames are:
 *
 *  <dl><dt>DefaultBinary</dt><dd>The default or native binary (or non-XML)
 *  encoding.</dd><dt>DefaultXML</dt><dd>The default XML encoding.</dd></dl>
 *
 *  Each DataType shall support at least one of these encodings. DataTypes that do
 *  not have a true binary encoding (e.g. they only have a non-XML text encoding)
 *  should use the DefaultBinary name to identify the encoding that is considered
 *  to be the default non-XML encoding. DataTypes that support at least one
 *  XML-based encoding shall identify one of the encodings as the DefaultXML
 *  encoding. Other standards bodies may define other well-known data encodings
 *  that could be supported.
 *
 *  If this parameter is not specified then the Server shall choose either the
 *  DefaultBinary or DefaultXML encoding according to what Message encoding (see
 *  Part 6 of the OPC UA Specification) is used for the Session. If the Server does
 *  not support the encoding that matches the Message encoding then the Server
 *  shall choose the default encoding that it does support.
 *
 *  If this parameter is specified for a MonitoredItem, the Server shall set the
 *  StructureChanged bit in the StatusCode if the DataTypeEncoding changes. The
 *  DataTypeEncoding changes if the DataTypeVersion of the DataTypeDescription or
 *  the DataTypeDictionary associated with the DataTypeEncoding changes.
*/
struct ua_readvalueid {
    struct ua_nodeid node_id;
    uint32_t attribute_id;
    struct ua_string index_range;
    struct ua_qualifiedname data_encoding;
};

void ua_readvalueid_init(struct ua_readvalueid *t);
void ua_readvalueid_clear(struct ua_readvalueid *t);
int ua_readvalueid_compare(const struct ua_readvalueid *a, const struct ua_readvalueid *b) UA_PURE_FUNCTION;
int ua_readvalueid_copy(struct ua_readvalueid *dst, const struct ua_readvalueid *src);

#endif /* _UABASE_READVALUEID_H_ */

