/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_BROWSEPATHRESULT_H_
#define _UABASE_BROWSEPATHRESULT_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/statuscode.h>
#include <uabase/structure/browsepathtarget.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_browsepathresult
 *  A structure that is defined as the type of the results parameter of the
 *  TranslateBrowsePathsToNodeIds service.
 *
 *  \var ua_browsepathresult::status_code
 *  StatusCode for the browse path.
 *
 *  \var ua_browsepathresult::num_targets
 *  Number of elements in \ref ua_browsepathresult::targets.
 *
 *  \var ua_browsepathresult::targets
 *  List of targets for the relativePath from the startingNode.
*/
struct ua_browsepathresult {
    ua_statuscode status_code;
    int32_t num_targets;
    struct ua_browsepathtarget *targets;
};

void ua_browsepathresult_init(struct ua_browsepathresult *t);
void ua_browsepathresult_clear(struct ua_browsepathresult *t);
int ua_browsepathresult_compare(const struct ua_browsepathresult *a, const struct ua_browsepathresult *b) UA_PURE_FUNCTION;
int ua_browsepathresult_copy(struct ua_browsepathresult *dst, const struct ua_browsepathresult *src);

#endif /* _UABASE_BROWSEPATHRESULT_H_ */

