/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_SIGNEDSOFTWARECERTIFICATE_H_
#define _UABASE_SIGNEDSOFTWARECERTIFICATE_H_

#include <platform/platform.h>
#include <uabase/base_config.h>
#include <uabase/bytestring.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_signedsoftwarecertificate
 *  A ByteString containing an encoded certificate.
 *
 *  The encoding of a SignedSoftwareCertificate depends on the security technology
 *  mapping and is defined completely in Part 6 of the OPC UA Specification.
 *
 *  \var ua_signedsoftwarecertificate::certificate_data
 *  The certificate data serialized as a ByteString.
 *
 *  \var ua_signedsoftwarecertificate::signature
 *  The signature for the certificateData.
*/
struct ua_signedsoftwarecertificate {
    struct ua_bytestring certificate_data;
    struct ua_bytestring signature;
};

void ua_signedsoftwarecertificate_init(struct ua_signedsoftwarecertificate *t);
void ua_signedsoftwarecertificate_clear(struct ua_signedsoftwarecertificate *t);
int ua_signedsoftwarecertificate_compare(const struct ua_signedsoftwarecertificate *a, const struct ua_signedsoftwarecertificate *b) UA_PURE_FUNCTION;
int ua_signedsoftwarecertificate_copy(struct ua_signedsoftwarecertificate *dst, const struct ua_signedsoftwarecertificate *src);

#endif /* _UABASE_SIGNEDSOFTWARECERTIFICATE_H_ */

