/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_AXISSCALEENUMERATION_H
#define _UABASE_AXISSCALEENUMERATION_H

#include <uabase/base_config.h>

/**
 * @defgroup ua_base_axisscaleenumeration ua_axisscaleenumeration
 * @ingroup ua_base_enums
 * @brief
 *  Identifies on which type of axis the data shall be displayed.
 * @{
 */

/** \enum ua_axisscaleenumeration
 *  Identifies on which type of axis the data shall be displayed.
 *
 *  \var UA_AXISSCALEENUMERATION_LINEAR
 *  Linear Scale
 *
 *  \var UA_AXISSCALEENUMERATION_LOG
 *  Log base 10 scale
 *
 *  \var UA_AXISSCALEENUMERATION_LN
 *  Log base e scale
*/
enum ua_axisscaleenumeration
{
    UA_AXISSCALEENUMERATION_LINEAR = 0,
    UA_AXISSCALEENUMERATION_LOG = 1,
    UA_AXISSCALEENUMERATION_LN = 2
};

#ifdef ENABLE_TO_STRING
const char* ua_axisscaleenumeration_to_string(enum ua_axisscaleenumeration axisscaleenumeration);
#endif /* ENABLE_TO_STRING */

/** @}*/

#endif /*_UABASE_AXISSCALEENUMERATION_H*/
