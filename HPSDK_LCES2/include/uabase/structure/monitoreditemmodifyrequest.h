/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_MONITOREDITEMMODIFYREQUEST_H_
#define _UABASE_MONITOREDITEMMODIFYREQUEST_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/structure/monitoringparameters.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_monitoreditemmodifyrequest
 *  A structure that is defined as the type of the itemsToModify parameter of the
 *  ModifyMonitoredItems service.
 *
 *  \var ua_monitoreditemmodifyrequest::monitored_item_id
 *  Server-assigned ID for the MonitoredItem.
 *
 *  \var ua_monitoreditemmodifyrequest::requested_parameters
 *  The requested values for the monitoring parameters.
 *
 *  If the number of notifications in the queue exceeds the new queue size, the
 *  notifications exceeding the size shall be discarded following the configured
 *  discard policy.
*/
struct ua_monitoreditemmodifyrequest {
    uint32_t monitored_item_id;
    struct ua_monitoringparameters requested_parameters;
};

void ua_monitoreditemmodifyrequest_init(struct ua_monitoreditemmodifyrequest *t);
void ua_monitoreditemmodifyrequest_clear(struct ua_monitoreditemmodifyrequest *t);
int ua_monitoreditemmodifyrequest_compare(const struct ua_monitoreditemmodifyrequest *a, const struct ua_monitoreditemmodifyrequest *b) UA_PURE_FUNCTION;
int ua_monitoreditemmodifyrequest_copy(struct ua_monitoreditemmodifyrequest *dst, const struct ua_monitoreditemmodifyrequest *src);

#endif /* _UABASE_MONITOREDITEMMODIFYREQUEST_H_ */

