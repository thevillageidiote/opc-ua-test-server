/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_CONTENTFILTER_H_
#define _UABASE_CONTENTFILTER_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/structure/contentfilterelement.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_contentfilter
 *  Defines a collection of elements that define filtering criteria.
 *
 *  Each element in the collection describes an operator and an array of operands
 *  to be used by the operator. See \ref ua_filteroperator for a description of the
 *  operators that can be used in a ContentFilter. The filter is evaluated by
 *  evaluating the first entry in the element array starting with the first operand
 *  in the operand array. The operands of an element may contain References to
 *  subelements resulting in the evaluation continuing to the referenced elements
 *  in the element array. If an element cannot be traced back to the starting
 *  element it is ignored. Extra operands for any operator shall result in an
 *  error.
 *
 *  \var ua_contentfilter::num_elements
 *  Number of elements in \ref ua_contentfilter::elements.
 *
 *  \var ua_contentfilter::elements
 *  List of operators and their operands that compose the filter criteria.
 *
 *  The filter is evaluated by starting with the first entry in this array.
*/
struct ua_contentfilter {
    int32_t num_elements;
    struct ua_contentfilterelement *elements;
};

void ua_contentfilter_init(struct ua_contentfilter *t);
void ua_contentfilter_clear(struct ua_contentfilter *t);
int ua_contentfilter_compare(const struct ua_contentfilter *a, const struct ua_contentfilter *b) UA_PURE_FUNCTION;
int ua_contentfilter_copy(struct ua_contentfilter *dst, const struct ua_contentfilter *src);

#endif /* _UABASE_CONTENTFILTER_H_ */

