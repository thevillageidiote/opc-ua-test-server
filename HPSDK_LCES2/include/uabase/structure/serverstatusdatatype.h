/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UABASE_SERVERSTATUSDATATYPE_H_
#define _UABASE_SERVERSTATUSDATATYPE_H_

#include <platform/platform.h>
#include <stdint.h>
#include <uabase/base_config.h>
#include <uabase/datetime.h>
#include <uabase/localizedtext.h>
#include <uabase/structure/buildinfo.h>
#include <uabase/structure/serverstate.h>

/**
 *  \ingroup ua_base_structure
 *  \struct ua_serverstatusdatatype
 *  Contains elements that describe the status of the Server.
 *
 *  \var ua_serverstatusdatatype::start_time
 *  Time (UTC) the server was started.
 *
 *  This is constant for the server instance and is not reset when the server
 *  changes state. Each instance of a server should keep the time when the process
 *  started.
 *
 *  \var ua_serverstatusdatatype::current_time
 *  The current time (UTC) as known by the server.
 *
 *  \var ua_serverstatusdatatype::state
 *  The current state of the server.
 *
 *  \var ua_serverstatusdatatype::build_info
 *  Build information of the server.
 *
 *  \var ua_serverstatusdatatype::seconds_till_shutdown
 *  Approximate number of seconds until the server will be shut down.
 *
 *  The value is only relevant once the state changes into SHUTDOWN.
 *
 *  \var ua_serverstatusdatatype::shutdown_reason
 *  An optional localized text indicating the reason for the shutdown.
 *
 *  The value is only relevant once the state changes into SHUTDOWN.
*/
struct ua_serverstatusdatatype {
    ua_datetime start_time;
    ua_datetime current_time;
    enum ua_serverstate state;
    struct ua_buildinfo build_info;
    uint32_t seconds_till_shutdown;
    struct ua_localizedtext shutdown_reason;
};

void ua_serverstatusdatatype_init(struct ua_serverstatusdatatype *t);
void ua_serverstatusdatatype_clear(struct ua_serverstatusdatatype *t);
int ua_serverstatusdatatype_compare(const struct ua_serverstatusdatatype *a, const struct ua_serverstatusdatatype *b) UA_PURE_FUNCTION;
int ua_serverstatusdatatype_copy(struct ua_serverstatusdatatype *dst, const struct ua_serverstatusdatatype *src);

#endif /* _UABASE_SERVERSTATUSDATATYPE_H_ */

