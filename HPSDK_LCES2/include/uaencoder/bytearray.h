/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UAENCODER_BYTEARRAY_H_
#define _UAENCODER_BYTEARRAY_H_

/* forward declarations */
struct ua_encoder_context;
struct ua_decoder_context;

/* helper functions for secure encode and decode bytearrays.
 * Secure means these functions are doing buffer length checks and switching
 * of buffers when buffer boundaries are reached.
 * This functions are used by other encoding routines like string, bytestring, guid, etc.
 * There are no unchecked versions of theses functions, because you can directly
 * use ua_buffer_read and ua_buffer_write for reading and writing unchecked array data.
 */
int ua_encode_bytearray(struct ua_encoder_context *ctx, const void *data, unsigned int len);
int ua_decode_bytearray(struct ua_decoder_context *ctx, void *data, unsigned int len);
int ua_decode_bytearray_skip(struct ua_decoder_context *ctx, unsigned int len);

#endif /* _UAENCODER_BYTEARRAY_H_*/
