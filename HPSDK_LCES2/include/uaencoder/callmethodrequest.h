/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UAENCODER_CALLMETHODREQUEST_H_
#define _UAENCODER_CALLMETHODREQUEST_H_


/**
 * @internal
 * @defgroup ua_encoder_callmethodrequest ua_callmethodrequest
 * @ingroup ua_encoder_structure
 * @{
 */

/* forward declarations */
struct ua_encoder_context;
struct ua_decoder_context;
struct ua_buffer;
struct ua_callmethodrequest;

int ua_encode_callmethodrequest(struct ua_encoder_context *ctx, const struct ua_callmethodrequest *val);
int ua_decode_callmethodrequest(struct ua_decoder_context *ctx, struct ua_callmethodrequest *val);

/** @}*/

#endif /* _UAENCODER_CALLMETHODREQUEST_H_ */

