/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UAENCODER_CALLREQUEST_H_
#define _UAENCODER_CALLREQUEST_H_

#include <uaencoder/uatype_config.h>

/**
 * @internal
 * @defgroup ua_encoder_callrequest ua_callrequest
 * @ingroup ua_encoder_service
 * @{
 */

/* forward declarations */
struct ua_encoder_context;
struct ua_decoder_context;
struct ua_buffer;
struct ua_callrequest;

#ifdef UAENCODER_ENCODE_SERVICE_CALLREQUEST
int ua_encode_callrequest(struct ua_encoder_context *ctx, const struct ua_callrequest *val);
#endif
#ifdef UAENCODER_DECODE_SERVICE_CALLREQUEST
int ua_decode_callrequest(struct ua_decoder_context *ctx, struct ua_callrequest *val);
#endif

/** @}*/

#endif /* _UAENCODER_CALLREQUEST_H_ */

