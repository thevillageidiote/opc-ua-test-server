/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/



#ifndef _UAENCODER_INT64_H_
#define _UAENCODER_INT64_H_

#include <stdint.h>
#include "uint64.h"

#define ua_encode_int64(ctx, val) ua_encode_uint64(ctx, (const uint64_t *)val)
#define ua_decode_int64_skip(ctx) ua_decode_uint64_skip(ctx)

#define ua_encode_int64_unchecked(ctx, val) ua_encode_uint64_unchecked(ctx, (const uint64_t *)val)
#define ua_decode_int64_unchecked(ctx, val) ua_decode_uint64_unchecked(ctx, (uint64_t *)val)

static inline int ua_decode_int64(struct ua_decoder_context *ctx, int64_t *val)
{
    return ua_decode_uint64(ctx, (uint64_t *) val);
}

#endif /* _UAENCODER_INT64_H_*/
