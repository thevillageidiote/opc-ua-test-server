# Macro for invoking the Interface C Compiler.

# add install prefix to search path for find_*
set(CMAKE_PREFIX_PATH ${CMAKE_INSTALL_PREFIX} ${SDKDIR})
# find icc executable. This is installed into CMAKE_INSTALL_PREFIX/bin
# by bootstrap and can be found simply using find_program.
# CMAKE_INSTALL_PREFIX is in the default search paths
find_program(UA_ICC_EXECUTABLE icc)
if (NOT EXISTS "${UA_ICC_EXECUTABLE}" AND NOT IS_ABSOLUTE CMAKE_INSTALL_PREFIX)
    set(UA_ICC_SEARCH_PATH "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_PREFIX}/bin")
    get_filename_component(UA_ICC_SEARCH_PATH ${UA_ICC_SEARCH_PATH} ABSOLUTE)
    find_program(UA_ICC_EXECUTABLE icc PATHS ${UA_ICC_SEARCH_PATH})
endif ()

if (EXISTS "${UA_ICC_EXECUTABLE}")
    set(UA_ICC_FOUND 1)
else ()
    message(FATAL_ERROR "Could not find icc executable. Did you forget to run ./bootstrap?")
endif ()

if (UA_ICC_EXECUTABLE)
    # we need to set the correct working directory, so that icc
    # can find its templates
    get_filename_component(ICC_WORK_DIR ${UA_ICC_EXECUTABLE} PATH)
    #message(STATUS "ICC_WORK_DIR: ${ICC_WORK_DIR}")
endif ()

macro (UA_WRAP_IDL _proxy_sources _stub_sources _idl)
    get_filename_component(IDL ${_idl} ABSOLUTE)
    # we need to invoke icc -p to get the list of outputfiles
    # that will be created later on
    #message(STATUS "${UA_ICC_EXECUTABLE} -p proxy -o ${CMAKE_CURRENT_BINARY_DIR} ${IDL}")
    execute_process(COMMAND ${UA_ICC_EXECUTABLE} -p proxy -o ${CMAKE_CURRENT_BINARY_DIR} ${IDL}
        OUTPUT_VARIABLE ${_proxy_sources}
        WORKING_DIRECTORY ${ICC_WORK_DIR}
        )
    execute_process(COMMAND ${UA_ICC_EXECUTABLE} -p stub -o ${CMAKE_CURRENT_BINARY_DIR} ${IDL}
        OUTPUT_VARIABLE ${_stub_sources}
        WORKING_DIRECTORY ${ICC_WORK_DIR}
        )
    execute_process(COMMAND ${UA_ICC_EXECUTABLE} -p all -o ${CMAKE_CURRENT_BINARY_DIR} ${IDL}
        OUTPUT_VARIABLE outfiles
        WORKING_DIRECTORY ${ICC_WORK_DIR}
        )
    #message(STATUS "_proxy_sources = ${${_proxy_sources}}")
    #message(STATUS "_stub_sources = ${${_stub_sources}}")
    #message(STATUS "outfiles = ${outfiles}")
    # add custom command to build process
    add_custom_command(OUTPUT ${outfiles}
        COMMAND ${UA_ICC_EXECUTABLE}
        ARGS -o ${CMAKE_CURRENT_BINARY_DIR} ${IDL}
        WORKING_DIRECTORY ${ICC_WORK_DIR}
        MAIN_DEPENDENCY ${IDL} VERBATIM)
endmacro ()

