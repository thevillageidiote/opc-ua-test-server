# - Try to find the mbedTLS encryption library
# Once done this will define
#
#  MBEDTLS_ROOT_DIR - Set this variable to the root installation of mbedTLS
#
# Read-Only variables:
#  MBEDTLS_FOUND - system has the mbedTLS library
#  MBEDTLS_INCLUDE_DIR - the mbedTLS include directory
#  MBEDTLS_LIBRARY - The library needed to use mbedTLS

if (${CMAKE_SYSTEM_NAME} STREQUAL "Windows")
  set(_MBEDTLS_ROOT_HINTS
    ${MBEDTLS_ROOT_DIR}
    ENV MBEDTLS_ROOT_DIR
    )
  file(TO_CMAKE_PATH "$ENV{PROGRAMFILES}" _programfiles)
  set(_MBEDTLS_ROOT_PATHS
    "${_programfiles}/mbedTLS"
    "C:/mbedTLS/"
    )
  unset(_programfiles)
  set(_MBEDTLS_ROOT_HINTS_AND_PATHS
    HINTS ${_MBEDTLS_ROOT_HINTS}
    PATHS ${_MBEDTLS_ROOT_PATHS}
    )
else ()
  set(_MBEDTLS_ROOT_HINTS
    ${MBEDTLS_ROOT_DIR}
    ENV MBEDTLS_ROOT_DIR
    )
  set(_MBEDTLS_ROOT_PATHS
    "/usr/local"
    )
  set(_MBEDTLS_ROOT_HINTS_AND_PATHS
    HINTS ${_MBEDTLS_ROOT_HINTS}
    PATHS ${_MBEDTLS_ROOT_PATHS}
    )
endif ()

find_path(MBEDTLS_INCLUDE_DIR
  NAMES
    mbedtls/ssl.h
  HINTS
    ${_MBEDTLS_INCLUDEDIR}
    ${_MBEDTLS_ROOT_HINTS_AND_PATHS}
  PATH_SUFFIXES
    include
)

find_library(MBEDTLS_LIBRARY
NAMES
  mbedtls
HINTS
  ${_MBEDTLS_LIBDIR}
  ${_MBEDTLS_ROOT_HINTS_AND_PATHS}
PATH_SUFFIXES
  lib
)

find_library(MBEDCRYPTO_LIBRARY
NAMES
  mbedcrypto
HINTS
  ${_MBEDTLS_LIBDIR}
  ${_MBEDTLS_ROOT_HINTS_AND_PATHS}
PATH_SUFFIXES
  lib
)

find_library(MBEDX509_LIBRARY
NAMES
  mbedx509
HINTS
  ${_MBEDTLS_LIBDIR}
  ${_MBEDTLS_ROOT_HINTS_AND_PATHS}
PATH_SUFFIXES
  lib
)

#include(${CMAKE_ROOT}/Modules/FindPackageHandleStandardArgs.cmake)
find_package(PackageHandleStandardArgs)

if (MBEDTLS_INCLUDE_DIR)
    find_package_handle_standard_args(mbedTLS
        REQUIRED_VARS
          MBEDTLS_LIBRARY
          MBEDCRYPTO_LIBRARY
          MBEDX509_LIBRARY
          MBEDTLS_INCLUDE_DIR
        FAIL_MESSAGE
          "Could NOT find mbedTLS, try to set the path to mbedTLS root folder in the system variable MBEDTLS_ROOT_DIR"
    )
    if (EXISTS "${MBEDTLS_INCLUDE_DIR}/mbedtls/version.h")
        # get version lines from file

        file(STRINGS "${MBEDTLS_INCLUDE_DIR}/mbedtls/version.h"
            MBEDTLS_MAJOR_VERSION
            REGEX "^#define[\t ]+MBEDTLS_VERSION_MAJOR.*")
        file(STRINGS "${MBEDTLS_INCLUDE_DIR}/mbedtls/version.h"
            MBEDTLS_MINOR_VERSION
            REGEX "^#define[\t ]+MBEDTLS_VERSION_MINOR.*")
        file(STRINGS "${MBEDTLS_INCLUDE_DIR}/mbedtls/version.h"
            MBEDTLS_PATCH_VERSION
            REGEX "^#define[\t ]+MBEDTLS_VERSION_PATCH.*")

        # extract version number from line using regex
        string(REGEX REPLACE "#define[\t ]+MBEDTLS_VERSION_MAJOR[\t ]+([0-9]+)" "\\1" MBEDTLS_MAJOR_VERSION ${MBEDTLS_MAJOR_VERSION})
        string(REGEX REPLACE "#define[\t ]+MBEDTLS_VERSION_MINOR[\t ]+([0-9]+)" "\\1" MBEDTLS_MINOR_VERSION ${MBEDTLS_MINOR_VERSION})
        string(REGEX REPLACE "#define[\t ]+MBEDTLS_VERSION_PATCH[\t ]+([0-9]+)" "\\1" MBEDTLS_PATCH_VERSION ${MBEDTLS_PATCH_VERSION})
        # create version number
        set(MBEDTLS_VERSION "${MBEDTLS_MAJOR_VERSION}.${MBEDTLS_MINOR_VERSION}.${MBEDTLS_PATCH_VERSION}")
    else ()
        set(MBEDTLS_VERSION "(unknown version)")
    endif ()
else ()
    find_package_handle_standard_args(mbedTLS "Could NOT find mbedTLS, try to set the path to mbedTLS root folder in the system variable MBEDTLS_ROOT_DIR"
        MBEDTLS_LIBRARIES
        MBEDTLS_INCLUDE_DIR
    )
endif ()

if (MBEDTLS_FOUND)
    include(CheckSymbolExists)
    check_symbol_exists(MBEDTLS_ZLIB_SUPPORT "${MBEDTLS_INCLUDE_DIR}/mbedtls/config.h" MBEDTLS_REQUIRES_ZLIB)
endif ()

mark_as_advanced(MBEDTLS_LIBRARY)
mark_as_advanced(MBEDCRYPTO_LIBRARY)
mark_as_advanced(MBEDX509_LIBRARY)
mark_as_advanced(MBEDTLS_INCLUDE_DIR)
mark_as_advanced(MBEDTLS_VERSION)

