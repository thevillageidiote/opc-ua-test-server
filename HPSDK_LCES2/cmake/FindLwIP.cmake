# - Try to find the LwIP TCP/IP library
# Once done this will define
#
#  LWIP_ROOT_DIR - Set this variable to the root installation of LwIP
#
# Read-Only variables:
#  LWIP_FOUND - system has the LwIP library
#  LWIP_INCLUDE_DIR - the LwIP include directory
#  LWIP_LIBRARY - The library needed to use LwIP

if (${CMAKE_SYSTEM_NAME} STREQUAL "Windows")
  set(_LWIP_ROOT_HINTS
    ${LWIP_ROOT_DIR}
    ENV LWIP_ROOT_DIR
    )
  file(TO_CMAKE_PATH "$ENV{PROGRAMFILES}" _programfiles)
  set(_LWIP_ROOT_PATHS
    "${_programfiles}/lwip"
    "C:/lwip/"
    )
  unset(_programfiles)
  set(_LWIP_ROOT_HINTS_AND_PATHS
    HINTS ${_LWIP_ROOT_HINTS}
    PATHS ${_LWIP_ROOT_PATHS}
    )
else ()
  set(_LWIP_ROOT_HINTS
    ${LWIP_ROOT_DIR}
    ENV LWIP_ROOT_DIR
    )
  set(_LWIP_ROOT_PATHS
    "/usr/local/include/lwip"
    )
  set(_LWIP_ROOT_HINTS_AND_PATHS
    HINTS ${_LWIP_ROOT_HINTS}
    PATHS ${_LWIP_ROOT_PATHS}
    )
endif ()

find_path(LWIP_INCLUDE_DIR
  NAMES
    lwip/tcp.h
  HINTS
    ${_LWIP_INCLUDEDIR}
    ${_LWIP_ROOT_HINTS_AND_PATHS}
  PATH_SUFFIXES
    include
)

find_library(LWIP_LIBRARY_RELEASE
NAMES
  lwip
HINTS
  ${_LWIP_LIBDIR}
  ${_LWIP_ROOT_HINTS_AND_PATHS}
PATH_SUFFIXES
  lib
)

find_library(LWIP_LIBRARY_DEBUG
NAMES
  lwip_d
  lwipd
HINTS
  ${_LWIP_LIBDIR}
  ${_LWIP_ROOT_HINTS_AND_PATHS}
PATH_SUFFIXES
  lib
)

include(SelectLibraryConfigurations)
select_library_configurations(LWIP)

find_package(PackageHandleStandardArgs)

if (LWIP_INCLUDE_DIR)
    find_package_handle_standard_args(LwIP
        REQUIRED_VARS
          LWIP_LIBRARY
          LWIP_INCLUDE_DIR
        FAIL_MESSAGE
          "Could NOT find LwIP, try to set the path to LwIP root folder in the system variable LWIP_ROOT_DIR"
    )
    if (EXISTS "${LWIP_INCLUDE_DIR}/lwip/init.h")
        # get version lines from file

        file(STRINGS "${LWIP_INCLUDE_DIR}/lwip/init.h"
            LWIP_MAJOR_VERSION
            REGEX "^#define[\t ]+LWIP_VERSION_MAJOR.*")
        file(STRINGS "${LWIP_INCLUDE_DIR}/lwip/init.h"
            LWIP_MINOR_VERSION
            REGEX "^#define[\t ]+LWIP_VERSION_MINOR.*")
        file(STRINGS "${LWIP_INCLUDE_DIR}/lwip/init.h"
            LWIP_PATCH_VERSION
            REGEX "^#define[\t ]+LWIP_VERSION_REVISION.*")

        # extract version number from line using regex
        string(REGEX REPLACE "#define[\t ]+LWIP_VERSION_MAJOR[\t ]+([0-9]+)" "\\1" LWIP_MAJOR_VERSION ${LWIP_MAJOR_VERSION})
        string(REGEX REPLACE "#define[\t ]+LWIP_VERSION_MINOR[\t ]+([0-9]+)" "\\1" LWIP_MINOR_VERSION ${LWIP_MINOR_VERSION})
        string(REGEX REPLACE "#define[\t ]+LWIP_VERSION_REVISION[\t ]+([0-9]+)" "\\1" LWIP_PATCH_VERSION ${LWIP_PATCH_VERSION})
        # create version number
        set(LWIP_VERSION "${LWIP_MAJOR_VERSION}.${LWIP_MINOR_VERSION}.${LWIP_PATCH_VERSION}")
        # remove "U"s if present
        string(REPLACE "U" "" LWIP_VERSION ${LWIP_VERSION})
    else ()
        set(LWIP_VERSION "(unknown version)")
    endif ()

    if (WIN32 AND NOT CYGWIN)
        get_filename_component(LWIP_LIBRARY_DIRECTORY ${LWIP_LIBRARY_RELEASE} DIRECTORY)
        list(APPEND LWIP_WIN_SUPPLEMENT "${LWIP_LIBRARY_DIRECTORY}/Packet.lib")
        list(APPEND LWIP_WIN_SUPPLEMENT "${LWIP_LIBRARY_DIRECTORY}/wpcap.lib")
        list(APPEND LWIP_WIN_SUPPLEMENT "${LWIP_LIBRARY_DIRECTORY}/pktif.lib")
    endif ()

else ()
    # untested
    #find_package_handle_standard_args(LwIP "Could NOT find LwIP, try to set the path to LwIP root folder in the system variable LWIP_ROOT_DIR"
    #    LWIP_LIBRARIES
    #    LWIP_INCLUDE_DIR
    #)
endif ()

mark_as_advanced(LWIP_INCLUDE_DIR)
mark_as_advanced(LWIP_VERSION)

