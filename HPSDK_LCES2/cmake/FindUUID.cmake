# - Try to find the UUID lirary (Universally Unique Identifier)
#
#  This module defines the following variables
#
#  UUID_FOUND - Was uuid found
#  UUID_INCLUDE_DIRS - the uuid include directories
#  UUID_LIBRARIES - the libraries to link against to use uuid
#

#=============================================================================
# Copyright 2014 Unified Automation GmbH
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distribute this file outside of CMake, substitute the full
#  License text for the above reference.)

if (UUID_LIBRARIES AND UUID_INCLUDE_DIRS)
    # if this was already provided by a toolchain or user
    # we avoid searching and just use it
    set(UUID_FOUND TRUE)
else ()

    # find the header file
    find_path(UUID_INCLUDE_DIR
        NAMES
            uuid/uuid.h
        HINTS
            /usr/include
            /usr/local/include
    )

    # find the library
    find_library(UUID_LIBRARY
        NAMES
            uuid
        PATHS
            /usr/lib
            /usr/local/lib
    )

    set(UUID_INCLUDE_DIRS ${UUID_INCLUDE_DIR})
    set(UUID_LIBRARIES ${UUID_LIBRARY})

    if (UUID_LIBRARIES AND UUID_INCLUDE_DIRS)
        set(UUID_FOUND TRUE)
    endif ()

    # some status info
    if (UUID_FOUND)
        message(STATUS "Found UUID header:  ${UUID_INCLUDE_DIRS}")
        message(STATUS "Found UUID library: ${UUID_LIBRARIES}")
    else ()
        set(UUID_FOUND FALSE)
    endif ()

    mark_as_advanced(UUID_INCLUDE_DIRS UUID_LIBRARIES UUID_INCLUDE_DIR UUID_LIBRARY)
endif ()

