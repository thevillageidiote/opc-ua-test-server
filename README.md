# README #


## What is this repository for? ##

  This repository contains the code for the OPC-UA test server running on both LCES2 and Ubuntu


## Using this repository ##

### Set up and installing any required dependencies ###

  - All required setup files and accompanying documentation can be found in the box folder [here](https://schneider-electric.box.com/s/xu2ua5teij6yms2q60c6hrqle1991an2)

  - Clone the git repository with the link below:  
    <code>git clone https://thevillageidiote@bitbucket.org/thevillageidiote/opc-ua-test-server.git </code>

### Building the code ###

*Important prerequisities:  
OpenSSL needs to be installed. The required packages are:   
<code> sudo apt-get install libssl1.0.0 libssl-dev </code>

Scripts that are used to build the UA Test Server, as well as the various CMakeList files for cross compilation are available in the Scripts/Build folder. 

To build the server for Ubuntu, run `<buildServerUbuntu.sh>`  
To build the server for the LCES2, run `<buildServerLCES2.sh>`  
To copy all the required files for the LCES2 from the build machine onto the LCES2, run `<copyServerLCES2.sh>`  
For the SSIO Server, there is a separate script `<copyServerSSIO.sh>` that copies the required files (which includes an additional binary file) onto the LCES2.   

Things to modify and edit in the code before running the build scripts:

- `<CMakeList Files>`  
  Found in Resources Folder
  Modify the names of the files to be included, and point the SDK directory to the appropriate one residing in your hard disk (if applicable).

  Note that the evaluation version of the UA SDK only provides precompiled binaries, and hence the appropriate one for the build environment needs to be used.  

  The precompiled binaries for each system are found in HPSDK_LCES2 / HPSDK_Ubuntu.  

- `<copyServerLCES2.sh>`   /  `<copyServerSSIO.sh>`  
  Modify the name and ip address of the destination directory to copy the built binary to. 


Other useful commands and information:  

- Creating precompiled binaries for nodes:  
  We can use the UA Modeller application provided by Unified Automations to create an information model. The trial version of the application allows us to export XML files for up to about 20 nodes. 

  We can then convert the XML files to precompiled binaries with the `xmltobin` program, which is available either in the Scripts or Resources/Info_model_File folder. The format of the command is as below:  

  `<./xml2bin -n [NSIDX] -v [INPUTFILE] [OUTPUTFILE]>`




### List of Test Scripts ###

- `<runServerWithLog.sh [timeInterval]>`  
  Starts the OPC-UA server and runs a log of the CPU% and Memory% consumption in the background. The Linux tool _top_ is used to generate the log. The log is a while loop, and sleeps for $timeInterval seconds everytime it logs the current CPU% and Mem% conusmption once. This timing is not very accurate (about 10% error), but should suffice as a means for generating an average.  

  The output of the log is written to the file _ps.log_ and is given in the format:  
  [CPU%] [Mem%]  

- `<killServer.sh>`  
  Kills an open uaserver process  
      
- `<copyFile.sh>`  
  Used on the LCES2. Copies the ps.log file produced by scripts to /media/sf_vmshare on the Ubuntu VM  

- _Deprecated_  
  - `<MemTest.sh [arg1] [arg2] [arg3]> `  

    Note: Run this script with Bash  
    Runs the OPC-UA server with different number of nodes, then prints the RAM used using the pmap utility in Linux to ps.log   (Ubuntu version)  

    Builds the OPC-UA server with different number of nodes for the LCES2, then SSHs into the LCES2 and prints the RAM used to ps.log. This scripts calls the MemTestHelperScript.sh which is in the LCES2 (LCES2 version)  
    
    Usage: 

    arg1 => Number of nodes to start mem test with  
    arg2 => Number of nodes to end with  
    arg3 => increment per test  
    i.e. for (int i= arg1; i<= arg2; i= i+arg3)  

    default values are:  
    arg1 = 100  
    arg2 = 500  
    arg3 = 100  

  - `<MemTestHelper.sh>`  
    Resides on the LCES2 and is called by `<MemTest.sh>`. This tool starts the uaserver and prints the memory consumption using 2 utilities. 
    - The condensed output of pmap is printed to ps.log in the following format:   
    [KBytes] [RSS] [Dirty]
    - The condensed output of top is printed to psTop.log in the following format:   
    [VIRT] [RES] [SHR]

  - `<logSysUsage.sh>`  
    To be started before server runs. Records the CPU% and Memory% consumed by the uaserver application using the ps aux utility to ps.log.   
    Can suffer from time related issues, and produces a smooth CPU% consumption curve. 

  - `<logSysUsageTop.sh>`  
    To be started before server runs. Records the CPU% and Memory% consumed by the uaserver application using the top utility to ps.log.  
    Produces a CPU% consumption curve that seems to more accurately reflect instantaneous CPU consumption.   

  - `<PrintMem.sh>`  
    Runs the currently built OPC-UA server and prints the RAM consumption after 60 seconds to ps.log 
         
         
         
### Who do I talk to? ###

This test server has been written by Choon Kiat Lee (choonkiat.lee@gmail.com) using the Unified Automation Evaluation SDK, and adapts code written by Brett Swan for the LCES2.