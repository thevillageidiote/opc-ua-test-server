#!/bin/bash

#navigate out of the current folder
cd ../../

#Delete current build folder and build project again
rm -r build_ubuntu

mkdir build_ubuntu
cd build_ubuntu

#cmake commands
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_INSTALL_PREFIX=$PWD -DCMAKE_BUILD_TYPE=Debug ..

#make
make

#copy files using copyFiles.sh
cd ../Scripts/Build
sh copyResources.sh

exit 3

