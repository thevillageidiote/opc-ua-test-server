#!/bin/bash

#remove host key (necessary to avoid errors with host key verification)
ssh-keygen -f '/home/lcesvp/.ssh/known_hosts' -R 192.168.1.66

#copy Resources to LCES2

cd Resources
scp ../../../build_LCES2/uaserver_test_model test_server_info_model.bin groups ns0.ua passwd settings.conf users root@192.168.1.66:/home/root

