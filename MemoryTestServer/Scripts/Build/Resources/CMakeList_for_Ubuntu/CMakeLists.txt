project(uaserver_test_model C)

cmake_minimum_required(VERSION 3.2)



# include sdk.cmake delivered with the SDK

include(../HPSDK_Ubuntu/sdk.cmake)



# create list of source files

set(SOURCES

    server_main.c

    custom_provider.c

    custom_provider_store.c

    custom_provider_nodes.c

)

# create list of header files

set(HEADERS

    custom_provider.h

    custom_provider_store.h

    numNodesDefine.h

    custom_provider_nodes.h

)

# create executable

add_executable(${PROJECT_NAME} ${SOURCES})



# specify libraries to link

target_link_libraries(${PROJECT_NAME}

    ${SDK_SERVER_LIBRARIES}

    ${SDK_BASE_LIBRARIES}

    ${OS_LIBS} ${SDK_SYSTEM_LIBS} m

)



# add optional install target

install(TARGETS ${PROJECT_NAME} DESTINATION bin)

# install binary address space file

install(FILES demomodel.bin DESTINATION bin)



# MS Visual Studio settings

include(VSSetDebugConfig)

vs_set_debug_config(TARGET ${PROJECT_NAME}

                    SET_ALL_BUILD

                    INSTALL_DIR_EXECUTABLE

                    WORKING_DIR "${CMAKE_INSTALL_PREFIX}/bin")

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER Applications/Examples)
