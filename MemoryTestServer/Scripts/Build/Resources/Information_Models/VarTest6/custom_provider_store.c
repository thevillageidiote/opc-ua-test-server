/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#include <uabase/statuscodes.h>

#include "custom_provider_store.h"
#include "numNodeDefinition.h"

/*! [store_values] */
//static 
static double g_custom_provider_values[NUM_NODES];
/*! [store_values] */

/*! [store_initial] */
int custom_store_set_initial_value(unsigned int idx, double value) {

    if (idx >= countof(g_custom_provider_values)) return -1;
    /* write value to the array */
    g_custom_provider_values[idx] = value;
    return 0;
}
/*! [store_initial] */

/*! [store_get] */
void custom_store_get_value(void *store, ua_node_t node, unsigned int idx, bool source_ts, struct ua_indexrange *range, unsigned int num_ranges, struct ua_datavalue *result)
{
    UA_UNUSED(node);
    UA_UNUSED(range);
    UA_UNUSED(store);

    /* check array bounds */
    if (idx >= countof(g_custom_provider_values)) {
        result->status = UA_SCBADINTERNALERROR;
        return;
    }

    /* there are only scalar values in the store */
    if (num_ranges > 0) {
        result->status = UA_SCBADINDEXRANGENODATA;
        return;
    }

    /* write value to result */
    ua_variant_set_uint32(&result->value, g_custom_provider_values[idx]);

    /* add source timestamp if requested */
    if (source_ts) ua_datetime_now(&result->source_timestamp);

    /* set good statuscode */
    result->status = 0;
}
/*! [store_get] */

/*! [store_attach] */
ua_statuscode custom_store_attach_value(void *store, ua_node_t node, unsigned int idx, struct ua_indexrange *range, unsigned int num_ranges, struct ua_datavalue *value)
{
    UA_UNUSED(range);
    UA_UNUSED(store);
    UA_UNUSED(node);

    /* check array bounds */
    if (idx >= countof(g_custom_provider_values)) return UA_SCBADINTERNALERROR;

    /* there are only scalar values in the store */
    if (num_ranges > 0) return UA_SCBADWRITENOTSUPPORTED;

    /* write of status or timestamp is not supported */
    if (value->status != 0 || value->server_timestamp != 0 || value->source_timestamp != 0) {
        return UA_SCBADWRITENOTSUPPORTED;
    }

    /* check type of value to write */
    if (value->value.type != UA_VT_DOUBLE) {
        return UA_SCBADTYPEMISMATCH;
    }

    /* write new value to the array */
    g_custom_provider_values[idx] = value->value.value.d;
    return 0;
}
/*! [store_attach] */
