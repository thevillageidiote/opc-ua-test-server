/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/


#include <stdio.h>
#include <uaserver/init.h>
#include <trace/trace.h>
#include <appconfig.h>
#include <uaprovider/server/uaprovider_server.h>
#include <platform/hostname.h>
#include <platform/shutdown.h>
#include <crashhandler/crashhandler.h>
#include <memory/memory_layout.h>
#include <config/appconfig_loader.h>
#include <memory/memory.h>
#include <config/appconfig_loader.h>
#include "custom_provider.h"


struct appconfig g_appconfig;

/*! [declare_provider_array] */
static struct uaserver_provider g_provider[] = {
    {-1, uaprovider_server_init},
    {-1, eval_pv_init}
};
/*! [declare_provider_array] */

int server_main_clear(void)
{
    uaserver_stop();
    uaserver_clear(g_provider, countof(g_provider));
    uaserver_clear_ipc();
    appconfig_clear();
    trace_closelog();
    ua_platform_cleanup();
    return 0;
}

int server_main_init(int trace_level, int facility_mask, const char *config_file)
{
    int ret;
    unsigned int i;
    static char fqdn[64];
    struct ua_string tmp;

    ua_get_fqdn(fqdn, sizeof(fqdn));

    /*! [init_crashhandler_platform_trace] */
    ret = crashhandler_install();
    if (ret != 0) {
        printf("ERROR: Could not install crashhandler\n");
        return ret;
    }

    ret = ua_platform_init();
    if (ret < 0) {
        printf("ERROR Could not init platform\n");
        goto out_platform;
    }

#ifdef HAVE_TRACE
    ret = trace_openlog(trace_level, facility_mask);
    if (ret < 0) {
        printf("ERROR Could not open log\n");
        goto out_trace;
    }
#else
    UA_UNUSED(trace_level);
    UA_UNUSED(facility_mask);
#endif
    /*! [init_crashhandler_platform_trace] */

    /*! [load_appconfig] */
    TRACE_INFO(TRACE_FAC_APPLICATION, "Loading configuration file...\n");
    ret = appconfig_load_from_file(config_file);
    if (ret != 0) goto out_config;
    /*! [load_appconfig] */

    /*! [product_info] */
    g_appconfig.manufacturer_name = "Schneider Electric";
    g_appconfig.product_name      = "Memory Test Server";
    g_appconfig.product_uri       = "urn:Schneider-Electric:MemTestServer";
    g_appconfig.application_name  = "MemTestServer";
    g_appconfig.application_uri   = "urn:Schneider-Electric:MemTestServer1";
    g_appconfig.dns               = fqdn;
    /*! [product_info] */

    /*! [init_ipc] */
    TRACE_INFO(TRACE_FAC_APPLICATION, "Initializing IPC...\n");
    ret = uaserver_init_ipc();
    if (ret < 0) goto out_ipc;

    memory_layout_set_prop_int(MEMORY_PROP_TRACE_LEVEL, trace_level);
    memory_layout_set_prop_int(MEMORY_PROP_TRACE_FACILITY, facility_mask);
    /*! [init_ipc] */

    /* replace [hostname] place holder in endpoint urls with fqdn */
    for (i = 0; i < g_appconfig.endpoint.num_endpoints; ++i) {
        ua_string_set(&tmp, g_appconfig.endpoint.endpoints[i].endpoint_url);
        if (ua_string_index_of(&tmp, "[hostname]") != -1) {
            ua_string_replace(&tmp, "[hostname]", fqdn);
            g_appconfig.endpoint.endpoints[i].endpoint_url = ua_strdup(ua_string_const_data(&tmp));
        }
        ua_string_clear(&tmp);
    }

    /*! [init_sdk] */
    TRACE_INFO(TRACE_FAC_APPLICATION, "Initializing providers...\n");
    ret = uaserver_init(g_provider, countof(g_provider), NULL);
    if (ret < 0) goto out_server_init;
    /*! [init_sdk] */

    /*! [start_server] */
    TRACE_INFO(TRACE_FAC_APPLICATION, "Starting server...\n");
    ret = uaserver_start();
    if (ret < 0) goto out_server_start;
    /*! [start_server] */

    return 0;

out_server_start:
    uaserver_clear(g_provider, countof(g_provider));
out_server_init:
    uaserver_clear_ipc();
out_ipc:
    appconfig_clear();
out_config:
#ifdef HAVE_TRACE
    trace_closelog();
out_trace:
#endif
    ua_platform_cleanup();
out_platform:
    return ret;
}

/*! [main_fct] */
int main(int argc, char *argv[])
{
    int ret;
    unsigned int i;

    UA_UNUSED(argc);
    UA_UNUSED(argv);

    ret = server_main_init(TRACE_LEVEL_ERROR | TRACE_LEVEL_WARNING, TRACE_FAC_ALL, "settings.conf");
    if (ret != 0) {
        printf("server_main_init() failed with error 0x%08x\n", ret);
        exit(EXIT_FAILURE);
    }

    printf("Server is up and running.\n");
    for (i = 0; i < g_appconfig.endpoint.num_endpoints; ++i) {
        printf("Listening on %s\n", g_appconfig.endpoint.endpoints[i].endpoint_url);
    }

    while (ua_shutdown_should_shutdown() == 0) {
        uaserver_do_com();
    }
    TRACE_NOTICE(TRACE_FAC_APPLICATION, "Shutting down server...\n");

    server_main_clear();

    return 0;
}
/*! [main_fct] */
