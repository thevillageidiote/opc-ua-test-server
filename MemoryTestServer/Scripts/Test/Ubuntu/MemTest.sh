#!/bin/bash

modify_num_nodes(){
	cd ../../../
	echo "#define NUM_NODES "$1 > numNodeDefinition.h
	cd Scripts/Test/Ubuntu
}

build_server(){
	#build server
	cd ../../Build
	sh buildServerUbuntu.sh
	if [ $? = 3 ]; then
		echo "Server Built"
	else
		echo "Server build failed"
	fi
	cd ../Test/Ubuntu
}

print_memory(){
	uaServerPID=$1
	sleep 30
	echo "The Memory Consumption for $i nodes is " >>ps.log
	pmap -x `ps aux | grep '[u]aserver' | awk '{print $2}'` | tail -n 1 >> ps.log
	echo "Memory Requirements written to ps.log"
	kill $uaServerPID
}


main(){

	#clear log file
	>ps.log

	for (( i=$1; i<=$2; i=$(($i+$3)) )) ;	do 
		modify_num_nodes $i
		build_server
		
		#run server
		cd ../../../build_ubuntu
		./uaserver_test_model &
		cd ../Scripts/Test/Ubuntu
		echo "Started server with "$i" nodes"
		print_memory $!

	done
}


#usage: 
#input 3 arguments as follows:
# arg1 => Number of nodes to start mem test with
# arg2 => Number of nodes to end with
# arg3 => increment per test
# i.e. for (int i= arg1; i<= arg2; i= i+arg3){}

#default values are: 
#  arg1 = 100
#  arg2 = 500
#  arg3 = 100

if [ $# -ne 3 ]; then
	echo "No parameters passed or Illegal number of parameters"
	arg1=100
	arg2=500	
	arg3=100
else
	arg1=$1
	arg2=$2
	arg3=$3

fi

main $arg1 $arg2 $arg3
