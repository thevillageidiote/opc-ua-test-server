#!/bin/bash

ServerPID=`ps aux | grep '[u]aserver' | awk '{print $2}'`

cleanup()
{
	kill $ServerPID
}

trap cleanup EXIT
