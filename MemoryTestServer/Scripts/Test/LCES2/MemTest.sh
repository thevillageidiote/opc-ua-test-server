#!/bin/bash

modify_num_nodes(){
	cd ../../../
	echo "#define NUM_NODES "$1 > numNodeDefinition.h
	cd Scripts/Test/LCES2
}

build_server(){
	#build server
	cd ../../Build
	bash buildServerLCES2.sh
	if [ $? = 3 ]; then
		echo "Server Built"
	else
		echo "Server build failed"
	fi
	sh copyServerLCES2.sh
	cd ../Test/LCES2
}

main(){

	#clear log file
	>ps.log

	for (( i=$1; i<=$2; i=$(($i+$3)) )) ;	do 
		modify_num_nodes $i
		build_server
		
		#run server
		run_line="cd /home/root/data/MemoryTestServer/Scripts/Test/LCES2; bash MemTestHelperScript.sh "$i 
		ssh root@192.168.1.63 "$run_line"
	done
}


#usage: 
#input 3 arguments as follows:
# arg1 => Number of nodes to start mem test with
# arg2 => Number of nodes to end with
# arg3 => increment per test
# i.e. for (int i= arg1; i<= arg2; i= i+arg3){}

#default values are: 
#  arg1 = 100
#  arg2 = 500
#  arg3 = 100

if [ $# -ne 3 ]; then
	echo "No parameters passed or Illegal number of parameters"
	arg1=100
	arg2=500	
	arg3=100
else
	arg1=$1
	arg2=$2
	arg3=$3

fi

main $arg1 $arg2 $arg3
