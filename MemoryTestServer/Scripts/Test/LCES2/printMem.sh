#!/bin/bash

cd ../../../build_LCES2

./uaserver_test_model  &

uaServerPID=$!

cd ../Scripts/Test/LCES2

sleep 60

echo "The Memory Consumption is " >ps.log

pmap -x `ps aux | grep '[u]aserver' | awk '{print $2}'` | tail -n 1 >> ps.log

echo "written"

kill $uaServerPID
