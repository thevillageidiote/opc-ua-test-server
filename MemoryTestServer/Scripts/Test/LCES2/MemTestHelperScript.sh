print_memory(){
	uaServerPID=$1
	sleep 20
#	echo "The Memory Consumption for" $NUM_NODES " nodes is " >>ps.log
#	pmap -x $uaServerPID | tail -n 1 >> ps.log
	echo "The Memory Consumption for "$NUM_NODES " nodes is "  >> psTop.log
	top -n 1 -w 512 -b -p $uaServerPID | tail -n 1 | awk '{print $13"\t"$14"\t"$5"\t"$6"\t"$7}'  >> psTop.log
	echo "Memory Requirements written to ps.log"
	kill $uaServerPID
}

NUM_NODES=$1

#Navigate to build directory
cd ../../../build_LCES2
#Run Test Server as a separate process
./uaserver_test_model &

#Change back to the same folder to print memory
cd ../Scripts/Test/LCES2
echo "Started server with "$NUM_NODES" nodes"

print_memory $!

