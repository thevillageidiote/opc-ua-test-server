#!/bin/bash

rm -f ps.log

if [ $# -eq 0 ]
	then 
		INTERVAL=2
	else 
		INTERVAL=$1

fi


echo "Log started with time interval of " $INTERVAL "s"

echo "The time interval is: " $INTERVAL >> ps.log

while true; 

do (echo "%CPU %MEM ARGS $(date)" && ps -e -o pcpu,pmem,args --sort=pcpu | cut -d" " -f1-5 | grep '[u]aserver') >> ps.log
 

sleep $INTERVAL; 

done

