# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/media/sf_vmshare/opc-ua/MemoryTestServer/custom_provider.c" "/media/sf_vmshare/opc-ua/MemoryTestServer/build_LCES2/CMakeFiles/uaserver_test_model.dir/custom_provider.c.o"
  "/media/sf_vmshare/opc-ua/MemoryTestServer/custom_provider_nodes.c" "/media/sf_vmshare/opc-ua/MemoryTestServer/build_LCES2/CMakeFiles/uaserver_test_model.dir/custom_provider_nodes.c.o"
  "/media/sf_vmshare/opc-ua/MemoryTestServer/custom_provider_store.c" "/media/sf_vmshare/opc-ua/MemoryTestServer/build_LCES2/CMakeFiles/uaserver_test_model.dir/custom_provider_store.c.o"
  "/media/sf_vmshare/opc-ua/MemoryTestServer/server_main.c" "/media/sf_vmshare/opc-ua/MemoryTestServer/build_LCES2/CMakeFiles/uaserver_test_model.dir/server_main.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "UA_LINUX"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/media/sf_vmshare/opc-ua/HPSDK_LCES2/include"
  "/media/sf_vmshare/opc-ua/HPSDK_LCES2/include/platform/linux"
  "."
  "/opt/poky/2.0.2/sysroots/cortexa7hf-vfp-vfpv4-d16-poky-linux-gnueabi/usr/include/openssl"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
