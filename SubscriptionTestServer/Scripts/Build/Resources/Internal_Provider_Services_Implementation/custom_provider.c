/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#include <memory/memory.h>
#include <uaserver/addressspace/addressspace.h>
#include <uaserver/valuestore/valuestore.h>
#include <uaprovider/provider.h>
#include <trace/trace.h>

#ifdef ENABLE_SERVICE_READ
#include <uaprovider/internal/uaprovider_internal_read.h>
#endif
#ifdef ENABLE_SERVICE_WRITE
#include <uaprovider/internal/uaprovider_internal_write.h>
#endif
#ifdef ENABLE_SERVICE_REGISTERNODES
#include <uaprovider/internal/uaprovider_internal_register.h>
#endif
#ifdef ENABLE_SERVICE_SUBSCRIPTION
#include <uaprovider/internal/uaprovider_internal_subscription.h>
#endif

#include "custom_provider_subscription.h"
#include "custom_provider_read.h"
#include "custom_provider_write.h"
#include "custom_provider_nodes.h"
#include "custom_provider.h"
#include <timer/timer.h>


/*! [nsidx_declaration] */
static uint16_t g_custom_provider_nsidx;

/*! [nsidx_declaration] */

/*! [memory_store_declaration] */
//static struct ua_variant g_memorystore_values[3];
//struct ua_memorystore g_memorystore;
/*! [memory_store_declaration] */

/*! [custom_store_idx] */
//uint8_t g_custom_store_idx;
/*! [custom_store_idx] */

//Declare timer
struct timer_base x_tmr_base;
uint32_t tmr_id = 0;

int timer_callback(uint64_t elapsed, void *data);

double g_custom_provider_values[NUM_NODES];


/*! [provider_cleanup] */
/* cleanup resources allocated by the custom provider */
static void eval_pv_cleanup(void)
{

    /* cleanup timers */
    timer_remove(&x_tmr_base, tmr_id, NULL);
    timer_base_clear(&x_tmr_base);

    return;
}
/*! [provider_cleanup] */

/*! [provider_init] */
int eval_pv_init(struct uaprovider *ctx)
/*! [provider_init] */
{
    struct ua_valuestore_interface store_if;
    struct ua_addressspace_config config;
    uint16_t eval_pv_nsidx;
    int ret;

    /*! [register_addressspace] */
    ua_memset(&config, 0, sizeof(config));
    config.nsidx          = UA_NSIDX_INVALID; /* automatically assign nsindex */
    config.max_variables  = NUM_NODES;
    config.max_objects    = NUM_NODES+2;
    config.max_references = (NUM_NODES*5)+3;
    config.max_strings    = (NUM_NODES*2)+3;

    /* register new namespace */
    ret = ua_addressspace_register(
        "http://www.schneider-electric.com/MemTestServer",
        &config,
        UA_INDEX_HASHTABLEQP);

    if (ret < 0) return ret;
    eval_pv_nsidx = (uint16_t) ret;
    g_custom_provider_nsidx = eval_pv_nsidx;

    /*! [register_addressspace] */

    /*! [register_nsidx] */
    /* register namespace index */
    ret = uaprovider_register_nsindex(ctx, eval_pv_nsidx);
    if (ret != 0) return ret;
    /*! [register_nsidx] */

    /*! [register_memorystore] */
    /* initialize memorystore context */
    //ret = ua_memorystore_init(&g_memorystore, 0, g_memorystore_values, countof(g_memorystore_values));
    //if (ret != 0) return ret;
    /*! [register_memorystore] */

    /*! [register_customstore] */
    /* register custom store */
    /*
    ua_valuestore_interface_init(&store_if);
    store_if.get_fct    = custom_store_get_value;
    store_if.attach_fct = custom_store_attach_value;
    ret = ua_valuestore_register_store(&store_if, &g_custom_store_idx);
    if (ret != 0) return ret;*/
    /*! [register_customstore] */

    /* setup timer for simulation */
    ret = timer_base_init(&x_tmr_base, 1);
    ret = timer_add(&x_tmr_base, &tmr_id, 90, timer_callback, NULL, TIMER_DEFAULT);
    ret = timer_activate(&x_tmr_base, tmr_id);

    /*! [call_create_nodes] */
    /* create nodes in the namespace */
    ret = eval_pv_create_nodes(eval_pv_nsidx);
    if (ret != 0) return ret;
    /*! [call_create_nodes] */

    /*! [register_service_handler] */
    /* register service handler */
#ifdef ENABLE_SERVICE_READ
    ctx->read            = custom_provider_read;
#endif
#ifdef ENABLE_SERVICE_WRITE
    ctx->write           = custom_provider_write;
#endif
#ifdef ENABLE_SERVICE_REGISTERNODES
    ctx->registernodes   = uaprovider_internal_registernodes;
    ctx->unregisternodes = uaprovider_internal_unregisternodes;
#endif
#ifdef ENABLE_SERVICE_SUBSCRIPTION
    ctx->add_item        = custom_provider_add_item;
    ctx->remove_item     = custom_provider_remove_item;
    ctx->modify_item     = custom_provider_modify_item;
    ctx->subscribe       = custom_provider_subscribe;
#endif
    /*! [register_service_handler] */
    /*! [register_cleanup] */
    /* register cleanup function */
    ctx->cleanup         = eval_pv_cleanup;
    /*! [register_cleanup] */

    return ret;
}

#define _USE_MATH_DEFINES
#include <math.h>

/* 
** The below software is NOT thread safe. I think I can get away with this
** simply because there is a single loop in server_main which runs OPC
** comms and timers.
*/
int timer_callback(uint64_t elapsed, void *data)
{
    int i;
    static double test_time = 0;
    
    UA_UNUSED(data);

	test_time += (((double)elapsed)/1000);
	
    for (i = 0; i < NUM_NODES; i++)
    {
        g_custom_provider_values[i] = 10.0 * sin(2 * M_PI * 0.001 * test_time * (i+1));
    }

    return 0;
}
