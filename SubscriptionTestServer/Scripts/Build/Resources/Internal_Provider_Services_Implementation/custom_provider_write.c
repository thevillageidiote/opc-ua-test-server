/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#include <uabase/uatype_config.h>
#ifdef ENABLE_SERVICE_WRITE
#include <uabase/service/writeresponse.h>
#include <uabase/service/writerequest.h>
#include <uabase/statuscodes.h>
#include <uabase/string.h>
#include <uabase/attributeid.h>
#include <uabase/accesslevel.h>
#include <uabase/structure/nodeclass.h>
#include <uaserver/addressspace/addressspace.h>
#include <uaserver/addressspace/variable.h>
#include <uaserver/attribute/write_internal.h>
#include <uaserver/valuestore/valuestore.h>
#include <uaprovider/provider.h>
#include <trace/trace.h>

#include "custom_provider.h"
#include "custom_provider_write.h"
#include "numNodeDefinition.h"

extern double g_custom_provider_values[NUM_NODES];


/*! [write] */
void custom_provider_write(struct uaprovider_write_ctx *ctx)
{
    unsigned int valueidx;
    struct ua_writerequest *req = ctx->req;
    struct ua_writeresponse *res = ctx->res;
    ua_node_t node;
    uint16_t ns_index;
    uint8_t storeidx = 0;
    int32_t i;
    int ret;

    for (i = 0; i < req->num_nodes; i++) {
        /* skip node from other namespace */
        ns_index = req->nodes[i].node_id.nsindex;
        if (!uaprovider_contains_namespace(ctx->cur_provider, ns_index)) {
            continue;
        }

        /* skip non-value attributes */
        if (req->nodes[i].attribute_id != UA_ATTRIBUTEID_VALUE) {
            res->results[i] = UA_SCBADATTRIBUTEIDINVALID;
            continue;
        }

        /* lookup node handle */
        node = ua_node_find(&req->nodes[i].node_id);
        if (node == UA_NODE_INVALID) continue;

        /* check index range */
        if (!ua_string_is_null(&req->nodes[i].index_range)) {
            res->results[i] = UA_SCBADWRITENOTSUPPORTED;
            continue;
        }

        /* check nodeclass */
        if (ua_node_get_nodeclass(node) != UA_NODECLASS_VARIABLE) {
            res->results[i] = UA_SCBADATTRIBUTEIDINVALID;
            continue;
        }

        /* if store index is set, use write_internal to write to store */
        ua_variable_get_store_index(node, &storeidx);
        if (storeidx != 0) {
            res->results[i] = uaserver_write_internal(
                        node,
                        ctx->session,
                        req->nodes[i].attribute_id,
                        NULL,
                        0,
                        &req->nodes[i].value);
            continue;
        }

        /* write of status or timestamp is not supported */
        if (req->nodes[i].value.status != 0 || req->nodes[i].value.server_timestamp != 0 || req->nodes[i].value.source_timestamp != 0) {
            res->results[i] = UA_SCBADWRITENOTSUPPORTED;
            continue;
        }

        /* check if the value is writeable */
        if ((ua_variable_get_access_level(node) & UA_ACCESSLEVEL_CURRENTWRITE) == 0) {
            res->results[i] = UA_SCBADNOTWRITABLE;
            continue;
        }

        /* check user access permission */
#ifdef UA_AUTHORIZATION_SUPPORT
        res->results[i] = ua_authorization_is_writable(node, &ctx->session->user_ctx);
        if (res->results[i] != 0) continue;
#endif

        /* get value index and check array bounds */
        ret = ua_variable_get_value_index(node, &valueidx);
        if (ret != 0 || valueidx >= countof(g_custom_provider_values)) {
            res->results[i] = UA_SCBADINTERNALERROR;
            continue;
        }

        /* check the datatype of the value to write */
        if (req->nodes[i].value.value.type != UA_VT_DOUBLE) {
            res->results[i] = UA_SCBADTYPEMISMATCH;
            continue;
        }

        /* write new value */
        g_custom_provider_values[valueidx] = req->nodes[i].value.value.value.d;
        res->results[i] = 0;
    }
    uaserver_write_complete(ctx);
}
/*! [write] */

#endif /* ENABLE_SERVICE_WRITE */
