/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.6                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.6, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.6/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef CUSTOM_PROVIDER_READ_H
#define CUSTOM_PROVIDER_READ_H

#include <uaprovider/provider.h>
#include <uaserver/session/session.h>
#include <uaserver/addressspace/addressspace.h>
#include <uabase/variant.h>
#include <uabase/structure/timestampstoreturn.h>
#include <uabase/indexrange.h>
#include <uabase/datavalue.h>
#include <uabase/uatype_config.h>

//#include "numNodeDefinition.h"

void custom_provider_read_value(
    ua_node_t node,
    struct uasession_session *session,
    enum ua_timestampstoreturn ts,
    struct ua_indexrange *index_range,
    unsigned int num_ranges,
    struct ua_datavalue *result);

#ifdef ENABLE_SERVICE_READ
void custom_provider_read(struct uaprovider_read_ctx *ctx);
#endif

//extern double g_custom_provider_values[NUM_NODES];


#endif /* end of include guard: CUSTOM_PROVIDER_READ_H */

