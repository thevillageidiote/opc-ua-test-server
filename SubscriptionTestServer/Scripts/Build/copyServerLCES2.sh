#!/bin/bash



#Check if SD card is mounted and if it is not, then mount it onto /home/root/data

ssh root@192.168.1.63 "if ! df | grep '/dev/mmcblk0p3' ; then mount /dev/mmcblk0p3 /home/root/data ; fi"


#copy built uaserver_test_model to LCES2
scp ../../build_LCES2/uaserver_test_model root@192.168.1.63:/home/root/data/MemoryTestServer/build_LCES2


#copy Resources to LCES2
scp Resources/test_server_info_model.bin root@192.168.1.63:/home/root/data/MemoryTestServer/build_LCES2
scp Resources/groups root@192.168.1.63:/home/root/data/MemoryTestServer/build_LCES2
scp Resources/ns0.ua root@192.168.1.63:/home/root/data/MemoryTestServer/build_LCES2
scp Resources/passwd root@192.168.1.63:/home/root/data/MemoryTestServer/build_LCES2
scp Resources/settings.conf root@192.168.1.63:/home/root/data/MemoryTestServer/build_LCES2
scp Resources/users root@192.168.1.63:/home/root/data/MemoryTestServer/build_LCES2
