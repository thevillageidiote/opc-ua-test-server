#!/bin/bash

rm -f ps.log

if [ $# -eq 1 ]
	then 
		INTERVAL=2
		PID=$1
	else 
		INTERVAL=$1
		PID=$2

fi


echo "Log started with time interval of " $INTERVAL "s"

echo "The time interval is: " $INTERVAL >> ps.log

echo "CPU%""\t""Mem%" >> ps.log

while true; 

do (top -n 1 -b -p $PID | tail -n 1 | awk '{print $9"\t"$10}' ) >> ps.log
 

sleep $INTERVAL; 

done

